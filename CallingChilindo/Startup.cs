﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CallingChilindo.Startup))]
namespace CallingChilindo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
