﻿using CallingChilindo.Business;
using CallingChilindo.Models;
using CallingChilindo.Models.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CallingChilindo.Controllers
{
    public class VouchersController : Controller
    {
        // GET: Vouchers
        [Authorize]
        [HttpGet]
        public ActionResult AddVouchers()
        {
            VouchersModel vouchersModel = new VouchersModel();
            VouchersFacade vouchersFacade = new VouchersFacade();
            vouchersModel.Vouchers = new VouchersEntity();
            vouchersModel.ListVoucherTypes = vouchersFacade.GetAllVoucherType();
            vouchersModel.ListVouchers = vouchersFacade.GetAllVouchers();
            return View(vouchersModel);
        }

        [HttpPost]
        public ActionResult AddVouchers(string jsonData)
        {
            try
            {
                var voucher = Newtonsoft.Json.JsonConvert.DeserializeObject<VouchersEntity>(jsonData);
                VouchersFacade vouchersFacade = new VouchersFacade();
                if (voucher.typeOfVoucher == 1 || voucher.typeOfVoucher == 2)
                {
                    try
                    {
                        voucher.val = Convert.ToDecimal(voucher.additional);
                    }
                    catch
                    {
                        voucher.val = 0;
                    }
                }
                else
                    voucher.val = 0;

                var isSuccess = vouchersFacade.AddVoucher(voucher);

                return Json(new
                {
                    Valid = isSuccess,
                    Message = string.Empty,
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    Valid = false,
                    Message = string.Empty,
                });
            }
        }
        [HttpPost]
        public ActionResult SetActive(string voucherId,string IsActive)
        {
            try
            {
                bool isActive = false;
                if (IsActive == "1")
                    isActive = true;
                VouchersFacade vouchersFacade = new VouchersFacade();

                if (vouchersFacade.UpdateVoucher(Convert.ToInt32(voucherId), isActive))
                {
                    return Json(new
                    {
                        Valid = true,
                        Error = string.Empty,
                    });
                }
                else
                {
                    return Json(new
                    {
                        Valid = false
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Valid = false
                });

            }
        }












    }
}