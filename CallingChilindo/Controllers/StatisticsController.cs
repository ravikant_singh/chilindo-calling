﻿using CallingChilindo.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Xml;
namespace CallingChilindo.Controllers
{
    [System.Web.Script.Services.ScriptService]
    public class StatisticsController : Controller
    {
        public ChilindoCallingEntities db = new ChilindoCallingEntities();
      
        //
        // GET: /Statistics/
        [Authorize]
        public ActionResult Index(string Paid, string WillPaid, string Cancelled)
        {
            string id = Paid;
            List<StatiscticsModel> model = new List<StatiscticsModel>();
            try
            {
                
                ChilindoCallingEntities objEntitites = new ChilindoCallingEntities();
                var modelBarChart = new List<BarchartStats>();
                var overviewStats = new List<overviewStats>();
                # region TopGrid
              List<SPO_Statisctics_Result> Statisctics = objEntitites.SPO_Statisctics().ToList();
                int PaidWillPayCancelled = 0;
                foreach (var item in Statisctics)
                {
                    PaidWillPayCancelled = 0;
                    BarchartStats f = new BarchartStats();
                    f.sUser = item.Caller;
                    if (Paid == "on")
                        PaidWillPayCancelled += Convert.ToInt32(item.Col9);
                    if (WillPaid == "on")
                        PaidWillPayCancelled += Convert.ToInt32(item.Col12);
                    if (Cancelled == "on")
                        PaidWillPayCancelled += Convert.ToInt32(item.Col24);
                    f.nTotalOtherCalls = PaidWillPayCancelled;
                    f.nTotalCalls= Convert.ToInt32(item.Col6);
                    modelBarChart.Add(f);
                    model.Add(new StatiscticsModel
                    {
                        Caller = item.Caller,
                        id = item.id,
                        status = item.status,
                        FullName=item.FullName,
                        Col1 = string.Format("{0} : {1}", item.Col1 / 60, (item.Col1 % 60).ToString().PadLeft(2,'0')),
                        Col1Footer = string.Format("{0} : {1}", item.Col1Footer / 60, (item.Col1Footer % 60).ToString().PadLeft(2, '0')),
                        Col2 = string.Format("{0} : {1}", item.Col2 / 60, (item.Col2 % 60).ToString().PadLeft(2, '0')),
                        Col2Footer = string.Format("{0} : {1}", item.Col2Footer / 60, (item.Col2Footer % 60).ToString().PadLeft(2, '0')),
                        Col3 = string.Format("{0} : {1}", item.Col3 / 60, (item.Col3 % 60).ToString().PadLeft(2, '0')),
                        Col3Footer = string.Format("{0} : {1}", item.Col3Footer / 60, (item.Col3Footer % 60).ToString().PadLeft(2, '0')),
                        Col4 = item.Col4,
                        Col4Footer = item.Col4Footer,
                        Col5 = item.Col5,
                        Col5Footer = item.Col5Footer,
                        Col6 = item.Col6,
                        Col6Footer = item.Col6Footer,
                        Col7 = item.Col7 * 100 / (item.Col4 != 0 ? item.Col4 :1) ,
                        Col7Footer = item.Col7Footer * 100 / (item.Col4Footer != 0 ? item.Col4Footer : 1),
                        Col8 = item.Col8 * 100 / (item.Col5 != 0 ? item.Col5 : 1),
                        Col8Footer = item.Col8Footer * 100 / (item.Col5Footer != 0 ? item.Col5Footer : 1),
                        Col9 = item.Col9 * 100 / (item.Col6 != 0 ? item.Col6 : 1),
                        Col9Footer = item.Col9Footer * 100 / (item.Col6Footer != 0 ? item.Col6Footer : 1),
                        Col10 = item.Col10 * 100 / (item.Col4 != 0 ? item.Col4 : 1),
                        Col10Footer = item.Col10Footer * 100 / (item.Col4Footer != 0 ? item.Col4Footer : 1),
                        Col11 = item.Col11 * 100 / (item.Col5 != 0 ? item.Col5 : 1),
                        Col11Footer = item.Col11Footer * 100 / (item.Col5Footer != 0 ? item.Col5Footer : 1),
                        Col12 = item.Col12 * 100 / (item.Col6 != 0 ? item.Col6 : 1),
                        Col12Footer = item.Col12Footer * 100 / (item.Col6Footer != 0 ? item.Col6Footer : 1),
                        Col13 = item.Col13 * 100 / (item.Col4 != 0 ? item.Col4 : 1),
                        Col13Footer = item.Col13Footer * 100 / (item.Col4Footer != 0 ? item.Col4Footer : 1),
                        Col14 = item.Col14 * 100 / (item.Col5 != 0 ? item.Col5 : 1),
                        Col14Footer = item.Col14Footer * 100 / (item.Col5Footer != 0 ? item.Col5Footer : 1),
                        Col15 = item.Col15 * 100 / (item.Col6 != 0 ? item.Col6 : 1),
                        Col15Footer = item.Col15Footer * 100 / (item.Col6Footer != 0 ? item.Col6Footer : 1),
                        Col16 = item.Col16 * 100 / (item.Col4 != 0 ? item.Col4 : 1),
                        Col16Footer = item.Col16Footer * 100 / (item.Col4Footer != 0 ? item.Col4Footer : 1),
                        Col17 = item.Col17 * 100 / (item.Col5 != 0 ? item.Col5 : 1),
                        Col17Footer = item.Col17Footer * 100 / (item.Col5Footer != 0 ? item.Col5Footer : 1),
                        Col18 = item.Col18 * 100 / (item.Col6 != 0 ? item.Col6 : 1),
                        Col18Footer = item.Col18Footer * 100 / (item.Col6Footer != 0 ? item.Col6Footer : 1),
                        Col19 = item.Col19 * 100 / (item.Col4 != 0 ? item.Col4 : 1),
                        Col19Footer = item.Col19Footer * 100 / (item.Col4Footer != 0 ? item.Col4Footer : 1),
                        Col20 = item.Col20 * 100 / (item.Col5 != 0 ? item.Col5 : 1),
                        Col20Footer = item.Col20Footer * 100 / (item.Col5Footer != 0 ? item.Col5Footer : 1),
                        Col21 = item.Col21 * 100 / (item.Col6 != 0 ? item.Col6 : 1),
                        Col21Footer = item.Col21Footer * 100 / (item.Col6Footer != 0 ? item.Col6Footer : 1),
                        Col22 = item.Col22 * 100 / (item.Col4 != 0 ? item.Col4 : 1),
                        Col22Footer = item.Col22Footer * 100 / (item.Col4Footer != 0 ? item.Col4Footer : 1),
                        Col23 = item.Col23 * 100 / (item.Col5 != 0 ? item.Col5 : 1),
                        Col23Footer = item.Col23Footer * 100 / (item.Col5Footer != 0 ? item.Col5Footer : 1),
                        Col24 = item.Col24 * 100 / (item.Col6 != 0 ? item.Col6 : 1),
                        Col24Footer = item.Col24Footer * 100 / (item.Col6Footer != 0 ? item.Col6Footer : 1)
                    });
                }
#endregion
                List<SPO_Statisctics_TopCallers_Result> Statisctics_TopCallers_Result = objEntitites.SPO_Statisctics_TopCallers().ToList();
                var dailyLog = new List<dailyLog>();
                var weeklyLog = new List<weeklyLog>();
                var modelTopCallers = new List<TopCallers>();
                bool flagOnce = true;
                foreach (var item in Statisctics_TopCallers_Result)
                {
                    try
                    {
                        dailyLog l = new dailyLog();
                        if (item.status == true)
                        { l.sUserName ="<label class=\"label label-success\">"+@item.Caller+"</label>"; }
                        else
                        { l.sUserName = "<label class=\"label label-danger\">" + @item.Caller + "</label>"; }
                        l.nPaid = item.Col1.ToString() + " (" + (item.Col1 * 100 / (item.Col1Total == 0 ? 1 : item.Col1Total)).ToString() + "%)";
                        dailyLog.Add(l);
                        weeklyLog w = new weeklyLog();
                        if (item.status == true)
                        { w.sUserName = "<label class=\"label label-success\">" + @item.Caller + "</label>"; }
                        else
                        { w.sUserName = "<label class=\"label label-danger\">" + @item.Caller + "</label>"; }
                        w.nPaid = item.Col2.ToString() + " (" + (item.Col2 * 100 / (item.Col2Total == 0 ? 1 : item.Col2Total)).ToString() + "%)";
                        weeklyLog.Add(w);
                        TopCallers tp = new TopCallers();
                        if (item.status == true)
                        { tp.sUserName = "<label class=\"label label-success\">" + @item.Caller + "</label>"; }
                        else
                        { tp.sUserName = "<label class=\"label label-danger\">" + @item.Caller + "</label>"; }
                        tp.nTotalCall = item.Col11.ToString();
                        modelTopCallers.Add(tp);
                        if (flagOnce)
                        {
                            overviewStats f = new overviewStats();
                            f.nAverage = item.Col3.ToString() + " (" + (item.Col3 * 100 / (item.Col3Total == 0 ? 1 : item.Col3Total)).ToString() + "%)";
                            f.nToday = item.Col4.ToString() + " (" + (item.Col4 * 100 / (item.Col4Total == 0 ? 1 : item.Col4Total)).ToString() + "%)";
                            f.nYesterday = item.Col5.ToString() + " (" + (item.Col5 * 100 / (item.Col5Total == 0 ? 1 : item.Col5Total)).ToString() + "%)";
                            f.nDay1 = item.Col6.ToString() + " (" + (item.Col6 * 100 / (item.Col6Total == 0 ? 1 : item.Col6Total)).ToString() + "%)";
                            f.nDay2 = item.Col7.ToString() + " (" + (item.Col7 * 100 / (item.Col7Total == 0 ? 1 : item.Col7Total)).ToString() + "%)";
                            f.nDay3 = item.Col8.ToString() + " (" + (item.Col8 * 100 / (item.Col8Total == 0 ? 1 : item.Col8Total)).ToString() + "%)";
                            f.nDay4 = item.Col9.ToString() + " (" + (item.Col9 * 100 / (item.Col9Total == 0 ? 1 : item.Col9Total)).ToString() + "%)";
                            f.nDay5 = item.Col10.ToString() + " (" + (item.Col10 * 100 / (item.Col10Total == 0 ? 1 : item.Col10Total)).ToString() + "%)";
                            overviewStats.Add(f);
                            ViewBag.overviewStats = overviewStats;
                        }
                        flagOnce = false;
                    }
                    catch (Exception xp) { }
                        
                        
                    ViewBag.dailyLog = dailyLog.OrderByDescending(n=>int.Parse(n.nPaid.Split(' ')[0].Trim()));
                    ViewBag.weeklyLog = weeklyLog.OrderByDescending(n => int.Parse(n.nPaid.Split(' ')[0].Trim()));
                    ViewBag.topCallers = modelTopCallers.OrderByDescending(k => int.Parse(k.nTotalCall.Split(' ')[0].Trim()));
                    ViewBag.BarChart = modelBarChart;
                    
                }
                return View(model);
            }
            catch (Exception xp) {
                return View(model);
            }
        }
        //
        // GET: /Statistics/
        [Authorize]
        public ActionResult Index2(string Paid, string WillPaid, string Cancelled)
        {
            string id = Paid;
            try
            {
                var overviewStats = new List<overviewStats>();
                var weeklyLog = new List<weeklyLog>();
                var dailyLog = new List<dailyLog>();
                var model = new List<Statistics>();
                var modelBarChart = new List<BarchartStats>();
                var modelTopCallers = new List<TopCallers>();
                var rec = from parentObj in db.AspNetUsers.OrderBy(x => x.FirstName) select new { Users = parentObj };
                if (rec != null)
                {
                    foreach (var x in rec)
                    {
                        try
                        {
                            Statistics st = new Statistics();

                            BarchartStats f = new BarchartStats();
                            st.sUserName = x.Users.FirstName;
                            st.bStatus = x.Users.isActive;

                            st.nHoursToday = Math.Round(CalculateLoggedHours(x.Users.Id, 1), 2);
                            st.nHoursWeek = Math.Round(CalculateLoggedHours(x.Users.Id, 2), 2);
                            st.nHoursTotal = Math.Round(CalculateLoggedHours(x.Users.Id, 3), 2);

                            st.nTotalCalls = GetCallsData(x.Users.Id, 0);
                            st.nTotalCallsToday = GetCallsData(x.Users.Id, 7);
                            st.nTotalCallsWeek = GetCallsData(x.Users.Id, 8);

                            st.nPaid = GetCallsData(x.Users.Id, 6);
                            try
                            {
                                st.sPaid = Math.Round((GetCallsData(x.Users.Id, 6) * 100.0 / st.nTotalCalls), 2).ToString();
                                st.sPaid = st.sPaid == "NaN" ? "0" : st.sPaid;
                            }
                            catch (Exception xp)
                            {
                                st.sPaid = "0";
                            }
                            try
                            {
                                st.sPaidToday = Math.Round((GetCallsData(x.Users.Id, 9) * 100.0 / st.nTotalCallsToday), 2).ToString();
                                st.sPaidToday = st.sPaidToday == "NaN" ? "0" : st.sPaidToday;
                            }
                            catch (Exception e)
                            {
                                st.sPaidToday = "0";
                            }
                            try
                            {
                                st.sPaidWeek = Math.Round((GetCallsData(x.Users.Id, 10) * 100.0 / st.nTotalCallsWeek), 2).ToString();
                                st.sPaidWeek = st.sPaidWeek == "NaN" ? "0" : st.sPaidWeek;
                            }
                            catch (Exception e)
                            {
                                st.sPaidWeek = "0";
                            }

                            st.nWillPay = GetCallsData(x.Users.Id, 2);
                            try
                            {
                                st.sWillPay = Math.Round((GetCallsData(x.Users.Id, 2) * 100.0 / st.nTotalCalls), 2).ToString();
                                st.sWillPay = st.sWillPay == "NaN" ? "0" : st.sWillPay;
                            }
                            catch (Exception xp)
                            {
                                st.sWillPay = "0";
                            }
                            try
                            {
                                st.sWillPayToday = Math.Round((GetCallsData(x.Users.Id, 11) * 100.0 / st.nTotalCallsToday), 2).ToString();
                                st.sWillPayToday = st.sWillPayToday == "NaN" ? "0" : st.sWillPayToday;
                            }
                            catch (Exception e)
                            {
                                st.sWillPayToday = "0";
                            } try
                            {
                                st.sWillPayWeek = Math.Round((GetCallsData(x.Users.Id, 12) * 100.0 / st.nTotalCallsWeek), 2).ToString();
                                st.sWillPayWeek = st.sWillPayWeek == "NaN" ? "0" : st.sWillPayWeek;
                            }
                            catch (Exception e)
                            {
                                st.sWillPayWeek = "0";
                            }
                            st.nNotPickedUp = (GetCallsData(x.Users.Id, 1));
                            try
                            {
                                st.sNotPickedUp = Math.Round(((st.nNotPickedUp * 100.0) / st.nTotalCalls), 2).ToString();
                                st.sNotPickedUp = st.sNotPickedUp == "NaN" ? "0" : st.sNotPickedUp;
                            }
                            catch (Exception E)
                            {
                                st.sNotPickedUp = "0";
                            }
                            try
                            {
                                st.sNotPickedUpToday = Math.Round((GetCallsData(x.Users.Id, 13) * 100.0 / st.nTotalCallsToday), 2).ToString();
                                st.sNotPickedUpToday = st.sNotPickedUpToday == "NaN" ? "0" : st.sNotPickedUpToday;
                            }
                            catch (Exception e)
                            {
                                st.sNotPickedUpToday = "0";
                            } try
                            {
                                st.sNotPickedUpWeek = Math.Round((GetCallsData(x.Users.Id, 14) * 100.0 / st.nTotalCallsWeek), 2).ToString();
                                st.sNotPickedUpWeek = st.sNotPickedUpWeek == "NaN" ? "0" : st.sNotPickedUpWeek;
                            }
                            catch (Exception e)
                            {
                                st.sNotPickedUpWeek = "0";
                            }
                            st.nChangeToCOD = GetCallsData(x.Users.Id, 15);
                            try
                            {
                                st.sChangeToCOD = Math.Round((st.nChangeToCOD * 100.0 / st.nTotalCalls), 2).ToString();
                                st.sChangeToCOD = st.sChangeToCOD == "NaN" ? "0" : st.sChangeToCOD;
                            }
                            catch (Exception E)
                            {
                                st.sChangeToCOD = "0";
                            }
                            try
                            {
                                st.sChangeToCODToday = Math.Round((GetCallsData(x.Users.Id, 16) * 100.0 / st.nTotalCallsToday), 2).ToString();
                                st.sChangeToCODToday = st.sChangeToCODToday == "NaN" ? "0" : st.sChangeToCODToday;
                            }
                            catch (Exception e)
                            {
                                st.sChangeToCODToday = "0";
                            }
                            try
                            {
                                st.sChangeToCODWeek = Math.Round((GetCallsData(x.Users.Id, 17) * 100.0 / st.nTotalCallsWeek), 2).ToString();
                                st.sChangeToCODWeek = st.sChangeToCODWeek == "NaN" ? "0" : st.sChangeToCODWeek;
                            }
                            catch (Exception E)
                            {
                                st.sChangeToCODWeek = "0";
                            }
                            st.nWrongNumber = GetCallsData(x.Users.Id, 18);
                            try
                            {
                                st.sWrongNumber = Math.Round((st.nWrongNumber * 100.0 / st.nTotalCalls), 2).ToString();
                                st.sWrongNumber = st.sWrongNumber == "NaN" ? "0" : st.sWrongNumber;
                            }
                            catch (Exception E)
                            {
                                st.sWrongNumber = "0";
                            }
                            try
                            {
                                st.sWrongNumberToday = Math.Round((GetCallsData(x.Users.Id, 19) * 100.0 / st.nTotalCallsToday), 2).ToString();
                                st.sWrongNumberToday = st.sWrongNumberToday == "NaN" ? "0" : st.sWrongNumberToday;
                            }
                            catch (Exception e)
                            {
                                st.sWrongNumberToday = "0";
                            } try
                            {
                                st.sWrongNumberWeek = Math.Round((GetCallsData(x.Users.Id, 20) * 100.0 / st.nTotalCallsWeek), 2).ToString();
                                st.sWrongNumberWeek = st.sWrongNumberWeek == "NaN" ? "0" : st.sWrongNumberWeek;
                            }
                            catch (Exception e)
                            {
                                st.sWrongNumberWeek = "0";
                            }
                            st.nCancelled = GetCallsData(x.Users.Id, 21);
                            try
                            {
                                st.sCancelled = Math.Round((st.nCancelled * 100.0 / st.nTotalCalls), 2).ToString();
                                st.sCancelled = st.sCancelled == "NaN" ? "0" : st.sCancelled;
                            }
                            catch (Exception E)
                            {
                                st.sCancelled = "0";
                            }
                            try
                            {
                                st.sCancelledToday = Math.Round((GetCallsData(x.Users.Id, 22) * 100.0 / st.nTotalCallsToday), 2).ToString();
                                st.sCancelledToday = st.sCancelledToday == "NaN" ? "0" : st.sCancelledToday;
                            }
                            catch (Exception e)
                            {
                                st.sCancelledToday = "0";
                            } try
                            {
                                st.sCancelledWeek = Math.Round((GetCallsData(x.Users.Id, 23) * 100.0 / st.nTotalCallsWeek), 2).ToString();
                                st.sCancelledWeek = st.sCancelledWeek == "NaN" ? "0" : st.sCancelledWeek;
                            }
                            catch (Exception e)
                            {
                                st.sCancelledWeek = "0";
                            }
                            model.Add(st);


                            f.nTotalCalls = st.nTotalCalls;
                            int otherCalls = 0;
                            if (Paid == "on")
                                otherCalls += st.nPaid;
                            if (WillPaid == "on")
                                otherCalls += st.nWillPay;
                            if (Cancelled == "on")
                                otherCalls += st.nCancelled;
                            f.nTotalOtherCalls = otherCalls;
                            modelBarChart.Add(f);

                            TopCallers tp = new TopCallers();
                            tp.sUserName = x.Users.FirstName;
                            tp.nTotalCall = GetTopCallers(x.Users.Id);
                            modelTopCallers.Add(tp);
                            //ViewBag.topCallers = modelTopCallers.OrderByDescending(k => k.nTotalCall); 

                        }
                        catch (Exception xp) { }

                        try
                        {
                            dailyLog l = new dailyLog();
                            l.sUserName = x.Users.FirstName;
                            l.nPaid = GetPaidStats(x.Users.Id);
                            try
                            {
                                l.nWillPay = Convert.ToInt32(l.nPaid.Split('(')[0]);
                            }
                            catch (Exception) { l.nWillPay = 0; }
                            dailyLog.Add(l);
                        }
                        catch (Exception xp) { }

                        try
                        {
                            weeklyLog i = new weeklyLog();
                            i.sUserName = x.Users.FirstName;
                            i.nPaid = OverviewStats(x.Users.Id, 0);
                            i.nWillPay = Convert.ToInt32(i.nPaid.Split('(')[0]);
                            weeklyLog.Add(i);
                        }
                        catch (Exception xp) { }
                    }
                    try
                    {
                        ViewBag.dailyLog = dailyLog.OrderByDescending(k => k.nWillPay);
                        ViewBag.weeklyLog = weeklyLog.OrderByDescending(k => k.nWillPay);
                        ViewBag.topCallers = modelTopCallers.OrderByDescending(k => k.nTotalCall);
                    }
                    catch (Exception xp) { }
                    try
                    {
                        overviewStats f = new overviewStats();
                        f.nAverage = OverviewStats(1);
                        f.nToday = OverviewStats(2);
                        f.nYesterday = OverviewStats(3);
                        f.nDay1 = OverviewStats(4);
                        f.nDay2 = OverviewStats(5);
                        f.nDay3 = OverviewStats(6);
                        f.nDay4 = OverviewStats(7);
                        f.nDay5 = OverviewStats(8);

                        overviewStats.Add(f);
                        ViewBag.overviewStats = overviewStats;
                    }
                    catch (Exception xp) { }

                    ViewBag.BarChart = modelBarChart;
                    List<Statistics> PercentagesOfTotals = new List<Statistics>();
                    PercentagesOfTotals.Add(getPercentagesOfTotals());
                    ViewBag.PercentagesOfTotals = PercentagesOfTotals;
                    ViewBag.loggedHoursToday = Math.Round(CalculateLoggedHours("", 4), 2);
                    ViewBag.loggedHoursWeek = Math.Round(CalculateLoggedHours("", 5), 2);
                    ViewBag.loggedHoursTotal = Math.Round(CalculateLoggedHours("", 6), 2);
                    return View(model.ToList());
                }
            }
            catch (Exception e)
            {
                //TempData["Error"] = "An error occurred, Please try again ";
            }

            return View();
        }
       
        private double CalculateLoggedHours(String id, int type)
        {
            var dateTemp = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            DateTime rawDate = DateTime.Now.AddDays(-1);
            var dateYesterday = new DateTime(rawDate.Year, rawDate.Month, rawDate.Day, 0, 0, 0);
                

            rawDate = DateTime.Now.AddDays(-7);
            var lastweek = new DateTime(rawDate.Year, rawDate.Month, rawDate.Day, 0, 0, 0);
            double userSeconds = 0;
            try
            {
                switch (type)
                {
                    case 3:
                        var data = db.tblUsersLogs.Where(x => x.nId == id && x.dtSignOutTime != null).ToList();
                        if (data != null)
                        {
                            foreach (var item in data)
                            {
                                TimeSpan? difference = item.dtSignOutTime - item.dtSignInTime;
                                userSeconds += difference.Value.TotalSeconds;
                            }
                        }
                        return userSeconds / 3600;
                    case 2:
                        var data2 = db.tblUsersLogs.Where(x => x.nId == id && x.dtSignInTime > lastweek && x.dtSignOutTime != null).ToList();
                        if (data2 != null)
                        {
                            foreach (var item in data2)
                            {
                                TimeSpan? difference = item.dtSignOutTime - item.dtSignInTime;
                                userSeconds += difference.Value.TotalSeconds;
                            }
                        }
                        return userSeconds / 3600;
                    case 1:
                        var data3 = db.tblUsersLogs.Where(x => x.nId == id && x.dtSignInTime < dateTemp && x.dtSignInTime > dateYesterday && x.dtSignOutTime != null).ToList();
                        if (data3 != null)
                        {
                            foreach (var item in data3)
                            {
                                TimeSpan? difference = item.dtSignOutTime - item.dtSignInTime;
                                userSeconds += difference.Value.TotalSeconds;
                            }
                        }
                        return userSeconds / 3600;
                    case 4:
                        var data4 = db.tblUsersLogs.Where(x => x.dtSignInTime > dateTemp && x.dtSignOutTime != null).ToList();
                        if (data4 != null)
                        {
                            foreach (var item in data4)
                            {
                                TimeSpan? difference = item.dtSignOutTime - item.dtSignInTime;
                                userSeconds += difference.Value.TotalSeconds;
                            }
                        }
                        return userSeconds / 3600;
                    case 5:
                        var data5 = db.tblUsersLogs.Where(x => x.dtSignInTime > lastweek && x.dtSignOutTime != null).ToList();
                        if (data5 != null)
                        {
                            foreach (var item in data5)
                            {
                                TimeSpan? difference = item.dtSignOutTime - item.dtSignInTime;
                                userSeconds += difference.Value.TotalSeconds;
                            }
                        }
                        return userSeconds / 3600;
                    case 6:
                        var data6 = db.tblUsersLogs.Where(x => x.dtSignOutTime != null).ToList();
                        if (data6 != null)
                        {
                            foreach (var item in data6)
                            {
                                TimeSpan? difference = item.dtSignOutTime - item.dtSignInTime;
                                userSeconds += difference.Value.TotalSeconds;
                            }
                        }
                        return userSeconds / 3600;
                    default:
                        return 0;

                }
            }
            catch (Exception e) { }
            return 0;
        }
        private Statistics getPercentagesOfTotals()
        {
            Statistics _object = new Statistics();
            var dateTemp = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);

            DateTime rawDate = DateTime.Now.AddDays(-7);
            var lastweek = new DateTime(rawDate.Year, rawDate.Month, rawDate.Day, 0, 0, 0);
            try
            {
                try
                {
                    _object.sPaidToday = (Math.Round(( (db.CallingLogs.Where(o => o.Event == 6 && o.logDate > dateTemp).Count() * 100.0 ) / db.CallingLogs.Where(o => o.logDate > dateTemp).Count()),2)).ToString();
                    _object.sPaidToday = _object.sPaidToday == "NaN" ? "0" : _object.sPaidToday;
                }catch(Exception e)
                {
                    _object.sPaidToday = "0";
                }
                try
                {
                    _object.sPaidWeek = (Math.Round((db.CallingLogs.Where(o => o.Event == 6 && o.logDate > lastweek).Count() * 100.0 / db.CallingLogs.Where(o => o.logDate > lastweek).Count()),2)).ToString();
                    _object.sPaidWeek = _object.sPaidWeek == "NaN" ? "0" : _object.sPaidWeek;
                }catch(Exception e)
                {
                   _object.sPaidWeek = "0";
                }
                try
                {
                    _object.sPaid = Math.Round((db.CallingLogs.Where(o => o.Event == 6).Count() * 100.0 / db.CallingLogs.Count()),2).ToString();
                    _object.sPaid = _object.sPaid == "NaN" ? "0" : _object.sPaid;
                }catch(Exception e)
                {
                   _object.sPaid = "0";
                }
                try
                {
                 _object.sWillPayToday = Math.Round((db.CallingLogs.Where(o => o.Event == 2 && o.logDate > dateTemp).Count() * 100.0 / db.CallingLogs.Where(o => o.logDate > dateTemp).Count() ),2).ToString();
                 _object.sWillPayToday = _object.sWillPayToday == "NaN" ? "0" : _object.sWillPayToday;
                }catch(Exception e)
                {
                   _object.sWillPayToday = "0";
                }
                try
                {
                _object.sWillPayWeek = Math.Round((db.CallingLogs.Where(o => o.Event == 2 && o.logDate > lastweek).Count() * 100.0 / db.CallingLogs.Where(o => o.logDate > lastweek).Count() ),2).ToString();
                _object.sWillPayWeek = _object.sWillPayWeek == "NaN" ? "0" : _object.sWillPayWeek;
                }catch(Exception e)
                {
                   _object.sWillPayWeek = "0";
                }
                try
                {
                _object.sWillPay = Math.Round((db.CallingLogs.Where(o => o.Event == 2).Count() * 100.0 / db.CallingLogs.Count() ),2).ToString();
                _object.sWillPay = _object.sWillPay == "NaN" ? "0" : _object.sWillPay;
                }catch(Exception e)
                {
                   _object.sWillPay = "0";
                }
                try
                {
                 _object.sNotPickedUpToday = Math.Round((db.CallingLogs.Where(o => o.Event == 1 && o.logDate > dateTemp).Count() * 100.0 / db.CallingLogs.Where(o => o.logDate > dateTemp).Count() ),2).ToString();
                 _object.sNotPickedUpToday = _object.sNotPickedUpToday == "NaN" ? "0" : _object.sNotPickedUpToday;
                }catch(Exception e)
                {
                   _object.sNotPickedUpToday = "0";
                }
                try
                {
                _object.sNotPickedUpWeek = Math.Round((db.CallingLogs.Where(o => o.Event == 1 && o.logDate > lastweek).Count() * 100.0 / db.CallingLogs.Where(o => o.logDate > lastweek).Count() ),2).ToString();
                _object.sNotPickedUpWeek = _object.sNotPickedUpWeek == "NaN" ? "0" : _object.sNotPickedUpWeek;
                }catch(Exception e)
                {
                    _object.sNotPickedUpWeek = "0";
                }
                try
                {
                _object.sNotPickedUp = Math.Round((db.CallingLogs.Where(o => o.Event == 1).Count() * 100.0 / db.CallingLogs.Count() ),2).ToString();
                _object.sNotPickedUp = _object.sNotPickedUp == "NaN" ? "0" : _object.sNotPickedUp;
                }
                catch (Exception E)
                {
                  _object.sNotPickedUp  = "0";
                }
                try
                {
                 _object.sChangeToCODToday = Math.Round((db.CallingLogs.Where(o => o.Event == 3 && o.logDate > dateTemp).Count() * 100.0 / db.CallingLogs.Where(o => o.logDate > dateTemp).Count() ),2).ToString();
                 _object.sChangeToCODToday = _object.sChangeToCODToday == "NaN" ? "0" : _object.sChangeToCODToday;
                }
                catch (Exception E)
                {
                   _object.sChangeToCODToday = "0";
                }
                try
                {
                _object.sChangeToCODWeek = Math.Round((db.CallingLogs.Where(o => o.Event == 3 && o.logDate > lastweek).Count() * 100.0 / db.CallingLogs.Where(o => o.logDate > lastweek).Count() ),2).ToString();
                _object.sChangeToCODWeek = _object.sChangeToCODWeek == "NaN" ? "0" : _object.sChangeToCODWeek;
                }
                catch (Exception E)
                {
                  _object.sChangeToCODWeek  = "0";
                }
                try
                {
                _object.sChangeToCOD = Math.Round((db.CallingLogs.Where(o => o.Event == 3).Count() * 100.0 / db.CallingLogs.Count() ),2).ToString();
                _object.sChangeToCOD = _object.sChangeToCOD == "NaN" ? "0" : _object.sChangeToCOD;
                }
                catch (Exception E)
                {
                   _object.sChangeToCOD = "0";
                }
                try
                {
                 _object.sWrongNumberToday = Math.Round((db.CallingLogs.Where(o => o.Event == 5 && o.logDate > dateTemp).Count() * 100.0 / db.CallingLogs.Where(o => o.logDate > dateTemp).Count() ),2).ToString();
                 _object.sWrongNumberToday = _object.sWrongNumberToday == "NaN" ? "0" : _object.sWrongNumberToday;
                }
                catch (Exception E)
                {
                   _object.sWrongNumberToday = "0";
                }
                try
                {
                 _object.sWrongNumberWeek = Math.Round((db.CallingLogs.Where(o => o.Event == 5 && o.logDate > lastweek).Count() * 100.0 / db.CallingLogs.Where(o => o.logDate > lastweek).Count() ),2).ToString();
                 _object.sWrongNumberWeek = _object.sWrongNumberWeek == "NaN" ? "0" : _object.sWrongNumberWeek;
                }
                catch (Exception E)
                {
                   _object.sWrongNumberWeek = "0";
                }
                try
                {
                _object.sWrongNumber = Math.Round((db.CallingLogs.Where(o => o.Event == 5).Count() * 100.0 / db.CallingLogs.Count() ),2).ToString();
                _object.sWrongNumber = _object.sWrongNumber == "NaN" ? "0" : _object.sWrongNumber;
                }
                catch (Exception E)
                {
                   _object.sWrongNumber = "0";
                }
                try
                {
                _object.sCancelledToday = Math.Round((db.CallingLogs.Where(o => o.Event == 4 && o.logDate > dateTemp).Count() * 100.0 / db.CallingLogs.Where(o => o.logDate > dateTemp).Count() ),2).ToString();
                _object.sCancelledToday = _object.sCancelledToday == "NaN" ? "0" : _object.sCancelledToday;
                }
                catch (Exception E)
                {
                   _object.sCancelledToday = "0";
                }
                try
                {
                _object.sCancelledWeek = Math.Round((db.CallingLogs.Where(o => o.Event == 4 && o.logDate > lastweek).Count() * 100.0 / db.CallingLogs.Where(o => o.logDate > lastweek).Count() ),2).ToString();
                _object.sCancelledWeek = _object.sCancelledWeek == "NaN" ? "0" : _object.sCancelledWeek;
                }
                catch (Exception E)
                {
                  _object.sCancelledWeek  = "0";
                }
                try
                {
                _object.sCancelled = Math.Round((db.CallingLogs.Where(o => o.Event == 4).Count() * 100.0 / db.CallingLogs.Count() ),2).ToString();
                _object.sCancelled = _object.sCancelled == "NaN" ? "0" : _object.sCancelled;
                }
                catch (Exception E)
                {
                    _object.sCancelled = "0";
                }  
                _object.nTotalCalls = db.CallingLogs.Count();
                _object.nTotalCallsToday = db.CallingLogs.Where(o => o.logDate > dateTemp).Count();
                _object.nTotalCallsWeek = db.CallingLogs.Where(o => o.logDate > lastweek).Count();

            }
            catch (Exception)
            {
                TempData["Error"] = "An error occurred, Please try again ";
            }
            
            return _object;
        }
        private int GetCallsData(string ID,int eventID)
        {
            var dateTemp = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            DateTime rawDate = DateTime.Now.AddDays(-1);
            var dateYesterday = new DateTime(rawDate.Year, rawDate.Month, rawDate.Day, 0, 0, 0);
            rawDate = DateTime.Now.AddDays(-7);
            var lastweek = new DateTime(rawDate.Year, rawDate.Month, rawDate.Day, 0, 0, 0);
            switch (eventID)
            {
                
                case 1:
                    return db.CallingLogs.Where(x => x.Caller == ID && x.Event == eventID).Count();
                case 2:
                case 3:
                case 4:
                case 5:
                case 6://Paid calls total
                    return db.CallingLogs.Where(x => x.Caller == ID && x.Event==eventID).Count();
                //Total calls per user
                case 0:
                    return db.CallingLogs.Where(x => x.Caller == ID).Count();
                    break;
                //Total calls per user today
                case 7:
                    return db.CallingLogs.Where(x => x.Caller == ID && x.logDate < dateTemp && x.logDate >dateYesterday).Count();
                    break;
                //Total calls per user last week
                case 8:
                    return db.CallingLogs.Where(x => x.Caller == ID && x.logDate > lastweek).Count();
                    break;

                //Paid calls per user today
                case 9:
                    return db.CallingLogs.Where(x => x.Caller == ID && x.Event == 6 && x.logDate < dateTemp && x.logDate > dateYesterday).Count();
                    break;
                //Paid calls per user last week
                case 10:
                    return db.CallingLogs.Where(x => x.Caller == ID && x.Event == 6 && x.logDate > lastweek).Count();
                    break;
                //Will Paid calls per user today
                case 11:
                    return db.CallingLogs.Where(x => x.Caller == ID && x.Event == 2 && x.logDate < dateTemp && x.logDate > dateYesterday).Count();
                    break;
                //Will Paid calls per user last week
                case 12:
                    return db.CallingLogs.Where(x => x.Caller == ID && x.Event == 2 && x.logDate > lastweek).Count();
                    break;
                case 13: // not picked up calls per user today
                    return db.CallingLogs.Where(x => x.Caller == ID && x.Event == 1 && x.logDate < dateTemp && x.logDate > dateYesterday).Count();
                    break;
                case 14: // not picked up calls per user last week
                    return db.CallingLogs.Where(x => x.Caller == ID && x.Event == 1 && x.logDate > lastweek).Count();
                    break;
                case 15: // change to cod total
                    return db.CallingLogs.Where(x => x.Caller == ID && x.Event == 3).Count();
                case 16: // change to cod per user today
                    return db.CallingLogs.Where(x => x.Caller == ID && x.Event == 3 && x.logDate < dateTemp && x.logDate > dateYesterday).Count();
                case 17: // change to cod per user last week
                    return db.CallingLogs.Where(x => x.Caller == ID && x.Event == 3 && x.logDate > lastweek).Count();
                case 18: // wrong numbers total
                    return db.CallingLogs.Where(x => x.Caller == ID && x.Event == 5).Count();
                case 19: // wrong numbers per user today
                    return db.CallingLogs.Where(x => x.Caller == ID && x.Event == 5 && x.logDate < dateTemp && x.logDate > dateYesterday).Count();
                case 20: // wrong numbers per user last week
                    return db.CallingLogs.Where(x => x.Caller == ID && x.Event == 5 && x.logDate > lastweek).Count();
                case 21: // cancel order calls total
                    return db.CallingLogs.Where(x => x.Caller == ID && x.Event == 4).Count();
                case 22: // cancel order calls per user today
                    return db.CallingLogs.Where(x => x.Caller == ID && x.Event == 4 && x.logDate < dateTemp && x.logDate > dateYesterday).Count();
                case 23: // cancel order calls per user last week
                    return db.CallingLogs.Where(x => x.Caller == ID && x.Event == 4 && x.logDate > lastweek).Count();

                default:
                    return db.CallingLogs.Where(x => x.Caller == ID).Count();
            }
            
        }
        private string GetPaidStats(string ID)
        {
            var dateTemp = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            DateTime rawDate = DateTime.Now.AddDays(-1);
            var dateYesterday = new DateTime(rawDate.Year, rawDate.Month, rawDate.Day, 0, 0, 0);
            int total = db.CallingLogs.Where(x => x.Caller == ID && x.logDate < dateTemp && x.logDate > dateYesterday).Count();
            int paid =  db.CallingLogs.Where(x => x.Caller == ID && x.logDate < dateTemp && x.logDate > dateYesterday && (x.Event == 2 || x.Event == 3 || x.Event == 6)).Count();
            float avg = 0;
            if (paid > 0)
            {
                avg = (paid * 100 / total);
                return paid + "(" + avg + "%)";
            }
            else
            {
                return "0 (0%)";
            }
        }
        private string GetTopCallers(string ID)
        {
            try
            {
                var dateTemp = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                DateTime rawDate = DateTime.Now.AddDays(-1);
                var dateYesterday = new DateTime(rawDate.Year, rawDate.Month, rawDate.Day, 0, 0, 0);

                var calls = db.CallingLogs.Where(x => x.Caller == ID && x.logDate < dateTemp && x.logDate > dateYesterday && x.callDuration > 0).ToList();
                var totalCalls = calls.Select(c => c.callDuration).Sum();
                TimeSpan t = TimeSpan.FromSeconds(Convert.ToDouble(totalCalls));

                string answer = string.Format("{0:D2}m {1:D2}s",
                                t.Minutes,
                                t.Seconds);
                return answer;
            }
            catch (Exception xp) { return "0"; }
        }
        private string OverviewStats(string ID, int typeStats)
        {
            
                var dateTemp = DateTime.Now.AddDays(-7);
                int total = db.CallingLogs.Where(x =>x.Caller==ID &&  x.logDate > dateTemp).Count();
                total = total == 0 ? 1 : total;
                int paid = db.CallingLogs.Where(x => x.Caller == ID && x.logDate > dateTemp && (x.Event == 2 || x.Event == 3 || x.Event == 6)).Count();
                float avg = (paid * 100 / total);
                return paid + " (" + avg + "%)";
        }
        private string OverviewStats(int typeStats)
        {
            if (typeStats == 0)
            {
                DateTime rawDate = DateTime.Now.AddDays(-7);
                var dateTemp = new DateTime(rawDate.Year, rawDate.Month, rawDate.Day, 0, 0, 0);
                int total = db.CallingLogs.Where(x => x.logDate > dateTemp).Count();
                total = total == 0 ? 1 : total;
                int paid = db.CallingLogs.Where(x => x.logDate > dateTemp && (x.Event == 2 || x.Event == 3 || x.Event == 6)).Count();
                float avg = (paid * 100 / total);
                return paid + " (" + avg + "%)";
            }
            else if (typeStats == 1)//Average
            {
                DateTime rawDate = DateTime.Now.AddDays(-7);
                var dateTempDayBeforeYesterday = new DateTime(rawDate.Year, rawDate.Month, rawDate.Day, 0, 0, 0);
                int free = db.CallingLogs.Where((x => x.logDate > dateTempDayBeforeYesterday && (x.Event == 2 || x.Event == 3 || x.Event == 6))).Count();
                free = free / 7;
                int total = db.CallingLogs.Where((x => x.logDate > dateTempDayBeforeYesterday)).Count();
                total = total == 0 ? 1 : total;
                total = total / 7;
                float avg = (free * 100 / total);
                return free + " (" + avg + "%)";
            }
            else if (typeStats == 2)//today
            {
                DateTime rawDate = DateTime.Now.AddDays(-1);
                var dateTemp = new DateTime(rawDate.Year, rawDate.Month, rawDate.Day, 0, 0, 0);
                int paid = db.CallingLogs.Where(x => x.logDate > dateTemp && (x.Event == 2 || x.Event == 3 || x.Event == 6)).Count();
                int total = db.CallingLogs.Where(x => x.logDate > dateTemp).Count();
                total = total == 0 ? 1 : total;
                float avg = (paid * 100 / total);
                return paid + " (" + avg + "%)";
            }
            else if (typeStats == 3) // yesterday
            {
                DateTime rawDate = DateTime.Now.AddDays(-1);
                var dateYesterday = new DateTime(rawDate.Year, rawDate.Month, rawDate.Day, 0, 0, 0);
                DateTime rawDatePrev = DateTime.Now.AddDays(-2);
                var dateTempDayBeforeYesterday = new DateTime(rawDatePrev.Year, rawDatePrev.Month, rawDatePrev.Day, 0, 0, 0);

                int free = db.CallingLogs.Where((x => x.logDate < dateYesterday && x.logDate > dateTempDayBeforeYesterday && (x.Event == 2 || x.Event == 3 || x.Event == 6))).Count();
                int total = db.CallingLogs.Where(x => x.logDate < dateYesterday && x.logDate > dateTempDayBeforeYesterday).Count();
                total = total == 0 ? 1 : total;
                float avg = (free * 100 / total); 
                return free + " (" + avg + "%)";
            }
            else if (typeStats == 4) // day1
            {
                DateTime rawDate = DateTime.Now.AddDays(-2);
                var dateYesterday = new DateTime(rawDate.Year, rawDate.Month, rawDate.Day, 0, 0, 0);
                DateTime rawDatePrev = DateTime.Now.AddDays(-3);
                var dateTempDayBeforeYesterday = new DateTime(rawDatePrev.Year, rawDatePrev.Month, rawDatePrev.Day, 0, 0, 0);
                int free = db.CallingLogs.Where((x => x.logDate < dateYesterday && x.logDate > dateTempDayBeforeYesterday && (x.Event == 2 || x.Event == 3 || x.Event == 6))).Count();
                int total = db.CallingLogs.Where(x => x.logDate < dateYesterday && x.logDate > dateTempDayBeforeYesterday).Count();
                total = total == 0 ? 1 : total;
                float avg = (free * 100 / total);
                return free + " (" + avg + "%)";
            }else if (typeStats == 5) // day2
            {
                DateTime rawDate = DateTime.Now.AddDays(-3);
                var dateYesterday = new DateTime(rawDate.Year, rawDate.Month, rawDate.Day, 0, 0, 0);
                DateTime rawDatePrev = DateTime.Now.AddDays(-4);
                var dateTempDayBeforeYesterday = new DateTime(rawDatePrev.Year, rawDatePrev.Month, rawDatePrev.Day, 0, 0, 0);
                int free = db.CallingLogs.Where((x => x.logDate < dateYesterday && x.logDate > dateTempDayBeforeYesterday && (x.Event == 2 || x.Event == 3 || x.Event == 6))).Count();
                int total = db.CallingLogs.Where(x => x.logDate < dateYesterday && x.logDate > dateTempDayBeforeYesterday).Count();
                total = total == 0 ? 1 : total;
                float avg = (free * 100 / total);
                return free + " (" + avg + "%)";
            }else if (typeStats == 6) // day3
            {
                DateTime rawDate = DateTime.Now.AddDays(-4);
                var dateYesterday = new DateTime(rawDate.Year, rawDate.Month, rawDate.Day, 0, 0, 0);
                DateTime rawDatePrev = DateTime.Now.AddDays(-5);
                var dateTempDayBeforeYesterday = new DateTime(rawDatePrev.Year, rawDatePrev.Month, rawDatePrev.Day, 0, 0, 0);
                int free = db.CallingLogs.Where((x => x.logDate < dateYesterday && x.logDate > dateTempDayBeforeYesterday && (x.Event == 2 || x.Event == 3 || x.Event == 6))).Count();
                int total = db.CallingLogs.Where(x => x.logDate < dateYesterday && x.logDate > dateTempDayBeforeYesterday).Count();
                total = total == 0 ? 1 : total;
                float avg = (free * 100 / total);
                return free + " (" + avg + "%)";
            }
            else if (typeStats == 7) // day4
            {
                DateTime rawDate = DateTime.Now.AddDays(-5);
                var dateYesterday = new DateTime(rawDate.Year, rawDate.Month, rawDate.Day, 0, 0, 0);
                DateTime rawDatePrev = DateTime.Now.AddDays(-6);
                var dateTempDayBeforeYesterday = new DateTime(rawDatePrev.Year, rawDatePrev.Month, rawDatePrev.Day, 0, 0, 0);
                int free = db.CallingLogs.Where((x => x.logDate < dateYesterday && x.logDate > dateTempDayBeforeYesterday && (x.Event == 2 || x.Event == 3 || x.Event == 6))).Count();
                int total = db.CallingLogs.Where(x => x.logDate < dateYesterday && x.logDate > dateTempDayBeforeYesterday).Count();
                total = total == 0 ? 1 : total;
                float avg = (free * 100 / total);
                return free + " (" + avg + "%)";
            }
            else  // day5
            {
                DateTime rawDate = DateTime.Now.AddDays(-6);
                var dateYesterday = new DateTime(rawDate.Year, rawDate.Month, rawDate.Day, 0, 0, 0);
                DateTime rawDatePrev = DateTime.Now.AddDays(-7);
                var dateTempDayBeforeYesterday = new DateTime(rawDatePrev.Year, rawDatePrev.Month, rawDatePrev.Day, 0, 0, 0);
                int free = db.CallingLogs.Where((x => x.logDate < dateYesterday && x.logDate > dateTempDayBeforeYesterday && (x.Event == 2 || x.Event == 3 || x.Event == 6))).Count();
                int total = db.CallingLogs.Where(x => x.logDate < dateYesterday && x.logDate > dateTempDayBeforeYesterday).Count();
                total = total == 0 ? 1 : total;
                float avg = (free * 100 / total);
                
                return free + " (" + avg + "%)";
            }
        }

        [Authorize]
        public ActionResult SpecificDurationStatistics()
        {
            //DateTime startDate = Convert.ToDateTime(Request.QueryString["startDate"]);
            //DateTime endDate = Convert.ToDateTime(Request.QueryString["endDate"]);
           // if (startDate == null && endDate == null)
            String[] dates=null;
            if (Request.QueryString["realdate"] != null)
            {
                String realdate = Request.QueryString["realdate"].ToString();
                dates = realdate.Split(',');
                //DateTime realdate = DateTime.ParseExact(Request.QueryString["realdate"], "M/d/yyyy H:mm", CultureInfo.InvariantCulture);
                ViewBag.DateTimeBasedStatistics = DateTimeBasedStatistics(dates);
            }
            else // 10/12/2015 3:35 AM-10/23/2015 3:35 AM,10/12/2015 3:35 AM-10/23/2015 3:35 AM,
            {
                String realdate = DateTime.Now.ToString() +"-"+ DateTime.Now.ToString()+",";
                dates = realdate.Split(',');
                ViewBag.DateTimeBasedStatistics = DateTimeBasedStatistics(dates);
            }
                

            //ViewBag.DateTimeBasedStatistics = DateTimeBasedStatistics(startDate, endDate);
            return View();
        }
        private List<Statistics> DateTimeBasedStatistics(string [] dates)
        {
            List<Statistics> stats = new List<Statistics>();
            var rec = from parentObj in db.AspNetUsers.OrderBy(x => x.FirstName) select new { Users = parentObj };
            if (rec != null)
            {
                foreach (var x in rec)
                {
                    try
                    {
                        Statistics st = new Statistics();

                        BarchartStats f = new BarchartStats();
                        st.sUserName = x.Users.FirstName;
                        st.bStatus = x.Users.isActive;

                        st.nHoursToday = Math.Round(CalculateDateTimeBasedLoggedHours(x.Users.Id, 1, dates), 2);
                        st.nHoursWeek = Math.Round(CalculateDateTimeBasedLoggedHours(x.Users.Id, 2, dates), 2);
                        st.nHoursTotal = Math.Round(CalculateDateTimeBasedLoggedHours(x.Users.Id, 3, dates), 2);

                        st.nTotalCalls = GetDateTimeBasedCallsData(x.Users.Id, 0, dates);
                        st.nTotalCallsToday = GetDateTimeBasedCallsData(x.Users.Id, 7, dates);
                        st.nTotalCallsWeek = GetDateTimeBasedCallsData(x.Users.Id, 8, dates);

                        st.nPaid = GetDateTimeBasedCallsData(x.Users.Id, 6, dates);
                        try
                        {
                            st.sPaid = Math.Round((GetDateTimeBasedCallsData(x.Users.Id, 6, dates) * 100.0 / st.nTotalCalls), 2).ToString();
                            st.sPaid = st.sPaid == "NaN" ? "0" : st.sPaid;
                        }
                        catch (Exception xp)
                        {
                            st.sPaid = "0";
                        }
                        try
                        {
                            st.sPaidToday = Math.Round((GetDateTimeBasedCallsData(x.Users.Id, 9, dates) * 100.0 / st.nTotalCallsToday), 2).ToString();
                            st.sPaidToday = st.sPaidToday == "NaN" ? "0" : st.sPaidToday;
                        }
                        catch (Exception e)
                        {
                            st.sPaidToday = "0";
                        }
                        try
                        {
                            st.sPaidWeek = Math.Round((GetDateTimeBasedCallsData(x.Users.Id, 10, dates) * 100.0 / st.nTotalCallsWeek), 2).ToString();
                            st.sPaidWeek = st.sPaidWeek == "NaN" ? "0" : st.sPaidWeek;
                        }
                        catch (Exception e)
                        {
                            st.sPaidWeek = "0";
                        }

                        st.nWillPay = GetDateTimeBasedCallsData(x.Users.Id, 2, dates);
                        try
                        {
                            st.sWillPay = Math.Round((GetDateTimeBasedCallsData(x.Users.Id, 2, dates) * 100.0 / st.nTotalCalls), 2).ToString();
                            st.sWillPay = st.sWillPay == "NaN" ? "0" : st.sWillPay;
                        }
                        catch (Exception xp)
                        {
                            st.sWillPay = "0";
                        }
                        try
                        {
                            st.sWillPayToday = Math.Round((GetDateTimeBasedCallsData(x.Users.Id, 11, dates) * 100.0 / st.nTotalCallsToday), 2).ToString();
                            st.sWillPayToday = st.sWillPayToday == "NaN" ? "0" : st.sWillPayToday;
                        }
                        catch (Exception e)
                        {
                            st.sWillPayToday = "0";
                        } try
                        {
                            st.sWillPayWeek = Math.Round((GetDateTimeBasedCallsData(x.Users.Id, 12, dates) * 100.0 / st.nTotalCallsWeek), 2).ToString();
                            st.sWillPayWeek = st.sWillPayWeek == "NaN" ? "0" : st.sWillPayWeek;
                        }
                        catch (Exception e)
                        {
                            st.sWillPayWeek = "0";
                        }
                        st.nNotPickedUp = (GetDateTimeBasedCallsData(x.Users.Id, 1, dates));
                        try
                        {
                            st.sNotPickedUp = Math.Round(((st.nNotPickedUp * 100.0) / st.nTotalCalls), 2).ToString();
                            st.sNotPickedUp = st.sNotPickedUp == "NaN" ? "0" : st.sNotPickedUp;
                        }
                        catch (Exception E)
                        {
                            st.sNotPickedUp = "0";
                        }
                        try
                        {
                            st.sNotPickedUpToday = Math.Round((GetDateTimeBasedCallsData(x.Users.Id, 13, dates) * 100.0 / st.nTotalCallsToday), 2).ToString();
                            st.sNotPickedUpToday = st.sNotPickedUpToday == "NaN" ? "0" : st.sNotPickedUpToday;
                        }
                        catch (Exception e)
                        {
                            st.sNotPickedUpToday = "0";
                        } try
                        {
                            st.sNotPickedUpWeek = Math.Round((GetDateTimeBasedCallsData(x.Users.Id, 14, dates) * 100.0 / st.nTotalCallsWeek), 2).ToString();
                            st.sNotPickedUpWeek = st.sNotPickedUpWeek == "NaN" ? "0" : st.sNotPickedUpWeek;
                        }
                        catch (Exception e)
                        {
                            st.sNotPickedUpWeek = "0";
                        }
                        st.nChangeToCOD = GetDateTimeBasedCallsData(x.Users.Id, 15, dates);
                        try
                        {
                            st.sChangeToCOD = Math.Round((st.nChangeToCOD * 100.0 / st.nTotalCalls), 2).ToString();
                            st.sChangeToCOD = st.sChangeToCOD == "NaN" ? "0" : st.sChangeToCOD;
                        }
                        catch (Exception e)
                        {
                            st.sChangeToCOD = "0";
                        }
                        try
                        {
                            st.sChangeToCODToday = Math.Round((GetDateTimeBasedCallsData(x.Users.Id, 16, dates) * 100.0 / st.nTotalCallsToday), 2).ToString();
                            st.sChangeToCODToday = st.sChangeToCODToday == "NaN" ? "0" : st.sChangeToCODToday;
                        }
                        catch (Exception e)
                        {
                            st.sChangeToCODToday = "0";
                        }
                        try
                        {
                            st.sChangeToCODWeek = Math.Round((GetDateTimeBasedCallsData(x.Users.Id, 17, dates) * 100.0 / st.nTotalCallsWeek), 2).ToString();
                            st.sChangeToCODWeek = st.sChangeToCODWeek == "NaN" ? "0" : st.sChangeToCODWeek;
                        }
                        catch (Exception e)
                        {
                            st.sChangeToCODWeek = "0";
                        }
                        st.nWrongNumber = GetDateTimeBasedCallsData(x.Users.Id, 18, dates);
                        try
                        {
                            st.sWrongNumber = Math.Round((st.nWrongNumber * 100.0 / st.nTotalCalls), 2).ToString();
                            st.sWrongNumber = st.sWrongNumber == "NaN" ? "0" : st.sWrongNumber;
                        }
                        catch (Exception e)
                        {
                            st.sWrongNumber = "0";
                        }
                        try
                        {
                            st.sWrongNumberToday = Math.Round((GetDateTimeBasedCallsData(x.Users.Id, 19, dates) * 100.0 / st.nTotalCallsToday), 2).ToString();
                            st.sWrongNumberToday = st.sWrongNumberToday == "NaN" ? "0" : st.sWrongNumberToday;
                        }
                        catch (Exception e)
                        {
                            st.sWrongNumberToday = "0";
                        } try
                        {
                            st.sWrongNumberWeek = Math.Round((GetDateTimeBasedCallsData(x.Users.Id, 20, dates) * 100.0 / st.nTotalCallsWeek), 2).ToString();
                            st.sWrongNumberWeek = st.sWrongNumberWeek == "NaN" ? "0" : st.sWrongNumberWeek;
                        }
                        catch (Exception e)
                        {
                            st.sWrongNumberWeek = "0";
                        }
                        st.nCancelled = GetDateTimeBasedCallsData(x.Users.Id, 21, dates);
                        try
                        {
                            st.sCancelled = Math.Round((st.nCancelled * 100.0 / st.nTotalCalls), 2).ToString();
                            st.sCancelled = st.sCancelled == "NaN" ? "0" : st.sCancelled;
                        }
                        catch (Exception E)
                        {
                            st.sCancelled = "0";
                        }
                        try
                        {
                            st.sCancelledToday = Math.Round((GetDateTimeBasedCallsData(x.Users.Id, 22, dates) * 100.0 / st.nTotalCallsToday), 2).ToString();
                            st.sCancelledToday = st.sCancelledToday == "NaN" ? "0" : st.sCancelledToday;
                        }
                        catch (Exception e)
                        {
                            st.sCancelledToday = "0";
                        } try
                        {
                            st.sCancelledWeek = Math.Round((GetDateTimeBasedCallsData(x.Users.Id, 23, dates) * 100.0 / st.nTotalCallsWeek), 2).ToString();
                            st.sCancelledWeek = st.sCancelledWeek == "NaN" ? "0" : st.sCancelledWeek;
                        }
                        catch (Exception e)
                        {
                            st.sCancelledWeek = "0";
                        }
                        stats.Add(st);
                    }
                    catch (Exception e) { TempData["Error"] = "An error occured. Please try again later."; }
                }

            }
            List<Statistics> DateTimeBasedPercentagesOfTotals = new List<Statistics>();
            DateTimeBasedPercentagesOfTotals.Add(getDateTimeBasedPercentagesOfTotals(dates));
            ViewBag.DateTimeBasedPercentagesOfTotals = DateTimeBasedPercentagesOfTotals;
            ViewBag.DateTimeBasedloggedHoursToday = Math.Round(CalculateDateTimeBasedLoggedHours("", 4, dates), 2);
            ViewBag.DateTimeBasedloggedHoursWeek = Math.Round(CalculateDateTimeBasedLoggedHours("", 5, dates), 2);
            ViewBag.DateTimeBasedloggedHoursTotal = Math.Round(CalculateDateTimeBasedLoggedHours("", 6, dates), 2);
            return stats;
        }
        private Statistics getDateTimeBasedPercentagesOfTotals(string [] dates)
        {
            Statistics _object = new Statistics();
            var dateTemp = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0).AddDays(-1);
            
            DateTime rawDate = DateTime.Now.AddDays(-7);
            var lastweek = new DateTime(rawDate.Year, rawDate.Month, rawDate.Day, 0, 0, 0);
            double count = 0;
            double countTotal = 0;
            DateTime from, to;
            String [] date = null;
            try
            {
                try
                {
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date=dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.Event == 6 && o.logDate > dateTemp).Count();
                        countTotal += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.logDate > dateTemp).Count();
                    }
                        _object.sPaidToday = Math.Round((count * 100 / countTotal ),2).ToString();
                    
                        _object.sPaidToday = _object.sPaidToday == "NaN" ? "0" : _object.sPaidToday;
                }
                catch (Exception e)
                {
                    _object.sPaidToday = "0";
                }
                try
                {
                    count = countTotal = 0;
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.Event == 6 && o.logDate > lastweek).Count();
                        countTotal += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.logDate > lastweek).Count();
                    }
                    _object.sPaidWeek = Math.Round((count * 100 / countTotal), 2).ToString(); 
                    _object.sPaidWeek = _object.sPaidWeek == "NaN" ? "0" : _object.sPaidWeek;
                }
                catch (Exception e)
                {
                    _object.sPaidWeek = "0";
                }
                try
                {
                    count = countTotal = 0;
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.Event == 6).Count();
                        countTotal += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to ).Count();
                    }
                    _object.sPaid = Math.Round((count * 100 / countTotal), 2).ToString(); 
                    _object.sPaid = _object.sPaid == "NaN" ? "0" : _object.sPaid;
                }
                catch (Exception e)
                {
                    _object.sPaid = "0";
                }
                try
                {
                    count = countTotal = 0;
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.Event == 2 && o.logDate > dateTemp).Count();
                        countTotal += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.logDate > dateTemp).Count();
                    }
                    _object.sWillPayToday = Math.Round((count * 100 / countTotal), 2).ToString();
                    _object.sWillPayToday = _object.sWillPayToday == "NaN" ? "0" : _object.sWillPayToday;
                }
                catch (Exception e)
                {
                    _object.sWillPayToday = "0";
                }
                try
                {
                    count = countTotal = 0;
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.Event == 2 && o.logDate > lastweek).Count();
                        countTotal += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.logDate > lastweek).Count();
                    }
                    _object.sWillPayWeek = Math.Round((count * 100 / countTotal), 2).ToString();
                    _object.sWillPayWeek = _object.sWillPayWeek == "NaN" ? "0" : _object.sWillPayWeek;
                }
                catch (Exception e)
                {
                    _object.sWillPayWeek = "0";
                }
                try
                {
                    count = countTotal = 0;
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.Event == 2).Count();
                        countTotal += db.CallingLogs.Count();
                    }
                    _object.sWillPay = Math.Round((count * 100 / countTotal), 2).ToString();
                    _object.sWillPay = _object.sWillPay == "NaN" ? "0" : _object.sWillPay;
                }
                catch (Exception e)
                {
                    _object.sWillPay = "0";
                }
                try
                {
                    count = countTotal = 0;
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.Event == 1 && o.logDate > dateTemp).Count();
                        countTotal += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.logDate > dateTemp).Count();
                    }
                    _object.sNotPickedUpToday = Math.Round((count * 100 / countTotal), 2).ToString();
                    _object.sNotPickedUpToday = _object.sNotPickedUpToday == "NaN" ? "0" : _object.sNotPickedUpToday;
                }
                catch (Exception e)
                {
                    _object.sNotPickedUpToday = "0";
                }
                try
                {
                    count = countTotal = 0;
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.Event == 1 && o.logDate > lastweek).Count();
                        countTotal += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.logDate > lastweek).Count();
                    }
                    _object.sNotPickedUpWeek = Math.Round((count * 100 / countTotal), 2).ToString();
                    _object.sNotPickedUpWeek = _object.sNotPickedUpWeek == "NaN" ? "0" : _object.sNotPickedUpWeek;
                }
                catch (Exception e)
                {
                    _object.sNotPickedUpWeek = "0";
                }
                try
                {
                    count = countTotal = 0;
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.Event == 1).Count();
                        countTotal += db.CallingLogs.Count();
                    }
                    _object.sNotPickedUp = Math.Round((count * 100 / countTotal), 2).ToString();
                    _object.sNotPickedUp = _object.sNotPickedUp == "NaN" ? "0" : _object.sNotPickedUp;
                }
                catch (Exception e)
                {
                    _object.sNotPickedUp = "0";
                }
                try
                {
                    count = countTotal = 0;
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.Event == 3 && o.logDate > dateTemp).Count();
                        countTotal += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.logDate > dateTemp).Count();
                    }
                    _object.sChangeToCODToday = Math.Round((count * 100 / countTotal), 2).ToString();
                    _object.sChangeToCODToday = _object.sChangeToCODToday == "NaN" ? "0" : _object.sChangeToCODToday;
                }
                catch (Exception e)
                {
                    _object.sChangeToCODToday = "0";
                }
                try
                {
                    count = countTotal = 0;
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.Event == 3 && o.logDate > lastweek).Count();
                        countTotal += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.logDate > lastweek).Count();
                    }
                    _object.sChangeToCODWeek = Math.Round((count * 100 / countTotal), 2).ToString();
                    _object.sChangeToCODWeek = _object.sChangeToCODWeek == "NaN" ? "0" : _object.sChangeToCODWeek;
                }
                catch (Exception e)
                {
                    _object.sChangeToCODWeek = "0";
                }
                try
                {
                    count = countTotal = 0;
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.Event == 3).Count();
                        countTotal += db.CallingLogs.Count();
                    }
                    _object.sChangeToCOD = Math.Round((count * 100 / countTotal), 2).ToString();
                    _object.sChangeToCOD = _object.sChangeToCOD == "NaN" ? "0" : _object.sChangeToCOD;
                }
                catch (Exception e)
                {
                    _object.sChangeToCOD = "0";
                }
                try
                {
                    count = countTotal = 0;
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.Event == 5 && o.logDate > dateTemp).Count();
                        countTotal += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.logDate > dateTemp).Count();
                    }
                    _object.sWrongNumberToday = Math.Round((count * 100 / countTotal), 2).ToString();
                    _object.sWrongNumberToday = _object.sWrongNumberToday == "NaN" ? "0" : _object.sWrongNumberToday;
                }
                catch (Exception e)
                {
                    _object.sWrongNumberToday = "0";
                }
                try
                {
                    count = countTotal = 0;
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.Event == 5 && o.logDate > lastweek).Count();
                        countTotal += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.logDate > lastweek).Count();
                    }
                    _object.sWrongNumberWeek = Math.Round((count * 100 / countTotal), 2).ToString();
                    _object.sWrongNumberWeek = _object.sWrongNumberWeek == "NaN" ? "0" : _object.sWrongNumberWeek;
                }
                catch (Exception e)
                {
                    _object.sWrongNumberWeek = "0";
                }
                try
                {
                    count = countTotal = 0;
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.Event == 5).Count();
                        countTotal += db.CallingLogs.Count();
                    }
                    _object.sWrongNumber = Math.Round((count * 100 / countTotal), 2).ToString();
                    _object.sWrongNumber = _object.sWrongNumber == "NaN" ? "0" : _object.sWrongNumber;
                }
                catch (Exception e)
                {
                    _object.sWrongNumber = "0";
                }
                try
                {
                    count = countTotal = 0;
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.Event == 4 && o.logDate > dateTemp).Count();
                        countTotal += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.logDate > dateTemp).Count();
                    }
                    _object.sCancelledToday = Math.Round((count * 100 / countTotal), 2).ToString();
                    _object.sCancelledToday = _object.sCancelledToday == "NaN" ? "0" : _object.sCancelledToday;
                }
                catch (Exception e)
                {
                    _object.sCancelledToday = "0";
                }
                try
                {
                    count = countTotal = 0;
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.Event == 4 && o.logDate > lastweek).Count();
                        countTotal += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.logDate > lastweek).Count();
                    }
                    _object.sCancelledWeek = Math.Round((count * 100 / countTotal), 2).ToString(); 
                    _object.sCancelledWeek = _object.sCancelledWeek == "NaN" ? "0" : _object.sCancelledWeek;
                }
                catch (Exception e)
                {
                    _object.sCancelledWeek = "0";
                }
                try
                {
                    count = countTotal = 0;
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.Event == 4).Count();
                        countTotal += db.CallingLogs.Count();
                    }
                    _object.sCancelled = Math.Round((count * 100 / countTotal), 2).ToString();
                    _object.sCancelled = _object.sCancelled == "NaN" ? "0" : _object.sCancelled;
                }
                catch (Exception e)
                {
                    _object.sCancelled = "0";
                }
                try
                {
                    count = countTotal = 0;
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        _object.nTotalCalls += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to).Count();
                        
                    }
                     
                }
                catch { }
                try {
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        _object.nTotalCallsToday += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.logDate > dateTemp).Count();
                    }
                    }
                catch { }

                try
                {
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        _object.nTotalCallsWeek += db.CallingLogs.Where(o => o.logDate >= from && o.logDate <= to && o.logDate > lastweek).Count();
                    }
                }
                catch { }
            }
            catch (Exception)
            {
                TempData["Error"] = "An error occurred, Please try again ";
            }

            return _object;
        }
        private int GetDateTimeBasedCallsData(string ID, int eventID, string [] dates)
        {
            var dateTemp = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0).AddDays(-1);
            DateTime rawDate = DateTime.Now.AddDays(-7);
            var lastweek = new DateTime(rawDate.Year, rawDate.Month, rawDate.Day, 0, 0, 0);
            string[] date = dates[0].Split('-');
            date[0] = formatDate(date[0]);
            date[1] = formatDate(date[1]);
            DateTime from;
            DateTime.TryParse(date[0], out from);
            DateTime to;
            DateTime.TryParse(date[1], out to);
            int count = 0;
            switch (eventID)
            {

                case 1:
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(x => x.logDate >= from && x.logDate <= to && x.Caller == ID && x.Event == eventID).Count();
                    }
                    return count;
                case 2:
                case 3:
                case 4:
                case 5:
                case 6://Paid calls total
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(x => x.logDate >= from && x.logDate <= to && x.Caller == ID && x.Event == eventID).Count();
                    }
                    return count;
                //Total calls per user
                case 0:
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(x => x.logDate >= from && x.logDate <= to && x.Caller == ID).Count();
                    }
                    return count;
                    break;
                //Total calls per user today
                case 7:
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(x => x.logDate >= from && x.logDate <= to && x.Caller == ID && x.logDate > dateTemp).Count();
                    }
                    return count;
                    break;
                //Total calls per user last week
                case 8:
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(x => x.logDate >= from && x.logDate <= to && x.Caller == ID && x.logDate > lastweek).Count();
                    }
                    return count;
                    break;

                //Paid calls per user today
                case 9:
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(x => x.logDate >= from && x.logDate <= to && x.Caller == ID && x.Event == 6 && x.logDate > dateTemp).Count();
                    }
                    return count;
                    break;
                //Paid calls per user last week
                case 10:
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(x => x.logDate >= from && x.logDate <= to && x.Caller == ID && x.Event == 6 && x.logDate > lastweek).Count();
                    }
                    return count;
                    break;
                //Will Paid calls per user today
                case 11:
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(x => x.logDate >= from && x.logDate <= to && x.Caller == ID && x.Event == 2 && x.logDate > dateTemp).Count();
                    }
                    return count;
                    break;
                //Will Paid calls per user last week
                case 12:
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(x => x.logDate >= from && x.logDate <= to && x.Caller == ID && x.Event == 2 && x.logDate > lastweek).Count();
                    }
                    return count;
                    break;
                case 13: // not picked up calls per user today
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(x => x.logDate >= from && x.logDate <= to && x.Caller == ID && x.Event == 1 && x.logDate > dateTemp).Count();
                    }
                    return count;
                    break;
                case 14: // not picked up calls per user last week
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(x => x.logDate >= from && x.logDate <= to && x.Caller == ID && x.Event == 1 && x.logDate > lastweek).Count();
                    }
                    return count;
                    break;
                case 15: // change to cod total
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(x => x.logDate >= from && x.logDate <= to && x.Caller == ID && x.Event == 3).Count();
                    }
                    return count;
                case 16: // change to cod per user today
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(x => x.logDate >= from && x.logDate <= to && x.Caller == ID && x.Event == 3 && x.logDate > dateTemp).Count();
                    }
                    return count;
                case 17: // change to cod per user last week
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(x => x.logDate >= from && x.logDate <= to && x.Caller == ID && x.Event == 3 && x.logDate > lastweek).Count();
                    }
                    return count;
                case 18: // wrong numbers total
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(x => x.logDate >= from && x.logDate <= to && x.Caller == ID && x.Event == 5).Count();
                    }
                    return count;
                case 19: // wrong numbers per user today
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(x => x.logDate >= from && x.logDate <= to && x.Caller == ID && x.Event == 5 && x.logDate > dateTemp).Count();
                    }
                    return count;
                case 20: // wrong numbers per user last week
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(x => x.logDate >= from && x.logDate <= to && x.Caller == ID && x.Event == 5 && x.logDate > lastweek).Count();
                    }
                    return count;
                case 21: // cancel order calls total
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(x => x.logDate >= from && x.logDate <= to && x.Caller == ID && x.Event == 4).Count();
                    }
                    return count;
                case 22: // cancel order calls per user today
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(x => x.logDate >= from && x.logDate <= to && x.Caller == ID && x.Event == 4 && x.logDate > dateTemp).Count();
                    }
                    return count;
                case 23: // cancel order calls per user last week
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        date = dates[i].Split('-');
                        date[0] = formatDate(date[0]);
                        date[1] = formatDate(date[1]);
                        DateTime.TryParse(date[0], out from);
                        DateTime.TryParse(date[1], out to);
                        count += db.CallingLogs.Where(x => x.logDate >= from && x.logDate <= to && x.Caller == ID && x.Event == 4 && x.logDate > lastweek).Count();
                    }
                    return count;

                default:
                    return db.CallingLogs.Where(x => x.logDate >= from && x.logDate <= to && x.Caller == ID).Count();
            }

        }
        private string formatDate(string date)
        {
            char [] array = date.ToCharArray();
            char temp = array[0];
            array[0] = array[3];
            array[3] = temp;
            temp = array[1];
            array[1] = array[4];
            array[4] = temp;
            date = new string(array);
            return date;
        }
        private double CalculateDateTimeBasedLoggedHours(String id, int type, string [] dates)
        {
            var dateTemp = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0).AddDays(-1);

            DateTime rawDate = DateTime.Now.AddDays(-7);
            var lastweek = new DateTime(rawDate.Year, rawDate.Month, rawDate.Day, 0, 0, 0);
            double userSeconds = 0;
            string[] date = dates[0].Split('-');
            date[0] = formatDate(date[0]);
            date[1] = formatDate(date[1]);
            DateTime from;
            DateTime.TryParse(date[0], out from);
            DateTime to ;
            DateTime.TryParse(date[1], out to);// (date[1], "M/d/yyyy H:mm", CultureInfo.InvariantCulture);
            try
            {
                switch (type)
                {
                    case 1:
                        var data3 = db.tblUsersLogs.Where(x => x.dtSignInTime >= from && x.dtSignOutTime <= to && x.nId == id && x.dtSignInTime > dateTemp && x.dtSignOutTime != null).ToList();
                            
                        for (int i = 1; i < dates.Length - 1; i++)
                        {
                            date = dates[i].Split('-');
                            date[0] = formatDate(date[0]);
                            date[1] = formatDate(date[1]);
                            DateTime.TryParse(date[0], out from);
                            DateTime.TryParse(date[1], out to);
                            data3.Concat(db.tblUsersLogs.Where(x => x.dtSignInTime >= from && x.dtSignOutTime <= to && x.nId == id && x.dtSignInTime > dateTemp && x.dtSignOutTime != null).ToList());
                        }
                        if (data3 != null)
                        {
                            foreach (var item in data3)
                            {
                                TimeSpan? difference = item.dtSignOutTime - item.dtSignInTime;
                                userSeconds += difference.Value.TotalSeconds;
                            }
                        }
                        return userSeconds / 3600;

                    case 2:
                        var data2 = db.tblUsersLogs.Where(x => x.dtSignInTime >= from && x.dtSignOutTime <= to && x.nId == id && x.dtSignInTime > lastweek && x.dtSignOutTime != null).ToList();
                        for (int i = 1; i < dates.Length - 1; i++)
                        {
                            date = dates[i].Split('-');
                            date[0] = formatDate(date[0]);
                            date[1] = formatDate(date[1]);                           
                            DateTime.TryParse(date[0], out from);
                            DateTime.TryParse(date[1], out to);
                            data2.Concat(db.tblUsersLogs.Where(x => x.dtSignInTime >= from && x.dtSignOutTime <= to && x.nId == id && x.dtSignInTime > lastweek && x.dtSignOutTime != null).ToList());
                        }
                        if (data2 != null)
                        {
                            foreach (var item in data2)
                            {
                                TimeSpan? difference = item.dtSignOutTime - item.dtSignInTime;
                                userSeconds += difference.Value.TotalSeconds;
                            }
                        }
                        return userSeconds / 3600;

                    case 3:
                        var data = db.tblUsersLogs.Where(x => x.dtSignInTime >= from && x.dtSignOutTime <= to && x.nId == id && x.dtSignOutTime != null).ToList();
                        for (int i = 1; i < dates.Length - 1; i++)
                        {
                            date = dates[i].Split('-');
                            date[0] = formatDate(date[0]);
                            date[1] = formatDate(date[1]);
                            DateTime.TryParse(date[0], out from);
                            DateTime.TryParse(date[1], out to);
                            data.Concat(db.tblUsersLogs.Where(x => x.dtSignInTime >= from && x.dtSignOutTime <= to && x.nId == id && x.dtSignOutTime != null).ToList());
                        }
                        if (data != null)
                        {
                            foreach (var item in data)
                            {
                                TimeSpan? difference = item.dtSignOutTime - item.dtSignInTime;
                                userSeconds += difference.Value.TotalSeconds;
                            }
                        }
                        return userSeconds / 3600;

                    case 4:
                        var data4 = db.tblUsersLogs.Where(x => x.dtSignInTime >= from && x.dtSignOutTime <= to && x.dtSignInTime > dateTemp && x.dtSignOutTime != null).ToList();
                        for (int i = 1; i < dates.Length - 1; i++)
                        {
                            date = dates[i].Split('-');
                            date[0] = formatDate(date[0]);
                            date[1] = formatDate(date[1]);
                            DateTime.TryParse(date[0], out from);
                            DateTime.TryParse(date[1], out to);
                            data4.Concat(db.tblUsersLogs.Where(x => x.dtSignInTime >= from && x.dtSignOutTime <= to && x.dtSignInTime > dateTemp && x.dtSignOutTime != null).ToList());
                        }
                        if (data4 != null)
                        {
                            foreach (var item in data4)
                            {
                                TimeSpan? difference = item.dtSignOutTime - item.dtSignInTime;
                                userSeconds += difference.Value.TotalSeconds;
                            }
                        }
                        return userSeconds / 3600;

                    case 5:
                        var data5 = db.tblUsersLogs.Where(x => x.dtSignInTime >= from && x.dtSignOutTime <= to && x.dtSignInTime > lastweek && x.dtSignOutTime != null).ToList();
                        for (int i = 1; i < dates.Length - 1; i++)
                        {
                            date = dates[i].Split('-');
                            date[0] = formatDate(date[0]);
                            date[1] = formatDate(date[1]);
                            DateTime.TryParse(date[0], out from);
                            DateTime.TryParse(date[1], out to);
                            data5.Concat(db.tblUsersLogs.Where(x => x.dtSignInTime >= from && x.dtSignOutTime <= to && x.dtSignInTime > lastweek && x.dtSignOutTime != null).ToList());
                        }
                        if (data5 != null)
                        {
                            foreach (var item in data5)
                            {
                                TimeSpan? difference = item.dtSignOutTime - item.dtSignInTime;
                                userSeconds += difference.Value.TotalSeconds;
                            }
                        }
                        return userSeconds / 3600;

                    case 6:
                        var data6 = db.tblUsersLogs.Where(x => x.dtSignInTime >= from && x.dtSignOutTime <= to && x.dtSignOutTime != null).ToList();
                        for (int i = 1; i < dates.Length - 1; i++)
                        {
                            date = dates[i].Split('-');
                            date[0] = formatDate(date[0]);
                            date[1] = formatDate(date[1]);
                            DateTime.TryParse(date[0], out from);
                            DateTime.TryParse(date[1], out to);
                            data6.Concat(db.tblUsersLogs.Where(x => x.dtSignInTime >= from && x.dtSignOutTime <= to && x.dtSignOutTime != null).ToList());
                        }
                        if (data6 != null)
                        {
                            foreach (var item in data6)
                            {
                                TimeSpan? difference = item.dtSignOutTime - item.dtSignInTime;
                                userSeconds += difference.Value.TotalSeconds;
                            }
                        }
                        return userSeconds / 3600;

                    default:
                        return 0;

                }
            }
            catch (Exception e) { TempData["Error"] = "An error occured. Please try again later."; }
            return 0;
        }

        [Authorize]
        public ActionResult MatchPayments()
        {
            var model = new List<MatchPayments>();
            var result = from parentObj in db.CallingLogs
                         join childObj in db.tblOrders on parentObj.orderId equals childObj.id
                         where parentObj.Event == 6 && parentObj.solved != 8 && parentObj.solved != 7 && childObj.orderstatusid == 1
                         orderby parentObj.logDate descending
                         select new
                         {
                             CallingLogs = parentObj,
                             Orders = childObj
                         };
            //result = result.Take(50);
            foreach (var rec in result)
            {
                MatchPayments logPaid = new MatchPayments();
                logPaid.ID = rec.CallingLogs.ID;
                logPaid.orderId = rec.CallingLogs.orderId;
                logPaid.accountId = rec.CallingLogs.accountId;
                logPaid.orderCreated = rec.CallingLogs.orderCreated;
                logPaid.logDate = rec.CallingLogs.logDate;
                logPaid.payLaterDate = rec.CallingLogs.payLaterDate;
                logPaid.amountPaid = rec.CallingLogs.amountpaid;
                logPaid.comments = rec.CallingLogs.comments;
                logPaid.phone = rec.Orders.phone;
                logPaid.email = rec.Orders.email;
                logPaid.solved = rec.CallingLogs.solved;
                model.Add(logPaid);
            }
            ViewBag.LogsPaid = model;

            return View();

        }
        [HttpPost]
        public string UpdateOrder(int nCallingLogId, string nOrderId, string nAction)
        {
            try
            {
                if (nAction == "8" || nAction == "7") {//8:Paid, 7:not paid
                    CallingLogs objCL = db.CallingLogs.Find(nCallingLogId);
                    objCL.solved = Convert.ToInt32(nAction);
                    db.Entry(objCL).State = EntityState.Modified;
                    if (db.SaveChanges() > 0) {
                        if (nAction == "7") {
                            tblOrders orderCurrent = db.tblOrders.Find(Convert.ToInt32(nOrderId));
                            orderCurrent.callBlockTime = DateTime.Now;//.AddDays(1);
                            db.Entry(orderCurrent).State = EntityState.Modified;
                            if (db.SaveChanges() > 0)
                            {
                                return "1";
                            }
                            else {
                                return "0";
                            }
                        }
                        return "1";
                    }
                }
                return "0";
            }
            catch (Exception xp) {
                return "0";
            }
        }

        //Paylater and Cancel
        [HttpPost]
        public string UpdateOrderExtended(int nCallingLogId, string nOrderId, string nAction, string payDate, string comments)
        {
            try
            {
                    CallingLogs objCL = db.CallingLogs.Find(nCallingLogId);
                    objCL.solved = Convert.ToInt32(nAction);
                    objCL.nextCall = DateTime.Parse(payDate);
                    objCL.Event = 2;//- For "not paid" we need to put it back in callling rotation by saving a new event "checked - not paid" (7)
                    db.Entry(objCL).State = EntityState.Modified;
                    if (db.SaveChanges() > 0){
                        tblOrders orderCurrent=db.tblOrders.Find(Convert.ToInt32(nOrderId));
                        objCL = new CallingLogs();
                        objCL.Caller = User.Identity.GetUserId();
                        objCL.Event = Convert.ToInt32(nAction);
                        objCL.logDate = DateTime.Now;
                        objCL.callDuration = 0;
                        objCL.nextCall = (nAction == "4") ? DateTime.Now : DateTime.Parse(payDate).AddDays(1);
                        objCL.orderId = orderCurrent.id;
                        objCL.accountId = orderCurrent.accountid;
                        objCL.Caller = User.Identity.GetUserId();
                        objCL.comments = comments;
                        objCL.orderCreated = orderCurrent.created;
                        objCL.payLaterDate = (nAction == "4") ? DateTime.Now : DateTime.Parse(payDate);
                        objCL.Scope = "Match Payment";
                        objCL.solved=Convert.ToInt32(nAction);
                        db.CallingLogs.Add(objCL);
                        if (db.SaveChanges() > 0) {
                            if (nAction == "4"){
                                //update blocktime to make it available again
 
                                //cancel the order
                                if (CancelOrder(GetToken(), orderCurrent.id.ToString(), 1)){
                                    return "1";
                                }
                                else {
                                    return "0";
                                }
                            }
                            return "1";
                        }
                    }
                return "0";
            }
            catch (Exception xp)
            {
                return "0";
            }
        }
        private string GetToken()
        {
            com.chilindo.beta.Generic obj = new com.chilindo.beta.Generic();
            XmlNode node = obj.GetAccessToken4API("api_orders", "E27D!4E8C", com.chilindo.beta.APIcalls.APIORDER);
            return node.ChildNodes[0].InnerText;
        }
        private bool CancelOrder(string Token, string Orderid, int type)
        {
            Orderid = db.tblOrders.Find(Convert.ToInt32(Orderid)).orderguid;
            //change to COD
            if (type == 0)
            {
                com.chilindo.beta.orders.APIOrders obj = new com.chilindo.beta.orders.APIOrders();
                XmlNode node = obj.ChangeDeliveryPaymentOnOrder(Token, Orderid, "COD", "COD");
                return true;
            }
            //change to Cancel
            if (type == 1)
            {
                com.chilindo.beta.orders.APIOrders obj = new com.chilindo.beta.orders.APIOrders();
                //XmlNode node = obj.ChangeDeliveryPaymentOnOrder(Token, Orderid, "COD", "COD");
                XmlNode node = obj.CancelOrder(Token, Orderid);
                return true;
            }
            return false;
        }
    }
}