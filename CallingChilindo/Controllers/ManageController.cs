﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using CallingChilindo.Models;

namespace CallingChilindo.Controllers
{
    [Authorize]
    public class ManageController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public ManageController()
        {
        }

        public ManageController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Manage/Index
        public async Task<ActionResult> Index(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.SetTwoFactorSuccess ? "Your two-factor authentication provider has been set."
                : message == ManageMessageId.Error ? "An error has occurred."
                : message == ManageMessageId.AddPhoneSuccess ? "Your phone number was added."
                : message == ManageMessageId.RemovePhoneSuccess ? "Your phone number was removed."
                : "";

            var userId = User.Identity.GetUserId();
            var model = new IndexViewModel
            {
                HasPassword = HasPassword(),
                PhoneNumber = await UserManager.GetPhoneNumberAsync(userId),
                TwoFactor = await UserManager.GetTwoFactorEnabledAsync(userId),
                Logins = await UserManager.GetLoginsAsync(userId),
                BrowserRemembered = await AuthenticationManager.TwoFactorBrowserRememberedAsync(userId)
            };
            return View(model);
        }

        //
        // POST: /Manage/RemoveLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RemoveLogin(string loginProvider, string providerKey)
        {
            ManageMessageId? message;
            var result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return RedirectToAction("ManageLogins", new { Message = message });
        }

        //
        // GET: /Manage/AddPhoneNumber
        public ActionResult AddPhoneNumber()
        {
            return View();
        }

        //
        // POST: /Manage/AddPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddPhoneNumber(AddPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            // Generate the token and send it
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), model.Number);
            if (UserManager.SmsService != null)
            {
                var message = new IdentityMessage
                {
                    Destination = model.Number,
                    Body = "Your security code is: " + code
                };
                await UserManager.SmsService.SendAsync(message);
            }
            return RedirectToAction("VerifyPhoneNumber", new { PhoneNumber = model.Number });
        }

        //
        // POST: /Manage/EnableTwoFactorAuthentication
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EnableTwoFactorAuthentication()
        {
            await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), true);
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction("Index", "Manage");
        }

        //
        // POST: /Manage/DisableTwoFactorAuthentication
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DisableTwoFactorAuthentication()
        {
            await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), false);
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction("Index", "Manage");
        }

        //
        // GET: /Manage/VerifyPhoneNumber
        public async Task<ActionResult> VerifyPhoneNumber(string phoneNumber)
        {
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), phoneNumber);
            // Send an SMS through the SMS provider to verify the phone number
            return phoneNumber == null ? View("Error") : View(new VerifyPhoneNumberViewModel { PhoneNumber = phoneNumber });
        }

        //
        // POST: /Manage/VerifyPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyPhoneNumber(VerifyPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePhoneNumberAsync(User.Identity.GetUserId(), model.PhoneNumber, model.Code);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                return RedirectToAction("Index", new { Message = ManageMessageId.AddPhoneSuccess });
            }
            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "Failed to verify phone");
            return View(model);
        }

        //
        // GET: /Manage/RemovePhoneNumber
        public async Task<ActionResult> RemovePhoneNumber()
        {
            var result = await UserManager.SetPhoneNumberAsync(User.Identity.GetUserId(), null);
            if (!result.Succeeded)
            {
                return RedirectToAction("Index", new { Message = ManageMessageId.Error });
            }
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction("Index", new { Message = ManageMessageId.RemovePhoneSuccess });
        }

       

        //
        // GET: /Manage/SetPassword
        public ActionResult SetPassword()
        {
            return View();
        }

        //
        // POST: /Manage/SetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SetPassword(SetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);
                if (result.Succeeded)
                {
                    var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                    if (user != null)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    }
                    return RedirectToAction("Index", new { Message = ManageMessageId.SetPasswordSuccess });
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Manage/ManageLogins
        public async Task<ActionResult> ManageLogins(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return View("Error");
            }
            var userLogins = await UserManager.GetLoginsAsync(User.Identity.GetUserId());
            var otherLogins = AuthenticationManager.GetExternalAuthenticationTypes().Where(auth => userLogins.All(ul => auth.AuthenticationType != ul.LoginProvider)).ToList();
            ViewBag.ShowRemoveButton = user.PasswordHash != null || userLogins.Count > 1;
            return View(new ManageLoginsViewModel
            {
                CurrentLogins = userLogins,
                OtherLogins = otherLogins
            });
        }

        //
        // POST: /Manage/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new AccountController.ChallengeResult(provider, Url.Action("LinkLoginCallback", "Manage"), User.Identity.GetUserId());
        }

        //
        // GET: /Manage/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback()
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
            if (loginInfo == null)
            {
                return RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
            }
            var result = await UserManager.AddLoginAsync(User.Identity.GetUserId(), loginInfo.Login);
            return result.Succeeded ? RedirectToAction("ManageLogins") : RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
        }

        //
        // GET: /Manage/ChangePassword
        public ActionResult ChangePassword()
        {
            return View("ChangePassword");
        }

        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            DateTime startTime = DateTime.Now;
            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    System.Net.Mail.MailMessage m = new System.Net.Mail.MailMessage(
                    new System.Net.Mail.MailAddress("sender@mydomain.com", "Calling chilindo"),
                        new System.Net.Mail.MailAddress(user.Email));
                    m.Subject = "Change Password";
                    string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                    m.Body = string.Format("<!DOCTYPE html><html class=\" js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths\"><!--<![endif]--><head>    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></head><body>    <table style=\"background-color:#eeeeee\" bgcolor=\"#eeeeee\" border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\"> <tbody><tr> <td rowspan=\"1\" colspan=\"1\" align=\"center\"> <div style=\"max-width:638px;margin-left:auto;margin-right:auto\" align=\"center\"> <table border=\"0\" width=\"1\" cellspacing=\"0\" cellpadding=\"0\"><tbody><tr><td style=\"line-height:1px\" rowspan=\"1\" colspan=\"1\"><img height=\"1\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"300\" style=\"display:block\" alt=\"\" src=\"https://ci6.googleusercontent.com/proxy/8vE4QuA1HJzQnYHb5UPj8W4doMObdMIRqC9Pd-Ej8pGM9-fYoCOCxU7PFMmmEObH-ENxH7d0Mfs_DAWvDlxJ9nG4eEUfw2LkQSGpqW8XqN3nII_1DPco=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/S.gif\" class=\"CToWUd\"></td></tr></tbody></table> <table style=\"margin-left:auto;margin-right:auto\" border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\"> <tbody><tr> <td valign=\"bottom\" rowspan=\"1\" colspan=\"1\"><img height=\"5\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" alt=\"\" src=\"https://ci6.googleusercontent.com/proxy/8vE4QuA1HJzQnYHb5UPj8W4doMObdMIRqC9Pd-Ej8pGM9-fYoCOCxU7PFMmmEObH-ENxH7d0Mfs_DAWvDlxJ9nG4eEUfw2LkQSGpqW8XqN3nII_1DPco=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/S.gif\" class=\"CToWUd\"></td> <td style=\"width:638px\" valign=\"top\" rowspan=\"1\" colspan=\"1\" align=\"center\"> <table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\"> <tbody><tr> <td style=\"padding:18px 0px 0px 0px\" valign=\"top\" width=\"100%\" rowspan=\"1\" colspan=\"1\" align=\"center\">   </td> </tr> </tbody></table> <table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\"> <tbody><tr> <td style=\"background-color:#ededed\" valign=\"top\" rowspan=\"1\" colspan=\"1\"><img height=\"16\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci6.googleusercontent.com/proxy/M-YlBDR647U55vm2GyOou-YlZ8G_3Pnkh5mmGegY4DoP1up-pYejj6TGKHE9KNYWGJpjGW4_W2ixhLoWhQ_bn_O0YXRbJzoG_3M2OrWyviWfnKKTcRLMRy63LeMWhY3i5HyrxTzoLk9FCgAioZyu=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowTL1.png\" class=\"CToWUd\"></td> <td style=\"background-color:#ebebeb\" valign=\"top\" rowspan=\"1\" colspan=\"1\"><img height=\"16\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci3.googleusercontent.com/proxy/nb5iMmbbMYatsdbQovCdfgA0fW1DjzjVTbnTFuZZyJHZ6pGkZj12BUjiy7pprDRdLmMsA5RIPiKaMuxlcZG7fNcCp28D4uhpPcredHji4RmTK0pzBi1pvtUm2PgyGQiosWi9yY3ofMu4d4Mgl9uf=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowTL2.png\" class=\"CToWUd\"></td> <td style=\"background-color:#e8e8e8\" valign=\"top\" rowspan=\"1\" colspan=\"1\"><img height=\"16\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci6.googleusercontent.com/proxy/quGOhMp-J8cvrrr9pHVyvzQKzcx3auHtfff2jsc6IQ9ibVpUwpLzOs-EK9ga6jup6tX_m94fK8pFVIMzH1XIc5Ufc_LXpZw6ef-tfGsNwYVoatVDn-j-wk02nQj4lx32TVxEeR1uvUZlQe0eS_6e=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowTL3.png\" class=\"CToWUd\"></td> <td style=\"background-color:#e5e5e5\" valign=\"top\" rowspan=\"1\" colspan=\"1\"><img height=\"16\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci6.googleusercontent.com/proxy/5D3X596A-iUAbVdeEKhXHN9kUiYtVMMG99l0z0uPhm7RrMhaYLtoOdxOh2ZZfAP34g104EipvRgRvaEUnzJMaiJooO2elUKOnim9Wu-YXKESUYNAyatpzoLkKgxTAffuHBQ8Is8ur_RvH-tId8Wl=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowTL4.png\" class=\"CToWUd\"></td> <td style=\"background-color:#e0e0e0\" valign=\"top\" rowspan=\"1\" colspan=\"1\"><img height=\"16\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci3.googleusercontent.com/proxy/UA62jDV4uC6zWGBOMdD1vqBf6-hvQqjWh3rE7YGVPYOn55-gOOaa-pudhU-fPZb99zxkuiUky-O1pKG9t7Ngr0QTHX71NLEpEi7bOgEQ4WDx-pgA4OeQJwKqDABo5YZcH_kFzDw6ihAIzm-s597R=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowTL5.png\" class=\"CToWUd\"></td> <td style=\"background-color:#dadada\" valign=\"top\" rowspan=\"1\" colspan=\"1\"><img height=\"16\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci4.googleusercontent.com/proxy/JYDvoX9d2elvgRi4xMFiRJPv3AiECk9HrXCLEgjcdLEO201THtQbb0jQWOjI0eW-dQ-j2HrYLLQuda6RxY6966NhUgMVoLDJEVSuyio7HO0LJ3K_gFRfyj_S7JdmYxrxEpepPIr9Emm-RhzMHDxH=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowTL6.png\" class=\"CToWUd\"></td> <td style=\"background-color:#d4d4d4\" valign=\"top\" rowspan=\"1\" colspan=\"1\"><img height=\"16\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci3.googleusercontent.com/proxy/-gWctf4HNcQz7EDkjrMyyLVatcY6CoKD1BDmwiQ5adDN-zsr3xK3WUmsTNsuMjOBgCDk73fs4MkXi0XcSeAUh8a2NdmW4w7SLuB7wQkTrg6cCstWkVtPVTzI_Zxy19NPHaYvNcnywaK_DV36E9o6=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowTL7.png\" class=\"CToWUd\"></td> <td style=\"background-color:#cccccc\" valign=\"top\" rowspan=\"1\" colspan=\"1\"><img height=\"16\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci3.googleusercontent.com/proxy/yDLzRN5O4qm34nyaW4vc46-iVVOiXSObyH1ilFtEaVmsxsJJ8Kc-zwfHsHu1mHDXSPTKng9HpKAwtzGr-Kv_hoSjtDcF6jLF0wOQ3bd0jGG4Ts_zX4eC6wMt3MFJBaPiBN0MItzrZBpJpJZXrg7a=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowTL8.png\" class=\"CToWUd\"></td> <td style=\"background-color:#c3c3c3\" valign=\"top\" rowspan=\"1\" colspan=\"1\"><img height=\"16\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci5.googleusercontent.com/proxy/3h2pzmXKQix61nmj55nlyUVR0kbTR4qK53SMsffQEmDizsD_IPXmnKrtEUya_TQ1yk0Rvs1ul7c-XwJrMz2z1k_BU112ndLod5u8oPYlgToetWGasKdkX9zXW17BPvnjlVpZsil1PQ5_WBoaKsM8=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowTL9.png\" class=\"CToWUd\"></td> <td valign=\"top\" width=\"100%\" rowspan=\"1\" colspan=\"1\" align=\"center\"> <table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\"> <tbody><tr> <td style=\"background-color:#ffffff;padding:0px 0px 0px 0px\" bgcolor=\"#ffffff\" valign=\"top\" width=\"100%\" rowspan=\"1\" colspan=\"1\" align=\"center\">  <table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\"><tbody><tr><td style=\"color:#383838;font-family: 'Open Sans', sans-serif;;font-size:11pt;padding:8px 22px 9px 10px\" valign=\"top\" rowspan=\"1\" colspan=\"1\" align=\"center\"><div><br><div style=\"font-size:8pt\"><img height=\"7\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"170\" src=\"https://ci5.googleusercontent.com/proxy/Td8BAhcDRJIVsuFtFaHprCk7-ChGC0UPRrBMEf3JADD7xEmbNUSSRzEJSR-TsY_jQ1lYA0Gj2pOmc33AIUS6H9vhvqtrsG9Qk_dg6z34ce_lptP8H-8Opo_3nQ=s0-d-e1-ft#https://imgssl.constantcontact.com/letters/images/1101116784221/T.png\" class=\"CToWUd\"><br></div><div>&nbsp;</div></div></td></tr></tbody></table> <a name=\"14b2bc7eb73c3f3a_LETTER.BLOCK5\"></a><table style=\"background-color:#37405c\" bgcolor=\"#37405C\" border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\"><tbody><tr><td style=\"color:#000000;background-color: #0099ff\" valign=\"top\" width=\"20\" rowspan=\"1\" colspan=\"1\" align=\"center\"><div style=\"text-align:center\" align=\"center\"><span>&nbsp;</span></div></td><td style=\"background-color:#0099ff;padding:4px 12px 3px 12px;font-size:22pt;font-family:Verdana,Geneva,Arial,Helvetica,sans-serif;color:rgb(255,255,255)\" bgcolor=\"#37405C\" valign=\"top\" rowspan=\"1\" colspan=\"1\" align=\"left\"><div style=\"text-align:center;margin-left:30px\" align=\"center\">Calling Chilindo &nbsp; &nbsp;</div></td></tr></tbody></table> <a name=\"14b2bc7eb73c3f3a_LETTER.BLOCK6\"></a><table style=\"display:table;background-color:#ffffff\" bgcolor=\"#FFFFFF\" border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\"><tbody><tr><td style=\"padding:6px 32px 5px 32px;font-size:10pt;font-family:Verdana,Geneva,Arial,Helvetica,sans-serif;color:rgb(55,64,92)\" valign=\"top\" rowspan=\"1\" colspan=\"1\" align=\"left\"><div style=\"text-align:left\" align=\"left\">Dear {0} {1}!</div></td></tr></tbody></table> </td> </tr> </tbody></table> </td> <td style=\"background-color:#c3c3c3\" valign=\"top\" rowspan=\"1\" colspan=\"1\"><img height=\"16\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci4.googleusercontent.com/proxy/uW7-J65GcBcFA8T1kfBWS6sevhcaCRkdpMrVmRQOujXtFBy97P2skhdZUWhHMQqZu5GdPIauwsVAb9--eWT0_J6jbINE45xfgSJ_dxaK7omQeKnyoOeNeTNWsCj2PVoixMuLFlON3_QdvAl5X8Q3=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowTR1.png\" class=\"CToWUd\"></td> <td style=\"background-color:#cccccc\" valign=\"top\" rowspan=\"1\" colspan=\"1\"><img height=\"16\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci3.googleusercontent.com/proxy/3yf49qWBaV7remLCiaoJq7GMac8hn2zLdWLAiQmSepbx0l-pBxMI_5tPyIyFF576NYe_MGj2QkTWbBpzquCFvF35QRJvBPDyrzAkU4UFeLdWLmkZjxztlHhNsr3jbrkTVWMoGkV9lvMtkp28n-Yr=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowTR2.png\" class=\"CToWUd\"></td> <td style=\"background-color:#d4d4d4\" valign=\"top\" rowspan=\"1\" colspan=\"1\"><img height=\"16\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci4.googleusercontent.com/proxy/q9syfH1wCa1_k7wwq8tn1Zlba0bX2cOacs18aU8o7I9jvYRueUZwi8y9y4MquXoSBGVSHv2f4YCMSjHPG3d-GMFdYTtptC49VfIdbkRh2rAZvGpA48gULcZ_oRFFTPOa3m5IqXce6GMUe21XnVxU=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowTR3.png\" class=\"CToWUd\"></td> <td style=\"background-color:#dadada\" valign=\"top\" rowspan=\"1\" colspan=\"1\"><img height=\"16\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci6.googleusercontent.com/proxy/mxXSfJq1GXZVAKCd8opt3tKt-E1JFXzIvAIpYbl7Uoxsfwn_CT38wceO73l_bpNEbyerlM8lVJkGf6LamPmgLgP0Xl5Y3n-3PrC05vkj07G-9g_2RBBG1gReUZ-TCNNkPoOz1-CWCaS0iSUtjzmJ=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowTR4.png\" class=\"CToWUd\"></td> <td style=\"background-color:#e0e0e0\" valign=\"top\" rowspan=\"1\" colspan=\"1\"><img height=\"16\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci4.googleusercontent.com/proxy/8cVOzuge5NWM1BceN0ls0KIIUYg52GYRSJvRdb5zNuoWgVRQccZI6Hikm_iUxYi_1yvgixYICzxrMGkhJKkn0MgAkeeevvd0_QcXk8U__o6KVcH8sqMz5zeImuVtu3UrlbGAmKmL5y22XISOJEw-=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowTR5.png\" class=\"CToWUd\"></td> <td style=\"background-color:#e5e5e5\" valign=\"top\" rowspan=\"1\" colspan=\"1\"><img height=\"16\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci4.googleusercontent.com/proxy/gjZXh8JkFGF6KkHPEulXSD5Jt63Tr1-6C9i6SMXbR42ojf94rTcLxi_p-YvMQ-ZI1GQmLJAHm5GX53CS82jErf5EQf1zxDeHW80qR4LBZdaSteP7_7k4HFWrMVpk5iKNpw4ON4T-Ld7I3WpwYIqv=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowTR6.png\" class=\"CToWUd\"></td> <td style=\"background-color:#e8e8e8\" valign=\"top\" rowspan=\"1\" colspan=\"1\"><img height=\"16\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci6.googleusercontent.com/proxy/iMbQ3cBJSTDQjNEalFaoqQ0E8GIWYXbX0fYDVhJg4xAgn91-ZSJaRTpkBS9bEnEwdvfZxpTf-Q1e3-pohwU3dvsVhKcRko-I07kHAvc34gYHTgn9jcW5p5paTgOgQ15fQ9s8xvaujqTDou-kkaeU=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowTR7.png\" class=\"CToWUd\"></td> <td style=\"background-color:#ebebeb\" valign=\"top\" rowspan=\"1\" colspan=\"1\"><img height=\"16\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci5.googleusercontent.com/proxy/dGbFnmrAx3F6hosXUXyyb2vQDH_2YbkGfK3h12TYUE2l40GBpf6OeH4v6E39RpmH2ZjYc5YN98OQ4jZaUnA5T24M8f5BlHNAdirmUMKCyRQWkxsSglb3S_xBSazAc1qlhnRc6WXTLc8zWWgpujao=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowTR8.png\" class=\"CToWUd\"></td> <td style=\"background-color:#ededed\" valign=\"top\" rowspan=\"1\" colspan=\"1\"><img height=\"16\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci5.googleusercontent.com/proxy/Xpin5DnkJkjkLvZviW2H2YbKAneHSDKc7ODgcZZMMnFzyi_HNr-y4LLExV9sXV3Bveza1Z2fcHERydPnaqhPV_YROYOdMrIhSWWN6un1JNpBrnR9WgfiLrbaFEZE8seGSmDRtQfbe72tWPRJa2KM=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowTR9.png\" class=\"CToWUd\"></td> </tr> <tr> <td style=\"background-color:#ededed\" valign=\"bottom\" rowspan=\"1\" colspan=\"1\"><img height=\"18\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci3.googleusercontent.com/proxy/5y5Fk6Uuv5OMFM9_V0dE2pJQZEzCCZvnwgBGIqCppF54mipVk2Kgh9NJeHh4SQXGFgxM26zE-yZMIK4EXp54gsFwiJiGYm0AY9MU5Upobp9B4j0K2KI_LoqNUwizvflwkgCahHqb5habqJFaGUGx=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowBL1.png\" class=\"CToWUd\"></td> <td style=\"background-color:#ebebeb\" valign=\"bottom\" rowspan=\"1\" colspan=\"1\"><img height=\"18\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci5.googleusercontent.com/proxy/WlCmXO2v4ci3A_SP0AT-N2wRVJfAyQNaTTnpJkjtG6XMYb9bTLg8JopIKLIl2ImfT904cDUfmC_lubb7gq1A15wfmOezrOmOeAly7t3KJBsiSzPKC3lTd4UGz8EYHH7SU1B0snKqE2SzTB5g4SoP=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowBL2.png\" class=\"CToWUd\"></td> <td style=\"background-color:#e8e8e8\" valign=\"bottom\" rowspan=\"1\" colspan=\"1\"><img height=\"18\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci3.googleusercontent.com/proxy/nNKxZcJOZJ3aQhfltHuwpsVqKc2W8jSNR6kgmWnk_JBhIRpt-o5CGauZTKpO4dv3tVs1tRK1PBTvuAjwB3RJGLa5Kyl3CXZWePZhj7XAxZ6aa1jnOv_0rW7zBdkRToQ98aty0Kt3Lkq8Mcbu6h9t=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowBL3.png\" class=\"CToWUd\"></td> <td style=\"background-color:#e5e5e5\" valign=\"bottom\" rowspan=\"1\" colspan=\"1\"><img height=\"18\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci4.googleusercontent.com/proxy/wQ3ydF2iJFge5fzIw34a1QiDM0ZCb6rDUbMIkHzdc7q6LS0KGloDxHn1-opG2X0rsH2h7x7UF8k1-4g0AwH5oeQfYVVQ3GLw_ZDFoZ8hwPxJeAX-iTb86pytOqscIYMzrRD2QpvqLKCqlp0svxN9=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowBL4.png\" class=\"CToWUd\"></td> <td style=\"background-color:#e0e0e0\" valign=\"bottom\" rowspan=\"1\" colspan=\"1\"><img height=\"18\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci3.googleusercontent.com/proxy/eGV8YG5hCYekdMbqEHinBAkJiynAq6IuyfjtSODJQbLozbl9jN0sMMDSCJhpSOcZXtbW6PXifJwx99WuagyQ5UWDa6pRChHfBXEvXtn9KZFvrVk55m8t_lA7ijCxa_jlijytUjbO7_bB5cZ8wqHD=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowBL5.png\" class=\"CToWUd\"></td> <td style=\"background-color:#dadada\" valign=\"bottom\" rowspan=\"1\" colspan=\"1\"><img height=\"18\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci6.googleusercontent.com/proxy/yatoZuedc6v7UqMZpdYkcAkXIGM2DZ1OK0GeS1OFKpJAgC_LNcGuxUub0-Ya8IQObh3fwPr8n_ioghLjczFEr_jmMFoP-fBBFokw6-VRS76kptVYrvTwc9eGeNTgoI3BBKrM_zGXvtDmGbaedkpT=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowBL6.png\" class=\"CToWUd\"></td> <td style=\"background-color:#d4d4d4\" valign=\"bottom\" rowspan=\"1\" colspan=\"1\"><img height=\"18\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci5.googleusercontent.com/proxy/qGc7-Na3qJTHd39bcz6oV6aWstx-QdSh7dVCt7wwTaf3vxBx_bLdUCzdyf6o2DP5qcu2cggsjzYLWX7A4UaSjHCHyqdhm1zNwx5Yl5LqZB-VcqVjqJyFHJ9kSM1w-8M7yH50QPWUPpR8OLuuoFFZ=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowBL7.png\" class=\"CToWUd\"></td> <td style=\"background-color:#cccccc\" valign=\"bottom\" rowspan=\"1\" colspan=\"1\"><img height=\"18\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci4.googleusercontent.com/proxy/1q0uRnzgkEvtL6mIkR9x7pE_WdwZvn-a_qNM3gNj2WNjMtvmH1LkJzYws2zMjEO4UuwjpUdnFNDo0YGfe1MK01_yPqEDe_ZuUcK92NaqaNpPMX8OX7J6iw8FTSez5oF0QPQaKGTTOFcnJGUO2epc=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowBL8.png\" class=\"CToWUd\"></td> <td style=\"background-color:#c3c3c3\" valign=\"bottom\" rowspan=\"1\" colspan=\"1\"><img height=\"18\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci4.googleusercontent.com/proxy/vbbKbIxvCOHg9Fx0edfFGptOIkcE42UXGSYNNeka6WaUhj-mTAe2PqS9cOPbiCssatNgQcmZqLvp52aI03zbq0d44YersZLxFyfG9d1VPgLK8qwMmj7h-hlgxeECbUb4bDxafxHnFwhdQC4O3gk7=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowBL9.png\" class=\"CToWUd\"></td> <td valign=\"top\" width=\"100%\" rowspan=\"1\" colspan=\"1\" align=\"center\"> <table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\"> <tbody><tr> <td style=\"background-color:#ffffff;padding:0px 12px 0px 12px\" bgcolor=\"#ffffff\" valign=\"top\" width=\"100%\" rowspan=\"1\" colspan=\"1\" align=\"center\"> <table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\"> <tbody><tr> <td style=\"padding-bottom:14px;height:1px;line-height:1px\" height=\"1\" valign=\"top\" rowspan=\"1\" colspan=\"1\" align=\"center\"><img height=\"1\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"5\" style=\"display:block\" alt=\"\" src=\"https://ci6.googleusercontent.com/proxy/8vE4QuA1HJzQnYHb5UPj8W4doMObdMIRqC9Pd-Ej8pGM9-fYoCOCxU7PFMmmEObH-ENxH7d0Mfs_DAWvDlxJ9nG4eEUfw2LkQSGpqW8XqN3nII_1DPco=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/S.gif\" class=\"CToWUd\"></td> </tr> </tbody></table>          <table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\"><tbody><tr><td style=\"color:#383838;font-family: 'Open Sans', sans-serif;;font-size:11pt;padding:8px 22px 9px 10px\" valign=\"top\" rowspan=\"1\" colspan=\"1\" align=\"center\"><div><div style=\"font-size:8pt;margin-left:30px\"><img height=\"7\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"170\" src=\"https://ci5.googleusercontent.com/proxy/Td8BAhcDRJIVsuFtFaHprCk7-ChGC0UPRrBMEf3JADD7xEmbNUSSRzEJSR-TsY_jQ1lYA0Gj2pOmc33AIUS6H9vhvqtrsG9Qk_dg6z34ce_lptP8H-8Opo_3nQ=s0-d-e1-ft#https://imgssl.constantcontact.com/letters/images/1101116784221/T.png\" class=\"CToWUd\"><br></div><div>You recently changed your password for your Calling Chilindo account. This request was made on {2}.</div></div></td></tr></tbody></table><table style=\"display:table\" border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\"><tbody><tr><td style=\"color:#383838;font-family: 'Open Sans', sans-serif;;font-size:11pt;padding:8px 20px 9px 20px\" valign=\"top\" rowspan=\"1\" colspan=\"1\" align=\"left\"><span><br><br></span></td></tr></tbody></table>     <table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\"><tbody><tr><td style=\"color:#383838;font-family: 'Open Sans', sans-serif;;font-size:11pt;padding:8px 20px 9px 20px\" valign=\"top\" rowspan=\"1\" colspan=\"1\" align=\"left\"><div>If you have additional questions, please contact us for further assistance.<br> <br>Thank you for using Calling chilindo app!</div><div><br> Sincerely,<br><div><span><br></span></div>Team<br> Calling chilindo<br></div></td></tr></tbody></table></td> </tr> <tr> <td style=\"background-color:#ffffff;padding:0px 12px 0px 12px\" bgcolor=\"#ffffff\" valign=\"top\" width=\"100%\" rowspan=\"1\" colspan=\"1\" align=\"center\">  </td> </tr> </tbody></table> <table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\"> <tbody><tr> <td style=\"line-height:1px\" valign=\"top\" rowspan=\"1\" colspan=\"1\"><img height=\"13\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"9\" style=\"display:block\" src=\"https://ci6.googleusercontent.com/proxy/Z-NcRGwWNRDUFm0T2P9DxyOLxdzoQL4Q1qrnRX4RTkMT-7FgJx0GeFXgDnYe2UxXdgZsRfJPyEGhKXa-4WFS1vHS5TsqbvR--IzlQ2bmTBBPOAMsJPzS8vQULRAnWzWB0o-ztn0IUPP32LP_4h3B3zzz9Ug=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowBottomBL.png\" align=\"top\" class=\"CToWUd\"></td> <td style=\"line-height:1px\" valign=\"top\" width=\"100%\" rowspan=\"1\" colspan=\"1\" align=\"center\"> <table style=\"background-color:#ededed\" border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\"> <tbody><tr><td style=\"background-color:#a3a3a3;line-height:1px\" height=\"1\" rowspan=\"1\" colspan=\"1\"><img height=\"1\" width=\"275\" src=\"https://ci6.googleusercontent.com/proxy/egrJRAi6wfSCJbxmGibJNxOqdBs0TU57Cf5u2FlUNZiXDNjY7t0dt4YfapMC9XaGIVWZGzZd09Z4wEvi5B2dxI8vVN0VGhJwbYVProbx81sS6DnFqgEV=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/H.gif\" align=\"left\" class=\"CToWUd\"><img height=\"1\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"275\" style=\"display:block\" src=\"https://ci6.googleusercontent.com/proxy/egrJRAi6wfSCJbxmGibJNxOqdBs0TU57Cf5u2FlUNZiXDNjY7t0dt4YfapMC9XaGIVWZGzZd09Z4wEvi5B2dxI8vVN0VGhJwbYVProbx81sS6DnFqgEV=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/H.gif\" class=\"CToWUd\"></td></tr> <tr><td style=\"background-color:#a9a9a9;line-height:1px\" height=\"1\" rowspan=\"1\" colspan=\"1\"><img height=\"1\" hspace=\"5\" src=\"https://ci4.googleusercontent.com/proxy/TkgvWT-7Gf7y7pOn9HoxGGmp8TVhMsRcHbb_7yQohdgZ8yqd5afDVBr8wVmwGTDMk4nCr_5b0ZXdU8LwF0urAzbfrkUsn21EFLV3y87cdbIqq8HhpZsZ=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/R.gif\" align=\"left\" class=\"CToWUd\"></td></tr> <tr><td style=\"background-color:#b1b1b1;line-height:1px\" height=\"1\" rowspan=\"1\" colspan=\"1\"><img height=\"1\" hspace=\"5\" src=\"https://ci4.googleusercontent.com/proxy/TkgvWT-7Gf7y7pOn9HoxGGmp8TVhMsRcHbb_7yQohdgZ8yqd5afDVBr8wVmwGTDMk4nCr_5b0ZXdU8LwF0urAzbfrkUsn21EFLV3y87cdbIqq8HhpZsZ=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/R.gif\" align=\"left\" class=\"CToWUd\"></td></tr> <tr><td style=\"background-color:#bababa;line-height:1px\" height=\"1\" rowspan=\"1\" colspan=\"1\"><img height=\"1\" hspace=\"5\" src=\"https://ci4.googleusercontent.com/proxy/TkgvWT-7Gf7y7pOn9HoxGGmp8TVhMsRcHbb_7yQohdgZ8yqd5afDVBr8wVmwGTDMk4nCr_5b0ZXdU8LwF0urAzbfrkUsn21EFLV3y87cdbIqq8HhpZsZ=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/R.gif\" align=\"left\" class=\"CToWUd\"></td></tr> <tr><td style=\"background-color:#c3c3c3;line-height:1px\" height=\"1\" rowspan=\"1\" colspan=\"1\"><img height=\"1\" hspace=\"5\" src=\"https://ci4.googleusercontent.com/proxy/TkgvWT-7Gf7y7pOn9HoxGGmp8TVhMsRcHbb_7yQohdgZ8yqd5afDVBr8wVmwGTDMk4nCr_5b0ZXdU8LwF0urAzbfrkUsn21EFLV3y87cdbIqq8HhpZsZ=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/R.gif\" align=\"left\" class=\"CToWUd\"></td></tr> <tr><td style=\"background-color:#cbcbcb;line-height:1px\" height=\"1\" rowspan=\"1\" colspan=\"1\"><img height=\"1\" hspace=\"5\" src=\"https://ci4.googleusercontent.com/proxy/TkgvWT-7Gf7y7pOn9HoxGGmp8TVhMsRcHbb_7yQohdgZ8yqd5afDVBr8wVmwGTDMk4nCr_5b0ZXdU8LwF0urAzbfrkUsn21EFLV3y87cdbIqq8HhpZsZ=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/R.gif\" align=\"left\" class=\"CToWUd\"></td></tr> <tr><td style=\"background-color:#d4d4d4;line-height:1px\" height=\"1\" rowspan=\"1\" colspan=\"1\"><img height=\"1\" hspace=\"5\" src=\"https://ci4.googleusercontent.com/proxy/TkgvWT-7Gf7y7pOn9HoxGGmp8TVhMsRcHbb_7yQohdgZ8yqd5afDVBr8wVmwGTDMk4nCr_5b0ZXdU8LwF0urAzbfrkUsn21EFLV3y87cdbIqq8HhpZsZ=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/R.gif\" align=\"left\" class=\"CToWUd\"></td></tr> <tr><td style=\"background-color:#d9d9d9;line-height:1px\" height=\"1\" rowspan=\"1\" colspan=\"1\"><img height=\"1\" hspace=\"5\" src=\"https://ci4.googleusercontent.com/proxy/TkgvWT-7Gf7y7pOn9HoxGGmp8TVhMsRcHbb_7yQohdgZ8yqd5afDVBr8wVmwGTDMk4nCr_5b0ZXdU8LwF0urAzbfrkUsn21EFLV3y87cdbIqq8HhpZsZ=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/R.gif\" align=\"left\" class=\"CToWUd\"></td></tr> <tr><td style=\"background-color:#e0e0e0;line-height:1px\" height=\"1\" rowspan=\"1\" colspan=\"1\"><img height=\"1\" hspace=\"5\" src=\"https://ci4.googleusercontent.com/proxy/TkgvWT-7Gf7y7pOn9HoxGGmp8TVhMsRcHbb_7yQohdgZ8yqd5afDVBr8wVmwGTDMk4nCr_5b0ZXdU8LwF0urAzbfrkUsn21EFLV3y87cdbIqq8HhpZsZ=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/R.gif\" align=\"left\" class=\"CToWUd\"></td></tr> <tr><td style=\"background-color:#e5e5e5;line-height:1px\" height=\"1\" rowspan=\"1\" colspan=\"1\"><img height=\"1\" hspace=\"5\" src=\"https://ci4.googleusercontent.com/proxy/TkgvWT-7Gf7y7pOn9HoxGGmp8TVhMsRcHbb_7yQohdgZ8yqd5afDVBr8wVmwGTDMk4nCr_5b0ZXdU8LwF0urAzbfrkUsn21EFLV3y87cdbIqq8HhpZsZ=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/R.gif\" align=\"left\" class=\"CToWUd\"></td></tr> <tr><td style=\"background-color:#e8e8e8;line-height:1px\" height=\"1\" rowspan=\"1\" colspan=\"1\"><img height=\"1\" hspace=\"5\" src=\"https://ci4.googleusercontent.com/proxy/TkgvWT-7Gf7y7pOn9HoxGGmp8TVhMsRcHbb_7yQohdgZ8yqd5afDVBr8wVmwGTDMk4nCr_5b0ZXdU8LwF0urAzbfrkUsn21EFLV3y87cdbIqq8HhpZsZ=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/R.gif\" align=\"left\" class=\"CToWUd\"></td></tr> <tr><td style=\"background-color:#ebebeb;line-height:1px\" height=\"1\" rowspan=\"1\" colspan=\"1\"><img height=\"1\" hspace=\"5\" src=\"https://ci4.googleusercontent.com/proxy/TkgvWT-7Gf7y7pOn9HoxGGmp8TVhMsRcHbb_7yQohdgZ8yqd5afDVBr8wVmwGTDMk4nCr_5b0ZXdU8LwF0urAzbfrkUsn21EFLV3y87cdbIqq8HhpZsZ=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/R.gif\" align=\"left\" class=\"CToWUd\"></td></tr> <tr><td style=\"background-color:#ededed;line-height:1px\" height=\"1\" rowspan=\"1\" colspan=\"1\"><img height=\"1\" hspace=\"5\" src=\"https://ci4.googleusercontent.com/proxy/TkgvWT-7Gf7y7pOn9HoxGGmp8TVhMsRcHbb_7yQohdgZ8yqd5afDVBr8wVmwGTDMk4nCr_5b0ZXdU8LwF0urAzbfrkUsn21EFLV3y87cdbIqq8HhpZsZ=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/R.gif\" align=\"left\" class=\"CToWUd\"></td></tr> </tbody></table> </td> <td style=\"line-height:1px\" valign=\"top\" rowspan=\"1\" colspan=\"1\"><img height=\"13\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"9\" style=\"display:block\" src=\"https://ci5.googleusercontent.com/proxy/5MER8iSIXCXAwP9Czh34JCNt9qPrG7hhjl86kd4xiQhBHJj0l41p6cvrfgjgjBjcQj9j-CZzdLNT2fjQ8zC0ueZGKbepXQClXPEjfG0mgkxzdThhJKXfrr-2QcDddppQewxiP-iBodF0__gJsj8XHXQ8ICc=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowBottomBR.png\" align=\"top\" class=\"CToWUd\"></td> </tr> </tbody></table> </td> <td style=\"background-color:#c3c3c3\" valign=\"bottom\" rowspan=\"1\" colspan=\"1\"><img height=\"18\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci5.googleusercontent.com/proxy/Mlc8tOIoxNXUZVlZhiTpEO9uHeM6mXrI9MLPGPj7ZxVqYte1sILRGZUYvS5ZPnfyg6S5jFHAbKTvl-OJatWQetry7w-2_0LcrVXaDBa1nCaZBw50Fe0FbklajmOw7eXBB0hUUpj1Ka0LRFOTzZms=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowBR1.png\" class=\"CToWUd\"></td> <td style=\"background-color:#cccccc\" valign=\"bottom\" rowspan=\"1\" colspan=\"1\"><img height=\"18\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci3.googleusercontent.com/proxy/kWbEO5ZLroyQmnw8M-o2sLGBeQ8mEtIarywJBWDS_OqLYnfIGvRIjrrt8KuZxkRjXVflmA-U83MxTOYJ2SL5ahjqxSnvt5PTKD_Xl0REmWOkTV_NSbfAFitczx0edGXfSE2nhhdtzxrjtBs13NlI=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowBR2.png\" class=\"CToWUd\"></td> <td style=\"background-color:#d4d4d4\" valign=\"bottom\" rowspan=\"1\" colspan=\"1\"><img height=\"18\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci3.googleusercontent.com/proxy/E8P7cHrFXps0D8QPdZ_OHZ3xD_vrkE6_9u-CndsSfbmizBk4RwnK47zs8P1Wgw3S7VAimXmo7f5bMrgW72HW4cM0-pYeWriHzSQbFDaqNla2OCmUnTZbe_dPiyHADQ9vXEfhh9oMGEMlzoX9XfQR=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowBR3.png\" class=\"CToWUd\"></td> <td style=\"background-color:#dadada\" valign=\"bottom\" rowspan=\"1\" colspan=\"1\"><img height=\"18\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci6.googleusercontent.com/proxy/Cw2KyTx5Yf3KlRC8t1CbJtGgA1_jn-vu6RFn_YGtkRWd_AAnMyo0qyx9EuQMhP0JD7ZonQlq4negA_iBCpLIIwtV8M27wI09DFa4vY1LYr44RgkWdjLm7bCVL5kfbZqTUCU9hPf03jLvBGCa3NJI=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowBR4.png\" class=\"CToWUd\"></td> <td style=\"background-color:#e0e0e0\" valign=\"bottom\" rowspan=\"1\" colspan=\"1\"><img height=\"18\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci6.googleusercontent.com/proxy/knmL8eOEfBtjRhsCjvFrzH7wW5P2l-pV7JRRzr8OXNAEHRvCUZPoyECaYkGq9pHiOdSUfTMyRLb5lAxYTf3llxVY6Iv7Ha3sbHcyCPFC44s1jEtPT4mhYW8ThCYVDlkcAyO09-ttzOcBsVlxzipw=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowBR5.png\" class=\"CToWUd\"></td> <td style=\"background-color:#e5e5e5\" valign=\"bottom\" rowspan=\"1\" colspan=\"1\"><img height=\"18\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci5.googleusercontent.com/proxy/MbempgQBwwu6pEJ6Ma94LBHvhygQurPIhgn-EkQFK-JvzeHtOk0G_Wemdwc044S2JM4FWty6TqQRSm9DTfjiJePRgMMS1iqSuOjaAqGj1qF8hudcUkFwnAw-6hGyOEqQ2vUhweZRw6ZeiQEogihZ=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowBR6.png\" class=\"CToWUd\"></td> <td style=\"background-color:#e8e8e8\" valign=\"bottom\" rowspan=\"1\" colspan=\"1\"><img height=\"18\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci5.googleusercontent.com/proxy/4BGNl-BqhSBl03IohI_ulCqoBsQJtxTaxgUlTmHq4-q32IAl-ajZzICg1sb3xt3n4M1DZYpfM2nYxaGkUUSp7WHgkKbx1a42aoMzVTO3YAv5cwqaNfAT7ckCy0O7n9C-L3fe7_XrfcNZgYi85VV8=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowBR7.png\" class=\"CToWUd\"></td> <td style=\"background-color:#ebebeb\" valign=\"bottom\" rowspan=\"1\" colspan=\"1\"><img height=\"18\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci4.googleusercontent.com/proxy/yxRpARtL6j3MCqMc_pSQlqD5dG7rKPMvTPudrE8iC_yFixJ8mGgr50_wDbLuGZyIajxAUk1xz5CQLIjCnjw9zQf5TKpyQ8AuSCVj7KePvHfQkwN8iYvioweK6Mq15qwD_9JGQ3wia9ofZSjVwdOc=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowBR8.png\" class=\"CToWUd\"></td> <td style=\"background-color:#ededed\" valign=\"bottom\" rowspan=\"1\" colspan=\"1\"><img height=\"18\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" style=\"display:block\" src=\"https://ci4.googleusercontent.com/proxy/SrjGllEu3d88cQ3nsXe4gACbUC2sCJOnKOkfrp6Omw_B9UDwjalmqUU39Z-zFCB49X6o4b8KBSmO07Gq_QO2fTlDtCqtRAf_2d8O1gziUw4WJZE3U6_H1J8YDPM5f38gJn1E8SR-FoP8tTPyMxuU=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/PT_CONFSUB_OuterShadowBR9.png\" class=\"CToWUd\"></td> </tr> </tbody></table> </td> <td valign=\"bottom\" rowspan=\"1\" colspan=\"1\"><img height=\"5\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" alt=\"\" src=\"https://ci6.googleusercontent.com/proxy/8vE4QuA1HJzQnYHb5UPj8W4doMObdMIRqC9Pd-Ej8pGM9-fYoCOCxU7PFMmmEObH-ENxH7d0Mfs_DAWvDlxJ9nG4eEUfw2LkQSGpqW8XqN3nII_1DPco=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/S.gif\" class=\"CToWUd\"></td> </tr> </tbody></table> </div> </td> </tr> <tr> <td rowspan=\"1\" colspan=\"1\" align=\"center\"> <div style=\"max-width:638px;margin-left:auto;margin-right:auto\" align=\"center\"> <table style=\"margin-left:auto;margin-right:auto\" border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\"> <tbody><tr> <td valign=\"bottom\" rowspan=\"1\" colspan=\"1\"><img height=\"5\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" alt=\"\" src=\"https://ci6.googleusercontent.com/proxy/8vE4QuA1HJzQnYHb5UPj8W4doMObdMIRqC9Pd-Ej8pGM9-fYoCOCxU7PFMmmEObH-ENxH7d0Mfs_DAWvDlxJ9nG4eEUfw2LkQSGpqW8XqN3nII_1DPco=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/S.gif\" class=\"CToWUd\"></td> <td style=\"width:638px\" valign=\"top\" rowspan=\"1\" colspan=\"1\" align=\"center\"> <table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\"> <tbody><tr> <td style=\"padding:0px 21px 18px 21px\" valign=\"top\" width=\"100%\" rowspan=\"1\" colspan=\"1\" align=\"center\"> <table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\"> <tbody><tr> <td style=\"color:#383838;font-family: 'Open Sans', sans-serif;;font-size:11pt;padding:8px 20px 9px 20px\" valign=\"top\" rowspan=\"1\" colspan=\"1\" align=\"center\"> <div> Copyright © 2015. All Rights Reserved.<br> <br> </div> </td> </tr> </tbody></table>  </td> </tr> </tbody></table> </td> <td valign=\"bottom\" rowspan=\"1\" colspan=\"1\"><img height=\"5\" vspace=\"0\" border=\"0\" hspace=\"0\" width=\"1\" alt=\"\" src=\"https://ci6.googleusercontent.com/proxy/8vE4QuA1HJzQnYHb5UPj8W4doMObdMIRqC9Pd-Ej8pGM9-fYoCOCxU7PFMmmEObH-ENxH7d0Mfs_DAWvDlxJ9nG4eEUfw2LkQSGpqW8XqN3nII_1DPco=s0-d-e1-ft#http://img.constantcontact.com/letters/images/1101116784221/S.gif\" class=\"CToWUd\"></td> </tr> </tbody></table> </div> </td> </tr> </tbody></table>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            </body></html>",
                    user.FirstName,
                    user.LastName,
                    DateTime.Now.ToLongDateString());
                    m.IsBodyHtml = true;
                    System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.gmail.com");
                    smtp.Credentials = new System.Net.NetworkCredential("callingchilindo@gmail.com", "Pak_1947");
                    smtp.EnableSsl = true;
                    smtp.Send(m);
                }
                TempData["Message"] = "Your pasword has been successfully changed.";
            }
            AddErrors(result);
            return View("ChangePassword", model);
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing && _userManager != null)
        //    {
        //        _userManager.Dispose();
        //        _userManager = null;
        //    }

        //    base.Dispose(disposing);
        //}

#region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        private bool HasPhoneNumber()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PhoneNumber != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }

#endregion
    }
}