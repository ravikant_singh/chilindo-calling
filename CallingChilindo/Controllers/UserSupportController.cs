﻿using CallingChilindo.Business;
using CallingChilindo.Models;
using CallingChilindo.Models.Class;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CallingChilindo.Controllers
{
    public class UserSupportController : Controller
    {
        // GET: UserSupport
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        public ActionResult Index()
        {
            UserSupportModel userSupportModel = new UserSupportModel();
            UserSupportFacade userSupportFacade = new UserSupportFacade();
            string orderInfo,typeSearch;
            if (Session["OrderInfo"] != null && Session["TypeOfSearch"] !=null && TempData["AccountId"] !=null)
            {
                orderInfo = (string)Session["OrderInfo"];
                typeSearch = (string)Session["TypeOfSearch"];
                var ac = TempData["AccountId"];
                int accountId = Convert.ToInt32(ac);
                userSupportModel.ListUserDetails = userSupportFacade.GetListUserOrders(orderInfo,typeSearch);
                userSupportModel.UserDetail = userSupportModel.ListUserDetails.Where(o => o.id == accountId).FirstOrDefault();
                userSupportModel.ListOrders = userSupportFacade.GetListOrders(accountId);
                userSupportModel.ListUserLogs = userSupportFacade.GetListUserLogs(accountId);
                userSupportModel.ListOrderLogs = new List<OrderLogEntity>();
                userSupportModel.ListItems = userSupportFacade.GetListItemBasket(accountId);
            }

            return View(userSupportModel);
        }
        [HttpGet]
        public ActionResult TestIframe()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SearchOrderDetail()//string orderInfo,string typeSearch
        {
            try
            {
                Session["OrderInfo"] = null;
                Session["TypeOfSearch"] = null;
                string orderInfo , typeSearch;
                orderInfo = Request.Form["txtOrderInfo"];
                typeSearch = Request.Form["TypeOfSearch"];
                UserSupportModel userSupportModel = new UserSupportModel();
                UserSupportFacade userSupportFacade = new UserSupportFacade();
                userSupportModel.ListUserDetails = userSupportFacade.GetListUserOrders(orderInfo, typeSearch);
                userSupportModel.UserDetail = userSupportModel.ListUserDetails.FirstOrDefault();
                userSupportModel.ListOrders = userSupportFacade.GetListOrders(userSupportModel.UserDetail.id);
                userSupportModel.ListUserLogs = userSupportFacade.GetListUserLogs(userSupportModel.UserDetail.id);
                userSupportModel.ListItems = userSupportFacade.GetListItemBasket(userSupportModel.UserDetail.id);
                userSupportModel.ListOrderLogs = new List<OrderLogEntity>();
                Session["OrderInfo"] = orderInfo;
                Session["TypeOfSearch"] = typeSearch;
                return View("Index", userSupportModel);
            }
            catch (Exception ex)
            {
                TempData["Warning"] = "There is no Order Information Found." + ex.Message;
                return RedirectToAction("Index", "UserSupport");
            }
        }
        [HttpPost]
        public ActionResult DisplayUser(string accountId)
        {
            try {
                TempData["AccountId"] = accountId;
                return Json(new
                {
                    Valid = true
                });
            }
            catch {
                return Json(new
                {
                    Valid = false
                });
            }
        }
        [HttpPost]
        public ActionResult UpdateUserInfo(string jsonData)
        {
            try
            {
                var userInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<UserDetailsEntity>(jsonData);
                UserSupportFacade userSupportFacade = new UserSupportFacade();

                var isSuccess = userSupportFacade.UpdateUserInfo(userInfo);

                return Json(new
                {
                    Valid = isSuccess,
                    Message = string.Empty,
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    Valid = false,
                    Message = string.Empty,
                });
            }
        }

        [HttpPost]
        public ActionResult SetBlackList(string accountId, bool isActive)
        {
            try
            {
               // bool isActive = false;
                //if (IsActive == "1")
                //    isActive = true;
                UserSupportFacade userSupportFacade = new UserSupportFacade();

                if (userSupportFacade.UpdateBlackList(Convert.ToInt32(accountId), isActive))
                {
                    return Json(new
                    {
                        Valid = true,
                        Error = string.Empty,
                    });
                }
                else
                {
                    return Json(new
                    {
                        Valid = false
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Valid = false
                });

            }
        }

        [HttpPost]
        public ActionResult UpdateLog(string accountId, string comment)
        {
            try
            {
                TempData["AccountId"] = accountId;
                UserSupportFacade userSupportFacade = new UserSupportFacade();
                var userStore = new UserStore<ApplicationUser>(new ApplicationDbContext());
                var manager = new UserManager<ApplicationUser>(userStore);
                var currentUser = manager.FindById(User.Identity.GetUserId());
                UserLogEntity userLog = new UserLogEntity();
                userLog.userid = Convert.ToInt32(accountId);
                userLog.comment = comment;
                userLog.auctionId = 0;
                userLog.orderId = 0;
                userLog.adminuser = currentUser.FirstName;
                var isSuccess = userSupportFacade.UpdateLog(userLog);

                return Json(new
                {
                    Valid = isSuccess,
                    Message = string.Empty,
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    Valid = false,
                    Message = string.Empty,
                });
            }
        }

        [HttpGet]
        public ActionResult ItemBasketPopup(string itemId)
        {
            UserSupportModel userSupportModel = new UserSupportModel();
            UserSupportFacade userSupportFacade = new UserSupportFacade();
            if (!string.IsNullOrEmpty(itemId))
            {
                userSupportModel.ItemBasket = userSupportFacade.GetItemBasket(itemId);
                userSupportModel.ListItemRelateds = userSupportFacade.GetListItemRelated(userSupportModel.ItemBasket.itemNo, "th");
            }
            else
            {
                if (TempData["ItemNo"] != null)
                {
                    var itemNo = (string)TempData["ItemNo"];
                    userSupportModel.ListItemRelateds = userSupportFacade.GetListItemRelated(itemNo, "th");
                    userSupportModel.ItemBasket = new ItemEntity();
                    userSupportModel.ItemBasket.itemNo = itemNo;
                    userSupportModel.ItemBasket.itemName = userSupportFacade.GetNameItemBasket(itemNo, "th");
                }
            }
            return View(userSupportModel);
        }
        [HttpPost]
        public JsonResult CheckItemNo(string itemNo)
        {
            try
            {
                UserSupportFacade userSupportFacade = new UserSupportFacade();
                bool checkData = userSupportFacade.CheckItemNo(itemNo);
                //bool checkData = false;
                ItemEntity itemObj = new ItemEntity();
                if (checkData)
                {
                    TempData["ItemNo"] = itemNo;
                }else
                {
                    TempData["ItemNo"] = null;
                }
                return Json(checkData);
            }
            catch
            {
                return Json(false);
            }

        }

        [HttpPost]
        public ActionResult DeleteItemBasket(string itemId,string itemNo,int accountid)
        {
            try
            {
                UserSupportFacade userSupportFacade = new UserSupportFacade();

                if (userSupportFacade.DeleteItemBasket(Convert.ToInt32(itemId),itemNo,accountid))
                {
                    return Json(new
                    {
                        Valid = true,
                        Error = string.Empty,
                    });
                }
                else
                {
                    return Json(new
                    {
                        Valid = false
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Valid = false
                });

            }
        }

        [HttpPost]
        public ActionResult EditItemBasket(int itemId)
        {
            try
            {
                TempData["ItemId"] = itemId;
                return Json(new
                {
                    Valid = true,
                    Message = string.Empty,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Valid = false,
                    Message = string.Empty,
                });
            }
        }

        [HttpPost]
        public ActionResult AddItemBasket(string jsonData)
        {
            try
            {
                var itemBasket = Newtonsoft.Json.JsonConvert.DeserializeObject<ItemEntity>(jsonData);
                UserSupportFacade userFacade = new UserSupportFacade();

                var isSuccess = userFacade.AddItemBasket(itemBasket,"");

                return Json(new
                {
                    Valid = isSuccess,
                    Message = string.Empty,
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    Valid = false,
                    Message = string.Empty,
                });
            }
        }

        [HttpPost]
        public ActionResult DisplayLogOrder(string orderId)
        {
            try
            {
                UserSupportModel userSupportModel = new UserSupportModel();
                UserSupportFacade userFacade = new UserSupportFacade();
                userSupportModel.ListOrderLogs = userFacade.GetListOrderLogs(Convert.ToInt32(orderId));
                return PartialView("~/Views/UserSupport/ListOrderLogs.cshtml", userSupportModel);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Valid = false,
                    Error = string.Empty
                });
            }
        }

        [HttpPost]
        public ActionResult UpdateOrderStatus(string jsonData,string comment,string status)
        {
            try
            {
                var orderIds = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderEntity>>(jsonData);
                UserSupportFacade userFacade = new UserSupportFacade();
                var valid = false;
                string _out = string.Empty;
                var userStore = new UserStore<ApplicationUser>(new ApplicationDbContext());
                var manager = new UserManager<ApplicationUser>(userStore);
                var currentUser = manager.FindById(User.Identity.GetUserId());
                if (!status.Equals("000"))
                {
                    valid = userFacade.UpdateOrderStatus(orderIds, currentUser.FirstName, Convert.ToInt16(status), comment);
                    _out = "Order status update successfully";
                }
                else
                {
                    List<int> orderMergeIds = new List<int>();
                    foreach (OrderEntity item in orderIds)
                    {
                        orderMergeIds.Add(item.id);
                    }
                    valid = userFacade.MergeOrders(orderMergeIds, 1, 0, out _out);
                }
                return Json(new
                {
                    Valid = valid,
                    Message = _out,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Valid = false,
                    Message = string.Empty,
                });
            }
        }

        [HttpPost]
        public ActionResult UpdateOrderAddress(string jsonData)
        {
            try
            {
                var userInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<UserDetailsEntity>(jsonData);
                UserSupportFacade userSupportFacade = new UserSupportFacade();

                var isSuccess = userSupportFacade.UpdateOrderInfo(userInfo);

                return Json(new
                {
                    Valid = isSuccess,
                    Message = string.Empty,
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    Valid = false,
                    Message = string.Empty,
                });
            }
        }
    }
}