﻿using CallingChilindo.Business;
using CallingChilindo.Models;
using CallingChilindo.Models.Class;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Linq;
using System.Web.Mvc;

namespace CallingChilindo.Controllers
{
    public class PaymentsController : Controller
    {
        private ChilindoCallingEntities db = new ChilindoCallingEntities();
        private IPaymentValidation _paymentValidation = new PaymentValidation();
        private Splitter splitter = new Splitter();

        // GET: Payments
        [HttpGet]
        public ActionResult Index()
        {
            PaymentModels paymentModel = new PaymentModels();
            PaymentsFacade paymentsFacade = new PaymentsFacade();

            int paymentReceiptsId = 0;
            if (TempData["PaymentReceiptsId"] != null)
            {
                paymentReceiptsId = (int)TempData["PaymentReceiptsId"];
            }

            paymentModel.PaymentReceipts = new PaymentReceiptsEntity();
            if (paymentReceiptsId > 0)
            {
                paymentModel.PaymentReceipts = paymentsFacade.GetPaymentReceipts(paymentReceiptsId);
            }

            paymentModel.Banks = paymentsFacade.GetBankAll();
            var userStore = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var manager = new UserManager<ApplicationUser>(userStore);
            var currentUser = manager.FindById(User.Identity.GetUserId());
            paymentModel.PaymentHistorys = paymentsFacade.GetPatmentHistorys(currentUser.FirstName);
            
            if (!currentUser.isCaller)
            {
                var users = db.AspNetUsers.Where(o => o.Id == currentUser.Id).FirstOrDefault();
                var UserLogin = new Users();
                UserLogin.sIdGuid = users.Id;
                UserLogin.UserName = users.FirstName + " " + users.LastName;
                paymentModel.User = UserLogin;
            }
            return View(paymentModel);
        }

        [HttpPost]
        public JsonResult CheckOrderById(string orderId)
        {
            PaymentsFacade paymentsFacade = new PaymentsFacade();
            try
            {
                bool hasOrder = false;
                string error = string.Empty;
                hasOrder = paymentsFacade.GetCheckOrderId(Convert.ToInt32(orderId));
                    //var list = (from o in db.tblOrders.Where(o => o.orderstatusid == 1 && o.deliveryoptionident == "BANKWIRE") select o.id);

                if (!hasOrder)
                {
                   error = "not found. Please try again!";
                }
            
                return Json(new
                {
                    Valid = hasOrder,
                    Message = string.Format("Order#{0} {1}", orderId, error),
                });
            }
            catch(Exception ex)
            {
                return Json(new
                {
                    Valid = false,
                    Message = string.Format("Order#{0} {1}", orderId, ex.Message),
                });
            }

        }
          
        [HttpPost]
        public ActionResult AddPaymentReceipts(string orderId, string amount, string datePaid, string seeReceipt, string bank, string comment)
        {
            try
            {
                var paymentReceipt = new PaymentReceiptsEntity();
                var paymentsFacade = new PaymentsFacade();
                var error = string.Empty;
                var isSuccess = false;
                var paymentError = _paymentValidation.Validate(orderId, amount, datePaid, seeReceipt, bank);

                if (paymentError == null)
                {
                    var amountList = splitter.Split(amount);
                    if (amountList != null)
                    {
                        var userStore = new UserStore<ApplicationUser>(new ApplicationDbContext());
                        var manager = new UserManager<ApplicationUser>(userStore);
                        var currentUser = manager.FindById(User.Identity.GetUserId());

                        int i = 0;
                        foreach (var item in amountList)
                        {
                            if (i == 0) { paymentReceipt.amount1 = (item != null) ? Convert.ToDecimal(item) : 0.0m; }

                            if (i == 1) { paymentReceipt.amount2 = (item != null) ? Convert.ToDecimal(item) : 0.0m; }

                            if (i == 2) { paymentReceipt.amount3 = (item != null) ? Convert.ToDecimal(item) : 0.0m; }
                            i++;
                        }

                        paymentReceipt.orderidents = orderId;
                        paymentReceipt.userId = currentUser.FirstName;
                        paymentReceipt.amountTotal = splitter.Sum(amount);
                        paymentReceipt.orderAmount = splitter.Sum(amount);
                        paymentReceipt.datePaid = Convert.ToDateTime(datePaid);
                        paymentReceipt.showReceipt = seeReceipt;
                        paymentReceipt.bankId = Convert.ToInt32(bank);
                        paymentReceipt.comment = comment;

                        var checkValidData = _paymentValidation.Validate(paymentReceipt);
                        if (checkValidData == null)
                        {
                            isSuccess = paymentsFacade.AddPaymentReceipt(paymentReceipt);
                            if (!isSuccess)
                            {
                                error = "Something went wrong when insert payment. Please try again!";
                            }
                        }
                        else
                        {
                            error = checkValidData.ErrorMessage;
                        }
                    }
                    else
                    {
                        error = paymentError.ErrorMessage;
                    }
                }
                return Json(new
                {
                    Valid = isSuccess,
                    Message = string.Format("{0} {1}", paymentError, error),
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    Valid = false,
                    Message = e.Message,
                });
            }
        }

        [HttpPost]
        public ActionResult DeletePaymentReceipts(string paymentReceiptId)
        {
            try
            {
                PaymentsFacade paymentsFacade = new PaymentsFacade();

                try
                {
                    var result = paymentsFacade.DeletePaymentReceipt(Convert.ToInt32(paymentReceiptId));
                    if (result)
                    {
                        return Json(new
                        {
                            Valid = true,
                            Error = string.Empty,
                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            Valid = false,
                            Error = "Delete was not successful!",
                        });
                    }
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Valid = false,
                        Error = ex.Message,
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Valid = false,
                    Error = ex.Message,
                });
            }
        }

        [HttpPost]
        public ActionResult EditPatmentReceipts(string paymentReceiptsId)
        {
            try
            {
                TempData["PaymentReceiptsId"] = paymentReceiptsId;
                return Json(new
                {
                    Valid = true,
                    Message = string.Empty,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Valid = false,
                    Message = ex.Message,
                });
            }
        }

    }
}