﻿using CallingChilindo.Models;
using CallingChilindo.Models.Class;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using System.Transactions;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using System.Threading;

namespace CallingChilindo.Controllers
{
    public class PickUpController : Controller
    {
        private ChilindoCallingEntities db = new ChilindoCallingEntities();
        // GET: PickUp
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult HandoutOrders()
        {
            return View();
        }

        public ActionResult CashTransaction()
        {
            ViewBag.TotalCashHolder = db.tblCashTransactions.Count();
            ViewBag.TransactionData = CaseTransactionData();
            ViewBag.TransactionDetailData = getLatestCashTranscationData();
            ViewBag.TransactionType = new SelectList(db.tblCashTransactionTypes.OrderBy(s => s.Name), "id", "Name");
            return View();
        }

        public ActionResult BackOrderpopup(string OID, string paidorunpaid)
        {
            int _OID = Convert.ToInt32(OID);
            BackOrderPopup getbackorders = new BackOrderPopup();
            getbackorders = (from j in db.tblOrders
                                 join i in db.tblOrderstatus on j.orderstatusid equals i.ident
                                 where j.id == _OID
                                 select new BackOrderPopup
                                 {

                                     OrderId = j.id,
                                     orderguid = j.orderguid,
                                     orderstatus = i.orderstatus,
                                     PaidOrUnpaid = paidorunpaid,
                                     deliveryoptionident = j.deliveryoptionident,
                                     accountid = j.accountid,
                                     email = j.email

                                 }).FirstOrDefault();
            var Basket = new List<tblBasket>();
            var getbasket = from parentObj in db.tblBasket
                            where parentObj.paymentorderid == _OID
                            orderby parentObj.id ascending
                            select new
                            {
                                Basket = parentObj,
                            };
            foreach (var itemBasket in getbasket)
            {
                tblBasket oneBasket = new tblBasket();
                oneBasket.id = itemBasket.Basket.id;
                oneBasket.itemnumber = itemBasket.Basket.itemnumber;
                oneBasket.created = itemBasket.Basket.created;
                oneBasket.antal = itemBasket.Basket.antal;
                oneBasket.basValuta = itemBasket.Basket.basValuta;
                oneBasket.basValutaPrice = itemBasket.Basket.basValutaPrice;
                oneBasket.basValutaPriceTotal = itemBasket.Basket.basValutaPriceTotal;
                oneBasket.paymentorderid = itemBasket.Basket.paymentorderid;
                oneBasket.accountident = itemBasket.Basket.accountident;
                Basket.Add(oneBasket);
            }
            ViewBag.BasketData = Basket;
            return View(getbackorders);
        }


        #region Registed Orders
        public ActionResult MultiOrdersPrintForPick()
        {
            ViewBag.Htmlstring = TempData["HtmlData"].ToString();
            return View();
        }
        [Authorize]
        [HttpPost]
        public ActionResult SearchUserAccount()
        {
            try
            {
                int? OrderId = 0; string _email = "", _username = "";
                List<int> AccountId = new List<int>();
                List<MultiUserForPickup> UsersPick = new List<MultiUserForPickup>();
                List<tblOrders> orderData = new List<tblOrders>();

                #region SearchAccountID
                if (Request.Form["OrderId"] != "" && Request.Form["Email"] != "" && Request.Form["UserName"] != "")
                {
                    OrderId = Convert.ToInt32(Request.Form["OrderId"]);
                    _email = Convert.ToString(Request.Form["Email"]).ToLower();
                    _username = Convert.ToString(Request.Form["UserName"]).ToLower();
                    orderData = db.tblOrders.Where(s => s.id == OrderId || s.email.StartsWith(_email) || s.username.StartsWith(_username)).ToList();
                }
                else if (Request.Form["OrderId"] != "" && Request.Form["Email"] != "")
                {
                    OrderId = Convert.ToInt32(Request.Form["OrderId"]);
                    _email = Convert.ToString(Request.Form["Email"]).ToLower();
                    orderData = db.tblOrders.Where(s => s.id == OrderId || s.email.StartsWith(_email)).ToList();
                }
                else if (Request.Form["OrderId"] != "" && Request.Form["UserName"] != "")
                {
                    OrderId = Convert.ToInt32(Request.Form["OrderId"]);
                    _username = Convert.ToString(Request.Form["UserName"]).ToLower();
                    orderData = db.tblOrders.Where(s => s.id == OrderId || s.username.StartsWith(_username)).ToList();
                }
                else if (Request.Form["Email"] != "" && Request.Form["UserName"] != "")
                {
                    _email = Convert.ToString(Request.Form["Email"]).ToLower();
                    _username = Convert.ToString(Request.Form["UserName"]).ToLower();
                    orderData = db.tblOrders.Where(s => s.email.StartsWith(_email) || s.username.StartsWith(_username)).ToList();
                }
                else if (Request.Form["OrderId"] != "")
                {
                    OrderId = Convert.ToInt32(Request.Form["OrderId"]);
                    orderData = db.tblOrders.Where(s => s.id == OrderId).ToList();
                }
                else if (Request.Form["Email"] != "")
                {
                    _email = Convert.ToString(Request.Form["Email"]).ToLower();
                    orderData = db.tblOrders.Where(s => s.email.StartsWith(_email)).ToList();
                }
                else if (Request.Form["UserName"] != "")
                {
                    _username = Convert.ToString(Request.Form["UserName"]).ToLower();
                    orderData = db.tblOrders.Where(s => s.username.StartsWith(_username)).ToList();
                }
                #endregion

                var uniqueaccountid = orderData.Select(s => s.accountid).Distinct().ToArray();


                if (uniqueaccountid.Count() == 0)
                {
                    TempData["Warning"] = "There is no AccountId Found.";
                    return View("Index");
                }
                else if (uniqueaccountid.Count() == 1)
                {
                    int? _accountid = uniqueaccountid[0];
                    var Customerorders = (from j in db.tblOrders
                                          join i in db.tblOrderstatus on j.orderstatusid equals i.ident //&& j.pickUpLocation == 2 
                                          where j.accountid == _accountid && j.deliverytypeident == "PICKUP" && (j.orderstatusid == 1 || j.orderstatusid == 5 || j.orderstatusid == 10 || j.orderstatusid == 12 || j.orderstatusid == 13 || j.orderstatusid == 35 || j.orderstatusid == 36 || j.orderstatusid == 71 || j.orderstatusid == 72)
                                          select new
                                          {
                                              accountid = j.accountid,
                                              id = j.id,
                                              name = j.name,
                                              username = j.username,
                                              orderstatus = i.orderstatus,
                                              PickupLocation = "Bearing",
                                              webAmount = j.webAmount,
                                              PaidOrUnpaid = "",
                                              deliveryoptionident = j.deliveryoptionident,
                                              orderstatusid = j.orderstatusid,
                                              orderguid = j.orderguid

                                          }).AsEnumerable().Select(s => new OrdersForPickup
                                          {

                                              accountid = s.accountid,
                                              id = s.id,
                                              name = s.name,
                                              username = s.username,
                                              orderstatus = s.orderstatus,
                                              PickupLocation = s.PickupLocation,
                                              webAmount = s.webAmount,
                                              PaidOrUnpaid = GetPaidOrUnpaidOrders(s.deliveryoptionident, s.orderstatusid),
                                              orderstatusid = s.orderstatusid,
                                              orderguid = s.orderguid



                                          }).ToList();


                    ViewBag.Customerorders = Customerorders;
                }
                else
                {
                    UsersPick = (from j in db.tblOrders
                                 where uniqueaccountid.Contains(j.accountid)
                                 select new MultiUserForPickup
                                 {
                                     accountid = j.accountid,
                                     name = j.name,
                                     username = j.username,
                                     email = j.email,
                                     OrderForPickUp = db.tblOrders.Where(s => s.deliverytypeident == "PICKUP"  && s.accountid == j.accountid && (s.orderstatusid == 1 || s.orderstatusid == 5 || s.orderstatusid == 10 || s.orderstatusid == 72)).Count(),

                                 }).Distinct().ToList();

                }
                ViewBag.UserCount = uniqueaccountid.Count();
                ViewBag.Userlist = UsersPick;

                return View("Index");


            }
            catch (Exception e)
            {
                TempData["Warning"] = "There is no AccountId Found.";
                return RedirectToAction("Index", "PickUp");
            }

        }

        [Authorize]

        public ActionResult GetUserOrdersIdByAccountId(int accountid)
        {
            try
            {

                var Customerorders = (from j in db.tblOrders
                                      join i in db.tblOrderstatus on j.orderstatusid equals i.ident
                                      where j.accountid == accountid && j.deliverytypeident == "PICKUP" && (j.orderstatusid == 1 || j.orderstatusid == 5 || j.orderstatusid == 10 || j.orderstatusid == 12 || j.orderstatusid == 13 || j.orderstatusid == 35 || j.orderstatusid == 36 || j.orderstatusid == 71 || j.orderstatusid == 72)
                                      select new
                                      {
                                          accountid = j.accountid,
                                          id = j.id,
                                          name = j.name,
                                          username = j.username,
                                          orderstatus = i.orderstatus,
                                          PickupLocation = "Bearing",
                                          webAmount = j.webAmount,
                                          PaidOrUnpaid = "",
                                          deliveryoptionident = j.deliveryoptionident,
                                          orderstatusid = j.orderstatusid,
                                          orderguid = j.orderguid

                                      }).AsEnumerable().Select(s => new OrdersForPickup
                                      {

                                          accountid = s.accountid,
                                          id = s.id,
                                          name = s.name,
                                          username = s.username,
                                          orderstatus = s.orderstatus,
                                          PickupLocation = s.PickupLocation,
                                          webAmount = s.webAmount,
                                          PaidOrUnpaid = GetPaidOrUnpaidOrders(s.deliveryoptionident, s.orderstatusid),
                                          orderstatusid = s.orderstatusid,
                                          orderguid = s.orderguid



                                      }).ToList();


                ViewBag.Customerorders = Customerorders;

                return View("Index");


            }
            catch (Exception e)
            {
                TempData["Warning"] = "There is no AccountId Found.";
                return RedirectToAction("Index", "PickUp");
            }

        }

        public string GetPaidOrUnpaidOrders(string optionident, int statusid)
        {
            string output = "";
            if (optionident == "COD")
            {
                if (statusid == 70)
                {
                    output = "Paid";
                }
                else if (statusid >= 1 && statusid <= 69)
                {
                    output = "Unpaid";
                }
            }
            else if (optionident == "CASH")
            {
                if (statusid == 35 || statusid == 36)
                {
                    output = "Paid";
                }
                else if ((statusid >= 1 && statusid <= 10) || statusid == 12)
                {
                    output = "Unpaid";
                }
            }
            else
            {
                if ((statusid >= 2 && statusid <= 99))
                {
                    output = "Paid";
                }
                else if (statusid == 1)
                {
                    output = "Unpaid";
                }
            }
            return output;
        }
        [Authorize]
        [HttpPost]
        public ActionResult SendOrderForPick(string OrderID)
        {
            try
            {
                bool flag = true;
                var _orderarray = OrderID.Split(',').ToArray();
                if (_orderarray.Length > 1)
                {
                    string Orderguid = "";
                    for (int i = 0; i < _orderarray.Length; i++)
                    {
                        var _OrderId = Convert.ToInt32(_orderarray[i]);
                        Orderguid = Orderguid + db.tblOrders.Find(_OrderId).orderguid + ",";
                    }
                    Orderguid = Orderguid.Trim(',');
                    var Token = GetToken();
                    com.chilindo.beta.orders.APIOrders obj = new com.chilindo.beta.orders.APIOrders();
                    XmlNode node = obj.SetOrderStatusSendToPacking(Token, OrderID);
                    XDocument doc = XDocument.Parse(node.OuterXml.ToString());
                    var status = doc.Descendants("status").Select(x => new
                    {
                        _status = (string)x.Attribute("code").Value
                    }).ToList();
                    if (status[0]._status == "OK")
                    {

                        var htmlstring = GetHTMLForPrint(Orderguid);

                        Dictionary<string, object> Msg = new Dictionary<string, object>();
                        Msg.Add("MsgCode", "1");
                        Msg.Add("Count", "2");
                        Msg.Add("htmlstring", htmlstring);
                        return Json(Msg, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var Error = doc.Descendants("status").Select(x => new
                        {
                            _message = (string)x.Attribute("code").Parent.Value
                        }).ToList();

                        Dictionary<string, object> error = new Dictionary<string, object>();
                        error.Add("ErrorCode", "-1");
                        error.Add("ErrorMessage", Error[0]._message);
                        return Json(error, JsonRequestBehavior.AllowGet);

                    }
                }
                else
                {
                    var _OrderId = Convert.ToInt32(OrderID);
                    var Orderguid = db.tblOrders.Find(_OrderId).orderguid;
                    var Token = GetToken();
                    com.chilindo.beta.orders.APIOrders obj = new com.chilindo.beta.orders.APIOrders();
                    XmlNode node = obj.SetOrderStatusSendToPacking(Token, OrderID);
                    XDocument doc = XDocument.Parse(node.OuterXml.ToString());
                    var status = doc.Descendants("status").Select(x => new
                    {
                        _status = (string)x.Attribute("code").Value
                    }).ToList();
                    if (status[0]._status == "OK")
                    {
                        Dictionary<string, object> Msg = new Dictionary<string, object>();
                        Msg.Add("MsgCode", "1");
                        Msg.Add("Count", "1");
                        Msg.Add("Orderguid", Orderguid.ToString());
                        return Json(Msg, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var Error = doc.Descendants("status").Select(x => new
                        {
                            _message = (string)x.Attribute("code").Parent.Value
                        }).ToList();

                        Dictionary<string, object> error = new Dictionary<string, object>();
                        error.Add("ErrorCode", "-1");
                        error.Add("ErrorMessage", Error[0]._message);
                        return Json(error, JsonRequestBehavior.AllowGet);
                    }

                }


            }
            catch (Exception Ex)
            {
                Dictionary<string, object> error = new Dictionary<string, object>();
                error.Add("ErrorCode", "-1");
                error.Add("ErrorMessage", "");
                return Json(error, JsonRequestBehavior.AllowGet);
            }

        }

        private string GetToken()
        {
            com.chilindo.beta.Generic obj = new com.chilindo.beta.Generic();
            XmlNode node = obj.GetAccessToken4API("api_orders", "E27D!4E8C", com.chilindo.beta.APIcalls.APIORDER);
            return node.ChildNodes[0].InnerText;
        }

        public string GetHTMLForPrint(string OrderGuId)
        {
            TempData["HtmlData"] = null;

            string finaloutput = "";
            string[] OrderGuIDarray = OrderGuId.Split(',');


            for (int i = 0; i < OrderGuIDarray.Length; i++)
            {
                string _orderguid = OrderGuIDarray[i];
                HtmlDocument doc = new HtmlWeb().Load("http://www.chilindo.com/backoffice/PrintPicklist.aspx?orderguid=" + _orderguid);
                finaloutput = finaloutput + doc.DocumentNode.OuterHtml.Replace("/includes/invoice.css", "http://www.chilindo.com/includes/invoice.css");


            }
            TempData["HtmlData"] = (finaloutput);
            return finaloutput;
        }

        #endregion

        #region For Handout Orders

        [Authorize]
        [HttpPost]
        public ActionResult SearchUserAccountForHandout()
        {
            try
            {
                int? OrderId = 0; string _email = "", _username = "";
                List<int> AccountId = new List<int>();
                List<MultiUserForPickup> UsersPick = new List<MultiUserForPickup>();
                List<tblOrders> orderData = new List<tblOrders>();

                #region SearchAccountID
                if (Request.Form["OrderId"] != "" && Request.Form["Email"] != "" && Request.Form["UserName"] != "")
                {
                    OrderId = Convert.ToInt32(Request.Form["OrderId"]);
                    _email = Convert.ToString(Request.Form["Email"]).ToLower();
                    _username = Convert.ToString(Request.Form["UserName"]).ToLower();
                    orderData = db.tblOrders.Where(s => s.id == OrderId || s.email.StartsWith(_email) || s.username.StartsWith(_username)).ToList();
                }
                else if (Request.Form["OrderId"] != "" && Request.Form["Email"] != "")
                {
                    OrderId = Convert.ToInt32(Request.Form["OrderId"]);
                    _email = Convert.ToString(Request.Form["Email"]).ToLower();
                    orderData = db.tblOrders.Where(s => s.id == OrderId || s.email.StartsWith(_email)).ToList();
                }
                else if (Request.Form["OrderId"] != "" && Request.Form["UserName"] != "")
                {
                    OrderId = Convert.ToInt32(Request.Form["OrderId"]);
                    _username = Convert.ToString(Request.Form["UserName"]).ToLower();
                    orderData = db.tblOrders.Where(s => s.id == OrderId || s.username.StartsWith(_username)).ToList();
                }
                else if (Request.Form["Email"] != "" && Request.Form["UserName"] != "")
                {
                    _email = Convert.ToString(Request.Form["Email"]).ToLower();
                    _username = Convert.ToString(Request.Form["UserName"]).ToLower();
                    orderData = db.tblOrders.Where(s => s.email.StartsWith(_email) || s.username.StartsWith(_username)).ToList();
                }
                else if (Request.Form["OrderId"] != "")
                {
                    OrderId = Convert.ToInt32(Request.Form["OrderId"]);
                    orderData = db.tblOrders.Where(s => s.id == OrderId).ToList();
                }
                else if (Request.Form["Email"] != "")
                {
                    _email = Convert.ToString(Request.Form["Email"]).ToLower();
                    orderData = db.tblOrders.Where(s => s.email.StartsWith(_email)).ToList();
                }
                else if (Request.Form["UserName"] != "")
                {
                    _username = Convert.ToString(Request.Form["UserName"]).ToLower();
                    orderData = db.tblOrders.Where(s => s.username.StartsWith(_username)).ToList();
                }
                #endregion

                var uniqueaccountid = orderData.Select(s => s.accountid).Distinct().ToArray();


                if (uniqueaccountid.Count() == 0)
                {
                    TempData["Warning"] = "There is no AccountId Found.";
                    return View("HandoutOrders");
                }
                else if (uniqueaccountid.Count() == 1)
                {
                    int? _accountid = uniqueaccountid[0];
                    var Customerorders = (from j in db.tblOrders
                                          join i in db.tblOrderstatus on j.orderstatusid equals i.ident
                                          where j.accountid == _accountid && j.deliverytypeident == "PICKUP"  && (j.orderstatusid == 10 || j.orderstatusid == 12)
                                          select new
                                          {
                                              accountid = j.accountid,
                                              id = j.id,
                                              name = j.name,
                                              username = j.username,
                                              orderstatus = i.orderstatus,
                                              PickupLocation = "Bearing",
                                              webAmount = j.webAmount,
                                              PaidOrUnpaid = "",
                                              deliveryoptionident = j.deliveryoptionident,
                                              orderstatusid = j.orderstatusid,
                                              orderguid = j.orderguid

                                          }).ToList();

                    List<OrdersForPickup> CustomerordersPaid = new List<OrdersForPickup>();
                    List<OrdersForPickup> CustomerordersUnPaid = new List<OrdersForPickup>();

                    for (int i = 0; i < Customerorders.Count(); i++)
                    {
                        var getpaidunpaid = GetPaidOrUnpaidOrdersForHandout(Customerorders[i].deliveryoptionident, Customerorders[i].orderstatusid);
                        if (getpaidunpaid == "Paid")
                        {
                            CustomerordersPaid.Add(new OrdersForPickup
                            {
                                accountid = Customerorders[i].accountid,
                                id = Customerorders[i].id,
                                name = Customerorders[i].name,
                                username = Customerorders[i].username,
                                orderstatus = Customerorders[i].orderstatus,
                                PickupLocation = Customerorders[i].PickupLocation,
                                webAmount = Customerorders[i].webAmount,
                                PaidOrUnpaid = "Paid",
                                orderstatusid = Customerorders[i].orderstatusid,
                                orderguid = Customerorders[i].orderguid,
                                deliveryoptionident = Customerorders[i].deliveryoptionident,

                            });
                        }
                        else if (getpaidunpaid == "Unpaid")
                        {
                            CustomerordersUnPaid.Add(new OrdersForPickup
                            {
                                accountid = Customerorders[i].accountid,
                                id = Customerorders[i].id,
                                name = Customerorders[i].name,
                                username = Customerorders[i].username,
                                orderstatus = Customerorders[i].orderstatus,
                                PickupLocation = Customerorders[i].PickupLocation,
                                webAmount = Customerorders[i].webAmount,
                                PaidOrUnpaid = "Unpaid",
                                orderstatusid = Customerorders[i].orderstatusid,
                                orderguid = Customerorders[i].orderguid,
                                deliveryoptionident = Customerorders[i].deliveryoptionident,

                            });
                        }
                    }

                    ViewBag.Customerorders = Customerorders;
                    ViewBag.CustomerordersPaid = CustomerordersPaid;
                    ViewBag.CustomerordersUnPaid = CustomerordersUnPaid;
                }
                else
                {
                    UsersPick = (from j in db.tblOrders
                                 where uniqueaccountid.Contains(j.accountid)
                                 select new MultiUserForPickup
                                 {
                                     accountid = j.accountid,
                                     name = j.name,
                                     username = j.username,
                                     email = j.email,
                                     OrderForPickUp = db.tblOrders.Where(s => s.deliverytypeident == "PICKUP"  &&  s.accountid == j.accountid && (s.orderstatusid == 10 || s.orderstatusid == 12)).Count(),

                                 }).Distinct().ToList();

                }
                ViewBag.UserCount = uniqueaccountid.Count();
                ViewBag.Userlist = UsersPick;

                return View("HandoutOrders");


            }
            catch (Exception e)
            {
                TempData["Warning"] = "There is no AccountId Found.";
                return RedirectToAction("HandoutOrders", "PickUp");
            }

        }

        public string GetPaidOrUnpaidOrdersForHandout(string optionident, int statusid)
        {
            string output = "";

            if (optionident == "CASH")
            {
                if (statusid == 12)
                {
                    output = "Paid"; //HandOut
                }
                else if (statusid == 10)
                {
                    output = "Unpaid"; //pay
                }
            }
            else if (optionident == "2C2P" || optionident == "BANKWIRE")
            {
                if (statusid == 10)
                {
                    output = "Paid"; //HandOut
                }

            }


            return output;
        }


        [Authorize]
        public ActionResult GetUserOrdersIdByAccountIdForHandout(int accountid)
        {
            try
            {

                var Customerorders = (from j in db.tblOrders
                                      join i in db.tblOrderstatus on j.orderstatusid equals i.ident
                                      where j.accountid == accountid && j.deliverytypeident == "PICKUP"  && (j.orderstatusid == 10 || j.orderstatusid == 12)
                                      select new
                                      {
                                          accountid = j.accountid,
                                          id = j.id,
                                          name = j.name,
                                          username = j.username,
                                          orderstatus = i.orderstatus,
                                          PickupLocation = "Bearing",
                                          webAmount = j.webAmount,
                                          PaidOrUnpaid = "",
                                          deliveryoptionident = j.deliveryoptionident,
                                          orderstatusid = j.orderstatusid,
                                          orderguid = j.orderguid

                                      }).ToList();

                List<OrdersForPickup> CustomerordersPaid = new List<OrdersForPickup>();
                List<OrdersForPickup> CustomerordersUnPaid = new List<OrdersForPickup>();

                for (int i = 0; i < Customerorders.Count(); i++)
                {
                    var getpaidunpaid = GetPaidOrUnpaidOrdersForHandout(Customerorders[i].deliveryoptionident, Customerorders[i].orderstatusid);
                    if (getpaidunpaid == "Paid")
                    {
                        CustomerordersPaid.Add(new OrdersForPickup
                        {
                            accountid = Customerorders[i].accountid,
                            id = Customerorders[i].id,
                            name = Customerorders[i].name,
                            username = Customerorders[i].username,
                            orderstatus = Customerorders[i].orderstatus,
                            PickupLocation = Customerorders[i].PickupLocation,
                            webAmount = Customerorders[i].webAmount,
                            PaidOrUnpaid = "Paid",
                            orderstatusid = Customerorders[i].orderstatusid,
                            orderguid = Customerorders[i].orderguid,
                            deliveryoptionident = Customerorders[i].deliveryoptionident,

                        });
                    }
                    else if (getpaidunpaid == "Unpaid")
                    {
                        CustomerordersUnPaid.Add(new OrdersForPickup
                        {
                            accountid = Customerorders[i].accountid,
                            id = Customerorders[i].id,
                            name = Customerorders[i].name,
                            username = Customerorders[i].username,
                            orderstatus = Customerorders[i].orderstatus,
                            PickupLocation = Customerorders[i].PickupLocation,
                            webAmount = Customerorders[i].webAmount,
                            PaidOrUnpaid = "Unpaid",
                            orderstatusid = Customerorders[i].orderstatusid,
                            orderguid = Customerorders[i].orderguid,
                            deliveryoptionident = Customerorders[i].deliveryoptionident,

                        });
                    }
                }

                ViewBag.Customerorders = Customerorders;
                ViewBag.CustomerordersPaid = CustomerordersPaid;
                ViewBag.CustomerordersUnPaid = CustomerordersUnPaid;

                return View("HandoutOrders");


            }
            catch (Exception e)
            {
                TempData["Warning"] = "There is no AccountId Found.";
                return RedirectToAction("HandoutOrders", "PickUp");
            }

        }

        public ActionResult GetOrderDataByOrderId(string OrderId)
        {

            string[] _OrderId = OrderId.Split(',');
            int[] _intOrderId = _OrderId.Select(n => Convert.ToInt32(n)).ToArray();
            decimal? initialtotal = 0;
            decimal? initialroundtotal = 0;           
            try
            {
                var DataResult = db.tblOrders.Where(s => _intOrderId.Contains(s.id)).Select(s => new
                {
                    accountid = s.accountid,
                    id = s.id,
                    name = s.name,
                    username = s.username,
                    webAmount = s.webAmount,


                }).AsEnumerable().Select(s => new OrdersForPopup
                {

                    accountid = s.accountid,
                    id = s.id,
                    name = s.name,
                    username = s.username,
                    webAmount = s.webAmount,
                    webRoundAmount = Math.Round(Convert.ToDecimal(s.webAmount), 0),
                    Status = GetOrderStatusFormBackOrder(s.id)[0],
                    ActionId = Convert.ToInt32(GetOrderStatusFormBackOrder(s.id)[1])
                });
                initialtotal = DataResult.Select(s => s.webAmount).Sum();
                initialroundtotal = DataResult.Select(s => s.webRoundAmount).Sum();
                return Json(new { success = true, DataResult = DataResult, Initialtotal = initialtotal, Initialroundtotal = initialroundtotal });

            }
            catch (Exception ex)
            {
                return Json(new { success = false });
            }

        }

        public List<string> GetOrderStatusFormBackOrder(int orderid)
        {
            List<string> listobj = new List<string>();
            string Status = "";
            string actionid = "";
            var backorders = db.tblBackOrders.ToList();
            if(backorders.Where(s=>s.OrderId == orderid).Count() > 0)
            {
                var result = backorders.Where(s => s.OrderId == orderid).FirstOrDefault();
                var action = result.Reason;
                if(action == 1)
                {
                    Status = "Missing Items split (" + result.NewOrderId + ")";
                    actionid = "0";
                }
                else if(action == 4)
                {
                    Status = "Missing Items Cancel (" + result.NewOrderId + ")";
                    actionid = "0";
                }
            }
            else if(backorders.Where(s => s.NewOrderId == orderid).Count() > 0)
            {
                var result = backorders.Where(s => s.NewOrderId == orderid).FirstOrDefault();
                var action = result.Reason;
                if (action == 1)
                {
                    Status = "ShipForFree(" + result.OrderId + ")";
                    actionid = "1";
                }
            }
            else
            {
                Status = "OK";
                actionid = "0";
            }
            listobj.Add(Status);
            listobj.Add(actionid);
            return listobj;

        }

        public ActionResult SetOrdersForHandOut(string OrderId)
        {
            bool flag = true;
            string[] _OrderArray = OrderId.Split(',');
            int[] _OrderInt = _OrderArray.Select(s => Convert.ToInt32(s)).ToArray();
            var Token = GetToken();
            try
            {
                com.chilindo.beta.orders.APIOrders obj = new com.chilindo.beta.orders.APIOrders();

                for (int i = 0; i < _OrderArray.Length; i++)
                {
                    int _OID = Convert.ToInt32(_OrderArray[i]);
                    XmlNode node = obj.SetHandOutOrder(Token, _OID);
                    XDocument doc = XDocument.Parse(node.OuterXml.ToString());
                    var status = doc.Descendants("status").Select(x => new
                    {
                        _status = (string)x.Attribute("code").Value
                    }).ToList();
                    if (status[0]._status == "OK")
                    {
                        flag = true;
                    }
                    else
                    {
                        if (status[0]._status == "ERROR")
                        {
                            var Error = doc.Descendants("status").Select(x => new
                            {
                                _message = (string)x.Attribute("code").Parent.Value
                            }).ToList();
                            flag = false;
                            Dictionary<string, object> error = new Dictionary<string, object>();
                            error.Add("ErrorCode", "-1");
                            error.Add("ErrorMessage", Error[0]._message);
                            return Json(error, JsonRequestBehavior.AllowGet);
                        }
                        flag = false;

                    }

                }

                Dictionary<string, object> Msg = new Dictionary<string, object>();

                if (flag)
                {
                    Msg.Add("MsgCode", "1");
                    return Json(Msg, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    Dictionary<string, object> error = new Dictionary<string, object>();
                    error.Add("ErrorCode", "-1");
                    error.Add("ErrorMessage", "");
                    return Json(error, JsonRequestBehavior.AllowGet);
                }


            }
            catch (Exception ex)
            {
                Dictionary<string, object> error = new Dictionary<string, object>();
                error.Add("ErrorCode", "-1");
                error.Add("ErrorMessage", "");
                return Json(error, JsonRequestBehavior.AllowGet);
            }


        }

        [Authorize]
        [HttpPost]
        public ActionResult ConfirmPayment(string TotalAmount, string username, string OrderId, string Amount,string ActionId)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {

                    try
                    {
                        var get_last_balance = db.tblCashTransactions.OrderByDescending(p => p.id).FirstOrDefault().Balance;
                        tblCashTransactions objcash = new tblCashTransactions();
                        objcash.Amount = Convert.ToDecimal(TotalAmount);
                        objcash.Balance = objcash.Amount + get_last_balance;
                        objcash.Currency = "THB";
                        objcash.TransactionTypeId = 1;
                        objcash.User = username;
                        objcash.Created = DateTime.Now;
                        db.tblCashTransactions.Add(objcash);
                        db.SaveChanges();

                        string[] _Orderid = OrderId.Split(',');
                        string[] _amount = Amount.Split(',');
                        string[] _Action = ActionId.Split(',');
                        int _transactionId = objcash.id;
                        for (int i = 0; i < _Orderid.Length; i++)
                        {
                            tblCashTransactionOrdersMapping objcashmapping = new tblCashTransactionOrdersMapping();
                            objcashmapping.CashTransactionId = _transactionId;
                            objcashmapping.OrderId = Convert.ToInt32(_Orderid[i]);
                            db.tblCashTransactionOrdersMapping.Add(objcashmapping);

                        }
                        db.SaveChanges();
                        var Token = GetToken();
                        com.chilindo.beta.orders.APIOrders objAPI = new com.chilindo.beta.orders.APIOrders();
                        bool flag = true;
                        for (int i = 0; i < _Orderid.Length; i++)
                        {
                            int _OID = Convert.ToInt32(_Orderid[i]);
                            int _amout = Convert.ToInt32(_amount[i]);
                            int _action = Convert.ToInt32(_Action[i]);
                            if(_action == 0)
                            {
                                XmlNode node = objAPI.SetOrderStatusPaidCash(Token, _OID, _amout);
                                XDocument doc = XDocument.Parse(node.OuterXml.ToString());
                                var status = doc.Descendants("status").Select(x => new
                                {
                                    _status = (string)x.Attribute("code").Value
                                }).ToList();
                                if (status[0]._status == "OK")
                                {
                                    flag = true;
                                }
                                else
                                {
                                    if (status[0]._status == "ERROR")
                                    {
                                        var Error = doc.Descendants("status").Select(x => new
                                        {
                                            _message = (string)x.Attribute("code").Parent.Value
                                        }).ToList();
                                        flag = false;
                                        Dictionary<string, object> error = new Dictionary<string, object>();
                                        error.Add("ErrorCode", "-1");
                                        error.Add("ErrorMessage", Error[0]._message);
                                        return Json(error, JsonRequestBehavior.AllowGet);
                                    }
                                    flag = false;
                                }
                            }
                            else if(_action == 1)
                            {
                               
                                XmlNode node = objAPI.SetOrderStatusPaidCash(Token, _OID, _amout);
                                XDocument doc = XDocument.Parse(node.OuterXml.ToString());
                                var status = doc.Descendants("status").Select(x => new
                                {
                                    _status = (string)x.Attribute("code").Value
                                }).ToList();
                                if (status[0]._status == "OK")
                                {
                                    var orderinfo = db.tblOrders.Where(s => s.id == _OID).FirstOrDefault();
                                    XmlNode node1 = objAPI.ShipPickUpOrder(Token, _OID, orderinfo.name, orderinfo.lastname, orderinfo.address, orderinfo.zip, orderinfo.province, orderinfo.districtsub, orderinfo.district, orderinfo.phone);
                                    XDocument doc1 = XDocument.Parse(node1.OuterXml.ToString());
                                    var status1 = doc1.Descendants("status").Select(x => new
                                    {
                                        _status1 = (string)x.Attribute("code").Value
                                    }).ToList();
                                    if (status1[0]._status1 == "OK")
                                    {
                                        flag = true;
                                    }
                                    else
                                    {
                                        var Error = doc1.Descendants("status").Select(x => new
                                        {
                                            _message1 = (string)x.Attribute("code").Parent.Value
                                        }).ToList();
                                        flag = false;
                                        Dictionary<string, object> error = new Dictionary<string, object>();
                                        error.Add("ErrorCode", "-1");
                                        error.Add("ErrorMessage", Error[0]._message1);
                                        return Json(error, JsonRequestBehavior.AllowGet);
                                    }

                                }
                                else
                                {
                                    if (status[0]._status == "ERROR")
                                    {
                                        var Error = doc.Descendants("status").Select(x => new
                                        {
                                            _message = (string)x.Attribute("code").Parent.Value
                                        }).ToList();
                                        flag = false;
                                        Dictionary<string, object> error = new Dictionary<string, object>();
                                        error.Add("ErrorCode", "-1");
                                        error.Add("ErrorMessage", Error[0]._message);
                                        return Json(error, JsonRequestBehavior.AllowGet);
                                    }
                                    flag = false;
                                }
                            }
                        


                        }
                        if (flag)
                        {
                            scope.Complete();
                            Dictionary<string, object> Msg = new Dictionary<string, object>();
                            Msg.Add("MsgCode", "1");
                            return Json(Msg, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            Dictionary<string, object> error = new Dictionary<string, object>();
                            error.Add("ErrorCode", "-1");
                            error.Add("ErrorMessage", "");
                            return Json(error, JsonRequestBehavior.AllowGet);
                        }


                    }
                    catch (Exception ex)
                    {
                        Dictionary<string, object> error = new Dictionary<string, object>();
                        error.Add("ErrorCode", "-1");
                        error.Add("ErrorMessage", "");
                        return Json(error, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                Dictionary<string, object> error = new Dictionary<string, object>();
                error.Add("ErrorCode", "-1");
                error.Add("ErrorMessage", "");
                return Json(error, JsonRequestBehavior.AllowGet);
            }

        }
        #endregion

        #region CashTransaction

        public List<CaseTransaction> CaseTransactionData()
        {
            var TransactionData = RunQuery<CaseTransaction>("Exec [dbo].[GetCashTransactionStatistics] {0}", 7).ToList();
            return TransactionData;
        }

        public DbRawSqlQuery<T> RunQuery<T>(string query, params object[] parameters)
        {
            return db.Database.SqlQuery<T>(query, parameters);
        }

        public List<CaseTransactionDetails> getLatestCashTranscationData()
        {
            var TransactionDetails = (from i in db.tblCashTransactions
                                      join j in db.tblCashTransactionOrdersMapping on i.id equals j.CashTransactionId
                                      orderby i.id descending
                                      select new
                                      {
                                          OrderId = j.OrderId,
                                          UserName = i.User,
                                          Amount = i.Amount,
                                          Currency = i.Currency,
                                          TransactionTypeId = i.TransactionTypeId,
                                          CreatedDate = i.Created,
                                          Balance = i.Balance,

                                      }).AsEnumerable().Select(s => new CaseTransactionDetails
                                      {

                                          OrderId = s.OrderId,
                                          UserName = s.UserName,
                                          Amount = s.Amount,
                                          Balance = s.Balance,
                                          Currency = s.Currency,
                                          TransactionType = db.tblCashTransactionTypes.Where(k => k.id == s.TransactionTypeId).Select(k => k.Name).FirstOrDefault(),
                                          CreatedDate = s.CreatedDate,

                                      }).Take(10);
            return TransactionDetails.ToList();
        }
        [Authorize]
        [HttpGet]
        public ActionResult GetCaseTransactionDetailsByDate(string date)
        {
            DateTime? createdate = Convert.ToDateTime(date);
            var TransactionDetails = (from i in db.tblCashTransactions
                                      join j in db.tblCashTransactionOrdersMapping on i.id equals j.CashTransactionId
                                      where DbFunctions.TruncateTime(i.Created) == DbFunctions.TruncateTime(createdate)
                                      select new
                                      {
                                          OrderId = j.OrderId,
                                          UserName = i.User,
                                          Amount = i.Amount,
                                          Currency = i.Currency,
                                          TransactionTypeId = i.TransactionTypeId,
                                          CreatedDate = i.Created,

                                      }).AsEnumerable().Select(s => new CaseTransactionDetails
                                      {

                                          OrderId = s.OrderId,
                                          UserName = s.UserName,
                                          Amount = s.Amount,
                                          Currency = s.Currency,
                                          TransactionType = db.tblCashTransactionTypes.Where(k => k.id == s.TransactionTypeId).Select(k => k.Name).FirstOrDefault(),
                                          CreatedDate = s.CreatedDate,

                                      });
            ViewBag.ResultDate = createdate;
            ViewBag.TotalCashHolder = null;
            ViewBag.TransactionData = null;
            ViewBag.TransactionDetailData = TransactionDetails;
            return View("CashTransaction");
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetFilterTransactionData(DateTime? FilterDate, string transactiontype)
        {
            try
            {
                List<tblCashTransactions> TransactionDetails = new List<tblCashTransactions>();
                if (FilterDate != null && transactiontype != "")
                {
                    int _type = Convert.ToInt32(transactiontype);
                    TransactionDetails = db.tblCashTransactions.Where(s => DbFunctions.TruncateTime(s.Created) == DbFunctions.TruncateTime(FilterDate) || s.TransactionTypeId == _type).ToList();
                }
                if (FilterDate != null)
                {
                    TransactionDetails = db.tblCashTransactions.Where(s => DbFunctions.TruncateTime(s.Created) == DbFunctions.TruncateTime(FilterDate)).ToList();
                }
                if (transactiontype != "")
                {
                    int _type = Convert.ToInt32(transactiontype);
                    TransactionDetails = db.tblCashTransactions.Where(s => s.TransactionTypeId == _type).ToList();
                }
                var OutputTransactionDetails = (from i in TransactionDetails
                                                join j in db.tblCashTransactionOrdersMapping on i.id equals j.CashTransactionId
                                                select new
                                                {
                                                    OrderId = j.OrderId,
                                                    UserName = i.User,
                                                    Amount = i.Amount,
                                                    Currency = i.Currency,
                                                    TransactionTypeId = i.TransactionTypeId,
                                                    CreatedDate = i.Created,
                                                    Balance = i.Balance,

                                                }).AsEnumerable().Select(s => new CaseTransactionDetails
                                                {

                                                    OrderId = s.OrderId,
                                                    UserName = s.UserName,
                                                    Amount = s.Amount,
                                                    Balance = s.Balance,
                                                    Currency = s.Currency,
                                                    TransactionType = db.tblCashTransactionTypes.Where(k => k.id == s.TransactionTypeId).Select(k => k.Name).FirstOrDefault(),
                                                    CreatedDate = s.CreatedDate,

                                                }).ToList();
                return Json(new { success = true, OutputTransactionDetails = OutputTransactionDetails });
            }
            catch (Exception ex)
            {
                return Json(new { success = false });
            }


        }


        #endregion

        #region Handlebackorders
        [Authorize]
        [HttpPost]
        public ActionResult ShipForFreeAction(string OrderId,string orderguid,string basketid,string PaidOrUnpaid)
        {
            try
            {

                // call Split splitOrder 
               
                var Token = GetToken();

                if(PaidOrUnpaid == "Unpaid")
                {
                    com.chilindo.beta.orders.APIOrders objAPI = new com.chilindo.beta.orders.APIOrders();
                    XmlNode node = objAPI.SplitOrder(Token, orderguid, basketid);

                    XDocument doc = XDocument.Parse(node.OuterXml.ToString());
                    var status = doc.Descendants("status").Select(x => new
                    {
                        _status = (string)x.Attribute("code").Value
                    }).ToList();

                    if (status[0]._status == "OK")
                    {
                        var neworderguid = doc.Descendants("order").Select(x => (string)x.Element("orderguid")).FirstOrDefault();
                        Thread.Sleep(9000);
                        var neworderid = db.tblOrders.Where(s => s.orderguid == neworderguid).Select(s => s.id).FirstOrDefault();
                        tblBackOrders objnew = new tblBackOrders();
                        objnew.OrderId = Convert.ToInt32(OrderId);
                        objnew.Reason = 1;
                        objnew.NewOrderId = neworderid;
                        objnew.Created = DateTime.Now;
                        db.tblBackOrders.Add(objnew);
                        db.SaveChanges();
                        Dictionary<string, object> Msg = new Dictionary<string, object>();
                        Msg.Add("MsgCode", "1");
                        return Json(Msg, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var Error = doc.Descendants("status").Select(x => new
                        {
                            _message = (string)x.Attribute("code").Parent.Value
                        }).ToList();
                        Dictionary<string, object> error = new Dictionary<string, object>();
                        error.Add("ErrorCode", "-1");
                        error.Add("ErrorMessage", Error[0]._message);
                        return Json(error, JsonRequestBehavior.AllowGet);


                    }
                }
                else 
                {
                    com.chilindo.beta.orders.APIOrders objAPI = new com.chilindo.beta.orders.APIOrders();
                    XmlNode node = objAPI.SplitOrder(Token, orderguid, basketid);

                    XDocument doc = XDocument.Parse(node.OuterXml.ToString());
                    var status = doc.Descendants("status").Select(x => new
                    {
                        _status = (string)x.Attribute("code").Value
                    }).ToList();

                    if (status[0]._status == "OK")
                    {
                        var neworderguid = doc.Descendants("order").Select(x => (string)x.Element("orderguid")).FirstOrDefault();
                        Thread.Sleep(9000);
                        var neworderid = db.tblOrders.Where(s => s.orderguid == neworderguid).FirstOrDefault();
                        tblBackOrders objnew = new tblBackOrders();
                        objnew.OrderId = Convert.ToInt32(OrderId);
                        objnew.Reason = 1;
                        objnew.NewOrderId = neworderid.id;
                        objnew.Created = DateTime.Now;
                        db.tblBackOrders.Add(objnew);
                        db.SaveChanges();
                        // call ShipPickUpOrder For PrePaid Order
                        
                        XmlNode node1 = objAPI.ShipPickUpOrder(Token, neworderid.id, neworderid.name, neworderid.lastname, neworderid.address, neworderid.zip, neworderid.province, neworderid.districtsub, neworderid.district, neworderid.phone);

                        XDocument doc1 = XDocument.Parse(node1.OuterXml.ToString());
                        var status1 = doc1.Descendants("status").Select(x => new
                        {
                            _status1 = (string)x.Attribute("code").Value
                        }).ToList();

                        if(status1[0]._status1 == "OK")
                        {
                            Dictionary<string, object> Msg = new Dictionary<string, object>();
                            Msg.Add("MsgCode", "1");
                            return Json(Msg, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            var Error = doc1.Descendants("status").Select(x => new
                            {
                                _message1 = (string)x.Attribute("code").Parent.Value
                            }).ToList();
                            Dictionary<string, object> error = new Dictionary<string, object>();
                            error.Add("ErrorCode", "-1");
                            error.Add("ErrorMessage", Error[0]._message1);
                            return Json(error, JsonRequestBehavior.AllowGet);
                        }
                        
                    }
                    else
                    {
                        var Error = doc.Descendants("status").Select(x => new
                        {
                            _message = (string)x.Attribute("code").Parent.Value
                        }).ToList();
                        Dictionary<string, object> error = new Dictionary<string, object>();
                        error.Add("ErrorCode", "-1");
                        error.Add("ErrorMessage", Error[0]._message);
                        return Json(error, JsonRequestBehavior.AllowGet);


                    }
                }
            
              
            }
            catch(Exception ex)
            {
                Dictionary<string, object> error = new Dictionary<string, object>();
                error.Add("ErrorCode", "-1");
                error.Add("ErrorMessage", "An error occurred, Please try again.");
                return Json(error, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult CancelAction(string OrderId, string orderguid, string basketid, string PaidOrUnpaid)
        {
            try
            {

                // call Split splitOrder 

                var Token = GetToken();

              
                    com.chilindo.beta.orders.APIOrders objAPI = new com.chilindo.beta.orders.APIOrders();
                    XmlNode node = objAPI.SplitOrder(Token, orderguid, basketid);

                    XDocument doc = XDocument.Parse(node.OuterXml.ToString());
                    var status = doc.Descendants("status").Select(x => new
                    {
                        _status = (string)x.Attribute("code").Value
                    }).ToList();

                    if (status[0]._status == "OK")
                    {
                        var neworderguid = doc.Descendants("order").Select(x => (string)x.Element("orderguid")).FirstOrDefault();
                        Thread.Sleep(9000);
                        var neworderid = db.tblOrders.Where(s => s.orderguid == neworderguid).FirstOrDefault();
                        tblBackOrders objnew = new tblBackOrders();
                        objnew.OrderId = Convert.ToInt32(OrderId);
                        objnew.Reason = 4;
                        objnew.NewOrderId = neworderid.id;
                        objnew.Created = DateTime.Now;
                        db.tblBackOrders.Add(objnew);
                        db.SaveChanges();
                        // call CancelOrder For paid or unpaid Order

                        XmlNode node1 = objAPI.CancelOrder(Token, neworderid.orderguid);

                        XDocument doc1 = XDocument.Parse(node1.OuterXml.ToString());
                        var status1 = doc1.Descendants("status").Select(x => new
                        {
                            _status1 = (string)x.Attribute("code").Value
                        }).ToList();

                        if (status1[0]._status1 == "OK")
                        {
                            Dictionary<string, object> Msg = new Dictionary<string, object>();
                            Msg.Add("MsgCode", "1");
                            return Json(Msg, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            var Error = doc1.Descendants("status").Select(x => new
                            {
                                _message1 = (string)x.Attribute("code").Parent.Value
                            }).ToList();
                            Dictionary<string, object> error = new Dictionary<string, object>();
                            error.Add("ErrorCode", "-1");
                            error.Add("ErrorMessage", Error[0]._message1);
                            return Json(error, JsonRequestBehavior.AllowGet);
                        }

                    }
                    else
                    {
                        var Error = doc.Descendants("status").Select(x => new
                        {
                            _message = (string)x.Attribute("code").Parent.Value
                        }).ToList();
                        Dictionary<string, object> error = new Dictionary<string, object>();
                        error.Add("ErrorCode", "-1");
                        error.Add("ErrorMessage", Error[0]._message);
                        return Json(error, JsonRequestBehavior.AllowGet);


                    }
               

            }
            catch
            {
                Dictionary<string, object> error = new Dictionary<string, object>();
                error.Add("ErrorCode", "-1");
                error.Add("ErrorMessage", "An error occurred, Please try again.");
                return Json(error, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}