﻿using CallingChilindo.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OfficeOpenXml;
using CallingChilindo.Models.Class;
using System.Web.Routing;
using System.Data.Entity.Infrastructure;

namespace CallingChilindo.Controllers
{
    public class UndeliveredCODController : Controller
    {
        private ChilindoCallingEntities db = new ChilindoCallingEntities();
        // GET: UndeliveredCOD
        public ActionResult ImportExcel()
        {
           
            //UndeliveredCOD
            var ErrorList = new List<UndeliveredCODDisp>();
            ErrorList =  TempData["ErrorList"] as List<UndeliveredCODDisp>;
            var model = new List<UndeliveredCODDisp>();
            var result = from parentObj in db.tblUndeliveredCODs
                         join carrier in db.tblCarriers on parentObj.CarrierID equals carrier.id
                         where parentObj.IsActive == true
                         orderby parentObj.CallBlockTime ascending select new { carriername = carrier.carrierName, parentObj = parentObj } ;
            foreach (var rec in result)
            {
                UndeliveredCODDisp obj = new UndeliveredCODDisp();
                obj.Invoice = rec.parentObj.Invoice;
                obj.aCommerreMark = rec.parentObj.CarrierRemark;
                obj.Address = rec.parentObj.CustomerAddress;
                obj.Customer = rec.parentObj.CustomerName;
                obj.Process = rec.parentObj.Process;
                obj.SerialNo = rec.parentObj.CarrierRef;
                obj.Tel = rec.parentObj.CustomerPhone;
                obj.FeedbackByClient = rec.parentObj.FeedbackByClient;
                obj.Dated = rec.parentObj.Dated;
                obj.CarrierName = rec.carriername;
                model.Add(obj);
            }
            ViewBag.carriers = new SelectList(db.tblCarriers.Where(s=>s.isCodAvailable == true).OrderBy(s => s.carrierName), "id", "carrierName");
            var codcarrie = db.tblUndeliveredCODs.Where(s => s.IsActive == true).Select(s => s.CarrierID).ToArray();
            ViewBag.carrierFilter = new SelectList(db.tblCarriers.Where(s => codcarrie.Contains(s.id)).OrderBy(s => s.carrierName), "id", "carrierName");
            ViewBag.UndeliveredCODs = model;
            ViewBag.ErrorList = ErrorList;
            ViewBag.CountErrorList = (ErrorList != null ? (ErrorList.Count > 0 ? 1 : 0) : 0);
            return View();
        }
        [Authorize]
        public ActionResult GetUndeliveredCODByCarrier(int? CarricerID, int ActionID)
        {
            //Update UndeliveredCOD
            if (ActionID == 2) {
                var OrderData = RunQuery<Int32>("Exec [dbo].[UndeliverCODArchiveBytblCarrier] {0}", CarricerID).ToList();
                //var obj = db.tblUndeliveredCODs.Where(s => s.CarrierID == CarricerID).ToList();
                //obj.ForEach(s => s.IsActive = false);
                //db.SaveChanges();
            } 
            //UndeliveredCOD                  
            var model = new List<UndeliveredCODDisp>();
            var result = from parentObj in db.tblUndeliveredCODs
                         join carrier in db.tblCarriers on parentObj.CarrierID equals carrier.id
                         where parentObj.IsActive == true && parentObj.CarrierID == CarricerID
                         orderby parentObj.CallBlockTime ascending
                         select new { carriername = carrier.carrierName, parentObj = parentObj };
            foreach (var rec in result)
            {
                UndeliveredCODDisp obj = new UndeliveredCODDisp();
                obj.Invoice = rec.parentObj.Invoice;
                obj.aCommerreMark = rec.parentObj.CarrierRemark;
                obj.Address = rec.parentObj.CustomerAddress;
                obj.Customer = rec.parentObj.CustomerName;
                obj.Process = rec.parentObj.Process;
                obj.SerialNo = rec.parentObj.CarrierRef;
                obj.Tel = rec.parentObj.CustomerPhone;
                obj.FeedbackByClient = rec.parentObj.FeedbackByClient;
                obj.Dated = rec.parentObj.Dated;
                obj.CarrierName = rec.carriername;
                model.Add(obj);
            }
            ViewBag.carriers = new SelectList(db.tblCarriers.Where(s => s.isCodAvailable == true).OrderBy(s => s.carrierName), "id", "carrierName");
            var codcarrie = db.tblUndeliveredCODs.Where(s => s.IsActive == true).Select(s => s.CarrierID).ToArray();
            ViewBag.carrierFilter = new SelectList(db.tblCarriers.Where(s => codcarrie.Contains(s.id)).OrderBy(s => s.carrierName), "id", "carrierName");
            ViewBag.UndeliveredCODs = model;
            ViewBag.ErrorList = null;
            ViewBag.CountErrorList = 0;
            return View("ImportExcel");
        }
        [HttpPost]
        public ActionResult ProcessImportExcel(FormCollection form)
        {
            var errorlist = new List<UndeliveredCODDisp>();
            try
            {
               
                if ((Request.Files["fuExcel"] != null) && (Request.Files["fuExcel"].ContentLength > 0) && !string.IsNullOrEmpty(Request.Files["fuExcel"].FileName))
                {
                    string fileName = Request.Files["fuExcel"].FileName;
                    string fileContentType = Request.Files["fuExcel"].ContentType;
                    byte[] fileBytes = new byte[Request.Files["fuExcel"].ContentLength];
                    var data = Request.Files["fuExcel"].InputStream.Read(fileBytes, 0, Convert.ToInt32(Request.Files["fuExcel"].ContentLength));
                    using (var package = new ExcelPackage(Request.Files["fuExcel"].InputStream))
                    {
                        var currentSheet = package.Workbook.Worksheets;
                        var workSheet = currentSheet.First();
                        var noOfCol = workSheet.Dimension.End.Column;
                        var noOfRow = workSheet.Dimension.End.Row;
                        var rowItr = 1;
                        if (workSheet.Cells[1, 1].Value.ToString().ToLower() == "invoice" || workSheet.Cells[1, 1].Value.ToString().ToLower() == "inv.") 
                        {
                            rowItr = 2;
                        }
                        
                        string Invoice = ""; string SerialNo = ""; string Order = ""; string Customer = ""; string Tel = "";
                        string Address = ""; string Process = ""; string aCommerRemark = ""; string FeedbackByClient = "";
                        string CarrierID = "";
                        string conn = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

                        string query = "";
                        SqlConnection con = new SqlConnection(conn);
                        SqlCommand cmd;
                        con.Open();
                        int Insertcount = 0, Updatecount = 0;
                        String guid = Guid.NewGuid().ToString(); // to use for checking which rows are inserted or updated and delete all other rows.
                        CarrierID = form["carrierid"] == "" ? "0" : form["carrierid"];
                        for (int rowIterator = rowItr; rowIterator <= noOfRow; rowIterator++)
                        {
                            try { Invoice = (workSheet.Cells[rowIterator, 1].Value.ToString().Length > 0 && workSheet.Cells[rowIterator, 1].Value.ToString() != "#N/A") ? workSheet.Cells[rowIterator, 1].Value.ToString() : getInvoice(workSheet.Cells[rowIterator, 2].Value.ToString()); }catch (Exception xp) { Invoice = "0"; }
                            try { SerialNo = workSheet.Cells[rowIterator, 2].Value.ToString(); } catch (Exception xp) { SerialNo = ""; }                           
                            try { Customer = workSheet.Cells[rowIterator, 3].Value.ToString(); } catch (Exception xp) { Customer = ""; }
                            try { Tel = workSheet.Cells[rowIterator, 4].Value.ToString(); } catch (Exception xp) { Tel = ""; }
                            try { Address = workSheet.Cells[rowIterator, 5].Value.ToString(); } catch (Exception xp) { Address = ""; }
                            try { Process = workSheet.Cells[rowIterator, 6].Value.ToString(); } catch (Exception xp) { Process = ""; }
                            try { aCommerRemark = workSheet.Cells[rowIterator, 7].Value.ToString(); } catch (Exception xp) { aCommerRemark = ""; }
                            try { FeedbackByClient = workSheet.Cells[rowIterator, 8].Value.ToString(); } catch (Exception xp) { FeedbackByClient = ""; }

                            //if (Invoice == "0" || Invoice == "" || Invoice.Length < 6) { Invoice = Order.Substring(Order.IndexOf("I") + 1); }
                            //if (SerialNo == "0" || SerialNo == "" || SerialNo.Length < 6) { Invoice = Order.Substring(Order.IndexOf("I") + 1); }
                            if((Invoice == "0" || Invoice == "" || Invoice.Length < 6) || (SerialNo == "0" || SerialNo == "" || SerialNo.Length < 6))
                            {
                                errorlist.Add(new UndeliveredCODDisp
                                {

                                    Invoice = Convert.ToInt32(Invoice),
                                    SerialNo = SerialNo,
                                    Customer = Customer,
                                    Tel = Tel,
                                    Address = Address,
                                    Process = Process,
                                    aCommerreMark = aCommerRemark,
                                    FeedbackByClient = FeedbackByClient,
                                    Dated = DateTime.Now.AddMinutes(-30),                                   
                                });
                            }
                            else
                            {
                                if (Tel.Length == 9 && Tel.ToCharArray().First().ToString() != "0") { Tel = "0" + Tel; }
                                if (Tel.Length < 9)
                                {
                                    cmd = new SqlCommand("select top 1 phone from tblOrders where id = " + Invoice, con);
                                    Tel = cmd.ExecuteScalar().ToString();
                                }
                                try
                                {
                                    if (Tel.Length > 0)

                                    {
                                        //query = String.Format("INSERT INTO TBLUNDELIVEREDCODS(INVOICE,SERIALNO,[ORDER],CUSTOMER,TEL,ADDRESS,PROCESS,ACOMMERREMARK,FEEDBACKBYCLIENT,DATED) VALUES({0},{1},'{2}',N'{3}','{4}',N'{5}','{6}',N'{7}',N'{8}',GETDATE());", Invoice, SerialNo, Order, Customer, Tel, Address, Process, aCommerRemark, aCommerRemark, FeedbackByClient);
                                        query = String.Format("Declare @carrierid int;Declare @Isinsert int; " +
                                                               "IF NOT EXISTS (select top 1 * from tblUndeliveredCODs WHERE invoice = {0})"
                                                                + " BEGIN insert into tblUndeliveredCODs"
                                                                + " (Invoice, CarrierRef,CustomerName,CustomerPhone,CustomerAddress,Process,CarrierRemark, feedbackByClient,Dated,CallBlockTime,guid,CarrierID,IsActive)"
                                                                + " VALUES({0},N'{1}',N'{2}',N'{3}',N'{4}',N'{5}',N'{6}',N'{7}',GETDATE(),DATEADD(minute, -30, GETDATE()),N'{8}', N'{9}',1)"
                                                                + " set @Isinsert = 1; END "
                                                                + " ELSE BEGIN "
                                                                + " select @carrierid = CarrierID from  tblUndeliveredCODs  WHERE invoice = {0} "
                                                                + " if (@carrierid = N'{9}') BEGIN "
                                                                + " update tblUndeliveredCODs"
                                                                + " set CarrierRemark = N'{6}', Dated = GETDATE(), guid = N'{8}', IsActive = 1 WHERE invoice = {0} and CarrierID=N'{9}' "
                                                                + " set @Isinsert = 0; end "
                                                                + " else begin "
                                                                + " update tblUndeliveredCODs"
                                                                + " set CarrierRemark = N'{6}', Dated = GETDATE(), guid = N'{8}',CarrierID=N'{9}', IsActive = 1 WHERE invoice = {0} "
                                                                + " set @Isinsert = 0; end "
                                                                + " END "
                                                                + " select @Isinsert "
                                                          , Invoice, SerialNo, Customer, Tel, Address, Process, aCommerRemark, FeedbackByClient, guid, CarrierID);
                                        DataTable dt = new DataTable();
                                        cmd = new SqlCommand(query, con);
                                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                                        da.Fill(dt);
                                        da.Dispose();
                                        if (dt.Rows[0][0].ToString() == "1")
                                        {
                                            //insert count
                                            Insertcount = Insertcount + 1;
                                        }
                                        else if (dt.Rows[0][0].ToString() == "0")
                                        {
                                            //update count
                                            Updatecount = Updatecount + 1;
                                        }

                                        query = String.Format("UPDATE tblUndeliveredCODs SET  IsActive = 0 WHERE ISNULL(guid, '') NOT LIKE N'{0}' and CarrierID=N'{1}'", guid, CarrierID);

                                        try
                                        {
                                            cmd = new SqlCommand(query, con);
                                            cmd.ExecuteNonQuery();
                                        }
                                        catch (Exception xp) { TempData["Error"] = "Could not delete old records. Please try again"; }
                                    }
                                }

                                catch (Exception xp)
                                {
                                    errorlist.Add(new UndeliveredCODDisp
                                    {

                                        Invoice = Convert.ToInt32(Invoice),
                                        SerialNo = SerialNo,
                                        Customer = Customer,
                                        Tel = Tel,
                                        Address = Address,
                                        Process = Process,
                                        aCommerreMark = aCommerRemark,
                                        FeedbackByClient = FeedbackByClient,
                                        Dated = DateTime.Now.AddMinutes(-30),

                                    });
                                    /*TempData["Error"] = "An error occurred, please try again." + noOfCol + ", " + noOfRow + ", " + rowIterator; */
                                }

                            }
                            

                        }

                        
                        con.Close(); 
                        TempData["Message"] = "Undelievered COD " + Insertcount  +" new and " + Updatecount  + " existing order(s) imported successfully." ;
                    }
                }
                }
            catch (Exception xp) { TempData["Error"] = "An error occurred, please try again k"; }
            TempData["ErrorList"] = errorlist;
            //RouteValueDictionary dict = new RouteValueDictionary();
            //dict.Add("ErrorList", errorlist);
            return RedirectToAction("ImportExcel");
        }
        private string getInvoice(string oId)
        {
            return "0"; 
            //try
            //{
            //    return (db.tblOrders.Where(o => o.trackingcode== oId && o.orderstatusid == 31).First()).id.ToString();
            //}
            //catch (Exception xp) { return ""; }
        }
        // GET: UndeliveredCOD
        [Authorize]
        [HttpPost]
        public ActionResult ExportExcel(FormCollection form)
        {
            bool chkSpecific = form["chkSpecific"] == "on" ? true : false;
            ExcelPackage pck = new ExcelPackage();
            string fileName = "Chilindo-UndeliveredCOD-Report_" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString().PadLeft(2, '0') + "-" + DateTime.Now.Day.ToString().PadLeft(2, '0');
            var ws = pck.Workbook.Worksheets.Add(fileName.Replace("Chilindo-UndeliveredCOD-Report_", ""));
            var result = chkSpecific ? 
                         (from parentObj in db.tblUndeliveredCODs
                          join carrier in db.tblCarriers on parentObj.CarrierID equals carrier.id into gj
                          from masterObj in gj.DefaultIfEmpty()
                          where (parentObj.FeedbackByClient.Contains("Cancel") || parentObj.FeedbackByClient.Contains("Reschedule") ) && parentObj.IsActive == true orderby parentObj.Invoice
                          select new { carriername = masterObj.carrierName, parentObj = parentObj }) 
                          : 
                          (from parentObj in db.tblUndeliveredCODs join carrier in db.tblCarriers on parentObj.CarrierID equals carrier.id into gj
                           from masterObj in gj.DefaultIfEmpty()
                           where parentObj.IsActive == true
                           orderby parentObj.Invoice
                           select new { carriername = masterObj.carrierName, parentObj = parentObj });

            bool flag = true;
            foreach (var rec in result)
            {
                if (flag) {
                    ws.Cells["A1"].Value = "Invoice";ws.Cells["A1"].Style.Font.Bold = true;
                    ws.Cells["B1"].Value = "CarrierRef"; ws.Cells["B1"].Style.Font.Bold = true;
                    //ws.Cells["C1"].Value = "Order"; ws.Cells["C1"].Style.Font.Bold = true;
                    ws.Cells["C1"].Value = "CustomerName"; ws.Cells["C1"].Style.Font.Bold = true;
                    ws.Cells["D1"].Value = "CustomerPhone"; ws.Cells["D1"].Style.Font.Bold = true;
                    ws.Cells["E1"].Value = "CustomerAddress"; ws.Cells["E1"].Style.Font.Bold = true;
                    ws.Cells["F1"].Value = "Process"; ws.Cells["F1"].Style.Font.Bold = true;
                    ws.Cells["G1"].Value = "CarrierRemark"; ws.Cells["G1"].Style.Font.Bold = true;
                    ws.Cells["H1"].Value = "FeedbackByClient"; ws.Cells["H1"].Style.Font.Bold = true;
                    ws.Cells["I1"].Value = "CarrierName"; ws.Cells["I1"].Style.Font.Bold = true;
                }
                flag = false;
            }
            int i = 2;
            foreach (var rec in result)
            {
                ws.Cells["A"+i].Value = rec.parentObj.Invoice;
                ws.Cells["B" + i].Value = rec.parentObj.CarrierRef;
                //ws.Cells["C" + i].Value =rec.parentObj.TrackingCode;
                ws.Cells["C" + i].Value = rec.parentObj.CustomerName;
                ws.Cells["D" + i].Value = rec.parentObj.CustomerPhone;
                ws.Cells["E" + i].Value = rec.parentObj.CustomerAddress;
                ws.Cells["F" + i].Value = rec.parentObj.Process;
                ws.Cells["G" + i].Value = rec.parentObj.CarrierRemark;
                ws.Cells["H" + i].Value = rec.parentObj.FeedbackByClient;
                ws.Cells["I" + i].Value = rec.carriername;
                i++;
            }

            Response.Clear();
            Response.ClearHeaders();
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            
            Response.AddHeader("content-disposition", "attachment;  filename="+ fileName + ".xlsx");
            Response.End();
            TempData["Message"] = "Undelievered COD order(s) with status cancel/reschedule are exported successfully.";
            return RedirectToAction("ImportExcel");
        }

        public ActionResult Calling()
        {
            return View();
        }
        public DbRawSqlQuery<T> RunQuery<T>(string query, params object[] parameters)
        {
            return db.Database.SqlQuery<T>(query, parameters);
        }
    }
    
}