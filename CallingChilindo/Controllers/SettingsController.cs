﻿using CallingChilindo.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CallingChilindo.Controllers
{
    public class SettingsController : Controller
    {
        //private UsersContext dbs = new UsersContext();
        private ChilindoCallingEntities db = new ChilindoCallingEntities();
        //
        // GET: /Settings/
        [Authorize]
        public ActionResult Index()
        {
            var userStore = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var manager = new UserManager<ApplicationUser>(userStore);
            var currentUser = manager.FindById(User.Identity.GetUserId());
            bool? isPrecedence2C2P=false;
            int settingCallBlockTime = 30;
            int? settingWaitFirstCall = 720;
            int settingWaitFirstCall2C2P = 10;
            int callType =1;
            if (!currentUser.isCaller)
            {
                try
                {
                    var model = new List<tblSettings>();
                    var result = from parentObj in db.tblSettings orderby parentObj.settingId select parentObj;
                    foreach (var rec in result)
                    {
                        tblSettings st = new tblSettings();
                        st.settingId = rec.settingId;
                        st.precedence2C2P = rec.precedence2C2P;
                        st.firstCallWait = rec.firstCallWait;
                        st.numberofTries = rec.numberofTries;
                        st.firstRecall = rec.firstRecall;
                        st.secondRecall = rec.secondRecall;
                        st.thirdRecall = rec.thirdRecall;
                        st.fourthRecall = rec.fourthRecall;
                        st.fifthRecall = rec.fifthRecall;
                        st.sixthRecall = rec.sixthRecall;
                        st.seventhRecall = rec.seventhRecall;
                        st.eighthRecall = rec.eighthRecall;
                        st.ninthRecall = rec.ninthRecall;
                        st.tenthRecall = rec.tenthRecall;
                        st.eleventhRecall = rec.eleventhRecall;
                        st.twelvthRecall = rec.twelvthRecall;
                        st.thirteenthRecall = rec.thirteenthRecall;
                        st.fourteenthRecall = rec.fourteenthRecall;
                        st.fifteenthRecall = rec.fifteenthRecall;
                        st.sixteenthRecall = rec.sixteenthRecall;
                        st.seventeenthRecall = rec.seventeenthRecall;
                        st.eighteenthRecall = rec.eighteenthRecall;
                        st.nineteenthRecall = rec.nineteenthRecall;
                        st.waitPayLater = rec.waitPayLater;
                        st.delay2C2P = rec.delay2C2P;
                        st.nCancelWrongNumberOrderAfterDays = rec.nCancelWrongNumberOrderAfterDays;
                        st.nCancelUnPickedUpOrderAfterDays = rec.nCancelUnPickedUpOrderAfterDays;
                        st.nAutoLogoutValue = rec.nAutoLogoutValue;
                        st.callblocktime = rec.callblocktime;
                        st.cancelOrderRetries = rec.cancelOrderRetries;

                        //Stats Data
                        isPrecedence2C2P=rec.precedence2C2P;
                        settingWaitFirstCall= rec.firstCallWait;
                        settingWaitFirstCall2C2P=rec.delay2C2P;
                        model.Add(st);
                    }
                    ViewBag.Settings = model;
                }
                catch (Exception e)
                {
                    TempData["Error"] = "An error occurred, Please try again ";
                }
                try {
                    ChilindoCallingEntities objChilindo = new ChilindoCallingEntities();
                    int Orders = Convert.ToInt32(RunQuery<int>("Exec [dbo].[getCountAvailableOrders] {0}, {1}, {2},{3}, {4}", isPrecedence2C2P, settingCallBlockTime, settingWaitFirstCall, settingWaitFirstCall2C2P, callType).FirstOrDefault());
                    //int Orders = Convert.ToInt32(objChilindo.getCountAvailableOrders(isPrecedence2C2P, settingCallBlockTime, settingWaitFirstCall, settingWaitFirstCall2C2P, callType).FirstOrDefault());
                    ViewBag.Orders = Orders;
                }
                catch (Exception xp) { 
            
                }
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Error");
            }
        }
        public DbRawSqlQuery<T> RunQuery<T>(string query, params object[] parameters)
        {
            return db.Database.SqlQuery<T>(query, parameters);
        }
        [Authorize]
        public ActionResult UndeliveredCOD()
        {
            var userStore = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var manager = new UserManager<ApplicationUser>(userStore);
            var currentUser = manager.FindById(User.Identity.GetUserId());

            if (!currentUser.isCaller)
            {
                try
                {
                    var model = new List<tblSettings>();
                    var result = from parentObj in db.tblSettings orderby parentObj.settingId select parentObj;
                    foreach (var rec in result)
                    {
                        tblSettings st = new tblSettings();
                        st.settingId = rec.settingId;
                        st.UCODTimeToWaitBeforeCall = rec.UCODTimeToWaitBeforeCall;
                        st.UCODTimeToWaitBeforeRecall = rec.UCODTimeToWaitBeforeRecall;
                        model.Add(st);
                    }
                    ViewBag.Settings = model;
                }
                catch (Exception e)
                {
                    TempData["Error"] = "An error occurred, Please try again ";
                }

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Error");
            }
        }
        [Authorize]
        [HttpPost]
        public ActionResult SaveUCOD(FormCollection form)
        {
            try
            {
                int UCODTimeToWaitBeforeCall = Convert.ToInt32(form["UCODTimeToWaitBeforeCall"].ToString());
                int UCODTimeToWaitBeforeRecall = Convert.ToInt32(form["UCODTimeToWaitBeforeRecall"].ToString());
                tblSettings st = db.tblSettings.Find(1);
                if (st != null)
                {
                    st.UCODTimeToWaitBeforeCall = Convert.ToInt32(UCODTimeToWaitBeforeCall);
                    st.UCODTimeToWaitBeforeRecall = Convert.ToInt32(UCODTimeToWaitBeforeRecall);
                    db.Entry(st).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["Message"] = "Settings Updated Successfully";
                }
               return RedirectToAction("UndeliveredCOD");
            }
            catch (Exception e)
            {
                TempData["Error"] = "An error occurred, Please try again ";
                RedirectToAction("UndeliveredCOD");
            }

            return RedirectToAction("UndeliveredCOD");

        }
        [Authorize]
        [HttpPost]
        public ActionResult Save(FormCollection form)
        {
            try
            {
                bool chk2C2Pbool = form["chk2C2P"] == "on" ? true : false;
                int txtnumberofTries = Convert.ToInt32(form["txtnumberofTries"].ToString());
                int delay2C2P = Convert.ToInt32(form["txt2c2pdelay"].ToString());
                int txtfirstCallWait = Convert.ToInt32(form["txtfirstCallWait"].ToString());
                int txtfirstRecall = Convert.ToInt32(form["txtfirstRecall"].ToString());
                int txtsecondRecall = Convert.ToInt32(form["txtsecondRecall"].ToString());
                int txtthirdRecall = Convert.ToInt32(form["txtthirdRecall"].ToString());
                int txtfourthRecall = Convert.ToInt32(form["txtfourthRecall"].ToString());
                int txtfifthRecall = Convert.ToInt32(form["txtfifthRecall"].ToString());
                int txtsixthRecall = Convert.ToInt32(form["txtsixthRecall"].ToString());
                int txtseventhRecall = Convert.ToInt32(form["txtseventhRecall"].ToString()); 
                int txteighthRecall = Convert.ToInt32(form["txteighthRecall"].ToString());
                int txtninthRecall = Convert.ToInt32(form["txtninthRecall"].ToString());
                int txttenthRecall = Convert.ToInt32(form["txttenthRecall"].ToString());
                int txteleventhRecall = Convert.ToInt32(form["txteleventhRecall"].ToString());
                
                int txtthirteenthRecall = Convert.ToInt32(form["txtthirteenthRecall"].ToString());
                int txtfourteenthRecall = Convert.ToInt32(form["txtfourteenthRecall"].ToString());
                int txtfifteenthRecall = Convert.ToInt32(form["txtfifteenthRecall"].ToString());
                int txtsixteenthRecall = Convert.ToInt32(form["txtsixteenthRecall"].ToString());
                int txtseventeenthRecall = Convert.ToInt32(form["txtseventeenthRecall"].ToString());
                int txteighteenthRecall = Convert.ToInt32(form["txteighteenthRecall"].ToString());
                int txtnineteenthRecall = Convert.ToInt32(form["txtnineteenthRecall"].ToString());
                int txttwelvthRecall = Convert.ToInt32(form["txteltwelvthRecall"].ToString());
            

                int txtwaitPayLater = Convert.ToInt32(form["txtwaitPayLater"].ToString());
                int txt2c2pdelay = Convert.ToInt32(form["txt2c2pdelay"].ToString());
                int txtautologoutafter = Convert.ToInt32(form["txtautologoutafter"].ToString());
                int txtcancelwrongnumberorderafterdays = Convert.ToInt32(form["txtcancelwrongnumberorderafterdays"].ToString());
                int txtcancelunpickeduporderafterdays = Convert.ToInt32(form["txtcancelunpickeduporderafterdays"].ToString());
                int callblocktime = Convert.ToInt32(form["txtcallBlockTime"].ToString());
                int cancelOrderRetries = Convert.ToInt32(form["txtcancelOrderRetries"].ToString());
                tblSettings st = db.tblSettings.Find(1);
                if (st != null)
                {
                    st.precedence2C2P = chk2C2Pbool;
                    st.numberofTries = txtnumberofTries;
                    st.firstCallWait = Convert.ToInt32( txtfirstCallWait);
                    st.numberofTries = Convert.ToInt32(txtnumberofTries);
                    st.firstRecall = Convert.ToInt32(txtfirstRecall);
                    st.secondRecall = Convert.ToInt32(txtsecondRecall);
                    st.thirdRecall = Convert.ToInt32(txtthirdRecall);
                    st.fourthRecall = Convert.ToInt32(txtfourthRecall);
                    st.fifthRecall = Convert.ToInt32(txtfifthRecall);
                    st.sixthRecall = Convert.ToInt32(txtsixthRecall);
                    st.seventhRecall = Convert.ToInt32(txtseventhRecall);
                    st.eighthRecall = Convert.ToInt32(txteighthRecall);
                    st.ninthRecall = Convert.ToInt32(txtninthRecall);
                    st.tenthRecall = Convert.ToInt32(txttenthRecall);
                    st.eleventhRecall = Convert.ToInt32(txteleventhRecall);
                    st.twelvthRecall = Convert.ToInt32(txttwelvthRecall);
                    st.thirteenthRecall = Convert.ToInt32(txtthirteenthRecall);
                    st.fourteenthRecall = Convert.ToInt32(txtfourteenthRecall);
                    st.fifteenthRecall = Convert.ToInt32(txtfifteenthRecall);
                    st.sixteenthRecall = Convert.ToInt32(txtsixteenthRecall);
                    st.seventeenthRecall = Convert.ToInt32(txtseventeenthRecall);
                    st.eighteenthRecall = Convert.ToInt32(txteighteenthRecall);
                    st.nineteenthRecall = Convert.ToInt32(txtnineteenthRecall);
                    st.delay2C2P = Convert.ToInt32(txt2c2pdelay);
                    st.nAutoLogoutValue = Convert.ToInt32(txtautologoutafter);
                    st.waitPayLater = Convert.ToInt32(txtwaitPayLater);
                    st.nCancelWrongNumberOrderAfterDays = Convert.ToInt32(txtcancelwrongnumberorderafterdays);
                    st.nCancelUnPickedUpOrderAfterDays = Convert.ToInt32(txtcancelunpickeduporderafterdays);
                    st.callblocktime = Convert.ToInt32(callblocktime);
                    st.cancelOrderRetries = Convert.ToInt32(cancelOrderRetries);
                    db.Entry(st).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["Message"] = "Settings Updated Successfully";
                }
               return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                TempData["Error"] = "An error occurred, Please try again "; RedirectToAction("Index");
            }

            return RedirectToAction("Index");

        }
    }
}