﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CallingChilindo.Models;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;



namespace CallingChilindo.Controllers
{
    public class CallerController : Controller
    {
        private ChilindoCallingEntities db = new ChilindoCallingEntities();
        private ChilindoCallingEntities dbUser = new ChilindoCallingEntities();
        [Authorize]
        public ActionResult Index()
        {
            var userStore = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var manager = new UserManager<ApplicationUser>(userStore);
            var currentUser = manager.FindById(User.Identity.GetUserId());

            if (!currentUser.isCaller)
            {
                var Model = new List<Users>();
                var users = from parentObj in db.AspNetUsers
                            select new { Users = parentObj };
                foreach (var item in users)
                {
                    try
                    {
                        Users user = new Users();
                        user.sIdGuid = item.Users.Id;
                        user.UserName = item.Users.FirstName + " " + item.Users.LastName;
                        user.isActive = item.Users.isActive;
                        user.isCaller = item.Users.isCaller;
                        user.CallNewest = item.Users.CallNewest;
                        try
                        {
                            user.dtSignintime = dbUser.tblUsersLogs.Where(s => s.nId == item.Users.Id).OrderByDescending(s => s.dtSignInTime).Take(1).FirstOrDefault().dtSignInTime;
                        }
                        catch (Exception xp) { user.dtSignintime = null; }
                        Model.Add(user);
                    }
                    catch (Exception xp) { }
                }


                return View(Model.ToList());
            }
            else
            {
                return RedirectToAction("Index", "Error"); 
            }
        }
        [Authorize]
        [HttpPost]
        public ActionResult SetActive(string ID)
        {
            try
            {
                var user = db.AspNetUsers.Where(x => x.Id == ID).FirstOrDefault();
                user.isActive = !user.isActive;
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                Dictionary<string, object> Msg = new Dictionary<string, object>();
                Msg.Add("MsgCode", "1");
                Msg.Add("Message", "Caller Status updated successfully.");
                return Json(Msg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception xp) {
                Dictionary<string, object> Msg = new Dictionary<string, object>();
                Msg.Add("MsgCode", "0");
                Msg.Add("Message", "Caller Status could not be updated.");
                return Json(Msg, JsonRequestBehavior.AllowGet);
            }
            
        }
        [Authorize]
        public ActionResult SetAdmin(string ID)
        {
            try
            {
                var user = db.AspNetUsers.Where(x => x.Id == ID).FirstOrDefault();
                user.isCaller = !user.isCaller;
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                Dictionary<string, object> Msg = new Dictionary<string, object>();
                Msg.Add("MsgCode", "1");
                Msg.Add("Message", "Caller Status updated successfully.");
                return Json(Msg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception xp)
            {
                Dictionary<string, object> Msg = new Dictionary<string, object>();
                Msg.Add("MsgCode", "0");
                Msg.Add("Message", "Caller Status could not be updated.");
                return Json(Msg, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SetCallNewest (string ID)
        {
            try
            {
                var user = db.AspNetUsers.Where(x => x.Id == ID).FirstOrDefault();
                user.CallNewest = !user.CallNewest;
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                Dictionary<string, object> Msg = new Dictionary<string, object>();
                Msg.Add("MsgCode", "1");
                Msg.Add("Message", "Caller Status updated successfully.");
                return Json(Msg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception xp)
            {
                Dictionary<string, object> Msg = new Dictionary<string, object>();
                Msg.Add("MsgCode", "0");
                Msg.Add("Message", "Caller Status could not be updated.");
                return Json(Msg, JsonRequestBehavior.AllowGet);
            }
        }
    }

}
