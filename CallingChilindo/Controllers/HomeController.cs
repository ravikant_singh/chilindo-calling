﻿using CallingChilindo.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Net;
using System.IO;
using MaccadkProj;
using System.Xml;
using System.Data.Entity.Infrastructure;
using CallingChilindo.Models.Class;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Hosting;
using System.Net.Http;
using System.Text;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;
namespace CallingChilindo.Controllers
{
    public class HomeController : Controller
    {
        private ChilindoCallingEntities db = new ChilindoCallingEntities();
        private maccadkEntities entities = new maccadkEntities();
        [Authorize]
        public ActionResult Welcome()
        {
            if(Session["Message"] != null)
            {
                TempData["Message"] = Session["Message"].ToString();
                Session["Message"] = null;
            }
            var codcarrie = db.tblUndeliveredCODs.Where(s => s.IsActive == true).Select(s => s.CarrierID).ToArray();
            ViewBag.carrierFilter = new SelectList(db.tblCarriers.Where(s => codcarrie.Contains(s.id)).OrderBy(s => s.carrierName), "id", "carrierName");
            return View();
        }

        public ActionResult Index()
        {
            Response.Redirect("/Account/Login?ReturnUrl=%2FHome%2FDashboard");
            return View();
        }
        [Authorize]
        [HttpPost()]
        [ValidateAntiForgeryToken()]
        public ActionResult Index(tblUsers user)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    var retreivedUser = db.AspNetUsers.Where(a => a.Email == user.sEmail && a.PasswordHash == user.sPassword).FirstOrDefault();
                    if (retreivedUser != null)
                    {
                        Session["LogedUserID"] = retreivedUser.Id.ToString();
                        Session["LogedUserFullname"] = retreivedUser.FirstName.ToString() + " " + retreivedUser.LastName.ToString();
                        return RedirectToAction("Dashboard", "Home");
                    }
                    else
                    {
                        TempData["Error"] = "Login Failed";
                        return View();
                    }

                }

            }
            catch (Exception xp)
            {
                TempData["Error"] = "Login Failed";
            }
            return View();
        }

        [Authorize]
        public ActionResult Dashboard(int? callType,int?carrierId)
        {
            if (callType == null)
                callType = 1;
            if (carrierId == null)
                carrierId = 0;
            string apiToken = "";
            try
            {
                apiToken = GetLogin();
                #region Init intializing and setting variable about AutoLog out for example if there is 15 mins inactivity then after 15 mins show popup and auto loggout
                int miliSecToLogout = 60000 * db.tblSettings.Find(1).nAutoLogoutValue;
                ViewBag.miliSecToLogout = miliSecToLogout;
                ViewBag.UserName = Session["LogedUserFullname"];
                ViewBag.Message = "Welcome to Calling Chilindo.";
                int? _accountid = 0;
                int nCallBlockTime = db.tblSettings.Find(1).callblocktime;
                int? nFirstCallWait = db.tblSettings.Find(1).firstCallWait;
                int nDelay2C2P = db.tblSettings.Find(1).delay2C2P;
                //Check if the debit card orders to process first 2C2p=debot card
                bool? precedence2C2P = db.tblSettings.First().precedence2C2P;
                int orderCurrentId = 0;
                string orderCurrentIdFormultiple = "";
                #endregion
                #region Load Options for the calling
                var modelLg = new List<LogEvents>();
                if (callType == 1)
                {
                    #region UnPaid Call option
                    var resultLg = from logEventsMapping in db.LogEventsMapping
                                   join logEvent in db.LogEvents
                                   on logEventsMapping.LogEventsId equals logEvent.ID
                                   join callingType in db.CallingTypes
                                   on logEventsMapping.CallingTypeId equals callingType.id
                                   where callingType.id == 1//unpaidOrders
                                   select logEvent;
                    foreach (var rec in resultLg)
                    {
                        LogEvents lg = new LogEvents();
                        lg.ID = rec.ID;
                        lg.type = rec.type;
                        modelLg.Add(lg);
                    }
                    #endregion
                }
                else if (callType == 2)
                {
                    #region Undelievered Call option
                    var resultLg = from logEventsMapping in db.LogEventsMapping
                                   join logEvent in db.LogEvents
                                   on logEventsMapping.LogEventsId equals logEvent.ID
                                   join callingType in db.CallingTypes
                                   on logEventsMapping.CallingTypeId equals callingType.id
                                   where callingType.id == 2//unpaidOrders
                                   select logEvent;
                    foreach (var rec in resultLg)
                    {
                        LogEvents lg = new LogEvents();
                        lg.ID = rec.ID;
                        lg.type = rec.type;
                        modelLg.Add(lg);
                    }
                    #endregion
                }
                else if (callType == 4)
                {
                    #region UnPaid Call option
                    var resultLg = from logEventsMapping in db.LogEventsMapping
                                   join logEvent in db.LogEvents
                                   on logEventsMapping.LogEventsId equals logEvent.ID
                                   join callingType in db.CallingTypes
                                   on logEventsMapping.CallingTypeId equals callingType.id
                                   where callingType.id == 4//suspiciousCODs
                                   select logEvent;
                    foreach (var rec in resultLg)
                    {
                        LogEvents lg = new LogEvents();
                        lg.ID = rec.ID;
                        lg.type = rec.type;
                        modelLg.Add(lg);
                    }
                    #endregion
                }

                ViewBag.LogEvents = modelLg;
                #endregion
                #region Fetch Orders
                string s = User.Identity.GetUserId();
                //ChilindoCallingEntities objEntities = new ChilindoCallingEntities();
                //var OrderData = objEntities.getOrdersToCall(User.Identity.GetUserId(), precedence2C2P, nCallBlockTime, nFirstCallWait, nDelay2C2P, callType).ToList();

                
                List<AddressControlObject> addressObiList = new List<AddressControlObject>();
                List<GetSingleJson> postcodeList = new List<GetSingleJson>();
                List<GetSingleJson> provinceList = new List<GetSingleJson>();
                List<GetSingleJson> districtList = new List<GetSingleJson>();
                List<GetSingleJson> subDistrictList = new List<GetSingleJson>();
                var OrderData = RunQuery<CallingChilindo.Models.getOrdersToCallV2_Result>("Exec [dbo].[getOrdersToCallV2] {0}, {1}, {2},{3}, {4}, {5},{6}, {7},{8}", User.Identity.GetUserId(), precedence2C2P, nCallBlockTime, nFirstCallWait, nDelay2C2P, callType, 1000, true, carrierId).ToList();
                if (OrderData.Count == 0)
                {
                    TempData["Warning"] = "There is no order to process.";
                    var model = new List<CallingChilindo.Models.getOrdersToCallV2_Result>();
                    return View(model);
                }
                else if (OrderData.Count == 1)
                {
                    #region Calling 1

                    var model = new List<CallingChilindo.Models.getOrdersToCallV2_Result>();
                    foreach (var rec in OrderData)
                    {
                        var orderToCall = new CallingChilindo.Models.getOrdersToCallV2_Result();
                        orderToCall.id = rec.id; orderToCall.created = rec.created;
                        orderCurrentId = orderToCall.id;
                        orderToCall.deliverytypeident = rec.deliverytypeident;
                        orderToCall.deliveryoptionident = rec.deliveryoptionident;
                        orderToCall.orderguid = rec.orderguid;

                        
                        AddressControlObject obj = GetOrderAddressControl(orderToCall.orderguid);
                        obj.orderId = orderToCall.id;
                        addressObiList.Add(obj);
                        orderToCall.orderstatusid = rec.orderstatusid;
                        orderToCall.username = rec.username;
                        if (rec.country.ToUpper() != "VN" && (rec.country.ToUpper() == "TH" || rec.country.ToUpper() == "MY" || rec.country.ToUpper() == "NO"))
                        {
                            string postcodeJson = GetPostcode(apiToken, rec.country, rec.orderlanguage);
                            try
                            {
                                postcodeList = new JavaScriptSerializer().Deserialize<List<GetSingleJson>>(postcodeJson);
                            }
                            catch (Exception)
                            {
                            }
                            ViewBag.postcodeList = postcodeList;
                        }
                        string postcode = "";
                        if (rec.country.ToUpper() == "TH" || rec.country.ToUpper() == "MY" || rec.country.ToUpper() == "NO" || rec.country.ToUpper() == "VN")
                        {
                            if (rec.country.ToUpper() != "VN")
                            {
                                foreach (var item in obj.addressControl)
                                {
                                    if (item.fieldIdentity == "postcode")
                                    {
                                        postcode = item.fieldValue;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                postcode = "";
                            }
                            string provinceJson = GetProvince(apiToken, rec.country, rec.orderlanguage, postcode);
                            try
                            {
                                provinceList = new JavaScriptSerializer().Deserialize<List<GetSingleJson>>(provinceJson);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        ViewBag.provinceList = provinceList;

                        string parameter1 = "";
                        string parameter2 = "";
                        if (rec.country.ToUpper() == "TH" || rec.country.ToUpper() == "MY" || rec.country.ToUpper() == "VN")
                        {
                            parameter1 = postcode;
                            foreach (var item in obj.addressControl)
                            {
                                if (item.fieldIdentity == "province")
                                {
                                    parameter2 = item.fieldValue;
                                    break;
                                }
                            }

                            string DistrictJson = GetDistrict(apiToken, rec.country, rec.orderlanguage, parameter1, parameter2);
                            try
                            {
                                districtList = new JavaScriptSerializer().Deserialize<List<GetSingleJson>>(DistrictJson);
                            }
                            catch (Exception)
                            {
                            }
                            ViewBag.districtList = districtList;
                        }
                        if (rec.country.ToUpper() == "TH" ||  rec.country.ToUpper() == "VN")
                        {

                            foreach (var item in obj.addressControl)
                            {
                                if (item.fieldIdentity == "province")
                                {
                                    if (rec.country.ToUpper() == "VN")
                                    {
                                        parameter1 = item.fieldValue;
                                    }
                                }
                                if (item.fieldIdentity == "postcode")
                                {
                                    if (rec.country.ToUpper() != "VN")
                                    {
                                        parameter1 = item.fieldValue;
                                    }
                                }

                                if (item.fieldIdentity == "district")
                                {
                                    parameter2 = item.fieldValue;
                                }
                            }
                            string subDistrictJson = GetSubDistrict(apiToken, rec.country, rec.orderlanguage, parameter1, parameter2);
                            try
                            {
                                subDistrictList = new JavaScriptSerializer().Deserialize<List<GetSingleJson>>(subDistrictJson);
                            }
                            catch (Exception)
                            {
                            }
                            ViewBag.subDistrictList = subDistrictList;
                        }




                        _accountid = rec.accountid;
                        orderToCall.accountid = rec.accountid;
                        orderToCall.email = rec.email;
                        orderToCall.name = rec.name;
                        orderToCall.lastname = rec.lastname;
                        orderToCall.address = rec.address;
                        orderToCall.zip = rec.zip;
                        orderToCall.city = rec.city;
                        orderToCall.province = rec.province;
                        orderToCall.provinceid = rec.provinceid;
                        orderToCall.district = rec.district;
                        orderToCall.country = rec.country;
                        orderToCall.countrytxt = rec.countrytxt;
                        orderToCall.phone = rec.phone;
                        orderToCall.comment = rec.comment;
                        orderToCall.webAmount = rec.webAmount;
                        orderToCall.webFragt = rec.webFragt;
                        orderToCall.webEkspgebyr = rec.webEkspgebyr;
                        orderToCall.webAmountAuctions = rec.webAmountAuctions;
                        orderToCall.webDiscount = rec.webDiscount;
                        orderToCall.paidDate = rec.paidDate;
                        orderToCall.sendDate = rec.sendDate;
                        orderToCall.trackingcode = rec.trackingcode;
                        orderToCall.districtsub = rec.districtsub;
                        //--  orderToCall.isCallAble = rec.isCallAble;
                        //--   orderToCall.callBlockTime = rec.callBlockTime;
                        orderToCall.webAmountSales = rec.webAmountSales;
                        orderToCall.pauseId = rec.pauseId;
                        //--    orderToCall.fetchGuid = rec.fetchGuid;
                        //--    orderToCall.aCommerreMark = rec.aCommerreMark;
                        model.Add(orderToCall);
                    }

                    Session["CallStart"] = DateTime.Now;//For Call duration calculation
                    if (callType == 1)
                    {
                        #region Prepare Call Logs Data for Unpaid Orders
                        var oldLogs = new List<viewCallingLogs>();
                        var oLogs = from parentObj in db.CallingLogs
                                    join childObj in db.AspNetUsers on parentObj.Caller equals childObj.Id
                                    join otherChild in db.LogEvents on parentObj.Event equals otherChild.ID
                                    where parentObj.orderId == orderCurrentId && parentObj.logType == 1
                                    orderby parentObj.logDate descending
                                    select new
                                    {
                                        CallingLogs = parentObj,
                                        Users = childObj,
                                        LogEvents = otherChild
                                    };
                        foreach (var item in oLogs)
                        {
                            viewCallingLogs lgs = new viewCallingLogs();
                            lgs.orderId = item.CallingLogs.orderId;
                            lgs.accountId = item.CallingLogs.accountId;
                            lgs.Caller = item.Users.FirstName + " " + item.Users.LastName;
                            lgs.Event = item.LogEvents.type;
                            lgs.logDate = item.CallingLogs.logDate;
                            lgs.nextCall = item.CallingLogs.nextCall;
                            lgs.orderCreated = item.CallingLogs.orderCreated;
                            lgs.Scope = item.CallingLogs.Scope;
                            oldLogs.Add(lgs);
                        }
                        ViewBag.CallingLogs = oldLogs;
                        #endregion
                    }
                    else if (callType == 2)
                    {
                        #region Prepare Call Logs Data for undelievered COD Orders
                        var oldLogs = new List<viewCallingLogs>();
                        var oLogs = from parentObj in db.CallingLogs
                                    join childObj in db.AspNetUsers on parentObj.Caller equals childObj.Id
                                    join otherChild in db.LogEvents on parentObj.Event equals otherChild.ID
                                    where parentObj.orderId == orderCurrentId && parentObj.logType == 2
                                    orderby parentObj.logDate descending
                                    select new
                                    {
                                        CallingLogs = parentObj,
                                        Users = childObj,
                                        LogEvents = otherChild
                                    };
                        foreach (var item in oLogs)
                        {
                            viewCallingLogs lgs = new viewCallingLogs();
                            lgs.orderId = item.CallingLogs.orderId;
                            lgs.accountId = item.CallingLogs.accountId;
                            lgs.Caller = item.Users.FirstName + " " + item.Users.LastName;
                            lgs.Event = item.LogEvents.type;
                            lgs.logDate = item.CallingLogs.logDate;
                            lgs.nextCall = item.CallingLogs.nextCall;
                            lgs.orderCreated = item.CallingLogs.orderCreated;
                            lgs.Scope = item.CallingLogs.Scope;
                            oldLogs.Add(lgs);
                        }
                        ViewBag.CallingLogs = oldLogs;
                        #endregion
                    }
                    else 
                    {
                        #region Prepare Call Logs Data for Suspicious COD Orders
                        var oldLogs = new List<viewCallingLogs>();
                        var oLogs = from parentObj in db.CallingLogs
                                    join childObj in db.AspNetUsers on parentObj.Caller equals childObj.Id
                                    join otherChild in db.LogEvents on parentObj.Event equals otherChild.ID
                                    where parentObj.orderId == orderCurrentId && parentObj.logType == 4
                                    orderby parentObj.logDate descending
                                    select new
                                    {
                                        CallingLogs = parentObj,
                                        Users = childObj,
                                        LogEvents = otherChild
                                    };
                        foreach (var item in oLogs)
                        {
                            viewCallingLogs lgs = new viewCallingLogs();
                            lgs.orderId = item.CallingLogs.orderId;
                            lgs.accountId = item.CallingLogs.accountId;
                            lgs.Caller = item.Users.FirstName + " " + item.Users.LastName;
                            lgs.Event = item.LogEvents.type;
                            lgs.logDate = item.CallingLogs.logDate;
                            lgs.nextCall = item.CallingLogs.nextCall;
                            lgs.orderCreated = item.CallingLogs.orderCreated;
                            lgs.Scope = item.CallingLogs.Scope;
                            oldLogs.Add(lgs);
                        }
                        ViewBag.CallingLogs = oldLogs;
                        #endregion
                    }

                    #region Prepare Orderline Data
                    var Basket = new List<tblBasket>();
                    var basket = from parentObj in db.tblBasket
                                 where parentObj.paymentorderid == orderCurrentId
                                 orderby parentObj.created descending
                                 select new
                                 {
                                     Basket = parentObj,
                                 };
                    foreach (var itemBasket in basket)
                    {
                        tblBasket oneBasket = new tblBasket();
                        oneBasket.id = itemBasket.Basket.id;
                        oneBasket.itemnumber = itemBasket.Basket.itemnumber;
                        oneBasket.created = itemBasket.Basket.created;
                        oneBasket.antal = itemBasket.Basket.antal;
                        oneBasket.basValuta = itemBasket.Basket.basValuta;
                        oneBasket.basValutaPrice = itemBasket.Basket.basValutaPrice;
                        oneBasket.basValutaPriceTotal = itemBasket.Basket.basValutaPriceTotal;
                        oneBasket.paymentorderid = itemBasket.Basket.paymentorderid;
                        oneBasket.accountident = itemBasket.Basket.accountident;
                        Basket.Add(oneBasket);
                    }
                    ViewBag.BasketData = Basket;
                    #endregion
                    ViewBag.OrderID = orderCurrentId;
                    ViewBag.accountid = _accountid;
                    ViewBag.addressList = addressObiList;
                    return View(model);
                    #endregion
                }
                else
                {
                    #region Calling 2
                    Session["CallStart"] = DateTime.Now;

                    string phoneN = "";
                    var modelN = new List<CallingChilindo.Models.getOrdersToCallV2_Result>();
                    int i = 0;
                    foreach (var rec in OrderData)
                    {
                        i = i + 1;
                        //getOrdersToCall_Result order = new getOrdersToCall_Result();
                        //phoneN = rec.phone + " ";
                        var orderToCall = new CallingChilindo.Models.getOrdersToCallV2_Result();
                        orderToCall.id = rec.id;
                        orderToCall.created = rec.created;
                        orderCurrentIdFormultiple = orderCurrentIdFormultiple + Convert.ToString(orderToCall.id) + ",";
                        orderCurrentId = orderToCall.id;
                        orderToCall.deliverytypeident = rec.deliverytypeident;
                        orderToCall.deliveryoptionident = rec.deliveryoptionident;
                        orderToCall.orderguid = rec.orderguid;
                        
                        AddressControlObject obj= GetOrderAddressControl(orderToCall.orderguid);


                        obj.orderId = orderToCall.id;
                        addressObiList.Add(obj);
                        orderToCall.orderstatusid = rec.orderstatusid;
                        orderToCall.username = rec.username;
                        if (rec.country.ToUpper() != "VN" && (rec.country.ToUpper() == "TH" || rec.country.ToUpper() == "MY" || rec.country.ToUpper() == "NO"))
                        {
                            string postcodeJson = GetPostcode(apiToken, rec.country, rec.orderlanguage);
                            try
                            {
                                postcodeList = new JavaScriptSerializer().Deserialize<List<GetSingleJson>>(postcodeJson);
                            }
                            catch (Exception)
                            {
                            }
                            ViewBag.postcodeList = postcodeList;
                        }

                        string postcode = "";
                        if (rec.country.ToUpper() == "TH" || rec.country.ToUpper() == "MY" || rec.country.ToUpper() == "NO" || rec.country.ToUpper() == "VN")
                        {
                            if (rec.country.ToUpper() != "VN")
                            {
                                foreach (var item in obj.addressControl)
                                {
                                    if (item.fieldIdentity == "postcode")
                                    {
                                        postcode = item.fieldValue;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                postcode = "";
                            }
                            string provinceJson = GetProvince(apiToken, rec.country, rec.orderlanguage, postcode);
                            try
                            {
                                provinceList = new JavaScriptSerializer().Deserialize<List<GetSingleJson>>(provinceJson);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        ViewBag.provinceList = provinceList;

                        string parameter1 = "";
                        string parameter2 = "";
                        if (rec.country.ToUpper() == "TH" || rec.country.ToUpper() == "MY" || rec.country.ToUpper() == "VN")
                        {
                            parameter1 = postcode;
                            foreach (var item in obj.addressControl)
                            {
                                if (item.fieldIdentity == "province")
                                {
                                    parameter2 = item.fieldValue;
                                    break;
                                }
                            }

                            string DistrictJson = GetDistrict(apiToken, rec.country, rec.orderlanguage, parameter1, parameter2);
                            try
                            {
                                districtList = new JavaScriptSerializer().Deserialize<List<GetSingleJson>>(DistrictJson);
                            }
                            catch (Exception)
                            {
                            }
                            ViewBag.districtList = districtList;
                        }
                        if (rec.country.ToUpper() == "TH" || rec.country.ToUpper() == "VN")
                        { 
                            foreach (var item in obj.addressControl)
                            {
                                if (item.fieldIdentity == "province")
                                {
                                    if (rec.country.ToUpper() == "VN")
                                    {
                                        parameter1 = item.fieldValue;
                                        break;
                                    }
                                }
                                if (item.fieldIdentity == "postcode")
                                {
                                    if (rec.country.ToUpper() != "VN")
                                    {
                                        parameter1 = item.fieldValue;
                                        break;
                                    }
                                }

                                if (item.fieldIdentity == "district")
                                {
                                    parameter2 = item.fieldValue;
                                    break;
                                }
                            }
                            string subDistrictJson = GetSubDistrict(apiToken, rec.country, rec.orderlanguage, parameter1, parameter2);
                            try
                            {
                                subDistrictList = new JavaScriptSerializer().Deserialize<List<GetSingleJson>>(subDistrictJson);
                            }
                            catch (Exception)
                            {
                            }
                            ViewBag.subDistrictList = subDistrictList;
                        }

                        _accountid = rec.accountid;

                        orderToCall.accountid = rec.accountid;
                        orderToCall.email = rec.email;
                        orderToCall.name = rec.name;
                        orderToCall.lastname = rec.lastname;
                        orderToCall.address = rec.address;
                        orderToCall.zip = rec.zip;
                        orderToCall.city = rec.city;
                        orderToCall.province = rec.province;
                        orderToCall.provinceid = rec.provinceid;
                        orderToCall.district = rec.district;
                        orderToCall.country = rec.country;
                        orderToCall.countrytxt = rec.countrytxt;
                        orderToCall.phone = rec.phone + " ";
                        orderToCall.comment = rec.comment;
                        orderToCall.webAmount = rec.webAmount;
                        orderToCall.webFragt = rec.webFragt;
                        orderToCall.webEkspgebyr = rec.webEkspgebyr;
                        orderToCall.webAmountAuctions = rec.webAmountAuctions;
                        orderToCall.webDiscount = rec.webDiscount;
                        orderToCall.paidDate = rec.paidDate;
                        orderToCall.sendDate = rec.sendDate;
                        orderToCall.trackingcode = rec.trackingcode;
                        orderToCall.districtsub = rec.districtsub;
                        //-- orderToCall.isCallAble = rec.isCallAble;
                        //-- orderToCall.callBlockTime = rec.callBlockTime;
                        orderToCall.webAmountSales = rec.webAmountSales;
                        orderToCall.pauseId = rec.pauseId;
                        //-- orderToCall.fetchGuid = rec.fetchGuid;
                        //--  orderToCall.aCommerreMark = rec.aCommerreMark;
                        //model.Add(orderToCall);
                        modelN.Add(orderToCall);
                    }
                    ViewBag.phoneN = phoneN;
                    int? accIdofNCurrentOrder = db.tblOrders.Find(orderCurrentId).accountid;
                    ViewBag.accIdofCurrentOrder = accIdofNCurrentOrder;
                    orderCurrentIdFormultiple = orderCurrentIdFormultiple.Trim(',');
                    string[] stringarrayformultiple = orderCurrentIdFormultiple.Split(',');
                    int?[] orderCurrentIntId = Array.ConvertAll(stringarrayformultiple, TryParseInt32);
                    var oldLogs = new List<viewCallingLogs>();
                    var oLogs = from parentObj in db.CallingLogs
                                join childObj in db.AspNetUsers on parentObj.Caller equals childObj.Id
                                join otherChild in db.LogEvents on parentObj.Event equals otherChild.ID
                                where orderCurrentIntId.Contains(parentObj.orderId) 
                                orderby parentObj.logDate descending
                                select new
                                {
                                    CallingLogs = parentObj,
                                    Users = childObj,
                                    LogEvents = otherChild
                                };
                    foreach (var item in oLogs)
                    {
                        viewCallingLogs lgs = new viewCallingLogs();
                        lgs.orderId = item.CallingLogs.orderId;
                        lgs.accountId = item.CallingLogs.accountId;
                        lgs.Caller = item.Users.FirstName + " " + item.Users.LastName;
                        lgs.Event = item.LogEvents.type;
                        lgs.logDate = item.CallingLogs.logDate;
                        lgs.nextCall = item.CallingLogs.nextCall;
                        lgs.orderCreated = item.CallingLogs.orderCreated;
                        lgs.Scope = item.CallingLogs.Scope;
                        oldLogs.Add(lgs);
                    }
                    ViewBag.CallingLogs = oldLogs;
                   
                   ViewBag.addressList = addressObiList;
                    ViewBag.accountid = _accountid;
                    return View("MultipleOrders", modelN);
                }
                #endregion
                #endregion
            }
            catch (Exception xp)
            {
                TempData["Error"] = "An error occurred, Please try again";
            }
            return View();
        }

        public class AddressControl
        {
            public string fieldDisplayName { get; set; }
            public string fieldIdentity { get; set; }
            public string fieldType { get; set; }
            public List<object> fieldDependentIdent { get; set; }
            public string fieldValue { get; set; }
            public bool isRequired { get; set; }
            public int displayOrder { get; set; }
            public bool isReadOnly { get; set; }
        }

        public class AddressControlObject
        {
            public int ?  orderId { get; set; }
            public bool isValid { get; set; }
            public string orderLangauge { get; set; }
            public List<AddressControl> addressControl { get; set; }
        }

        public static AddressControlObject GetOrderAddressControl(string guid)
        {
            string body;
            using (StreamReader reader = new StreamReader(HostingEnvironment.MapPath("~/Templates/GetOrderAddressControl.json")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("[orderGuid]", guid);
            string resultContent = "";
            string GetOrderAddressControlUrl = "https://stage-api.chilindo.com/api/Address/v1/GetOrderAddressControl";
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11;
            var client = new HttpClient();
            client.BaseAddress = new Uri(GetOrderAddressControlUrl);
            //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            StringReader sr = new StringReader(body);
            StringContent theContent = new StringContent(sr.ReadToEnd(), Encoding.UTF8, "application/json");
            var result = client.PostAsync(GetOrderAddressControlUrl, theContent).Result;
            resultContent = result.Content.ReadAsStringAsync().Result;
            AddressControlObject obj = new JavaScriptSerializer().Deserialize<AddressControlObject>(resultContent);
            return obj;
        }

        public class GetLoginJson
        {
            public string token { get; set; }
            public int userId { get; set; }
        }

        public static string GetLogin()
        {
            string token = "";
            try
            {
                string body;
                using (StreamReader reader = new StreamReader(HostingEnvironment.MapPath("~/Templates/Login.json")))
                {
                    body = reader.ReadToEnd();
                }
                string resultContent = "";
                string GetOrderAddressControlUrl = "https://stage-api.chilindo.com/api/Account/v1/Login";
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11;
                var client = new HttpClient();
                client.BaseAddress = new Uri(GetOrderAddressControlUrl);
                //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                StringReader sr = new StringReader(body);
                StringContent theContent = new StringContent(sr.ReadToEnd(), Encoding.UTF8, "application/json");
                var result = client.PostAsync(GetOrderAddressControlUrl, theContent).Result;
                resultContent = result.Content.ReadAsStringAsync().Result;
                GetLoginJson obj = new JavaScriptSerializer().Deserialize<GetLoginJson>(resultContent);
                token = obj.token;
                token = UpdateUserDeviceInfo(token);
            }
            catch(Exception)
            {

            }
            return token;
        }

        public class UpdateUserDeviceInfoJson
        {
            public bool isSuccess { get; set; }
        }
        public static string UpdateUserDeviceInfo(string token)
        {
            string tok = "";
            try
            {
                string body;
                using (StreamReader reader = new StreamReader(HostingEnvironment.MapPath("~/Templates/UpdateUserDeviceInfo.json")))
                {
                    body = reader.ReadToEnd();
                }
                string resultContent = "";
                string GetOrderAddressControlUrl = "https://stage-api.chilindo.com/api/Account/v1/UpdateUserDeviceInfo";
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11;
                var client = new HttpClient();
                client.BaseAddress = new Uri(GetOrderAddressControlUrl);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                StringReader sr = new StringReader(body);
                StringContent theContent = new StringContent(sr.ReadToEnd(), Encoding.UTF8, "application/json");
                var result = client.PostAsync(GetOrderAddressControlUrl, theContent).Result;
                resultContent = result.Content.ReadAsStringAsync().Result;
                UpdateUserDeviceInfoJson obj = new JavaScriptSerializer().Deserialize<UpdateUserDeviceInfoJson>(resultContent);
                if(obj.isSuccess == true)
                {
                    tok = token;
                }
            }
            catch (Exception)
            {

            }
            return tok;
        }
        public class GetSingleJson
        {
            public string value { get; set; }
        }

        public static string GetPostcode(string token,string domain,string lang)
        {
            string resultContent = "";
            try
            {
                string body;
                using (StreamReader reader = new StreamReader(HostingEnvironment.MapPath("~/Templates/GetPostcode.json")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("[domain]", domain);
                body = body.Replace("[language]", lang);
                string GetOrderAddressControlUrl = "https://stage-api.chilindo.com/api/Address/v1/GetPostcode";

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11;
                var client = new HttpClient();
                client.BaseAddress = new Uri(GetOrderAddressControlUrl);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                StringReader sr = new StringReader(body);
                StringContent theContent = new StringContent(sr.ReadToEnd(), Encoding.UTF8, "application/json");
                var result = client.PostAsync(GetOrderAddressControlUrl, theContent).Result;
                resultContent = result.Content.ReadAsStringAsync().Result;
            }
            catch (Exception)
            {
            }
            return resultContent;
        }

        public static string GetProvince(string token, string domain, string lang,string postcode)
        {
            string resultContent = "";
            try
            {
                string body;
                using (StreamReader reader = new StreamReader(HostingEnvironment.MapPath("~/Templates/GetProvince.json")))
                {
                    body = reader.ReadToEnd();
                }

                if (domain.ToUpper() == "TH")
                {
                    body = body.Replace("[postcode]", postcode);
                    body = body.Replace("[domain]", domain);
                    body = body.Replace("[language]", lang);
                }
                else if (domain.ToUpper() == "MY" || domain.ToUpper() == "NO")
                {
                    body = body.Replace("[postcode]", postcode);
                    body = body.Replace("[domain]", domain);
                    body = body.Replace("[language]", "");
                }
                else if (domain.ToUpper() == "VN" )
                {
                    body = body.Replace("[postcode]", "");
                    body = body.Replace("[domain]", domain);
                    body = body.Replace("[language]", "");
                }

                string GetOrderAddressControlUrl = "https://stage-api.chilindo.com/api/Address/v1/GetProvince";
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11;
                var client = new HttpClient();
                client.BaseAddress = new Uri(GetOrderAddressControlUrl);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                StringReader sr = new StringReader(body);
                StringContent theContent = new StringContent(sr.ReadToEnd(), Encoding.UTF8, "application/json");
                var result = client.PostAsync(GetOrderAddressControlUrl, theContent).Result;
                resultContent = result.Content.ReadAsStringAsync().Result;
            }
            catch (Exception)
            {
            }
            return resultContent;
        }
        public static string GetDistrict(string token, string domain, string lang, string paramter1,string parameter2)
        {
            string resultContent = "";
            try
            {
                string body;
                using (StreamReader reader = new StreamReader(HostingEnvironment.MapPath("~/Templates/GetDistrict.json")))
                {
                    body = reader.ReadToEnd();
                }
                if (domain.ToUpper() == "TH")
                {
                    body = body.Replace("[parameter1]", paramter1);
                    body = body.Replace("[parameter2]", parameter2);
                    body = body.Replace("[domain]", domain);
                    body = body.Replace("[language]", lang);
                }

                else if (domain.ToUpper() == "MY")
                {
                    body = body.Replace("[parameter1]", paramter1);
                    body = body.Replace("[parameter2]", parameter2);
                    body = body.Replace("[domain]", domain);
                    body = body.Replace("[language]", "");
                }

                else if (domain.ToUpper() == "VN")
                {
                    body = body.Replace("[parameter1]", "");
                    body = body.Replace("[parameter2]", parameter2);
                    body = body.Replace("[domain]", domain);
                    body = body.Replace("[language]", "");
                }
              
                string GetOrderAddressControlUrl = "https://stage-api.chilindo.com/api/Address/v1/GetDistrict";
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11;
                var client = new HttpClient();
                client.BaseAddress = new Uri(GetOrderAddressControlUrl);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                StringReader sr = new StringReader(body);
                StringContent theContent = new StringContent(sr.ReadToEnd(), Encoding.UTF8, "application/json");
                var result = client.PostAsync(GetOrderAddressControlUrl, theContent).Result;
                resultContent = result.Content.ReadAsStringAsync().Result;
            }
            catch (Exception)
            {
            }
            return resultContent;
        }

        public static string GetSubDistrict(string token, string domain, string lang, string paramter1, string parameter2)
        {
            string resultContent = "";
            try
            {
                string body;
                using (StreamReader reader = new StreamReader(HostingEnvironment.MapPath("~/Templates/GetSubDistrict.json")))
                {
                    body = reader.ReadToEnd();
                }
                if (domain.ToUpper() == "TH")
                {
                    body = body.Replace("[parameter1]", paramter1);
                    body = body.Replace("[parameter2]", parameter2);
                    body = body.Replace("[domain]", domain);
                    body = body.Replace("[language]", lang);
                }

                else  if (domain.ToUpper() == "VN")
                {
                    body = body.Replace("[parameter1]", paramter1);
                    body = body.Replace("[parameter2]", parameter2);
                    body = body.Replace("[domain]", domain);
                    body = body.Replace("[language]", "");
                }
                string GetOrderAddressControlUrl = "https://stage-api.chilindo.com/api/Address/v1/GetSubDistrict";
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11;
                var client = new HttpClient();
                client.BaseAddress = new Uri(GetOrderAddressControlUrl);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                StringReader sr = new StringReader(body);
                StringContent theContent = new StringContent(sr.ReadToEnd(), Encoding.UTF8, "application/json");
                var result = client.PostAsync(GetOrderAddressControlUrl, theContent).Result;
                resultContent = result.Content.ReadAsStringAsync().Result;
            }
            catch (Exception)
            {
            }
            return resultContent;
        }


        public static int? TryParseInt32(string text)
        {
            int value;
            if (int.TryParse(text, out value))
            {
                return value;
            }
            else
            {
                return null;
            }
        }

        public DbRawSqlQuery<T> RunQuery<T>(string query, params object[] parameters)
        {
            return db.Database.SqlQuery<T>(query, parameters);
        }

        [Authorize]
        [HttpPost]
        //public ActionResult SearchOrder()
        //{
        //    try
        //    {
        //        int nOrderId = Convert.ToInt32(Request.Form["OrderId"]);

        //        var orderData = db.tblOrders.Find(nOrderId);
        //        int orderCurrentId = orderData.id;
        //        int callType = orderData.orderstatusid == 1 ? 1 : 2;
        //        #region Load Options for the calling
        //        var modelLg = new List<LogEvents>();
        //        if (callType == 1)
        //        {
        //            #region UnPaid Call option
        //            var resultLg = from logEventsMapping in db.LogEventsMapping
        //                           join logEvent in db.LogEvents
        //                           on logEventsMapping.LogEventsId equals logEvent.ID
        //                           join callingType in db.CallingTypes
        //                           on logEventsMapping.CallingTypeId equals callingType.id
        //                           where callingType.id == 1//unpaidOrders
        //                           select logEvent;
        //            foreach (var rec in resultLg)
        //            {
        //                LogEvents lg = new LogEvents();
        //                lg.ID = rec.ID;
        //                lg.type = rec.type;
        //                modelLg.Add(lg);
        //            }
        //            #endregion
        //        }
        //        else if (callType == 2)
        //        {
        //            #region Undelievered Call option
        //            var resultLg = from logEventsMapping in db.LogEventsMapping
        //                           join logEvent in db.LogEvents
        //                           on logEventsMapping.LogEventsId equals logEvent.ID
        //                           join callingType in db.CallingTypes
        //                           on logEventsMapping.CallingTypeId equals callingType.id
        //                           where callingType.id == 2//unpaidOrders
        //                           select logEvent;
        //            foreach (var rec in resultLg)
        //            {
        //                LogEvents lg = new LogEvents();
        //                lg.ID = rec.ID;
        //                lg.type = rec.type;
        //                modelLg.Add(lg);
        //            }
        //            #endregion
        //        }
        //        ViewBag.LogEvents = modelLg;
        //        #endregion
        //        if (callType == 1)
        //        {
        //            #region CheckMultipleOrders
        //            // Check if this order's user has multple orders so that we can decide to go to Calling 1 or calling 2
        //            int? accIdofCurrentOrder = orderData.accountid;

        //            var orderSum = from order in db.tblOrders
        //                           where order.accountid == accIdofCurrentOrder && order.orderstatusid == 1 && (order.deliveryoptionident == "BANKWIRE" || order.deliveryoptionident.Contains("2C2P"))
        //                           group order by order.accountid
        //                               into grp
        //                           select new
        //                           {
        //                               AccountId = grp.Key,
        //                               Count = grp.Select(x => x.accountid).Count()
        //                           };
        //            int currentOrderCount = 0;
        //            foreach (var row in orderSum.OrderBy(x => x.AccountId))
        //            {
        //                currentOrderCount = row.Count;
        //            }
        //            if (currentOrderCount == 0)
        //            {
        //                TempData["Warning"] = "There is no order to process.";
        //                return RedirectToAction("Welcome", "Home");
        //            }
        //            else if (currentOrderCount == 1)
        //            {
        //                var model = new List<CallingChilindo.Models.getOrdersToCallV2_Result>();
        //                #region set OrderData
        //                var orderToCall = new CallingChilindo.Models.getOrdersToCallV2_Result();
        //                orderToCall.id = orderData.id;
        //                orderToCall.created = orderData.created;
        //                orderToCall.deliverytypeident = orderData.deliverytypeident;
        //                orderToCall.deliveryoptionident = orderData.deliveryoptionident;
        //                orderToCall.orderguid = orderData.orderguid;
        //                orderToCall.orderstatusid = orderData.orderstatusid;
        //                orderToCall.username = orderData.username;
        //                orderToCall.accountid = orderData.accountid;
        //                orderToCall.email = orderData.email;
        //                orderToCall.name = orderData.name;
        //                orderToCall.lastname = orderData.lastname;
        //                orderToCall.address = orderData.address;
        //                orderToCall.zip = orderData.zip;
        //                orderToCall.city = orderData.city;
        //                orderToCall.province = orderData.province;
        //                orderToCall.provinceid = orderData.provinceid;
        //                orderToCall.district = orderData.district;
        //                orderToCall.country = orderData.country;
        //                orderToCall.countrytxt = orderData.countrytxt;
        //                orderToCall.phone = orderData.phone;
        //                orderToCall.comment = orderData.comment;
        //                orderToCall.webAmount = orderData.webAmount;
        //                orderToCall.webFragt = orderData.webFragt;
        //                orderToCall.webEkspgebyr = orderData.webEkspgebyr;
        //                orderToCall.webAmountAuctions = orderData.webAmountAuctions;
        //                orderToCall.webDiscount = orderData.webDiscount;
        //                orderToCall.paidDate = orderData.paidDate;
        //                orderToCall.sendDate = orderData.sendDate;
        //                orderToCall.trackingcode = orderData.trackingcode;
        //                orderToCall.districtsub = orderData.districtsub;
        //               // orderToCall.isCallAble = orderData.isCallable;
        //              //  orderToCall.callBlockTime = orderData.callBlockTime;
        //                orderToCall.webAmountSales = 0;
        //               // orderToCall.fetchGuid = Guid.NewGuid();
        //               // orderToCall.aCommerreMark = "";
        //                model.Add(orderToCall);
        //                #endregion
        //                Session["CallStart"] = DateTime.Now;
        //                if (!Request.RawUrl.Contains("localhost"))
        //                {
        //                    var orderCurrent = db.tblOrders.Find(orderCurrentId);
        //                    orderCurrent.isCallable = false;
        //                    orderCurrent.callBlockTime = DateTime.Now;
        //                    db.Entry(orderCurrent).State = EntityState.Modified;
        //                    db.SaveChanges();
        //                }
        //                var oldLogs = new List<viewCallingLogs>();
        //                var oLogs = from parentObj in db.CallingLogs
        //                            join childObj in db.AspNetUsers on parentObj.Caller equals childObj.Id
        //                            join otherChild in db.LogEvents on parentObj.Event equals otherChild.ID
        //                            where parentObj.orderId == orderCurrentId
        //                            orderby parentObj.logDate descending
        //                            select new
        //                            {
        //                                CallingLogs = parentObj,
        //                                Users = childObj,
        //                                LogEvents = otherChild
        //                            };
        //                foreach (var item in oLogs)
        //                {
        //                    viewCallingLogs lgs = new viewCallingLogs();
        //                    lgs.orderId = item.CallingLogs.orderId;
        //                    lgs.accountId = item.CallingLogs.accountId;
        //                    lgs.Caller = item.Users.FirstName + " " + item.Users.LastName;
        //                    lgs.Event = item.LogEvents.type;
        //                    lgs.logDate = item.CallingLogs.logDate;
        //                    lgs.nextCall = item.CallingLogs.nextCall;
        //                    lgs.orderCreated = item.CallingLogs.orderCreated;
        //                    lgs.Scope = item.CallingLogs.Scope;
        //                    oldLogs.Add(lgs);
        //                }
        //                ViewBag.CallingLogs = oldLogs;

        //                //orderline
        //                var Basket = new List<tblBasket>();
        //                var basket = from parentObj in db.tblBasket
        //                             where parentObj.paymentorderid == orderCurrentId
        //                             orderby parentObj.created descending
        //                             select new
        //                             {
        //                                 Basket = parentObj,
        //                             };
        //                foreach (var itemBasket in basket)
        //                {
        //                    tblBasket oneBasket = new tblBasket();
        //                    oneBasket.id = itemBasket.Basket.id;
        //                    oneBasket.itemnumber = itemBasket.Basket.itemnumber;
        //                    oneBasket.created = itemBasket.Basket.created;
        //                    oneBasket.antal = itemBasket.Basket.antal;
        //                    oneBasket.basValuta = itemBasket.Basket.basValuta;
        //                    oneBasket.basValutaPrice = itemBasket.Basket.basValutaPrice;
        //                    oneBasket.basValutaPriceTotal = itemBasket.Basket.basValutaPriceTotal;
        //                    oneBasket.paymentorderid = itemBasket.Basket.paymentorderid;
        //                    oneBasket.accountident = itemBasket.Basket.accountident;
        //                    Basket.Add(oneBasket);
        //                }
        //                ViewBag.BasketData = Basket;
        //                ViewBag.OrderID = orderCurrentId;
        //                ViewBag.calltype = 1;
        //                ViewBag.accountid = accIdofCurrentOrder;
        //                return View("Dashboard", model);
        //            }
        //            #endregion
        //        }
        //        else if (callType == 2)
        //        {
        //            #region CheckMultipleOrders
        //            // Check if this order's user has multple orders so that we can decide to go to Calling 1 or calling 2
        //            int? accIdofCurrentOrder = orderData.accountid;

        //            //var result = from parentObj in db.ParentTable 
        //            //             join childObj in db.ChildObj 
        //            //             on parentObj.PrimaryKey equals childObj.ForeignKey 
        //            //             where childObj.propertyName && !parentObj.PropertyName && message.PropertyName 
        //            //             select msg;

        //            var orderSum = from parentObj in db.tblOrders
        //                           join childObj in db.tblUndeliveredCODs
        //                           on parentObj.id equals childObj.Invoice
        //                           where parentObj.orderstatusid == 31 && parentObj.accountid == accIdofCurrentOrder && childObj.IsActive == true
        //                           group parentObj by parentObj.accountid
        //                               into grp
        //                           select new
        //                           {
        //                               AccountId = grp.Key,
        //                               Count = grp.Select(x => x.accountid).Count()
        //                           };
        //            int currentOrderCount = 0;
        //            foreach (var row in orderSum.OrderBy(x => x.AccountId))
        //            {
        //                currentOrderCount = row.Count;
        //            }
        //            if (currentOrderCount == 0)
        //            {
        //                TempData["Warning"] = "There is no order to process.";
        //                return RedirectToAction("Welcome", "Home");
        //            }
        //            else if (currentOrderCount == 1)
        //            {
        //                #region UNCOD Calling 1
        //                var newOrderData = from parentObj in db.tblOrders
        //                                   join childObj in db.tblUndeliveredCODs
        //                                   on parentObj.id equals childObj.Invoice
        //                                   where parentObj.id == orderCurrentId
        //                                   select new
        //                                   {
        //                                       Order = parentObj,
        //                                       UNCOD = childObj
        //                                   };
        //                var model = new List<CallingChilindo.Models.getOrdersToCallV2_Result>();
        //                #region set OrderData
        //                foreach (var item in newOrderData)
        //                {
        //                    var orderToCall = new CallingChilindo.Models.getOrdersToCallV2_Result();
        //                    orderToCall.id = item.Order.id;
        //                    orderToCall.created = item.Order.created;
        //                    orderToCall.deliverytypeident = item.Order.deliverytypeident;
        //                    orderToCall.deliveryoptionident = item.Order.deliveryoptionident;
        //                    orderToCall.orderguid = item.Order.orderguid;
        //                    orderToCall.orderstatusid = item.Order.orderstatusid;
        //                    orderToCall.username = item.Order.username;
        //                    orderToCall.accountid = item.Order.accountid;
        //                    orderToCall.email = item.Order.email;
        //                    orderToCall.name = item.Order.name;
        //                    orderToCall.lastname = item.Order.lastname;
        //                    orderToCall.address = item.Order.address;
        //                    orderToCall.zip = item.Order.zip;
        //                    orderToCall.city = item.Order.city;
        //                    orderToCall.province = item.Order.province;
        //                    orderToCall.provinceid = item.Order.provinceid;
        //                    orderToCall.district = item.Order.district;
        //                    orderToCall.country = item.Order.country;
        //                    orderToCall.countrytxt = item.Order.countrytxt;
        //                    orderToCall.phone = item.Order.phone;
        //                    orderToCall.comment = item.Order.comment;
        //                    orderToCall.webAmount = item.Order.webAmount;
        //                    orderToCall.webFragt = item.Order.webFragt;
        //                    orderToCall.webEkspgebyr = item.Order.webEkspgebyr;
        //                    orderToCall.webAmountAuctions = item.Order.webAmountAuctions;
        //                    orderToCall.webDiscount = item.Order.webDiscount;
        //                    orderToCall.paidDate = item.Order.paidDate;
        //                    orderToCall.sendDate = item.Order.sendDate;
        //                    orderToCall.trackingcode = item.Order.trackingcode;
        //                    orderToCall.districtsub = item.Order.districtsub;
        //                   // orderToCall.isCallAble = item.Order.isCallable;
        //                  //  orderToCall.callBlockTime = item.Order.callBlockTime;
        //                    orderToCall.webAmountSales = 0;
        //                  //  orderToCall.fetchGuid = Guid.NewGuid();
        //                   // orderToCall.aCommerreMark = item.UNCOD.CarrierRemark;
        //                    model.Add(orderToCall);
        //                }

        //                #endregion
        //                Session["CallStart"] = DateTime.Now;
        //                if (!Request.RawUrl.Contains("localhost"))
        //                {
        //                    var orderCurrent = db.tblOrders.Find(orderCurrentId);
        //                    orderCurrent.isCallable = false;
        //                    orderCurrent.callBlockTime = DateTime.Now;
        //                    db.Entry(orderCurrent).State = EntityState.Modified;
        //                    db.SaveChanges();
        //                }
        //                var oldLogs = new List<viewCallingLogs>();
        //                var oLogs = from parentObj in db.CallingLogs
        //                            join childObj in db.AspNetUsers on parentObj.Caller equals childObj.Id
        //                            join otherChild in db.LogEvents on parentObj.Event equals otherChild.ID
        //                            where parentObj.orderId == orderCurrentId
        //                            orderby parentObj.logDate descending
        //                            select new
        //                            {
        //                                CallingLogs = parentObj,
        //                                Users = childObj,
        //                                LogEvents = otherChild
        //                            };
        //                foreach (var item in oLogs)
        //                {
        //                    viewCallingLogs lgs = new viewCallingLogs();
        //                    lgs.orderId = item.CallingLogs.orderId;
        //                    lgs.accountId = item.CallingLogs.accountId;
        //                    lgs.Caller = item.Users.FirstName + " " + item.Users.LastName;
        //                    lgs.Event = item.LogEvents.type;
        //                    lgs.logDate = item.CallingLogs.logDate;
        //                    lgs.nextCall = item.CallingLogs.nextCall;
        //                    lgs.orderCreated = item.CallingLogs.orderCreated;
        //                    lgs.Scope = item.CallingLogs.Scope;
        //                    oldLogs.Add(lgs);
        //                }
        //                ViewBag.CallingLogs = oldLogs;

        //                //orderline
        //                var Basket = new List<tblBasket>();
        //                var basket = from parentObj in db.tblBasket
        //                             where parentObj.paymentorderid == orderCurrentId
        //                             orderby parentObj.created descending
        //                             select new
        //                             {
        //                                 Basket = parentObj,
        //                             };
        //                foreach (var itemBasket in basket)
        //                {
        //                    tblBasket oneBasket = new tblBasket();
        //                    oneBasket.id = itemBasket.Basket.id;
        //                    oneBasket.itemnumber = itemBasket.Basket.itemnumber;
        //                    oneBasket.created = itemBasket.Basket.created;
        //                    oneBasket.antal = itemBasket.Basket.antal;
        //                    oneBasket.basValuta = itemBasket.Basket.basValuta;
        //                    oneBasket.basValutaPrice = itemBasket.Basket.basValutaPrice;
        //                    oneBasket.basValutaPriceTotal = itemBasket.Basket.basValutaPriceTotal;
        //                    oneBasket.paymentorderid = itemBasket.Basket.paymentorderid;
        //                    oneBasket.accountident = itemBasket.Basket.accountident;
        //                    Basket.Add(oneBasket);
        //                }
        //                ViewBag.BasketData = Basket;
        //                ViewBag.OrderID = orderCurrentId;
        //                ViewBag.calltype = 2;
        //                ViewBag.accountid = accIdofCurrentOrder;
        //                return View("Dashboard", model);
        //                #endregion
        //            }
        //            else if (currentOrderCount == 2)
        //            {
        //                #region UNCOD Calling 2
        //                var newOrderData = from parentObj in db.tblOrders
        //                                   join childObj in db.tblUndeliveredCODs
        //                                   on parentObj.id equals childObj.Invoice
        //                                   where parentObj.accountid == accIdofCurrentOrder 
        //                                   select new
        //                                   {
        //                                       Order = parentObj,
        //                                       UNCOD = childObj
        //                                   };
        //                var model = new List<CallingChilindo.Models.getOrdersToCallV2_Result>();
        //                #region set OrderData
        //                foreach (var item in newOrderData)
        //                {
        //                    var orderToCall = new CallingChilindo.Models.getOrdersToCallV2_Result();
        //                    orderToCall.id = item.Order.id;
        //                    orderToCall.created = item.Order.created;
        //                    orderToCall.deliverytypeident = item.Order.deliverytypeident;
        //                    orderToCall.deliveryoptionident = item.Order.deliveryoptionident;
        //                    orderToCall.orderguid = item.Order.orderguid;
        //                    orderToCall.orderstatusid = item.Order.orderstatusid;
        //                    orderToCall.username = item.Order.username;
        //                    orderToCall.accountid = item.Order.accountid;
        //                    orderToCall.email = item.Order.email;
        //                    orderToCall.name = item.Order.name;
        //                    orderToCall.lastname = item.Order.lastname;
        //                    orderToCall.address = item.Order.address;
        //                    orderToCall.zip = item.Order.zip;
        //                    orderToCall.city = item.Order.city;
        //                    orderToCall.province = item.Order.province;
        //                    orderToCall.provinceid = item.Order.provinceid;
        //                    orderToCall.district = item.Order.district;
        //                    orderToCall.country = item.Order.country;
        //                    orderToCall.countrytxt = item.Order.countrytxt;
        //                    orderToCall.phone = item.Order.phone;
        //                    orderToCall.comment = item.Order.comment;
        //                    orderToCall.webAmount = item.Order.webAmount;
        //                    orderToCall.webFragt = item.Order.webFragt;
        //                    orderToCall.webEkspgebyr = item.Order.webEkspgebyr;
        //                    orderToCall.webAmountAuctions = item.Order.webAmountAuctions;
        //                    orderToCall.webDiscount = item.Order.webDiscount;
        //                    orderToCall.paidDate = item.Order.paidDate;
        //                    orderToCall.sendDate = item.Order.sendDate;
        //                    orderToCall.trackingcode = item.Order.trackingcode;
        //                    orderToCall.districtsub = item.Order.districtsub;
        //                   // orderToCall.isCallAble = item.Order.isCallable;
        //                  //  orderToCall.callBlockTime = item.Order.callBlockTime;
        //                    orderToCall.webAmountSales = 0;
        //                  //  orderToCall.fetchGuid = Guid.NewGuid();
        //                   // orderToCall.aCommerreMark = item.UNCOD.CarrierRemark;
        //                    model.Add(orderToCall);
        //                }

        //                #endregion
        //                Session["CallStart"] = DateTime.Now;
        //                if (!Request.RawUrl.Contains("localhost"))
        //                {
        //                    var orderCurrent = db.tblOrders.Find(orderCurrentId);
        //                    orderCurrent.isCallable = false;
        //                    orderCurrent.callBlockTime = DateTime.Now;
        //                    db.Entry(orderCurrent).State = EntityState.Modified;
        //                    db.SaveChanges();
        //                }
        //                var oldLogs = new List<viewCallingLogs>();
        //                var oLogs = from parentObj in db.CallingLogs
        //                            join childObj in db.AspNetUsers on parentObj.Caller equals childObj.Id
        //                            join otherChild in db.LogEvents on parentObj.Event equals otherChild.ID
        //                            where parentObj.orderId == orderCurrentId
        //                            orderby parentObj.logDate descending
        //                            select new
        //                            {
        //                                CallingLogs = parentObj,
        //                                Users = childObj,
        //                                LogEvents = otherChild
        //                            };
        //                foreach (var item in oLogs)
        //                {
        //                    viewCallingLogs lgs = new viewCallingLogs();
        //                    lgs.orderId = item.CallingLogs.orderId;
        //                    lgs.accountId = item.CallingLogs.accountId;
        //                    lgs.Caller = item.Users.FirstName + " " + item.Users.LastName;
        //                    lgs.Event = item.LogEvents.type;
        //                    lgs.logDate = item.CallingLogs.logDate;
        //                    lgs.nextCall = item.CallingLogs.nextCall;
        //                    lgs.orderCreated = item.CallingLogs.orderCreated;
        //                    lgs.Scope = item.CallingLogs.Scope;
        //                    oldLogs.Add(lgs);
        //                }
        //                ViewBag.CallingLogs = oldLogs;

        //                ViewBag.accIdofCurrentOrder = accIdofCurrentOrder;
        //                ViewBag.OrderID = orderCurrentId;
        //                ViewBag.calltype = 2;
        //                ViewBag.accountid = accIdofCurrentOrder;
        //                return View("MultipleOrders", model);
        //                #endregion
        //            }
        //            #endregion
        //        }
        //        Dictionary<string, object> error = new Dictionary<string, object>();
        //        error.Add("MessageCode", "0");
        //        error.Add("Message", "Value get");
        //        return Json(error, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception xp)
        //    {
        //        TempData["Warning"] = "There is no order to process.";
        //        return RedirectToAction("Welcome", "Home");
        //    }
        //}

        public ActionResult SearchOrder()
        {
            var model = new List<MaccadkProj.tblOrders>();
            int nOrderId = Convert.ToInt32(Request.Form["OrderId"]);
            int callType = 1;

            // If orderstatusid = 1, then call type = 1
            // If orderstatusid = 31, then call type = 2
            // If orderstatusid = 10, then call type = 3
            //  If orderstatusid = 5, then call type = 4
            try
            {
               var orderInfo = entities.tblOrders.Where(x => x.id == nOrderId).FirstOrDefault();
                if (orderInfo.orderstatusid == 1 || orderInfo.orderstatusid == 5)
                {
                    callType = orderInfo.orderstatusid == 1 ? 1 : 4;

                    #region Init intializing and setting variable about AutoLog out for example if there is 15 mins inactivity then after 15 mins show popup and auto loggout
                    int miliSecToLogout = 60000 * db.tblSettings.Find(1).nAutoLogoutValue;
                    ViewBag.miliSecToLogout = miliSecToLogout;
                    ViewBag.UserName = Session["LogedUserFullname"];
                    ViewBag.Message = "Welcome to Calling Chilindo.";
                    int? _accountid = 0;
                    int nCallBlockTime = db.tblSettings.Find(1).callblocktime;
                    int? nFirstCallWait = db.tblSettings.Find(1).firstCallWait;
                    int nDelay2C2P = db.tblSettings.Find(1).delay2C2P;
                    //Check if the debit card orders to process first 2C2p=debot card
                    bool? precedence2C2P = db.tblSettings.First().precedence2C2P;
                    int orderCurrentId = 0;
                    string orderCurrentIdFormultiple = "";
                    #endregion
                    #region Load Options for the calling
                    var modelLg = new List<LogEvents>();
                    if (callType == 1)
                    {
                        #region UnPaid Call option
                        var resultLg = from logEventsMapping in db.LogEventsMapping
                                       join logEvent in db.LogEvents
                                       on logEventsMapping.LogEventsId equals logEvent.ID
                                       join callingType in db.CallingTypes
                                       on logEventsMapping.CallingTypeId equals callingType.id
                                       where callingType.id == 1//unpaidOrders
                                       select logEvent;
                        foreach (var rec in resultLg)
                        {
                            LogEvents lg = new LogEvents();
                            lg.ID = rec.ID;
                            lg.type = rec.type;
                            modelLg.Add(lg);
                        }
                        #endregion
                    }
                    else if (callType == 2)
                    {
                        #region Undelievered Call option
                        var resultLg = from logEventsMapping in db.LogEventsMapping
                                       join logEvent in db.LogEvents
                                       on logEventsMapping.LogEventsId equals logEvent.ID
                                       join callingType in db.CallingTypes
                                       on logEventsMapping.CallingTypeId equals callingType.id
                                       where callingType.id == 2//unpaidOrders
                                       select logEvent;
                        foreach (var rec in resultLg)
                        {
                            LogEvents lg = new LogEvents();
                            lg.ID = rec.ID;
                            lg.type = rec.type;
                            modelLg.Add(lg);
                        }
                        #endregion
                    }
                    else if (callType == 4)
                    {
                        #region UnPaid Call option
                        var resultLg = from logEventsMapping in db.LogEventsMapping
                                       join logEvent in db.LogEvents
                                       on logEventsMapping.LogEventsId equals logEvent.ID
                                       join callingType in db.CallingTypes
                                       on logEventsMapping.CallingTypeId equals callingType.id
                                       where callingType.id == 4//suspiciousCODs
                                       select logEvent;
                        foreach (var rec in resultLg)
                        {
                            LogEvents lg = new LogEvents();
                            lg.ID = rec.ID;
                            lg.type = rec.type;
                            modelLg.Add(lg);
                        }
                        #endregion
                    }

                    ViewBag.LogEvents = modelLg;
                    #endregion
                    #region Fetch Orders
                    string s = User.Identity.GetUserId();
                    //ChilindoCallingEntities objEntities = new ChilindoCallingEntities();
                    //var OrderData = objEntities.getOrdersToCall(User.Identity.GetUserId(), precedence2C2P, nCallBlockTime, nFirstCallWait, nDelay2C2P, callType).ToList();

                    ///Address API
                    //  List<AddressControlObject> addressObiList = new List<AddressControlObject>();
                    var OrderData = entities.tblOrders.Where(x => x.id == nOrderId).ToList();
                    if (OrderData.Count == 0)
                    {
                        TempData["Warning"] = "There is no order to process.";
                        model = new List<MaccadkProj.tblOrders>();
                        return View(model);
                    }
                    else if (OrderData.Count == 1)
                    {
                        #region Calling 1

                        foreach (var rec in OrderData)
                        {
                            var orderToCall = new MaccadkProj.tblOrders();
                            orderToCall.id = rec.id; orderToCall.created = rec.created;
                            orderCurrentId = orderToCall.id;
                            orderToCall.deliverytypeident = rec.deliverytypeident;
                            orderToCall.deliveryoptionident = rec.deliveryoptionident;
                            orderToCall.orderguid = rec.orderguid;

                            ///Address API
                            //AddressControlObject obj = GetOrderAddressControl(orderToCall.orderguid);
                            //obj.orderId = orderToCall.id;
                            //addressObiList.Add(obj);
                            orderToCall.orderstatusid = rec.orderstatusid;
                            orderToCall.username = rec.username;

                            _accountid = rec.accountid;
                            orderToCall.accountid = rec.accountid;
                            orderToCall.email = rec.email;
                            orderToCall.name = rec.name;
                            orderToCall.lastname = rec.lastname;
                            orderToCall.address = rec.address;
                            orderToCall.zip = rec.zip;
                            orderToCall.city = rec.city;
                            orderToCall.province = rec.province;
                            orderToCall.provinceid = rec.provinceid;
                            orderToCall.district = rec.district;
                            orderToCall.country = rec.country;
                            orderToCall.countrytxt = rec.countrytxt;
                            orderToCall.phone = rec.phone;
                            orderToCall.comment = rec.comment;
                            orderToCall.webAmount = rec.webAmount;
                            orderToCall.webFragt = rec.webFragt;
                            orderToCall.webEkspgebyr = rec.webEkspgebyr;
                            orderToCall.webAmountAuctions = rec.webAmountAuctions;
                            orderToCall.webDiscount = rec.webDiscount;
                            orderToCall.paidDate = rec.paidDate;
                            orderToCall.sendDate = rec.sendDate;
                            orderToCall.trackingcode = rec.trackingcode;
                            orderToCall.districtsub = rec.districtsub;
                            //--  orderToCall.isCallAble = rec.isCallAble;
                            //--   orderToCall.callBlockTime = rec.callBlockTime;
                            orderToCall.webAmountSales = rec.webAmountSales;

                            //Not Available
                            //orderToCall.pauseId = rec.pauseId;
                            //--    orderToCall.fetchGuid = rec.fetchGuid;
                            //--    orderToCall.aCommerreMark = rec.aCommerreMark;
                            model.Add(orderToCall);
                        }

                        Session["CallStart"] = DateTime.Now;//For Call duration calculation
                        if (callType == 1)
                        {
                            #region Prepare Call Logs Data for Unpaid Orders
                            var oldLogs = new List<viewCallingLogs>();
                            var oLogs = from parentObj in db.CallingLogs
                                        join childObj in db.AspNetUsers on parentObj.Caller equals childObj.Id
                                        join otherChild in db.LogEvents on parentObj.Event equals otherChild.ID
                                        where parentObj.orderId == orderCurrentId && parentObj.logType == 1
                                        orderby parentObj.logDate descending
                                        select new
                                        {
                                            CallingLogs = parentObj,
                                            Users = childObj,
                                            LogEvents = otherChild
                                        };
                            foreach (var item in oLogs)
                            {
                                viewCallingLogs lgs = new viewCallingLogs();
                                lgs.orderId = item.CallingLogs.orderId;
                                lgs.accountId = item.CallingLogs.accountId;
                                lgs.Caller = item.Users.FirstName + " " + item.Users.LastName;
                                lgs.Event = item.LogEvents.type;
                                lgs.logDate = item.CallingLogs.logDate;
                                lgs.nextCall = item.CallingLogs.nextCall;
                                lgs.orderCreated = item.CallingLogs.orderCreated;
                                lgs.Scope = item.CallingLogs.Scope;
                                oldLogs.Add(lgs);
                            }
                            ViewBag.CallingLogs = oldLogs;
                            #endregion
                        }
                        else if (callType == 2)
                        {
                            #region Prepare Call Logs Data for undelievered COD Orders
                            var oldLogs = new List<viewCallingLogs>();
                            var oLogs = from parentObj in db.CallingLogs
                                        join childObj in db.AspNetUsers on parentObj.Caller equals childObj.Id
                                        join otherChild in db.LogEvents on parentObj.Event equals otherChild.ID
                                        where parentObj.orderId == orderCurrentId && parentObj.logType == 2
                                        orderby parentObj.logDate descending
                                        select new
                                        {
                                            CallingLogs = parentObj,
                                            Users = childObj,
                                            LogEvents = otherChild
                                        };
                            foreach (var item in oLogs)
                            {
                                viewCallingLogs lgs = new viewCallingLogs();
                                lgs.orderId = item.CallingLogs.orderId;
                                lgs.accountId = item.CallingLogs.accountId;
                                lgs.Caller = item.Users.FirstName + " " + item.Users.LastName;
                                lgs.Event = item.LogEvents.type;
                                lgs.logDate = item.CallingLogs.logDate;
                                lgs.nextCall = item.CallingLogs.nextCall;
                                lgs.orderCreated = item.CallingLogs.orderCreated;
                                lgs.Scope = item.CallingLogs.Scope;
                                oldLogs.Add(lgs);
                            }
                            ViewBag.CallingLogs = oldLogs;
                            #endregion
                        }
                        else
                        {
                            #region Prepare Call Logs Data for Suspicious COD Orders
                            var oldLogs = new List<viewCallingLogs>();
                            var oLogs = from parentObj in db.CallingLogs
                                        join childObj in db.AspNetUsers on parentObj.Caller equals childObj.Id
                                        join otherChild in db.LogEvents on parentObj.Event equals otherChild.ID
                                        where parentObj.orderId == orderCurrentId && parentObj.logType == 4
                                        orderby parentObj.logDate descending
                                        select new
                                        {
                                            CallingLogs = parentObj,
                                            Users = childObj,
                                            LogEvents = otherChild
                                        };
                            foreach (var item in oLogs)
                            {
                                viewCallingLogs lgs = new viewCallingLogs();
                                lgs.orderId = item.CallingLogs.orderId;
                                lgs.accountId = item.CallingLogs.accountId;
                                lgs.Caller = item.Users.FirstName + " " + item.Users.LastName;
                                lgs.Event = item.LogEvents.type;
                                lgs.logDate = item.CallingLogs.logDate;
                                lgs.nextCall = item.CallingLogs.nextCall;
                                lgs.orderCreated = item.CallingLogs.orderCreated;
                                lgs.Scope = item.CallingLogs.Scope;
                                oldLogs.Add(lgs);
                            }
                            ViewBag.CallingLogs = oldLogs;
                            #endregion
                        }

                        #region Prepare Orderline Data
                        var Basket = new List<tblBasket>();
                        var basket = from parentObj in db.tblBasket
                                     where parentObj.paymentorderid == orderCurrentId
                                     orderby parentObj.created descending
                                     select new
                                     {
                                         Basket = parentObj,
                                     };
                        foreach (var itemBasket in basket)
                        {
                            tblBasket oneBasket = new tblBasket();
                            oneBasket.id = itemBasket.Basket.id;
                            oneBasket.itemnumber = itemBasket.Basket.itemnumber;
                            oneBasket.created = itemBasket.Basket.created;
                            oneBasket.antal = itemBasket.Basket.antal;
                            oneBasket.basValuta = itemBasket.Basket.basValuta;
                            oneBasket.basValutaPrice = itemBasket.Basket.basValutaPrice;
                            oneBasket.basValutaPriceTotal = itemBasket.Basket.basValutaPriceTotal;
                            oneBasket.paymentorderid = itemBasket.Basket.paymentorderid;
                            oneBasket.accountident = itemBasket.Basket.accountident;
                            Basket.Add(oneBasket);
                        }
                        ViewBag.BasketData = Basket;
                        #endregion
                        ViewBag.OrderID = orderCurrentId;
                        ViewBag.accountid = _accountid;
                        return View(model);
                        #endregion
                    }

                    #endregion
                    return View(model);
                }
                else
                {
                    TempData["Error"] = "Search not allowed.";
                    return RedirectToAction("Welcome", "Home");
                }
            }
            catch (Exception xp)
            {
                TempData["Error"] = "An error occurred, Please try again";
                return RedirectToAction("Welcome", "Home");
            }
           
        }

        [Authorize]        
        public ActionResult SearchOrderForMerge(string OrderId)
        {
            try
            {
                int nOrderId = Convert.ToInt32(OrderId);

                var orderData = db.tblOrders.Find(nOrderId);
                int orderCurrentId = orderData.id;
                int callType = orderData.orderstatusid == 1 ? 1 : 2;
                #region Load Options for the calling
                var modelLg = new List<LogEvents>();
                if (callType == 1)
                {
                    #region UnPaid Call option
                    var resultLg = from logEventsMapping in db.LogEventsMapping
                                   join logEvent in db.LogEvents
                                   on logEventsMapping.LogEventsId equals logEvent.ID
                                   join callingType in db.CallingTypes
                                   on logEventsMapping.CallingTypeId equals callingType.id
                                   where callingType.id == 1//unpaidOrders
                                   select logEvent;
                    foreach (var rec in resultLg)
                    {
                        LogEvents lg = new LogEvents();
                        lg.ID = rec.ID;
                        lg.type = rec.type;
                        modelLg.Add(lg);
                    }
                    #endregion
                }
                else if (callType == 2)
                {
                    #region Undelievered Call option
                    var resultLg = from logEventsMapping in db.LogEventsMapping
                                   join logEvent in db.LogEvents
                                   on logEventsMapping.LogEventsId equals logEvent.ID
                                   join callingType in db.CallingTypes
                                   on logEventsMapping.CallingTypeId equals callingType.id
                                   where callingType.id == 2//unpaidOrders
                                   select logEvent;
                    foreach (var rec in resultLg)
                    {
                        LogEvents lg = new LogEvents();
                        lg.ID = rec.ID;
                        lg.type = rec.type;
                        modelLg.Add(lg);
                    }
                    #endregion
                }
                ViewBag.LogEvents = modelLg;
                #endregion
                if (callType == 1)
                {
                    #region CheckMultipleOrders
                    // Check if this order's user has multple orders so that we can decide to go to Calling 1 or calling 2
                    int? accIdofCurrentOrder = orderData.accountid;

                    var orderSum = from order in db.tblOrders
                                   where order.accountid == accIdofCurrentOrder && order.orderstatusid == 1 && (order.deliveryoptionident == "BANKWIRE" || order.deliveryoptionident.Contains("2C2P"))
                                   group order by order.accountid
                                       into grp
                                   select new
                                   {
                                       AccountId = grp.Key,
                                       Count = grp.Select(x => x.accountid).Count()
                                   };
                    int currentOrderCount = 0;
                    foreach (var row in orderSum.OrderBy(x => x.AccountId))
                    {
                        currentOrderCount = row.Count;
                    }
                    if (currentOrderCount == 0)
                    {
                        TempData["Warning"] = "There is no order to process.";
                        return RedirectToAction("Welcome", "Home");
                    }
                    else if (currentOrderCount == 1)
                    {
                        var model = new List<CallingChilindo.Models.getOrdersToCallV2_Result>();
                        #region set OrderData
                        var orderToCall = new CallingChilindo.Models.getOrdersToCallV2_Result();
                        orderToCall.id = orderData.id;
                        orderToCall.created = orderData.created;
                        orderToCall.deliverytypeident = orderData.deliverytypeident;
                        orderToCall.deliveryoptionident = orderData.deliveryoptionident;
                        orderToCall.orderguid = orderData.orderguid;
                        orderToCall.orderstatusid = orderData.orderstatusid;
                        orderToCall.username = orderData.username;
                        orderToCall.accountid = orderData.accountid;
                        orderToCall.email = orderData.email;
                        orderToCall.name = orderData.name;
                        orderToCall.lastname = orderData.lastname;
                        orderToCall.address = orderData.address;
                        orderToCall.zip = orderData.zip;
                        orderToCall.city = orderData.city;
                        orderToCall.province = orderData.province;
                        orderToCall.provinceid = orderData.provinceid;
                        orderToCall.district = orderData.district;
                        orderToCall.country = orderData.country;
                        orderToCall.countrytxt = orderData.countrytxt;
                        orderToCall.phone = orderData.phone;
                        orderToCall.comment = orderData.comment;
                        orderToCall.webAmount = orderData.webAmount;
                        orderToCall.webFragt = orderData.webFragt;
                        orderToCall.webEkspgebyr = orderData.webEkspgebyr;
                        orderToCall.webAmountAuctions = orderData.webAmountAuctions;
                        orderToCall.webDiscount = orderData.webDiscount;
                        orderToCall.paidDate = orderData.paidDate;
                        orderToCall.sendDate = orderData.sendDate;
                        orderToCall.trackingcode = orderData.trackingcode;
                        orderToCall.districtsub = orderData.districtsub;
                     //   orderToCall.isCallAble = orderData.isCallable;
                    //    orderToCall.callBlockTime = orderData.callBlockTime;
                        orderToCall.webAmountSales = 0;
                    //    orderToCall.fetchGuid = Guid.NewGuid();
                     //   orderToCall.aCommerreMark = "";
                        model.Add(orderToCall);
                        #endregion
                        Session["CallStart"] = DateTime.Now;
                        if (!Request.RawUrl.Contains("localhost"))
                        {
                            var orderCurrent = db.tblOrders.Find(orderCurrentId);
                            orderCurrent.isCallable = false;
                            orderCurrent.callBlockTime = DateTime.Now;
                            db.Entry(orderCurrent).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        var oldLogs = new List<viewCallingLogs>();
                        var oLogs = from parentObj in db.CallingLogs
                                    join childObj in db.AspNetUsers on parentObj.Caller equals childObj.Id
                                    join otherChild in db.LogEvents on parentObj.Event equals otherChild.ID
                                    where parentObj.orderId == orderCurrentId
                                    orderby parentObj.logDate descending
                                    select new
                                    {
                                        CallingLogs = parentObj,
                                        Users = childObj,
                                        LogEvents = otherChild
                                    };
                        foreach (var item in oLogs)
                        {
                            viewCallingLogs lgs = new viewCallingLogs();
                            lgs.orderId = item.CallingLogs.orderId;
                            lgs.accountId = item.CallingLogs.accountId;
                            lgs.Caller = item.Users.FirstName + " " + item.Users.LastName;
                            lgs.Event = item.LogEvents.type;
                            lgs.logDate = item.CallingLogs.logDate;
                            lgs.nextCall = item.CallingLogs.nextCall;
                            lgs.orderCreated = item.CallingLogs.orderCreated;
                            lgs.Scope = item.CallingLogs.Scope;
                            oldLogs.Add(lgs);
                        }
                        ViewBag.CallingLogs = oldLogs;

                        //orderline
                        var Basket = new List<tblBasket>();
                        var basket = from parentObj in db.tblBasket
                                     where parentObj.paymentorderid == orderCurrentId
                                     orderby parentObj.created descending
                                     select new
                                     {
                                         Basket = parentObj,
                                     };
                        foreach (var itemBasket in basket)
                        {
                            tblBasket oneBasket = new tblBasket();
                            oneBasket.id = itemBasket.Basket.id;
                            oneBasket.itemnumber = itemBasket.Basket.itemnumber;
                            oneBasket.created = itemBasket.Basket.created;
                            oneBasket.antal = itemBasket.Basket.antal;
                            oneBasket.basValuta = itemBasket.Basket.basValuta;
                            oneBasket.basValutaPrice = itemBasket.Basket.basValutaPrice;
                            oneBasket.basValutaPriceTotal = itemBasket.Basket.basValutaPriceTotal;
                            oneBasket.paymentorderid = itemBasket.Basket.paymentorderid;
                            oneBasket.accountident = itemBasket.Basket.accountident;
                            Basket.Add(oneBasket);
                        }
                        ViewBag.BasketData = Basket;
                        ViewBag.OrderID = orderCurrentId;
                        ViewBag.calltype = 1;
                        ViewBag.accountid = accIdofCurrentOrder;
                        TempData["Message"] = "Mereged order loaded";
                        return View("Dashboard", model);
                    }
                    #endregion
                }
                else if (callType == 2)
                {
                    #region CheckMultipleOrders
                    // Check if this order's user has multple orders so that we can decide to go to Calling 1 or calling 2
                    int? accIdofCurrentOrder = orderData.accountid;

                    //var result = from parentObj in db.ParentTable 
                    //             join childObj in db.ChildObj 
                    //             on parentObj.PrimaryKey equals childObj.ForeignKey 
                    //             where childObj.propertyName && !parentObj.PropertyName && message.PropertyName 
                    //             select msg;

                    var orderSum = from parentObj in db.tblOrders
                                   join childObj in db.tblUndeliveredCODs
                                   on parentObj.id equals childObj.Invoice
                                   where parentObj.orderstatusid == 31 && parentObj.accountid == accIdofCurrentOrder && childObj.IsActive == true
                                   group parentObj by parentObj.accountid
                                       into grp
                                   select new
                                   {
                                       AccountId = grp.Key,
                                       Count = grp.Select(x => x.accountid).Count()
                                   };
                    int currentOrderCount = 0;
                    foreach (var row in orderSum.OrderBy(x => x.AccountId))
                    {
                        currentOrderCount = row.Count;
                    }
                    if (currentOrderCount == 0)
                    {
                        TempData["Warning"] = "There is no order to process.";
                        return RedirectToAction("Welcome", "Home");
                    }
                    else if (currentOrderCount == 1)
                    {
                        #region UNCOD Calling 1
                        var newOrderData = from parentObj in db.tblOrders
                                           join childObj in db.tblUndeliveredCODs
                                           on parentObj.id equals childObj.Invoice
                                           where parentObj.id == orderCurrentId
                                           select new
                                           {
                                               Order = parentObj,
                                               UNCOD = childObj
                                           };
                        var model = new List<CallingChilindo.Models.getOrdersToCallV2_Result>();
                        #region set OrderData
                        foreach (var item in newOrderData)
                        {
                            var orderToCall = new CallingChilindo.Models.getOrdersToCallV2_Result();
                            orderToCall.id = item.Order.id;
                            orderToCall.created = item.Order.created;
                            orderToCall.deliverytypeident = item.Order.deliverytypeident;
                            orderToCall.deliveryoptionident = item.Order.deliveryoptionident;
                            orderToCall.orderguid = item.Order.orderguid;
                            orderToCall.orderstatusid = item.Order.orderstatusid;
                            orderToCall.username = item.Order.username;
                            orderToCall.accountid = item.Order.accountid;
                            orderToCall.email = item.Order.email;
                            orderToCall.name = item.Order.name;
                            orderToCall.lastname = item.Order.lastname;
                            orderToCall.address = item.Order.address;
                            orderToCall.zip = item.Order.zip;
                            orderToCall.city = item.Order.city;
                            orderToCall.province = item.Order.province;
                            orderToCall.provinceid = item.Order.provinceid;
                            orderToCall.district = item.Order.district;
                            orderToCall.country = item.Order.country;
                            orderToCall.countrytxt = item.Order.countrytxt;
                            orderToCall.phone = item.Order.phone;
                            orderToCall.comment = item.Order.comment;
                            orderToCall.webAmount = item.Order.webAmount;
                            orderToCall.webFragt = item.Order.webFragt;
                            orderToCall.webEkspgebyr = item.Order.webEkspgebyr;
                            orderToCall.webAmountAuctions = item.Order.webAmountAuctions;
                            orderToCall.webDiscount = item.Order.webDiscount;
                            orderToCall.paidDate = item.Order.paidDate;
                            orderToCall.sendDate = item.Order.sendDate;
                            orderToCall.trackingcode = item.Order.trackingcode;
                            orderToCall.districtsub = item.Order.districtsub;
                         //   orderToCall.isCallAble = item.Order.isCallable;
                         //   orderToCall.callBlockTime = item.Order.callBlockTime;
                            orderToCall.webAmountSales = 0;
                       //     orderToCall.fetchGuid = Guid.NewGuid();
                       //     orderToCall.aCommerreMark = item.UNCOD.CarrierRemark;
                            model.Add(orderToCall);
                        }

                        #endregion
                        Session["CallStart"] = DateTime.Now;
                        if (!Request.RawUrl.Contains("localhost"))
                        {
                            var orderCurrent = db.tblOrders.Find(orderCurrentId);
                            orderCurrent.isCallable = false;
                            orderCurrent.callBlockTime = DateTime.Now;
                            db.Entry(orderCurrent).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        var oldLogs = new List<viewCallingLogs>();
                        var oLogs = from parentObj in db.CallingLogs
                                    join childObj in db.AspNetUsers on parentObj.Caller equals childObj.Id
                                    join otherChild in db.LogEvents on parentObj.Event equals otherChild.ID
                                    where parentObj.orderId == orderCurrentId
                                    orderby parentObj.logDate descending
                                    select new
                                    {
                                        CallingLogs = parentObj,
                                        Users = childObj,
                                        LogEvents = otherChild
                                    };
                        foreach (var item in oLogs)
                        {
                            viewCallingLogs lgs = new viewCallingLogs();
                            lgs.orderId = item.CallingLogs.orderId;
                            lgs.accountId = item.CallingLogs.accountId;
                            lgs.Caller = item.Users.FirstName + " " + item.Users.LastName;
                            lgs.Event = item.LogEvents.type;
                            lgs.logDate = item.CallingLogs.logDate;
                            lgs.nextCall = item.CallingLogs.nextCall;
                            lgs.orderCreated = item.CallingLogs.orderCreated;
                            lgs.Scope = item.CallingLogs.Scope;
                            oldLogs.Add(lgs);
                        }
                        ViewBag.CallingLogs = oldLogs;

                        //orderline
                        var Basket = new List<tblBasket>();
                        var basket = from parentObj in db.tblBasket
                                     where parentObj.paymentorderid == orderCurrentId
                                     orderby parentObj.created descending
                                     select new
                                     {
                                         Basket = parentObj,
                                     };
                        foreach (var itemBasket in basket)
                        {
                            tblBasket oneBasket = new tblBasket();
                            oneBasket.id = itemBasket.Basket.id;
                            oneBasket.itemnumber = itemBasket.Basket.itemnumber;
                            oneBasket.created = itemBasket.Basket.created;
                            oneBasket.antal = itemBasket.Basket.antal;
                            oneBasket.basValuta = itemBasket.Basket.basValuta;
                            oneBasket.basValutaPrice = itemBasket.Basket.basValutaPrice;
                            oneBasket.basValutaPriceTotal = itemBasket.Basket.basValutaPriceTotal;
                            oneBasket.paymentorderid = itemBasket.Basket.paymentorderid;
                            oneBasket.accountident = itemBasket.Basket.accountident;
                            Basket.Add(oneBasket);
                        }
                        ViewBag.BasketData = Basket;
                        ViewBag.OrderID = orderCurrentId;
                        ViewBag.calltype = 2;
                        ViewBag.accountid = accIdofCurrentOrder;
                        return View("Dashboard", model);
                        #endregion
                    }
                    else if (currentOrderCount == 2)
                    {
                        #region UNCOD Calling 2
                        var newOrderData = from parentObj in db.tblOrders
                                           join childObj in db.tblUndeliveredCODs
                                           on parentObj.id equals childObj.Invoice
                                           where parentObj.accountid == accIdofCurrentOrder
                                           select new
                                           {
                                               Order = parentObj,
                                               UNCOD = childObj
                                           };
                        var model = new List<CallingChilindo.Models.getOrdersToCallV2_Result>();
                        #region set OrderData
                        foreach (var item in newOrderData)
                        {
                            var orderToCall = new CallingChilindo.Models.getOrdersToCallV2_Result();
                            orderToCall.id = item.Order.id;
                            orderToCall.created = item.Order.created;
                            orderToCall.deliverytypeident = item.Order.deliverytypeident;
                            orderToCall.deliveryoptionident = item.Order.deliveryoptionident;
                            orderToCall.orderguid = item.Order.orderguid;
                            orderToCall.orderstatusid = item.Order.orderstatusid;
                            orderToCall.username = item.Order.username;
                            orderToCall.accountid = item.Order.accountid;
                            orderToCall.email = item.Order.email;
                            orderToCall.name = item.Order.name;
                            orderToCall.lastname = item.Order.lastname;
                            orderToCall.address = item.Order.address;
                            orderToCall.zip = item.Order.zip;
                            orderToCall.city = item.Order.city;
                            orderToCall.province = item.Order.province;
                            orderToCall.provinceid = item.Order.provinceid;
                            orderToCall.district = item.Order.district;
                            orderToCall.country = item.Order.country;
                            orderToCall.countrytxt = item.Order.countrytxt;
                            orderToCall.phone = item.Order.phone;
                            orderToCall.comment = item.Order.comment;
                            orderToCall.webAmount = item.Order.webAmount;
                            orderToCall.webFragt = item.Order.webFragt;
                            orderToCall.webEkspgebyr = item.Order.webEkspgebyr;
                            orderToCall.webAmountAuctions = item.Order.webAmountAuctions;
                            orderToCall.webDiscount = item.Order.webDiscount;
                            orderToCall.paidDate = item.Order.paidDate;
                            orderToCall.sendDate = item.Order.sendDate;
                            orderToCall.trackingcode = item.Order.trackingcode;
                            orderToCall.districtsub = item.Order.districtsub;
                         //   orderToCall.isCallAble = item.Order.isCallable;
                        //    orderToCall.callBlockTime = item.Order.callBlockTime;
                            orderToCall.webAmountSales = 0;
                       //     orderToCall.fetchGuid = Guid.NewGuid();
                       //     orderToCall.aCommerreMark = item.UNCOD.CarrierRemark;
                            model.Add(orderToCall);
                        }

                        #endregion
                        Session["CallStart"] = DateTime.Now;
                        if (!Request.RawUrl.Contains("localhost"))
                        {
                            var orderCurrent = db.tblOrders.Find(orderCurrentId);
                            orderCurrent.isCallable = false;
                            orderCurrent.callBlockTime = DateTime.Now;
                            db.Entry(orderCurrent).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        var oldLogs = new List<viewCallingLogs>();
                        var oLogs = from parentObj in db.CallingLogs
                                    join childObj in db.AspNetUsers on parentObj.Caller equals childObj.Id
                                    join otherChild in db.LogEvents on parentObj.Event equals otherChild.ID
                                    where parentObj.orderId == orderCurrentId
                                    orderby parentObj.logDate descending
                                    select new
                                    {
                                        CallingLogs = parentObj,
                                        Users = childObj,
                                        LogEvents = otherChild
                                    };
                        foreach (var item in oLogs)
                        {
                            viewCallingLogs lgs = new viewCallingLogs();
                            lgs.orderId = item.CallingLogs.orderId;
                            lgs.accountId = item.CallingLogs.accountId;
                            lgs.Caller = item.Users.FirstName + " " + item.Users.LastName;
                            lgs.Event = item.LogEvents.type;
                            lgs.logDate = item.CallingLogs.logDate;
                            lgs.nextCall = item.CallingLogs.nextCall;
                            lgs.orderCreated = item.CallingLogs.orderCreated;
                            lgs.Scope = item.CallingLogs.Scope;
                            oldLogs.Add(lgs);
                        }
                        ViewBag.CallingLogs = oldLogs;

                        ViewBag.accIdofCurrentOrder = accIdofCurrentOrder;
                        ViewBag.OrderID = orderCurrentId;
                        ViewBag.calltype = 2;
                        ViewBag.accountid = accIdofCurrentOrder;
                        return View("MultipleOrders", model);
                        #endregion
                    }
                    #endregion
                }
                Dictionary<string, object> error = new Dictionary<string, object>();
                error.Add("MessageCode", "0");
                error.Add("Message", "Value get");
                return Json(error, JsonRequestBehavior.AllowGet);
            }
            catch (Exception xp)
            {
                TempData["Warning"] = "There is no order to process.";
                return RedirectToAction("Welcome", "Home");
            }
        }
        [Authorize]
        [HttpPost]
        public ActionResult UpdateCallingLog(FormCollection form)
        {
            int orderId = 0;
            try
            {
                DateTime dtStartCall = Convert.ToDateTime(Session["CallStart"]);
                int nCallDuration = Convert.ToInt32(DateTime.Now.Subtract(dtStartCall).TotalSeconds);
                string id = Request.Form["id"];
                orderId = Convert.ToInt32(id);
                string optradioTODO = Request.Form["optradioTODO"];
                string optSearchOrder = "0";
                try
                {
                    optSearchOrder =Request.Form["optradioTODO"];
                    optSearchOrder = optSearchOrder.Trim();
                }
                catch(Exception)
                {
                }
                string logType = Request.Form["logType"];
                string dtDate = Request.Form["dtDate"];
                var orderCheck = entities.tblOrders.Where(x => x.id == orderId).FirstOrDefault();
                if(orderCheck.orderstatusid != 5 && logType == "4" )
                {
                    Dictionary<string, object> error = new Dictionary<string, object>();
                    error.Add("MsgCode", "-1");
                    error.Add("Message", "Order status is not valid.");
                    return Json(error, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    LogCall(optradioTODO, id, dtDate, nCallDuration, logType);
                    if (optradioTODO == "2")
                    {
                        SetNextCallDateInTblOrders(id, dtDate);
                    }
                    if(optSearchOrder == "1")
                    {
                        Session["Message"] = "Calling log updated successfully.";
                    }
                    Dictionary<string, object> Msg = new Dictionary<string, object>();
                    Msg.Add("MsgCode", "1");
                    Msg.Add("Message", "Calling log updated successfully.");
                    Msg.Add("optSearchOrder", optSearchOrder);
                    return Json(Msg, JsonRequestBehavior.AllowGet);
                }
               
            }
            catch (Exception xp)
            {
                tblErrorLogs errorlog = new tblErrorLogs();
                try
                {
                    errorlog.detail = xp.ToString();
                }
                catch (Exception)
                {
                }

                try
                {

                    errorlog.errorMsg = xp.InnerException.ToString();
                }
                catch (Exception)
                {
                    errorlog.errorMsg = "";
                }

                try
                {
                    errorlog.lineNum = 0;
                }
                catch (Exception)
                {
                }

                try
                {
                    errorlog.orderId = orderId;
                }
                catch (Exception)
                {
                }


                errorLog(errorlog);
                Dictionary<string, object> error = new Dictionary<string, object>();
                error.Add("MsgCode", "-1");
                error.Add("Message", "An error occurred, Please try again.");
                return Json(error, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult UpdateOrderInfo(FormCollection form)
        {
            try
            {
                string resultContent = "";
                //string Token = GetToken();
                //string id = Request.Form["id"];

                string orderGuid = "";
                string firstName = "";
                string lastName = "";
                string postcode = "";
                string province = "";
                string district = "";
                string districtsub = "";
                string phone = "";
                string deliveryInfo = "";
                string addressLine1 = "";
                string domain = "";
                string accountId = "";

                try
                {
                    orderGuid = Request.Form["hdnOrderGuid"];
                }
                catch(Exception)
                {
                }

                try
                {
                    firstName = Request.Form["firstname"];
                }
                catch (Exception)
                {
                }

                try
                {
                    lastName = Request.Form["lastname"];
                }
                catch (Exception)
                {
                }

                try
                {
                    postcode = Request.Form["postcode"];
                }
                catch (Exception)
                {
                }

                try
                {
                    province = Request.Form["province"];
                }
                catch (Exception)
                {
                }

                try
                {
                    district = Request.Form["district"];
                }
                catch (Exception)
                {
                }

                try
                {
                    districtsub = Request.Form["districtsub"];
                }
                catch (Exception)
                {
                }

                try
                {
                    phone = Request.Form["phone"];
                }
                catch (Exception)
                {
                }

                try
                {
                    deliveryInfo = Request.Form["info"];

                }
                catch (Exception)
                {
                }
                try
                {
                    addressLine1 = Request.Form["address"];
                }
                catch (Exception)
                {
                }

                try
                {
                    domain = Request.Form["hdnCountry"];

                }
                catch (Exception)
                {
                }

                try
                {
                    accountId = Request.Form["hdnAccount"];
                }
                catch (Exception)
                {
                }

                

                string body;
                using (StreamReader reader = new StreamReader(HostingEnvironment.MapPath("~/Templates/SaveOrderAddress.json")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("[orderGuid]", orderGuid);
                body = body.Replace("[firstName]", firstName);
                body = body.Replace("[lastName]", lastName);
                body = body.Replace("[postalCode]", postcode);
                body = body.Replace("[province]",province);
                body = body.Replace("[district]", district);
                body = body.Replace("[subDistrict]", districtsub);
                body = body.Replace("[phone]", phone);
                body = body.Replace("[deliveryInfo]", deliveryInfo);
                body = body.Replace("[addressLine1]", addressLine1);
                body = body.Replace("[domain]", domain);
                body = body.Replace("[accountId]", accountId);


                string GetOrderAddressControlUrl = "https://stage-api.chilindo.com/api/Address/v1/SaveOrderAddress";
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11;
                var client = new HttpClient();
                client.BaseAddress = new Uri(GetOrderAddressControlUrl);
                StringReader sr = new StringReader(body);
                StringContent theContent = new StringContent(sr.ReadToEnd(), Encoding.UTF8, "application/json");
                var result = client.PostAsync(GetOrderAddressControlUrl, theContent).Result;
                resultContent = result.Content.ReadAsStringAsync().Result;


                SaveOrderAddressJson res = new JavaScriptSerializer().Deserialize<SaveOrderAddressJson>(resultContent);

                //AddressControlObject s =GetOrderAddressControl(orderGuid);

                //com.chilindo.beta.orders.APIOrders obj = new com.chilindo.beta.orders.APIOrders();
                //XmlNode node = obj.UpdateOrderInfo(Token, id, Firstname, Lastname, Address, Postcode, Province, District, Subdistrict, Comment, Phone);
                //XmlDocument doc = new XmlDocument();
                //doc.LoadXml(node.OuterXml);
                //string jsonobj = JsonConvert.SerializeXmlNode(doc);
                Dictionary<string, object> Msg = new Dictionary<string, object>();
                if(res.isValid == true)
                {
                    Msg.Add("MsgCode", "1");
                    Msg.Add("Message", "OrderInfo updated successfully.");
                }
                else
                {
                    Msg.Add("MsgCode", "-1");
                    var jsonSerialiser = new JavaScriptSerializer();
                    var json = jsonSerialiser.Serialize(res.errorList);
                    Msg.Add("Message", json);
                }
                
                return Json(Msg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception xp)
            {
                Dictionary<string, object> error = new Dictionary<string, object>();
                error.Add("MsgCode", "-1");
                error.Add("Message", "An error occurred, Please try again.");
                return Json(error, JsonRequestBehavior.AllowGet);
            }
        }

        public class ErrorList
        {
            public string ident { get; set; }
            public string validationType { get; set; }
        }

        public class SaveOrderAddressJson
        {
            public bool isValid { get; set; }
            public List<ErrorList> errorList { get; set; }
        }

        [Authorize]
        [HttpPost]
        public ActionResult UpdateMultipleOrderInfo(FormCollection form)
        {
            try
            {
                string Token = GetToken();
                string id = Request.Form["id"];
                string Firstname = Request.Form["pnlOrderName"];
                string Lastname = Request.Form["pnlLastName"];
                string Address = Request.Form["pnlOrderStreet"];
                string Postcode = Request.Form["pnlPostCode"];
                string Province = Request.Form["pnlProvince"];
                string District = Request.Form["pnlDistrict"];
                string Subdistrict = Request.Form["pnlSubDistrict"];
                string Comment = Request.Form["pnlComment"];
                string Phone = Request.Form["pnlPhone"];
                com.chilindo.beta.orders.APIOrders obj = new com.chilindo.beta.orders.APIOrders();
                XmlNode node = obj.UpdateOrderInfo(Token, id, Firstname, Lastname, Address, Postcode, Province, District, Subdistrict, Comment, Phone);
               
                Dictionary<string, object> Msg = new Dictionary<string, object>();
                Msg.Add("MsgCode", "1");
                Msg.Add("Message", "OrderInfo updated successfully.");
                return Json(Msg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception xp)
            {
                Dictionary<string, object> error = new Dictionary<string, object>();
                error.Add("ErrorCode", "-1");
                error.Add("ErrorMessage", "An error occurred, Please try again.");
                return Json(error, JsonRequestBehavior.AllowGet);
            }
        }

        public class postcodeModel
        {
            public int id { get; set; }
            public string postcode { get; set; }
        }

        [Authorize]
        [HttpPost]
        public ActionResult LoadProvinceAPI(postcodeModel postcodeModel)
        {
            List<GetSingleJson> provinceList = new List<GetSingleJson>();
            maccadkEntities entities = new maccadkEntities();
            string apiToken = "";
            try
            {
                apiToken = GetLogin();
                var orderInfo = entities.tblOrders.Where(x => x.id == postcodeModel.id).FirstOrDefault();
                string postcode = "";

                if (orderInfo.country.ToUpper() == "TH" || orderInfo.country.ToUpper() == "MY" || orderInfo.country.ToUpper() == "NO" || orderInfo.country.ToUpper() == "VN")
                {
                    postcode = postcodeModel.postcode;
                    string ProvinceJson = GetProvince(apiToken, orderInfo.country, orderInfo.orderlanguage, postcode);
                    provinceList = new JavaScriptSerializer().Deserialize<List<GetSingleJson>>(ProvinceJson);
                }
            }
            catch (Exception)
            {
            }
            var jsonSerialiser = new JavaScriptSerializer();
            var json = jsonSerialiser.Serialize(provinceList);
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public class provinceModel
        {
            public int id { get; set; }
            public string province { get; set; }
            public string postcode { get; set; }
        }

        [Authorize]
        [HttpPost]
        public ActionResult LoadDistrictAPI(provinceModel provinceModel)
        {
            List<GetSingleJson> districtList = new List<GetSingleJson>();
            maccadkEntities entities = new maccadkEntities();
            string apiToken = "";
            try
            {
                apiToken = GetLogin();
                var orderInfo = entities.tblOrders.Where(x => x.id == provinceModel.id).FirstOrDefault();

                string parameter1 = "";
                string parameter2 = provinceModel.province;
                if (orderInfo.country.ToUpper() == "TH" || orderInfo.country.ToUpper() == "MY" || orderInfo.country.ToUpper() == "VN")
                {
                    if (orderInfo.country.ToUpper() != "VN")
                    {
                        parameter1 = provinceModel.postcode;
                    }
                    
                    string DistrictJson = GetDistrict(apiToken, orderInfo.country, orderInfo.orderlanguage, parameter1, parameter2);
                    districtList = new JavaScriptSerializer().Deserialize<List<GetSingleJson>>(DistrictJson);
                }
            }
            catch (Exception)
            {
            }
            var jsonSerialiser = new JavaScriptSerializer();
            var json = jsonSerialiser.Serialize(districtList);
            return Json(json, JsonRequestBehavior.AllowGet);
        }


        public class DistrictModel
        {
            public int id { get; set; }
            public string province { get; set; }
            public string postcode { get; set; }
            public string district { get; set; }
        }

        [Authorize]
        [HttpPost]
        public ActionResult LoadSubDistrictAPI(DistrictModel DistrictModel)
        {
            List<GetSingleJson> subDistrictList = new List<GetSingleJson>();
            maccadkEntities entities = new maccadkEntities();
            string apiToken = "";
            try
            {
                apiToken = GetLogin();
                var orderInfo = entities.tblOrders.Where(x => x.id == DistrictModel.id).FirstOrDefault();

                string parameter1 = "";
                string parameter2 = DistrictModel.district;
                if (orderInfo.country.ToUpper() == "TH" || orderInfo.country.ToUpper() == "VN")
                {
                    if (orderInfo.country.ToUpper() == "VN")
                    {
                        parameter1 = DistrictModel.province;
                    }
                    else
                    {
                        parameter1 = DistrictModel.postcode;
                    }

                    string SubDistrictJson = GetSubDistrict(apiToken, orderInfo.country, orderInfo.orderlanguage, parameter1, parameter2);
                    subDistrictList = new JavaScriptSerializer().Deserialize<List<GetSingleJson>>(SubDistrictJson);
                }
            }
            catch (Exception)
            {
            }
            var jsonSerialiser = new JavaScriptSerializer();
            var json = jsonSerialiser.Serialize(subDistrictList);
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult UpdateMergeOrders(FormCollection form)
        {
            try
            {
                string Token = GetToken();
                string id = Request.Form["id"];               
                com.chilindo.beta.orders.APIOrders obj = new com.chilindo.beta.orders.APIOrders();
                XmlNode node = obj.MergeOrders(Token, id, 1);
                var xml = new XmlDocument();
                xml.LoadXml(node.OuterXml);
                var Jsonstring = Newtonsoft.Json.JsonConvert.SerializeObject(xml);
                JObject jsonObj = JObject.Parse(Jsonstring);
                var response = jsonObj["chilindo"]["status"]["#text"];
                string[] numbers = Regex.Split(response.ToString(), @"\D+");
                int newOrderId = 0;
                foreach (string value in numbers)
                {
                    if (!string.IsNullOrEmpty(value))
                    {
                        newOrderId = int.Parse(value);                       
                    }
                };


                Dictionary <string, object> Msg = new Dictionary<string, object>();
                Msg.Add("MsgCode", "1");               
                Msg.Add("NewOrderId", Convert.ToString(newOrderId));
                Msg.Add("Message", "Merge Orders successfully.");
                return Json(Msg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception xp)
            {
                Dictionary<string, object> error = new Dictionary<string, object>();
                error.Add("ErrorCode", "-1");
                error.Add("ErrorMessage", "An error occurred, Please try again.");
                return Json(error, JsonRequestBehavior.AllowGet);
            }
        }


        private string RenderPartialViewToString(Controller controller, string viewName)
        {
            try
            {
                //controller.ViewData.Model = model;
                using (StringWriter sw = new StringWriter())
                {
                    ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                    ViewContext viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                    viewResult.View.Render(viewContext, sw);

                    return sw.ToString();
                }
            }
            catch (Exception e)
            {

                TempData["Error"] = "An error occurred, Please try again ";
            }
            return "";

        }

        public ActionResult EmailWrongNumber()
        {
            return View();
        }
        [Authorize]
        public ActionResult UpdateMultipleCallingLog(FormCollection form)
        {
            try
            {
                DateTime dtStartCall = Convert.ToDateTime(Session["CallStart"]);
                int nCallDuration = Convert.ToInt32(DateTime.Now.Subtract(dtStartCall).TotalSeconds);

                string id = Request.Form["id"].Contains(",") ? Request.Form["id"].Substring(0, Request.Form["id"].IndexOf(',')) : Request.Form["id"];
                string optradioTODO = Request.Form["optradioTODO"];
                string dtDate = Request.Form["dtDate"];
                string Scope = Request.Form["scope"];
                string logType = Request.Form["logType"];
                if (Scope == "1")
                {
                    if (optradioTODO == "3")
                    {
                        UpdateOrder(GetToken(), id, 0);
                    }
                    else if (optradioTODO == "4" )
                    {
                        UpdateOrder(GetToken(), id, 1);
                    }
                    else if (optradioTODO == "12")
                    {
                        int orderId = Convert.ToInt32(id);
                        maccadkEntities maccadk = new maccadkEntities();
                        ChilindoCallingEntities entities = new ChilindoCallingEntities();
                        string agentName = "";
                        try
                        {
                            string u = User.Identity.GetUserId();
                            agentName = entities.AspNetUsers.Where(x => x.Id == u).FirstOrDefault().UserName;
                        }
                        catch (Exception)
                        {
                        }
                        maccadk.setOrderCODCalling(orderId, agentName, "APPROVE");
                    }
                    else if (optradioTODO == "13")
                    {
                        int oId = Convert.ToInt32(id);
                        maccadkEntities maccadk = new maccadkEntities();
                        ChilindoCallingEntities entities = new ChilindoCallingEntities();
                        string agentName = "";
                        try
                        {
                            string u = User.Identity.GetUserId();
                            agentName = entities.AspNetUsers.Where(x => x.Id == u).FirstOrDefault().UserName;
                        }
                        catch (Exception)
                        {
                        }
                        maccadk.setOrderCODCalling(oId, agentName, "CANCEL");
                    }
                    else if (optradioTODO == "14")
                    {
                        int oId = Convert.ToInt32(id);
                        maccadkEntities maccadk = new maccadkEntities();
                        ChilindoCallingEntities entities = new ChilindoCallingEntities();
                        string agentName = "";
                        try
                        {
                            string u = User.Identity.GetUserId();
                            agentName = entities.AspNetUsers.Where(x => x.Id == u).FirstOrDefault().UserName;
                        }
                        catch (Exception)
                        {
                        }
                        maccadk.setOrderCODCalling(oId, agentName, "CALLALWAYS");
                    }
                    else
                        LogCallMultiple(optradioTODO, id, dtDate, "All", nCallDuration, logType);
                    if (optradioTODO == "2")
                        SetMultipleNextCalldateinTblOrders(id, dtDate);
                }
                else
                {
                    string[] AllIds = Request.Form["id"].Split(',');
                    for (int i = 0; i < AllIds.Length; i++)
                    {
                        optradioTODO = "optradioTODO_" + (i + 1);
                        optradioTODO = Request.Form[optradioTODO];
                        string date = "dtDate_" + (i + 1);
                        if (optradioTODO == "3")
                        {
                            UpdateOrder(GetToken(), AllIds[i], 0);
                        }
                        else if (optradioTODO == "4" )
                        {
                            UpdateOrder(GetToken(), AllIds[i], 1);
                        }
                        else if (optradioTODO == "12")
                        {
                            int orderId = Convert.ToInt32(id);
                            maccadkEntities maccadk = new maccadkEntities();
                            ChilindoCallingEntities entities = new ChilindoCallingEntities();
                            string agentName = "";
                            try
                            {
                                string u = User.Identity.GetUserId();
                                agentName = entities.AspNetUsers.Where(x => x.Id == u).FirstOrDefault().UserName;
                            }
                            catch (Exception)
                            {
                            }
                            maccadk.setOrderCODCalling(orderId, agentName, "APPROVE");
                        }
                        else if (optradioTODO == "13")
                        {
                            int oId = Convert.ToInt32(id);
                            maccadkEntities maccadk = new maccadkEntities();
                            ChilindoCallingEntities entities = new ChilindoCallingEntities();
                            string agentName = "";
                            try
                            {
                                string u = User.Identity.GetUserId();
                                agentName = entities.AspNetUsers.Where(x => x.Id == u).FirstOrDefault().UserName;
                            }
                            catch (Exception)
                            {
                            }
                            maccadk.setOrderCODCalling(oId, agentName, "CANCEL");
                        }
                        else if (optradioTODO == "14")
                        {
                            int oId = Convert.ToInt32(id);
                            maccadkEntities maccadk = new maccadkEntities();
                            ChilindoCallingEntities entities = new ChilindoCallingEntities();
                            string agentName = "";
                            try
                            {
                                string u = User.Identity.GetUserId();
                                agentName = entities.AspNetUsers.Where(x => x.Id == u).FirstOrDefault().UserName;
                            }
                            catch (Exception)
                            {
                            }
                            maccadk.setOrderCODCalling(oId, agentName, "CALLALWAYS");
                        }
                        else if (optradioTODO == "2")
                            SetNextCallDateInTblOrders(AllIds[i], Request.Form[date]);
                        LogCallMultiple(optradioTODO, AllIds[i], Request.Form[date], (i + 1) + " of " + AllIds.Length, nCallDuration, logType);
                    }
                }

                Dictionary<string, object> Msg = new Dictionary<string, object>();
                Msg.Add("MsgCode", "1");
                Msg.Add("Message", "Calling log updated successfully.");
                return Json(Msg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception xp)
            {
                Dictionary<string, object> error = new Dictionary<string, object>();
                error.Add("MsgCode", "-1");
                error.Add("Message", "An error occurred, Please try again.");
                return Json(error, JsonRequestBehavior.AllowGet);
            }
        }

        private void LogCall(string optionSelected, string orderId, string selectedDate, int duration, string logType)
        {
            CallingLogs callingLog = new CallingLogs();
            callingLog.Caller = User.Identity.GetUserId();
            callingLog.Event = Convert.ToInt32(optionSelected);
            callingLog.logDate = DateTime.Now;
            callingLog.callDuration = duration;

            if (optionSelected == "1" || optionSelected == "11")
            {
                callingLog.nextCall = PhoneNotPickedGetNextCallDate(orderId, optionSelected);
                if (logType == "2")
                {
                    callingLog.comments = "Phone not picked up: called on - " + DateTime.Now.ToString("dd/MM/yyyy");
                    UpdateFeedBackByclient(Convert.ToInt32(orderId), callingLog.comments);
                }
                if(optionSelected == "11")
                {
                    int oId = Convert.ToInt32(orderId);
                    maccadkEntities maccadk = new maccadkEntities();
                    string agentName = "";
                    try
                    {
                        string u = User.Identity.GetUserId();
                        agentName = db.AspNetUsers.Where(x => x.Id == u).FirstOrDefault().UserName;
                    }
                    catch (Exception)
                    {
                    }
                    maccadk.setOrderCODCalling(oId, agentName, "NOANSWER");
                }
            }
            else if (optionSelected == "12")
            {
                int oId = Convert.ToInt32(orderId);
                maccadkEntities maccadk = new maccadkEntities();
                string agentName = "";
                try
                {
                    string u = User.Identity.GetUserId();
                    agentName = db.AspNetUsers.Where(x => x.Id == u).FirstOrDefault().UserName;
                }
                catch (Exception)
                {
                }
                maccadk.setOrderCODCalling(oId, agentName, "APPROVE");
            }
            else if (optionSelected == "2")
            {
                int nValue = 7200;
                var firstRecord = db.tblSettings.ToList().FirstOrDefault();
                if (firstRecord != null)
                {
                    nValue = Convert.ToInt32(firstRecord.waitPayLater);
                }
                DateTime nextCallDate = DateTime.Parse(selectedDate);
                callingLog.nextCall = nextCallDate.AddSeconds(nValue);
                callingLog.payLaterDate = Convert.ToDateTime(selectedDate);
            }
            else if (optionSelected == "3")
            {

                UpdateOrder(GetToken(), orderId, 0);
            }
            else if (optionSelected == "13")
            {
                int oId = Convert.ToInt32(orderId);
                maccadkEntities maccadk = new maccadkEntities();
                string agentName = "";
                try
                {
                    string u = User.Identity.GetUserId();
                    agentName = db.AspNetUsers.Where(x => x.Id == u).FirstOrDefault().UserName;
                }
                catch (Exception)
                {
                }
                maccadk.setOrderCODCalling(oId, agentName, "CANCEL");
            }
            else if (optionSelected == "14")
            {
                int oId = Convert.ToInt32(orderId);
                maccadkEntities maccadk = new maccadkEntities();
                string agentName = "";
                try
                {
                    string u = User.Identity.GetUserId();
                    agentName = db.AspNetUsers.Where(x => x.Id == u).FirstOrDefault().UserName;
                }
                catch (Exception)
                {
                }
                maccadk.setOrderCODCalling(oId, agentName, "CALLALWAYS");
            }
            else if (optionSelected == "4" )
            {
                if (logType == "1" || logType == "4")
                    UpdateOrder(GetToken(), orderId, 1);
                else if (logType == "2")
                {
                    callingLog.comments = "Cancel Order - requested on:" + DateTime.Now.ToString("dd/MM/yyyy");
                    UpdateFeedBackByclient(Convert.ToInt32(orderId), callingLog.comments);
                    NeverCallThisClient(Convert.ToInt32(orderId));
                }
            }
            else if (optionSelected == "6")
            {
                callingLog.payLaterDate = DateTime.Parse(selectedDate);
                callingLog.amountpaid = Math.Round(Convert.ToDouble(Request.Form["amountPaid"].ToString()), 2);
                callingLog.comments = Request.Form["comments"].ToString();
            }
            else if (optionSelected == "5")
            {
                if (logType == "1")
                {
                    try { EmailClient(orderId); } catch (Exception xp) { }
                    try { CancelOrderBatchJob(); } catch (Exception xp) { }
                    try { CheckCreatedIfMoreThan7DaysOldToCancelOrder(orderId); } catch (Exception xp) { }
                }
                else if (logType == "2")
                {
                    try { EmailClient(orderId); } catch (Exception xp) { }
                    callingLog.comments = "Wrong Number - called on:" + DateTime.Now.ToString("dd/MM/yyyy") + " - " + Request.Form["comments"].ToString();
                    UpdateFeedBackByclient(Convert.ToInt32(orderId), callingLog.comments);
                }
            }
            else if (optionSelected == "9")
            {
                if (logType == "2")
                {
                    //Max 4 days from now
                    callingLog.payLaterDate = DateTime.ParseExact(selectedDate, "dd/MM/yyyy", null);
                    callingLog.comments = "Reschedule for: " + callingLog.payLaterDate + " - " + Request.Form["comments"].ToString();
                    UpdateFeedBackByclient(Convert.ToInt32(orderId), callingLog.comments);
                    int? _orderId = Convert.ToInt32(orderId);
                    tblUndeliveredCODs orderCurrent = db.tblUndeliveredCODs.Where(x => x.Invoice == _orderId).First();
                    //make the order available for next call after 2 days of the expiry
                    orderCurrent.CallBlockTime = callingLog.payLaterDate.Value.AddDays(2);
                    db.Entry(orderCurrent).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            else if (optionSelected == "10")
            {
                if (logType == "2")
                {
                    callingLog.comments = "Already Received - requested on:" + DateTime.Now.ToString("dd/MM/yyyy");
                    UpdateFeedBackByclient(Convert.ToInt32(orderId), callingLog.comments);
                    NeverCallThisClient(Convert.ToInt32(orderId));
                }

            }
           
            callingLog.orderId = Convert.ToInt32(orderId);
            callingLog.accountId = entities.tblOrders.Find(Convert.ToInt32(orderId)).accountid;
            callingLog.orderCreated = entities.tblOrders.Find(Convert.ToInt32(orderId)).created;
            callingLog.logType = Convert.ToInt32(logType);
            db.CallingLogs.Add(callingLog);
            db.SaveChanges();

        }

        private void errorLog(tblErrorLogs error)
        {
            try
            {
                db.tblErrorLogs.Add(error);
                db.SaveChanges();
            }
            catch(Exception xp)
            {
            }
        }

        private void EmailClient(string orderId)
        {
            var Order = entities.tblOrders.Find(Convert.ToInt32(orderId));
            string Data = RenderPartialViewToString(this, "EmailWrongNumber");
            Data = Data.Replace("[UserName]", Order.name);
            System.Net.Mail.MailMessage mailMessage =
            (System.Configuration.ConfigurationManager.AppSettings["isTestEmail"] == "1") ?
                mailMessage = new System.Net.Mail.MailMessage(new System.Net.Mail.MailAddress(System.Configuration.ConfigurationManager.AppSettings["MailgunSmtpUsername"], "www.Chilindo.com"),
                    new System.Net.Mail.MailAddress(System.Configuration.ConfigurationManager.AppSettings["testEmail"]))
            :
                mailMessage = new System.Net.Mail.MailMessage(new System.Net.Mail.MailAddress(System.Configuration.ConfigurationManager.AppSettings["MailgunSmtpUsername"], "www.Chilindo.com"),
                new System.Net.Mail.MailAddress(Order.email));
            mailMessage.Subject = System.Configuration.ConfigurationManager.AppSettings["WrongPhoneNumberSubject"];
            mailMessage.Body = Data;
            mailMessage.IsBodyHtml = true;
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(System.Configuration.ConfigurationManager.AppSettings["MailgunSmtpServer"]);
            smtp.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["MailgunSmtpUsername"], System.Configuration.ConfigurationManager.AppSettings["MailgunSmtpPassword"]);
            smtp.EnableSsl = true;
            smtp.Send(mailMessage);
        }

        private void LogCallMultiple(string optionSelected, string orderId, string selectedDate, string Scope, int duration, string logType)
        {
            CallingLogs callingLog = new CallingLogs();
            callingLog.Caller = User.Identity.GetUserId();
            callingLog.Event = Convert.ToInt32(optionSelected);
            callingLog.logDate = DateTime.Now;
            callingLog.Scope = Scope;
            callingLog.callDuration = duration;
            if (optionSelected == "1" || optionSelected == "11")
            {
                callingLog.nextCall = PhoneNotPickedGetNextCallDate(orderId, optionSelected);
                if (logType == "2")
                {
                    callingLog.comments = "Phone not picked up: called on - " + DateTime.Now.ToString("dd/MM/yyyy");
                    UpdateFeedBackByclient(Convert.ToInt32(orderId), callingLog.comments);
                }
                if (optionSelected == "11")
                {
                    int oId = Convert.ToInt32(orderId);
                    maccadkEntities maccadk = new maccadkEntities();
                    ChilindoCallingEntities entities = new ChilindoCallingEntities();
                    string agentName = "";
                    try
                    {
                        string u = User.Identity.GetUserId();
                        agentName = entities.AspNetUsers.Where(x => x.Id == u).FirstOrDefault().UserName;
                    }
                    catch (Exception)
                    {
                    }
                    maccadk.setOrderCODCalling(oId, agentName, "NOANSWER");
                }
            }
            else if (optionSelected == "2")
            {
                DateTime nextCallDate = DateTime.Parse(selectedDate);
                int nValue = 7200;
                var firstRecord = db.tblSettings.ToList().FirstOrDefault();
                if (firstRecord != null)
                {
                    nValue = Convert.ToInt32(firstRecord.waitPayLater);
                }
                callingLog.nextCall = nextCallDate.AddSeconds(nValue);
                callingLog.payLaterDate = Convert.ToDateTime(selectedDate);

            }
            else if (optionSelected == "4")
            {
                if (logType == "2")
                {
                    callingLog.comments = "Cancel Order - requested on:" + DateTime.Now.ToString("dd/MM/yyyy");
                    UpdateFeedBackByclient(Convert.ToInt32(orderId), callingLog.comments);
                    NeverCallThisClient(Convert.ToInt32(orderId));
                }
            }
            else if (optionSelected == "5")
            {
                if (logType == "1")
                {
                    try { EmailClient(orderId); } catch (Exception xp) { }
                    try { CancelOrderBatchJob(); } catch (Exception xp) { }
                    try { CheckCreatedIfMoreThan7DaysOldToCancelOrder(orderId); } catch (Exception xp) { }
                }
                else if (logType == "2")
                {
                    try { EmailClient(orderId); } catch (Exception xp) { }
                    callingLog.comments = "Wrong Number - called on:" + DateTime.Now.ToString("dd/MM/yyyy") + " - " + Request.Form["comments"].ToString();
                    UpdateFeedBackByclient(Convert.ToInt32(orderId), callingLog.comments);

                }
            }
            else if (optionSelected == "6")
            {
                callingLog.payLaterDate = DateTime.ParseExact(selectedDate, "mm/dd/yyyy", null);
                if (Scope == "All")
                {
                    callingLog.amountpaid = Math.Round(Convert.ToDouble(Request.Form["amountPaid"].ToString()), 2);
                    callingLog.comments = Request.Form["comments"].ToString();
                }
                else
                {
                    callingLog.amountpaid = Math.Round(Convert.ToDouble(Request.Form["amountPaid_" + Scope[0]].ToString()), 2);
                    callingLog.comments = Request.Form["comments_" + Scope[0]].ToString();

                }
            }
            else if (optionSelected == "9")
            {
                if (logType == "2")
                {
                    //Max 4 days from now
                    callingLog.payLaterDate = DateTime.ParseExact(selectedDate, "dd/MM/yyyy", null);
                    callingLog.comments = "Reschedule for: " + callingLog.payLaterDate + " - " + Request.Form["comments"].ToString();
                    UpdateFeedBackByclient(Convert.ToInt32(orderId), callingLog.comments);
                    int? _orderId = Convert.ToInt32(orderId);
                    tblUndeliveredCODs orderCurrent = db.tblUndeliveredCODs.Where(x => x.Invoice == _orderId).First();
                    //make the order available for next call after 2 days of the expiry
                    orderCurrent.CallBlockTime = callingLog.payLaterDate.Value.AddDays(2);
                    db.Entry(orderCurrent).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            else if (optionSelected == "10")
            {
                if (logType == "2")
                {
                    callingLog.comments = "Already Received - requested on:" + DateTime.Now.ToString("dd/MM/yyyy");
                    UpdateFeedBackByclient(Convert.ToInt32(orderId), callingLog.comments);
                    NeverCallThisClient(Convert.ToInt32(orderId));
                }

            }
            callingLog.orderId = Convert.ToInt32(orderId);
            callingLog.accountId = db.tblOrders.Find(Convert.ToInt32(orderId)).accountid;
            callingLog.orderCreated = db.tblOrders.Find(Convert.ToInt32(orderId)).created;
            callingLog.logType = Convert.ToInt32(logType);
            db.CallingLogs.Add(callingLog);
            db.SaveChanges();

        }

        public ActionResult LogoutRedirect()
        {
            try
            {
                if (Request.QueryString["id"] != null)
                {
                    var callable = db.tblOrders.Find(int.Parse(Request.QueryString["id"]));
                    callable.isCallable = true;
                    db.Entry(callable).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            catch (Exception xp) { }
            Response.Redirect("/Account/Logoff");
            return View();
        }
        public ActionResult LogoutNRedirect()
        {
            try
            {
                if (Request.QueryString["id"] != null)
                {
                    int accountID = int.Parse(Request.QueryString["id"]);
                    var resultCallable = from parentObj in db.tblOrders where parentObj.accountid == accountID select parentObj;
                    foreach (var rec in resultCallable)
                    {
                        try
                        {
                            var callable = db.tblOrders.Find(rec.id);
                            callable.isCallable = true;
                            db.Entry(callable).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        catch (Exception xp) { }
                    }
                }
            }
            catch (Exception xp) { }
            Response.Redirect("/Account/Logoff");
            return View();
        }

        public ActionResult Cancel(int id)
        {
            try
            {
                db.tblOrders.Find(id).isCallable = true;
                db.tblOrders.Find(id).callBlockTime = null;
                db.SaveChanges();
                TempData["Message"] = "Order cancelled successfully.";
                return RedirectToAction("Welcome", "Home");
            }
            catch (Exception xp)
            {
                return RedirectToAction("Login", "Account");
            }
        }
        public ActionResult CancelMultipleOrders(int accountID)
        {
            try
            {
                var result = db.tblOrders.Where(o => o.accountid == accountID).ToList();
                foreach (var item in result)
                {
                    item.isCallable = true;
                }
                db.SaveChanges();
                TempData["Warning"] = "Order cancelled successfully.";
                return RedirectToAction("Welcome", "Home");
            }
            catch (Exception xp)
            {
                return RedirectToAction("Login", "Account");
            }
        }
        public ActionResult GetTotalOrderCount(int accountID)
        {
            try
            {
                var orderobj = db.tblOrders.Where(s => s.accountid == accountID).FirstOrDefault();
                int OrdersTotal = 0, OrdersTotalCancelled = 0, OrdersTotalPaid = 0, OrdersTotalUnPaid = 0;
                if (orderobj != null)
                {
                    OrdersTotal = db.tblOrders.Where(s => s.accountid == accountID && s.orderstatusid <= 100).Count();
                    OrdersTotalCancelled = db.tblOrders.Where(s => s.accountid == accountID && s.orderstatusid == 100).Count();
                    var getdeliverytype = orderobj.deliveryoptionident;
                    if (getdeliverytype == "COD")
                    {
                        OrdersTotalPaid = db.tblOrders.Where(s => s.accountid == accountID && s.orderstatusid == 70).Count();
                        OrdersTotalUnPaid = db.tblOrders.Where(s => s.accountid == accountID && (s.orderstatusid >= 1 && s.orderstatusid <= 99) && s.orderstatusid != 70).Count();
                    }
                    else if (getdeliverytype == "CASH")
                    {
                        OrdersTotalPaid = db.tblOrders.Where(s => s.accountid == accountID && (s.orderstatusid == 12 || s.orderstatusid == 35 || s.orderstatusid == 36)).Count();
                        OrdersTotalUnPaid = db.tblOrders.Where(s => s.accountid == accountID && (s.orderstatusid >= 1 && s.orderstatusid <= 99) && (s.orderstatusid != 12 || s.orderstatusid != 35 || s.orderstatusid != 36)).Count();
                    }
                    else
                    {
                        OrdersTotalPaid = db.tblOrders.Where(s => s.accountid == accountID && (s.orderstatusid >= 2 && s.orderstatusid <= 99)).Count();
                        OrdersTotalUnPaid = db.tblOrders.Where(s => s.accountid == accountID && (s.orderstatusid == 1)).Count();
                    }
                }
                Dictionary<string, object> Msg = new Dictionary<string, object>();
                Msg.Add("MsgCode", "1");
                Msg.Add("OrdersTotal", OrdersTotal.ToString());
                Msg.Add("OrdersTotalCancelled", OrdersTotalCancelled.ToString());
                Msg.Add("OrdersTotalPaid", OrdersTotalPaid.ToString());
                Msg.Add("OrdersTotalUnPaid", OrdersTotalUnPaid.ToString());
                return Json(Msg, JsonRequestBehavior.AllowGet);


            }
            catch (Exception e)
            {
                Dictionary<string, object> error = new Dictionary<string, object>();
                error.Add("ErrorCode", "-1");
                error.Add("ErrorMessage", "An error occurred, Please try again.");
                return Json(error, JsonRequestBehavior.AllowGet);
            }
            
        }
        private void SetNextCallDateInTblOrders(string id, string selectedDate)
        {
            try
            {
                db.tblOrders.Find(Convert.ToInt32(id)).callBlockTime = DateTime.Parse(selectedDate).AddMinutes(30);
                db.SaveChanges();
            }
            catch (Exception xp)
            {
            }
        }
        private void SetMultipleNextCalldateinTblOrders(string id, string selectedDate)
        {
            try
            {
                int? accountId = db.tblOrders.Find(Convert.ToInt32(id)).accountid;
                var allOrdersOfThisaccountId = from parentObj in db.tblOrders where parentObj.accountid == accountId select parentObj;
                foreach (var item in allOrdersOfThisaccountId)
                {
                    db.tblOrders.Find(item.id).callBlockTime = DateTime.Parse(selectedDate).AddMinutes(30);
                    db.SaveChanges();
                }
            }
            catch (Exception xp)
            {
            }
        }
        private DateTime PhoneNotPickedGetNextCallDate(string orderID,string optionSelected)
        {
            int nValue = 7200;
            var firstRecord = db.tblSettings.ToList().FirstOrDefault();
            int nTotalCallsSoFar = db.CallingLogs.Where(x => x.orderId.ToString() == orderID).Count();
            if (optionSelected != "11")
            {
                if (nTotalCallsSoFar > db.tblSettings.Find(1).cancelOrderRetries) CancelOrderOnNoPickUp(orderID);
            }
            switch (nTotalCallsSoFar)
            {
                case 0:
                    {
                        if (firstRecord != null)
                        {
                            nValue = Convert.ToInt32(firstRecord.firstRecall);
                        }
                        return DateTime.Now.AddSeconds(nValue);
                    }

                case 1:
                    {
                        if (firstRecord != null)
                        {
                            nValue = Convert.ToInt32(firstRecord.secondRecall);
                        }
                        return DateTime.Now.AddSeconds(nValue);
                    }
                case 2:
                    {
                        if (firstRecord != null)
                        {
                            nValue = Convert.ToInt32(firstRecord.thirdRecall);
                        }
                        return DateTime.Now.AddSeconds(nValue);
                    }
                case 3:
                    {
                        if (firstRecord != null)
                        {
                            nValue = Convert.ToInt32(firstRecord.fourthRecall);
                        }
                        return DateTime.Now.AddSeconds(nValue);
                    }
                case 4:
                    {
                        if (firstRecord != null)
                        {
                            nValue = Convert.ToInt32(firstRecord.fifthRecall);
                        }
                        return DateTime.Now.AddSeconds(nValue);
                    }
                case 5:
                    {
                        if (firstRecord != null)
                        {
                            nValue = Convert.ToInt32(firstRecord.sixthRecall);
                        }
                        return DateTime.Now.AddSeconds(nValue);
                    }
                case 6:
                    {
                        if (firstRecord != null)
                        {
                            nValue = Convert.ToInt32(firstRecord.seventhRecall);
                        }
                        return DateTime.Now.AddSeconds(nValue);
                    }
                case 7:
                    {
                        if (firstRecord != null)
                        {
                            nValue = Convert.ToInt32(firstRecord.eighthRecall);
                        }
                        return DateTime.Now.AddSeconds(nValue);
                    }
                case 8:
                    {
                        if (firstRecord != null)
                        {
                            nValue = Convert.ToInt32(firstRecord.ninthRecall);
                        }
                        return DateTime.Now.AddSeconds(nValue);
                    }
                case 9:
                    {
                        if (firstRecord != null)
                        {
                            nValue = Convert.ToInt32(firstRecord.tenthRecall);
                        }
                        return DateTime.Now.AddSeconds(nValue);
                    }
                case 10:
                    {
                        if (firstRecord != null)
                        {
                            nValue = Convert.ToInt32(firstRecord.eleventhRecall);
                        }
                        return DateTime.Now.AddSeconds(nValue);
                    }
                case 11:
                    {
                        if (firstRecord != null)
                        {
                            nValue = Convert.ToInt32(firstRecord.twelvthRecall);
                        }
                        return DateTime.Now.AddSeconds(nValue);
                    }

                case 12:
                    {
                        if (firstRecord != null)
                        {
                            nValue = Convert.ToInt32(firstRecord.thirteenthRecall);
                        }
                        return DateTime.Now.AddSeconds(nValue);
                    }


                case 13:
                    {
                        if (firstRecord != null)
                        {
                            nValue = Convert.ToInt32(firstRecord.fourteenthRecall);
                        }
                        return DateTime.Now.AddSeconds(nValue);
                    }
                case 14:
                    {
                        if (firstRecord != null)
                        {
                            nValue = Convert.ToInt32(firstRecord.fifteenthRecall);
                        }
                        return DateTime.Now.AddSeconds(nValue);
                    }
                case 15:
                    {
                        if (firstRecord != null)
                        {
                            nValue = Convert.ToInt32(firstRecord.sixteenthRecall);
                        }
                        return DateTime.Now.AddSeconds(nValue);
                    }
                case 16:
                    {
                        if (firstRecord != null)
                        {
                            nValue = Convert.ToInt32(firstRecord.seventeenthRecall);
                        }
                        return DateTime.Now.AddSeconds(nValue);
                    }
                case 17:
                    {
                        if (firstRecord != null)
                        {
                            nValue = Convert.ToInt32(firstRecord.eighteenthRecall);
                        }
                        return DateTime.Now.AddSeconds(nValue);
                    }

                default: return DateTime.Now.AddSeconds(10800);
            }

        }
        private void CancelOrderBatchJob()
        {
            //var logs = db.CallingLogs.Where(o => o.Event == 5).ToList();
            var logs = from parentObj in db.CallingLogs join childObj in entities.tblOrders on parentObj.orderId equals childObj.id where parentObj.Event == 5 && childObj.orderstatusid == 1 select new { CallingLogs = parentObj };
            foreach (var item in logs)
            {
                try
                {
                    if ((DateTime.Now - db.tblOrders.Where(o => o.id == item.CallingLogs.orderId && o.orderstatusid == 1).First().created).Days >= db.tblSettings.First().nCancelWrongNumberOrderAfterDays)
                    {
                        UpdateOrder(GetToken(), item.CallingLogs.orderId.ToString(), 1);
                    }
                }
                catch (Exception xp) { }
            }
        }
        private void CancelOrderOnNoPickUp(string orderID)
        {
            try
            {
                int nOrderID = int.Parse(orderID);
                if ((DateTime.Now - db.tblOrders.Where(o => o.id == nOrderID).First().created).Days >= db.tblSettings.First().nCancelUnPickedUpOrderAfterDays)
                {
                    UpdateOrder(GetToken(), orderID, 1);
                }
            }
            catch (Exception xp) { }
        }
        private bool CheckCreatedIfMoreThan7DaysOldToCancelOrder(string orderId)
        {
            try
            {
                var order = entities.tblOrders.Find(int.Parse(orderId));
                if (order.created < DateTime.Now.AddDays(-1 * db.tblSettings.Find(1).nCancelUnPickedUpOrderAfterDays))
                    UpdateOrder(GetToken(), orderId, 1);
                return true;
            }
            catch (Exception xp)
            {
                return true;
            }
        }
        private string GetToken()
        {
            com.chilindo.beta.Generic obj = new com.chilindo.beta.Generic();
            XmlNode node = obj.GetAccessToken4API("api_orders", "E27D!4E8C", com.chilindo.beta.APIcalls.APIORDER);
            return node.ChildNodes[0].InnerText;
        }
        private bool UpdateOrder(string Token, string Orderid, int type)
        {
            Orderid = entities.tblOrders.Find(Convert.ToInt32(Orderid)).orderguid;
            //change to COD
            if (type == 0)
            {
                com.chilindo.beta.orders.APIOrders obj = new com.chilindo.beta.orders.APIOrders();              
                XmlNode node = obj.ChangeDeliveryPaymentOnOrder(Token, Orderid, "COD", "COD");
                return true;
            }
            //change to Cancel
            if (type == 1)
            {
                com.chilindo.beta.orders.APIOrders obj = new com.chilindo.beta.orders.APIOrders();
                //XmlNode node = obj.ChangeDeliveryPaymentOnOrder(Token, Orderid, "COD", "COD");
                XmlNode node = obj.CancelOrder(Token, Orderid);
                return true;
            }
            return false;
        }
        
        private void UpdateFeedBackByclient(int nOrderId, string feedback)
        {
            tblUndeliveredCODs orderCurrent = db.tblUndeliveredCODs.Where(x => x.Invoice == nOrderId).First();
            orderCurrent.FeedbackByClient = feedback;
            db.Entry(orderCurrent).State = EntityState.Modified;
            db.SaveChanges();
        }
        private void NeverCallThisClient(int nOrderId)
        {
            tblUndeliveredCODs orderCurrent = db.tblUndeliveredCODs.Where(x => x.Invoice == nOrderId).First();
            orderCurrent.CallBlockTime = DateTime.Now.AddYears(1);
            db.Entry(orderCurrent).State = EntityState.Modified;
            db.SaveChanges();
        }
        #region Commented Code
        //private DateTime UCODPhoneNotPickedGetNextCallDate(string orderID)
        //{
        //    int nValue = 7200;
        //    var firstRecord = db.Settings.ToList().FirstOrDefault();
        //    int nTotalCallsSoFar = db.CallingCODUndeliveredLogs.Where(x => x.orderId.ToString() == orderID).Count();
        //    //if (nTotalCallsSoFar > db.Settings.Find(1).cancelOrderRetries) CancelOrderOnNoPickUp(orderID);
        //    switch (nTotalCallsSoFar)
        //    {
        //        case 0:
        //            {
        //                if (firstRecord != null)
        //                {
        //                    nValue = firstRecord.firstRecall;
        //                }
        //                return DateTime.Now.AddSeconds(nValue);
        //            }

        //        case 1:
        //            {
        //                if (firstRecord != null)
        //                {
        //                    nValue = firstRecord.secondRecall;
        //                }
        //                return DateTime.Now.AddSeconds(nValue);
        //            }
        //        case 2:
        //            {
        //                if (firstRecord != null)
        //                {
        //                    nValue = firstRecord.thirdRecall;
        //                }
        //                return DateTime.Now.AddSeconds(nValue);
        //            }
        //        case 3:
        //            {
        //                if (firstRecord != null)
        //                {
        //                    nValue = firstRecord.fourthRecall;
        //                }
        //                return DateTime.Now.AddSeconds(nValue);
        //            }
        //        case 4:
        //            {
        //                if (firstRecord != null)
        //                {
        //                    nValue = firstRecord.fifthRecall;
        //                }
        //                return DateTime.Now.AddSeconds(nValue);
        //            }
        //        default: return DateTime.Now;
        //    }

        //}
        //[Authorize]
        //public ActionResult UpdateUCODMultipleLog(FormCollection form)
        //{
        //    try
        //    {
        //        DateTime dtStartCall = Convert.ToDateTime(Session["CallStart"]);
        //        int nCallDuration = Convert.ToInt32(DateTime.Now.Subtract(dtStartCall).TotalSeconds);

        //        string id = Request.Form["id"].Contains(",") ? Request.Form["id"].Substring(0, Request.Form["id"].IndexOf(',')) : Request.Form["id"];
        //        string optradioTODO = Request.Form["optradioTODO"];
        //        string dtDate = Request.Form["dtDate"];
        //        string Scope = Request.Form["scope"];
        //        if (Scope == "1")
        //        {
        //            LogUCODCallMultiple(optradioTODO, id, dtDate, "All", nCallDuration);
        //        }
        //        else
        //        {
        //            string[] AllIds = Request.Form["id"].Split(',');
        //            for (int i = 0; i < AllIds.Length; i++)
        //            {
        //                optradioTODO = "optradioTODO_" + (i + 1);
        //                optradioTODO = Request.Form[optradioTODO];
        //                string date = "dtDate_" + (i + 1);
        //                LogUCODCallMultiple(optradioTODO, AllIds[i], Request.Form[date], (i + 1) + " of " + AllIds.Length, nCallDuration);
        //            }
        //        }

        //        Dictionary<string, object> Msg = new Dictionary<string, object>();
        //        Msg.Add("MsgCode", "1");
        //        Msg.Add("Message", "Calling log updated successfully.");
        //        return Json(Msg, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception xp)
        //    {
        //        Dictionary<string, object> error = new Dictionary<string, object>();
        //        error.Add("ErrorCode", "-1");
        //        error.Add("ErrorMessage", "An error occurred, Please try again.");
        //        return Json(error, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //[Authorize]
        //[HttpPost]
        //public ActionResult UpdateCODUndeliveredCallingLog(FormCollection form)
        //{
        //    try
        //    {
        //        //int n = DateTime.Now.Minute - Session["nCallDuration"];
        //        //Session["CallEnd"] = DateTime.Now.Subtract - ;
        //        DateTime dtStartCall = Convert.ToDateTime(Session["CallStart"]);
        //        int nCallDuration = Convert.ToInt32(DateTime.Now.Subtract(dtStartCall).TotalSeconds);
        //        //DateTime.now Session["CallStart"];
        //        //int nCallEnd = int.Parse(DateTime.Now.ToString("mm"));
        //        //int nCallStart = Convert.ToInt32( Session["CallStart"]);
        //        //int nCallDuration = nCallEnd - nCallStart;

        //        string id = Request.Form["id"];
        //        string optradioTODO = Request.Form["optradioTODO"];
        //        string dtDate = Request.Form["dtDate"];

        //        LogUndeliveredCODCall(optradioTODO, id, dtDate, nCallDuration);
        //        //if (optradioTODO == "2") {
        //        //    SetNextCalldateinTblOrders(id,dtDate);
        //        //}
        //        Dictionary<string, object> Msg = new Dictionary<string, object>();
        //        Msg.Add("MsgCode", "1");
        //        Msg.Add("Message", "COD Undelivered Calling log updated successfully.");
        //        return Json(Msg, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception xp)
        //    {
        //        Dictionary<string, object> error = new Dictionary<string, object>();
        //        error.Add("ErrorCode", "-1");
        //        error.Add("ErrorMessage", "An error occurred, Please try again.");
        //        return Json(error, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //private void LogUndeliveredCODCall(string optionSelected, string orderId, string selectedDate, int duration)
        //{
        //    CallingCODUndeliveredLogs callingLog = new CallingCODUndeliveredLogs();
        //    callingLog.Caller = User.Identity.GetUserId();
        //    callingLog.Event = Convert.ToInt32(optionSelected);
        //    callingLog.logDate = DateTime.Now;
        //    callingLog.callDuration = duration;
        //    int nOrderId = Convert.ToInt32(orderId);
        //    if (optionSelected == "1")
        //    {
        //        callingLog.nextCall = UCODPhoneNotPickedGetNextCallDate(orderId);
        //        callingLog.comments = "Phone not picked up: called on - " + DateTime.Now.ToString("dd/MM/yyyy");
        //        //NeverCallThisClient(nOrderId);
        //        UpdateFeedBackByclient(nOrderId, callingLog.comments);
        //    }
        //    else if (optionSelected == "4")
        //    {
        //        callingLog.comments = "Cancel Order - requested on:" + DateTime.Now.ToString("dd/MM/yyyy");
        //        UpdateFeedBackByclient(nOrderId, callingLog.comments);
        //        NeverCallThisClient(nOrderId);
        //    }
        //    else if (optionSelected == "5")
        //    {
        //        callingLog.comments = "Wrong Number - called on:" + DateTime.Now.ToString("dd/MM/yyyy");
        //        //NeverCallThisClient(nOrderId);
        //        //UpdateFeedBackByclient(nOrderId, callingLog.comments);

        //    }
        //    else if (optionSelected == "9")
        //    {
        //        //4 days starting 
        //        callingLog.payLaterDate = DateTime.ParseExact(selectedDate, "dd/MM/yyyy", null);
        //        callingLog.comments = "Reschedule for: " + callingLog.payLaterDate + " - " + Request.Form["comments"].ToString();
        //        UpdateFeedBackByclient(nOrderId, callingLog.comments);
        //        tblUndeliveredCODs orderCurrent = dbOrderCallables.tblUndeliveredCODs.Where(x => x.SerialNo == nOrderId.ToString()).First();
        //        orderCurrent.CallBlockTime = callingLog.payLaterDate.Value.AddDays(2);
        //        dbOrderCallables.Entry(orderCurrent).State = EntityState.Modified;
        //        dbOrderCallables.SaveChanges();
        //    }
        //    callingLog.orderId = Convert.ToInt32(orderId);
        //    callingLog.orderCreated = db.tblOrders.Find(Convert.ToInt32(orderId)).created;
        //    db.CallingCODUndeliveredLogs.Add(callingLog);
        //    db.SaveChanges();

        //}
        //private void LogUCODCallMultiple(string optionSelected, string orderId, string selectedDate, string Scope, int duration)
        //{
        //    CallingCODUndeliveredLogs callingLog = new CallingCODUndeliveredLogs();
        //    callingLog.Caller = User.Identity.GetUserId();
        //    callingLog.Event = Convert.ToInt32(optionSelected);
        //    callingLog.logDate = DateTime.Now;
        //    callingLog.Scope = Scope;
        //    callingLog.callDuration = duration;
        //    int nOrderId = Convert.ToInt32(orderId);

        //    if (optionSelected == "1")
        //    {
        //        callingLog.nextCall = UCODPhoneNotPickedGetNextCallDate(orderId);
        //        callingLog.comments = "Phone not picked up: called on - " + DateTime.Now.ToString("dd/MM/yyyy");
        //        //UpdateFeedBackByclient(nOrderId, callingLog.comments);
        //    }
        //    else if (optionSelected == "4")
        //    {
        //        callingLog.comments = "Cancel Order - requested on:" + DateTime.Now.ToString("dd/MM/yyyy");
        //        UpdateFeedBackByclient(nOrderId, callingLog.comments);
        //        NeverCallThisClient(nOrderId);
        //    }
        //    else if (optionSelected == "5")
        //    {
        //        callingLog.comments = "Wrong Number - called on:" + DateTime.Now.ToString("dd/MM/yyyy");
        //        //UpdateFeedBackByclient(nOrderId, callingLog.comments);
        //    }
        //    else if (optionSelected == "9")
        //    {
        //        callingLog.payLaterDate = DateTime.ParseExact(selectedDate, "mm/dd/yyyy", null);
        //        if (Scope == "All")
        //        {
        //            callingLog.comments = "Reschedule for: - " + callingLog.payLaterDate + " - " + Request.Form["comments"].ToString();
        //        }
        //        else
        //        {
        //            callingLog.comments = "Reschedule for: - " + callingLog.payLaterDate + " - " + Request.Form["comments_" + Scope[0]].ToString();
        //        }
        //        UpdateFeedBackByclient(nOrderId, callingLog.comments);
        //        tblUndeliveredCODs orderCurrent = dbOrderCallables.tblUndeliveredCODs.Where(x => x.SerialNo == nOrderId.ToString()).First();
        //        orderCurrent.CallBlockTime = callingLog.payLaterDate.Value.AddDays(2); ;
        //        dbOrderCallables.Entry(orderCurrent).State = EntityState.Modified;
        //        dbOrderCallables.SaveChanges();
        //    }
        //    callingLog.orderId = Convert.ToInt32(orderId);
        //    callingLog.orderCreated = db.tblOrders.Find(Convert.ToInt32(orderId)).created;
        //    db.CallingCODUndeliveredLogs.Add(callingLog);
        //    db.SaveChanges();

        //}
        //[Authorize]
        //public ActionResult UndeliveredCODDashboard()
        //{
        //    try
        //    {
        //        #region Init intializing and setting variable about AutoLog out for example if there is 15 mins inactivity then after 15 mins show popup and auto loggout
        //        int miliSecToLogout = 60000 * db.Settings.Find(1).nAutoLogoutValue;
        //        ViewBag.miliSecToLogout = miliSecToLogout;
        //        ViewBag.UserName = Session["LogedUserFullname"];
        //        ViewBag.Message = "Welcome to Calling Chilindo.";
        //        var model = new List<UndeliveredCODsFullInfo>();
        //        int hoursToTakeOrder = 0;
        //        ////After receiving the order call the user after this much time
        //        hoursToTakeOrder = -1 * db.Settings.Find(1).UCODTimeToWaitBeforeCall/60;
        //        //Minus the time from current time
        //        DateTime startCallingFrom = DateTime.Now.AddHours(hoursToTakeOrder);//DateTime.Now;//
        //        //Block time 30 mins the current order to be unavailable for ither users
        //        //DateTime callBlockTime = DateTime.Now.AddMinutes(-1 * db.Settings.Find(1).callBlockTime);
        //        DateTime callBlockTime = DateTime.Now.AddMinutes(-1 * 30);

        //        ////Check if the debit card orders to process first 2C2p=debot card
        //        //bool precedence2C2P = db.Settings.First().precedence2C2P;
        //        #endregion
        //        #region 4 Options //pick the 4 options to process on the orders by calling i.e phone not picked, wrong number, change to cod etc
        //        var modelLg = new List<LogEvents>();
        //        //var resultLg = (from parentObj in db.LogEvents where (new Int32[] { 1, 4, 5, 9 }).Contains(parentObj.ID) select parentObj);
        //        var resultLg = from logEventsMapping in db.LogEventsMappings
        //                       join logEvent in db.LogEvents
        //                       on logEventsMapping.LogEventId equals logEvent.ID
        //                       join callingType in db.CallingTypes
        //                       on logEventsMapping.CallingTypeId equals callingType.id
        //                       where callingType.id == 2//unpaidOrders
        //                       select logEvent;
        //        foreach (var rec in resultLg)
        //        {
        //            LogEvents lg = new LogEvents();
        //            lg.ID = rec.ID;
        //            lg.type = rec.type;
        //            modelLg.Add(lg);
        //        }
        //        ViewBag.LogEvents = modelLg;
        //        #endregion
        //        //#region Fetch Orders
        //            string userId = User.Identity.GetUserId();
        //        #region CallUndeliveredCOD
        //            var result = (from parentObj in db.tblOrders
        //                          join childObj in db.tblUndeliveredCODs on parentObj.id equals Convert.ToInt32(childObj.SerialNo)
        //                          where childObj.CallBlockTime <= callBlockTime
        //                          orderby childObj.CallBlockTime ascending
        //                          select new
        //                          {
        //                              Order = parentObj,
        //                              UDCODORDER = childObj
        //                          }).Take(1);
        //        if (result.Count() == 0)
        //                {
        //                    TempData["Warning"] = "There is no order to process.";
        //                    return View(model);
        //                }
        //        int orderCurrentId = -1;
        //        string orderCurrentTel = "";
        //        string orderCurrentSerialNo = "";
        //        foreach (var rec in result)
        //                {
        //                    UndeliveredCODsFullInfo order = new UndeliveredCODsFullInfo();
        //                    order.Id = rec.Order.id;
        //                    orderCurrentId = rec.Order.id;
        //                    order.Invoice = rec.UDCODORDER.Invoice;
        //                    order.SerialNo = rec.UDCODORDER.SerialNo;
        //                    orderCurrentSerialNo = rec.UDCODORDER.SerialNo;
        //                    order.Order = rec.UDCODORDER.Order;
        //                    order.Customer = rec.UDCODORDER.Customer;
        //                    order.Tel = rec.UDCODORDER.Tel;
        //                    orderCurrentTel = rec.UDCODORDER.Tel;
        //                    order.Address = rec.UDCODORDER.Address;
        //                    order.Process = rec.UDCODORDER.Process;
        //                    order.aCommerRemark = rec.UDCODORDER.aCommerRemark;
        //                    order.FeedbackByClient = rec.UDCODORDER.FeedbackByClient;
        //                    order.name = rec.Order.name;
        //                    order.username = rec.Order.username;
        //                    order.email = rec.Order.email;
        //                    order.deliveryoptionident = rec.Order.deliveryoptionident;
        //                    order.deliverytypeident = rec.Order.deliverytypeident;
        //                    order.created = rec.Order.created;
        //                    order.district = rec.Order.district;
        //                    order.orderguid = rec.Order.orderguid;
        //                    order.districtsub = rec.Order.districtsub;
        //                    order.comment = rec.Order.comment;
        //                    order.address = rec.Order.address;
        //                    order.city = rec.Order.city;
        //                    order.zip = rec.Order.zip;
        //            model.Add(order);
        //                }
        //        ViewBag.UdCOD = model;
        //        #endregion

        //        #region CheckMultipleOrders
        //        // Check if this order's user has multple orders so that we can decide to go to Calling 1 or calling 2
        //        var orderSum = from order in db.tblUndeliveredCODs
        //                       where order.Tel == orderCurrentTel
        //                       group order by order.Tel
        //                           into grp
        //                           select new
        //                           {
        //                               Count = grp.Select(x => x.Tel).Count()
        //                           };
        //        int currentOrderCount = 0;
        //        ViewBag.phoneN = orderCurrentTel;
        //        foreach (var row in orderSum.OrderBy(x => x.Count))
        //        {
        //            currentOrderCount = row.Count;
        //        }

        //        #endregion
        //        if (currentOrderCount == 0)
        //        {
        //            TempData["Warning"] = "There is no order to process.";
        //            return View(model);
        //        }
        //        else if (currentOrderCount == 1)
        //        {
        //            #region Calling1
        //            Session["CallStart"] = DateTime.Now;
        //            if (!Request.RawUrl.Contains("localhost"))
        //            {
        //                tblUndeliveredCODs orderCurrent = db.tblUndeliveredCODs.Where(x=>x.SerialNo==orderCurrentId.ToString()).First();
        //                orderCurrent.CallBlockTime = DateTime.Now.AddMinutes(30);
        //                db.Entry(orderCurrent).State = EntityState.Modified;
        //                db.SaveChanges();
        //            }
        //            var oldLogs = new List<viewCallingUndeliveredCODLogs>();
        //            var oLogs = from parentObj in db.CallingCODUndeliveredLogs
        //                        join childObj in db.Users on parentObj.Caller equals childObj.Id
        //                        join otherChild in db.LogEvents on parentObj.Event equals otherChild.ID
        //                        where parentObj.orderId == orderCurrentId
        //                        orderby parentObj.logDate descending
        //                        select new
        //                        {
        //                            CallingLogs = parentObj,
        //                            Users = childObj,
        //                            LogEvents = otherChild
        //                        };
        //            foreach (var item in oLogs)
        //            {
        //                viewCallingUndeliveredCODLogs lgs = new viewCallingUndeliveredCODLogs();
        //                lgs.orderId = item.CallingLogs.orderId;
        //                lgs.Caller = item.Users.FirstName + " " + item.Users.LastName;
        //                lgs.Event = item.LogEvents.type;
        //                lgs.logDate = item.CallingLogs.logDate;
        //                lgs.nextCall = item.CallingLogs.nextCall;
        //                //lgs.orderCreated = item.CallingLogs.orderCreated;
        //                lgs.Scope = item.CallingLogs.Scope;
        //                oldLogs.Add(lgs);
        //            }
        //            ViewBag.CallingUndeliveredCODLogs = oldLogs;

        //            //orderline
        //            var Basket = new List<tblBasket>();
        //            var basket = from parentObj in db.Basket
        //                         where parentObj.paymentorderid == Convert.ToInt32(orderCurrentSerialNo)
        //                         orderby parentObj.created descending
        //                         select new
        //                         {
        //                             Basket = parentObj,
        //                         };
        //            foreach (var itemBasket in basket)
        //            {
        //                tblBasket oneBasket = new tblBasket();
        //                oneBasket.id = itemBasket.Basket.id;
        //                oneBasket.itemnumber = itemBasket.Basket.itemnumber;
        //                oneBasket.created = itemBasket.Basket.created;
        //                oneBasket.antal = itemBasket.Basket.antal;
        //                oneBasket.basValuta = itemBasket.Basket.basValuta;
        //                oneBasket.basValutaPrice = itemBasket.Basket.basValutaPrice;
        //                oneBasket.basValutaPriceTotal = itemBasket.Basket.basValutaPriceTotal;
        //                oneBasket.paymentorderid = itemBasket.Basket.paymentorderid;
        //                oneBasket.accountident = itemBasket.Basket.accountident;
        //                Basket.Add(oneBasket);
        //            }
        //            ViewBag.BasketData = Basket;
        //            ViewBag.OrderID = orderCurrentSerialNo;
        //            return View(model);
        //            #endregion
        //        }
        //        else
        //        {
        //            #region Calling 2
        //            Session["CallStart"] = DateTime.Now;
        //            var modelN = new List<UndeliveredCODsFullInfo>();
        //            DateTime startCallingFromN = DateTime.Now.AddHours(-1 * db.Settings.Find(1).firstCallWait);
        //            var resultN = (from parentObj in db.tblOrders
        //                            join childObj in db.tblUndeliveredCODs on parentObj.id equals Convert.ToInt32(childObj.SerialNo)
        //                            where childObj.CallBlockTime <= callBlockTime && childObj.Tel == orderCurrentTel
        //                            orderby childObj.CallBlockTime ascending
        //                            select new
        //                            {
        //                                Order = parentObj,
        //                                UDCODORDER = childObj
        //                            });
        //            tblUndeliveredCODs orderCurrent;

        //            foreach (var rec in resultN)
        //            {
        //                UndeliveredCODsFullInfo order = new UndeliveredCODsFullInfo();
        //                order.Id = rec.Order.id;
        //                orderCurrentId = rec.Order.id;
        //                order.Invoice = rec.UDCODORDER.Invoice;
        //                order.SerialNo = rec.UDCODORDER.SerialNo;
        //                orderCurrentSerialNo = rec.UDCODORDER.SerialNo;
        //                order.Order = rec.UDCODORDER.Order;
        //                order.Customer = rec.UDCODORDER.Customer;
        //                order.Tel = rec.UDCODORDER.Tel;
        //                orderCurrentTel = rec.UDCODORDER.Tel;
        //                order.Address = rec.UDCODORDER.Address;
        //                order.Process = rec.UDCODORDER.Process;
        //                order.aCommerRemark = rec.UDCODORDER.aCommerRemark;
        //                order.FeedbackByClient = rec.UDCODORDER.FeedbackByClient;
        //                order.name = rec.Order.name;
        //                order.username = rec.Order.username;
        //                order.email = rec.Order.email;
        //                order.deliveryoptionident = rec.Order.deliveryoptionident;
        //                order.deliverytypeident = rec.Order.deliverytypeident;
        //                order.created = rec.Order.created;
        //                order.district = rec.Order.district;
        //                order.orderguid = rec.Order.orderguid;
        //                order.districtsub = rec.Order.districtsub;
        //                order.comment = rec.Order.comment;
        //                order.address = rec.Order.address;
        //                order.city = rec.Order.city;
        //                order.zip = rec.Order.zip;
        //                modelN.Add(order);
        //                if (!Request.RawUrl.Contains("localhost"))
        //                {
        //                    tblUndeliveredCODs modifyCallBlock = dbIndividualCLRec.tblUndeliveredCODs.Where(x => x.Id == rec.UDCODORDER.Id).First();
        //                    modifyCallBlock.CallBlockTime = DateTime.Now.AddMinutes(30);
        //                    dbIndividualCLRec.Entry(modifyCallBlock).State = EntityState.Modified;
        //                    dbIndividualCLRec.SaveChanges();
        //                }
        //            }


        //            var oldLogs = new List<viewCallingUndeliveredCODLogs>();
        //            int[] orderIdsToFind = (from parentObj in db.tblUndeliveredCODs
        //                                       where parentObj.Tel == orderCurrentTel
        //                                       select Convert.ToInt32(parentObj.SerialNo)).ToArray();
        //            var oLogs = from parentObj in db.CallingCODUndeliveredLogs
        //                        join childObj in db.Users on parentObj.Caller equals childObj.Id
        //                        join otherChild in db.LogEvents on parentObj.Event equals otherChild.ID
        //                        where orderIdsToFind.Contains(parentObj.orderId)
        //                        orderby parentObj.logDate descending
        //                        select new
        //                        {
        //                            CallingLogs = parentObj,
        //                            Users = childObj,
        //                            LogEvents = otherChild
        //                        };
        //            foreach (var item in oLogs)
        //            {
        //                viewCallingUndeliveredCODLogs lgs = new viewCallingUndeliveredCODLogs();
        //                lgs.orderId = item.CallingLogs.orderId;
        //                lgs.Caller = item.Users.FirstName + " " + item.Users.LastName;
        //                lgs.Event = item.LogEvents.type;
        //                lgs.logDate = item.CallingLogs.logDate;
        //                lgs.nextCall = item.CallingLogs.nextCall;
        //                //lgs.orderCreated = item.CallingLogs.orderCreated;
        //                lgs.Scope = item.CallingLogs.Scope;
        //                oldLogs.Add(lgs);
        //            }
        //            ViewBag.CallingUndeliveredCODLogs = oldLogs;
        //            ViewBag.UdCODCount = modelN.Count();
        //            ViewBag.UdCOD = modelN;
        //            return View("UndeliveredCODMultipleOrders", modelN);
        //        }
        //        #endregion

        //    }
        //    catch (Exception xp)
        //    {
        //        TempData["Error"] = "An error occurred, Please try again";
        //    }
        //    return View();
        //}
        #endregion
    }
}