﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CallingChilindo.Models
{
   
    public class tblUsers
    {

       
        [Key]
        public int nUserId { get; set; }
        public string sUsername { get; set; }
        public string sFirstname { get; set; }
        public string sLastname { get; set; }
        [Required]
        [EmailAddress]
        [Display(Name = "Email:")]
        public string sEmail { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password:")]
        public string sPassword { get; set; }

    }
}