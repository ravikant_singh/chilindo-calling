﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallingChilindo.Models
{
    public class BarchartStats
    {
        public string sUser { get; set; }
        public int nTotalCalls { get; set; }
        public int nTotalOtherCalls { get; set; }
    }
}