﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallingChilindo.Models
{
    public class overviewStats
    {
        public string sUserName { get; set; }
        public string nAverage { get; set; }
        public string nToday { get; set; }
        public string nYesterday { get; set; }
        public string nDay1 { get; set; }
        public string nDay2 { get; set; }
        public string nDay3 { get; set; }
        public string nDay4 { get; set; }
        public string nDay5 { get; set; }


    }
}