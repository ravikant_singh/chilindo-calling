﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallingChilindo.Models
{
    public class Users
    {
        public int Id { get; set; }
        public string sIdGuid { get; set; }
        public string UserName { get; set; }
        public bool? isActive {get;set;}
        public bool? isCaller { get; set; }
        public int nId { get; set; }
        public bool CallNewest { get; set; }
        public Nullable<DateTime> dtSignintime { get; set; }
        
    }
}