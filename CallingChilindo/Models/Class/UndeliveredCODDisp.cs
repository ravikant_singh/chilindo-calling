﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallingChilindo.Models.Class
{
    public class UndeliveredCODDisp
    {
        public int Id { get; set; }
        public Nullable<int> Invoice { get; set; }
        public string SerialNo { get; set; }
        public string TrackingCode { get; set; }
        public string Customer { get; set; }
        public string Tel { get; set; }
        public string Address { get; set; }
        public string Process { get; set; }
        public string aCommerreMark { get; set; }
        public string FeedbackByClient { get; set; }
        public System.DateTime Dated { get; set; }
        public Nullable<System.DateTime> CallBlockTime { get; set; }
        public string guid { get; set; }
        public string CarrierName { get; set; }
    }
}