﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallingChilindo.Models.Class
{
    [Serializable]
    public class SearchFilter
    {
        public int PageSize { get; set; }
        public int TotalRecords { get; set; }
        public int PageNo { get; set; }
        public string SortField { get; set; }
        public string SortOrder { get; set; }
    }
}