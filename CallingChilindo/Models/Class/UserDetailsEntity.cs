﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CallingChilindo.Models.Class
{
    public class UserDetailsEntity
    {
        [DisplayName("ID")]
        public int id { set; get; }
        [DisplayName("User Name")]
        public string userName { set; get; }
        [DisplayName("First Name")]
        public string firstName { set; get; }
        [DisplayName("Last Name")]
        public string lastName { set; get; }
        [DisplayName("Email")]
        public string email { set; get; }
        [DisplayName("Address")]
        public string address { set; get; }
        [DisplayName("Province")]
        public string province { set; get; }
        [DisplayName("District")]
        public string district { set; get; }
        [DisplayName("District Sub")]
        public string districtSub { set; get; }
        [DisplayName("Building")]
        public string building { set; get; }
        [DisplayName("State")]
        public string state { set; get; }
        [DisplayName("PostOffice")]
        public string postOffice { set; get; }
        [DisplayName("Zip")]
        public string zip { get; set; }
        [DisplayName("City")]
        public string city { get; set; }
        [DisplayName("Country")]
        public string country { get; set; }
        [DisplayName("Phone")]
        public string phone { get; set; }
        [DisplayName("LINE ID")]
        public string lineId { get; set; }
        [DisplayName("Wholeseller")]
        public bool? wholeseller { get; set; }
        [DisplayName("Credit Limit")]
        public decimal? creditLimit { get; set; }
        public int userStatus { get; set; }
        [DisplayName("Last login")]
        public string lastLogin { get; set; }
        [DisplayName("Signup")]
        public string signUp { get; set; }
        [DisplayName("Facebook profile")]
        public string facebookProfile { get; set; }
        public bool blacklist { get; set; }


    }
}