﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CallingChilindo.Models.Class
{
    [Serializable]
    public class VouchersEntity
    {
        public int id { get; set; }
        public DateTime? created { get; set; }
        [Required]
        [DisplayName("Voucher Code")]
        public string voucherCode { get; set; }
        [Required]
        [DisplayName("Max Redemptions")]
        public int? maxRedemptions { get; set; }
        [Required]
        [DisplayName("Amount")]
        public string additional { get; set; }
        [Required]
        [DisplayName("Type of voucher")]
        public int? typeOfVoucher { get; set; }
        [Required]
        [DisplayName("Min. Purchase Price")]
        public decimal? minPurchasePrice { get; set; }
        public DateTime? valid { get; set; }
        public decimal? val { get; set; }
        public int? useInbasket { get; set; }
        public string voucherType { get; set; }
        public int? voucherTypeId { get; set; }
        public bool? isActive { get; set; }
    }
}