﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallingChilindo.Models.Class
{
    public class MultiUserForPickup
    {
        public Nullable<int> accountid { get; set; }
        public string name { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public int OrderForPickUp { get; set; }
    }

    public class OrdersForPickup
    {
        public Nullable<int> accountid { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public string username { get; set; }
        public int orderstatusid { get; set; }
        public string orderstatus { get; set; }
        public string PickupLocation { get; set; }
        public Nullable<decimal> webAmount { get; set; }
        public string PaidOrUnpaid { get; set; }
        public string orderguid { get; set; }
        public string deliveryoptionident { get; set; }

    }
    public class OrdersForPopup
    {
        public Nullable<int> accountid { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public string username { get; set; }
        public Nullable<decimal> webAmount { get; set; }
        public Nullable<decimal> webRoundAmount { get; set; }
        public string Status { get; set; }
        public int ActionId { get; set; }

    }

    public class CaseTransaction
    {
        public DateTime Date { get; set; }
        public int ReceiveCount  { get; set; }
        public Nullable<decimal> Amount { get; set; }
    }

    public class CaseTransactionDetails
    {
        public int? OrderId { get; set; }
        public string UserName { get; set; }
        public string TransactionType { get; set; }
        public string Currency { get; set; }
        public Nullable<decimal> Amount { get; set; }      
        public Nullable<DateTime> CreatedDate { get; set; }
        public Nullable<decimal> Balance { get; set; }
    }

    public class BackOrderPopup
    {
        public int? OrderId { get; set; }
        public string orderguid { get; set; }
        public string deliveryoptionident { get; set; }
        public string orderstatus { get; set; }
        public Nullable<int> accountid { get; set; }
        public string email { get; set; }
        public string PaidOrUnpaid { get; set; }

    }



}