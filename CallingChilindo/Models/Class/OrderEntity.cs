﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallingChilindo.Models.Class
{
    public class OrderEntity
    {
        public int id { get; set; }
        public string orderguid { get; set; }
        public DateTime? date { get; set; }
        public string status { get; set; }
        public DateTime? bundledDate { get; set; }
        public string trackingcode { get; set; }
        public string action { get; set; }
        public string displayOrderId
        {
            get
            {
                string _out = "INV" + id.ToString().PadLeft(8, '0');
                return _out;
            }
        }
    }
}