﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallingChilindo.Models.Class
{
    public class UserLogEntity
    {
        public int id { get; set; }
        public DateTime created { get; set; }
        public int userid { get; set; }
        public int auctionId { get; set; }
        public int orderId { get; set; }
        public string comment { get; set; }
        public string adminuser { get; set; }
    }
}