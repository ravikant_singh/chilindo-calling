﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallingChilindo.Models.Class
{
    [Serializable]
    public class PaymentReceiptsEntity
    {
        public int id { get; set; }
        public DateTime created { get; set; }

        [DisplayName("Order")]
        [MaxLength(7)]
        [RequiredAttribute(ErrorMessage = "ValErr_Required")]
        [RegularExpressionAttribute("([0-9]+)")]
        public string orderId1 { get; set; }

        [DisplayName("Order")]
        public string orderId2 { get; set; }

        [DisplayName("Order")]
        public string orderId3 { get; set; }

        [DisplayName("Order")]
        public string orderId4 { get; set; }

        [DisplayName("Order")]
        public string orderId5 { get; set; }

        [DisplayName("Order")]
        public string orderId6 { get; set; }

        [DisplayName("Order")]
        public string orderidents { get; set; }

        [DisplayName("Amount")]
        public decimal? amount1 { get; set; }

        [DisplayName("Amount")]
        public decimal? amount2 { get; set; }

        [DisplayName("Amount")]
        public decimal? amount3 { get; set; }

        public decimal? orderAmount { get; set; }
        public decimal? amountTotal { get; set; }

        [DisplayName("Date Paid")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{dd-MM-yyyy}")]
        public DateTime? datePaid { get; set; }

        [DisplayName("Do you see a reciept?")]
        public string showReceipt { get; set; }

        [DisplayName("Bank")]
        public int? bankId { get; set; }
        public string bankName { get; set; }
        [DisplayName("Comment")]
        public string comment { get; set; }

        [DisplayName("Confrimed By")]
        public string userId { get; set; }
        public string matchType { get; set; }
        public string datePaidDisplay
        {
            get
            {
                string result = null;

                if (datePaid.HasValue)
                {
                    result = datePaid.Value.ToString("dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                return result;
            }
        }
        public string showReceiptDisplay
        {
            get
            {
                string result = null;

                if (!string.IsNullOrEmpty(showReceipt))
                {
                    if (showReceipt == "Y")
                        result = "Yes";
                    else
                        result = "No";
                }
                return result;
            }
        }
        
    }
}
