﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallingChilindo.Models.Class
{
    public class ItemEntity
    {
        public int itemId { get; set; }
        public string itemName { get; set; }
        public string itemNo { get; set; }
        public string size { get; set; }
        public decimal? price { get; set; }
        public string imagePath { get; set; }
        public string userEmail { get; set; }
        public int accountId { get; set; }
        public string currency { get; set; }
    }
}