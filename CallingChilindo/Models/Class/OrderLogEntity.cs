﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallingChilindo.Models.Class
{
    public class OrderLogEntity
    {
        public DateTime created { get; set; }
        public string supporter { get; set; }
        public string status { get; set; }
        public string comment { get; set; }
    }
}