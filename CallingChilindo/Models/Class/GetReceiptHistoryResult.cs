﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallingChilindo.Models.Class
{
    public class GetReceiptHistoryResult
    {
       public int id { get; set; }
       public DateTime created { get; set; }
       public string orderidents { get; set; }
       public decimal? amount1 { get; set; }
       public decimal? amount2 { get; set; }
       public decimal? amount3 { get; set; }
       public decimal? amounttotal { get; set; }
       public decimal? orderamount { get; set; }
       public DateTime? datepaid { get; set; }
       public string shownreceipt { get; set; }
       public int? bankid { get; set; }
       public string comment { get; set; }
       public string userid { get; set; }
       public string matchtype { get; set; }
       public string bankident { get; set; }
       public int? orderid { get; set; }
    }
}