﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallingChilindo.Models.Class
{
    public class getOrdersToCall_Result
    {
        public int id { get; set; }
        public System.DateTime created { get; set; }
        public string deliverytypeident { get; set; }
        public string deliveryoptionident { get; set; }
        public string orderguid { get; set; }
        public int orderstatusid { get; set; }
        public string username { get; set; }
        public Nullable<int> accountid { get; set; }
        public string email { get; set; }
        public string name { get; set; }
        public string lastname { get; set; }
        public string address { get; set; }
        public string zip { get; set; }
        public string city { get; set; }
        public string province { get; set; }
        public Nullable<int> provinceid { get; set; }
        public string district { get; set; }
        public string country { get; set; }
        public string countrytxt { get; set; }
        public string phone { get; set; }
        public string comment { get; set; }
        public Nullable<decimal> webAmount { get; set; }
        public Nullable<decimal> webFragt { get; set; }
        public Nullable<decimal> webEkspgebyr { get; set; }
        public Nullable<decimal> webAmountAuctions { get; set; }
        public Nullable<decimal> webDiscount { get; set; }
        public Nullable<System.DateTime> paidDate { get; set; }
        public Nullable<System.DateTime> sendDate { get; set; }
        public string trackingcode { get; set; }
        public string districtsub { get; set; }
        public bool isCallAble { get; set; }
        public Nullable<System.DateTime> callBlockTime { get; set; }
        public decimal webAmountSales { get; set; }
        public Nullable<System.Guid> fetchGuid { get; set; }
        public string aCommerreMark { get; set; }
    }
}