﻿using CallingChilindo.Models.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallingChilindo.Models
{
    public class PaymentModels
    {
        public PaymentReceiptsEntity PaymentReceipts { get; set; }
        public IQueryable<BankEntity> Banks { get; set; }
        public Users User { get; set;}
        public List<PaymentReceiptsEntity> PaymentHistorys { get; set; }
    }
}