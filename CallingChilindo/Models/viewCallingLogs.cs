﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallingChilindo.Models
{
    public class viewCallingLogs
    {
        public int? orderId { get; set; }
        public int? accountId { get; set; }
        public string Caller { get; set; }
        public string Event { get; set; }
        public DateTime? logDate { get; set; }
        public DateTime? nextCall { get; set; }
        public DateTime? orderCreated { get; set; }
        public string Scope { get; set; }
      

    }
}
