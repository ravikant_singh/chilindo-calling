﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CallingChilindo.Models
{
    public class Statistics
    {
        //public string Id { get; set; }
        public string sUserName { get; set; }
        public bool? bStatus { get; set; }

        public double nHoursToday { get; set; }
        public double nHoursWeek { get; set; }
        public double nHoursTotal { get; set; }

        public int nTotalCalls { get; set; }
        public int nTotalCallsToday { get; set; }
        public int nTotalCallsWeek { get; set; }

        public int nPaid { get; set; }
        public string sPaid { get; set; }
        public string sPaidToday { get; set; }
        public string sPaidWeek { get; set; }

        public int nWillPay { get; set; }
        public string sWillPay { get; set; }
        public string sWillPayToday { get; set; }
        public string sWillPayWeek { get; set; }

        public int nNotPickedUp { get; set; }
        public string sNotPickedUp { get; set; }
        public string sNotPickedUpToday { get; set; }
        public string sNotPickedUpWeek { get; set; }

        public int nChangeToCOD { get; set; }
        public string sChangeToCOD { get; set; }
        public string sChangeToCODToday { get; set; }
        public string sChangeToCODWeek { get; set; }

        public int nWrongNumber { get; set; }
        public string sWrongNumber { get; set; }
        public string sWrongNumberToday { get; set; }
        public string sWrongNumberWeek { get; set; }

        public int nCancelled { get; set; }
        public string sCancelled { get; set; }
        public string sCancelledToday { get; set; }
        public string sCancelledWeek { get; set; }
    }
    public class StatiscticsModel
    {
        public string id { get; set; }
        public Nullable<bool> status { get; set; }
        public string FullName { get; set; }
        public string Caller { get; set; }
        public string Col1 { get; set; }
        public string Col1Footer { get; set; }
        public string Col2 { get; set; }
        public string Col2Footer { get; set; }
        public string Col3 { get; set; }
        public string Col3Footer { get; set; }
        public Nullable<int> Col4 { get; set; }
        public Nullable<int> Col4Footer { get; set; }
        public Nullable<int> Col5 { get; set; }
        public Nullable<int> Col5Footer { get; set; }
        public Nullable<int> Col6 { get; set; }
        public Nullable<int> Col6Footer { get; set; }
        public Nullable<int> Col7 { get; set; }
        public Nullable<int> Col7Footer { get; set; }
        public Nullable<int> Col8 { get; set; }
        public Nullable<int> Col8Footer { get; set; }
        public Nullable<int> Col9 { get; set; }
        public Nullable<int> Col9Footer { get; set; }
        public Nullable<int> Col10 { get; set; }
        public Nullable<int> Col10Footer { get; set; }
        public Nullable<int> Col11 { get; set; }
        public Nullable<int> Col11Footer { get; set; }
        public Nullable<int> Col12 { get; set; }
        public Nullable<int> Col12Footer { get; set; }
        public Nullable<int> Col13 { get; set; }
        public Nullable<int> Col13Footer { get; set; }
        public Nullable<int> Col14 { get; set; }
        public Nullable<int> Col14Footer { get; set; }
        public Nullable<int> Col15 { get; set; }
        public Nullable<int> Col15Footer { get; set; }
        public Nullable<int> Col16 { get; set; }
        public Nullable<int> Col16Footer { get; set; }
        public Nullable<int> Col17 { get; set; }
        public Nullable<int> Col17Footer { get; set; }
        public Nullable<int> Col18 { get; set; }
        public Nullable<int> Col18Footer { get; set; }
        public Nullable<int> Col19 { get; set; }
        public Nullable<int> Col19Footer { get; set; }
        public Nullable<int> Col20 { get; set; }
        public Nullable<int> Col20Footer { get; set; }
        public Nullable<int> Col21 { get; set; }
        public Nullable<int> Col21Footer { get; set; }
        public Nullable<int> Col22 { get; set; }
        public Nullable<int> Col22Footer { get; set; }
        public Nullable<int> Col23 { get; set; }
        public Nullable<int> Col23Footer { get; set; }
        public Nullable<int> Col24 { get; set; }
        public Nullable<int> Col24Footer { get; set; }
    }
}