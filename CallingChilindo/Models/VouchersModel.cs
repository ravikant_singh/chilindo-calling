﻿using CallingChilindo.Models.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallingChilindo.Models
{
    public class VouchersModel
    {
        public VouchersEntity Vouchers { get; set; }
        public List<VouchersEntity> ListVouchers { get; set; }
        public IQueryable<VoucherTypeEntity> ListVoucherTypes { get; set; }
        public SearchFilter SearchFilter { get; set; }
    }
}