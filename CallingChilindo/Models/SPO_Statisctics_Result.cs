//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CallingChilindo.Models
{
    using System;
    
    public partial class SPO_Statisctics_Result
    {
        public string id { get; set; }
        public Nullable<bool> status { get; set; }
        public string Caller { get; set; }
        public string FullName { get; set; }
        public Nullable<int> Col1 { get; set; }
        public Nullable<int> Col1Footer { get; set; }
        public Nullable<int> Col2 { get; set; }
        public Nullable<int> Col2Footer { get; set; }
        public Nullable<int> Col3 { get; set; }
        public Nullable<int> Col3Footer { get; set; }
        public Nullable<int> Col4 { get; set; }
        public Nullable<int> Col4Footer { get; set; }
        public Nullable<int> Col5 { get; set; }
        public Nullable<int> Col5Footer { get; set; }
        public Nullable<int> Col6 { get; set; }
        public Nullable<int> Col6Footer { get; set; }
        public Nullable<int> Col7 { get; set; }
        public Nullable<int> Col7Footer { get; set; }
        public Nullable<int> Col8 { get; set; }
        public Nullable<int> Col8Footer { get; set; }
        public Nullable<int> Col9 { get; set; }
        public Nullable<int> Col9Footer { get; set; }
        public Nullable<int> Col10 { get; set; }
        public Nullable<int> Col10Footer { get; set; }
        public Nullable<int> Col11 { get; set; }
        public Nullable<int> Col11Footer { get; set; }
        public Nullable<int> Col12 { get; set; }
        public Nullable<int> Col12Footer { get; set; }
        public Nullable<int> Col13 { get; set; }
        public Nullable<int> Col13Footer { get; set; }
        public Nullable<int> Col14 { get; set; }
        public Nullable<int> Col14Footer { get; set; }
        public Nullable<int> Col15 { get; set; }
        public Nullable<int> Col15Footer { get; set; }
        public Nullable<int> Col16 { get; set; }
        public Nullable<int> Col16Footer { get; set; }
        public Nullable<int> Col17 { get; set; }
        public Nullable<int> Col17Footer { get; set; }
        public Nullable<int> Col18 { get; set; }
        public Nullable<int> Col18Footer { get; set; }
        public Nullable<int> Col19 { get; set; }
        public Nullable<int> Col19Footer { get; set; }
        public Nullable<int> Col20 { get; set; }
        public Nullable<int> Col20Footer { get; set; }
        public Nullable<int> Col21 { get; set; }
        public Nullable<int> Col21Footer { get; set; }
        public Nullable<int> Col22 { get; set; }
        public Nullable<int> Col22Footer { get; set; }
        public Nullable<int> Col23 { get; set; }
        public Nullable<int> Col23Footer { get; set; }
        public Nullable<int> Col24 { get; set; }
        public Nullable<int> Col24Footer { get; set; }
    }
}
