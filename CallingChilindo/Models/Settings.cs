﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallingChilindo.Models
{
    public class tblSettings
    {
        [Key]
        public int settingId { get; set; }
        public bool MergeOrders { get; set; }
        public bool precedence2C2P { get; set; }
        public int firstCallWait { get; set; }
        public int numberofTries { get; set; }
        public int firstRecall { get; set; }
        public int secondRecall { get; set; }
        public int thirdRecall { get; set; }
        public int fourthRecall { get; set; }
        public int fifthRecall { get; set; }
        public int sixthRecall { get; set; }
        public int seventhRecall { get; set; }
        public int eighthRecall { get; set; }
        public int ninthRecall { get; set; }
        public int tenthRecall { get; set; }
        public int eleventhRecall{ get; set; }
        public int twelvthRecall{ get; set; }
        public int thirteenthRecall { get; set; }
        public int fourteenthRecall { get; set; }
        public int fifteenthRecall { get; set; }
        public int sixteenthRecall { get; set; }
        public int seventeenthRecall { get; set; }
        public int eighteenthRecall { get; set; }
        public int nineteenthRecall { get; set; }
        public int waitPayLater { get; set; }
        public int delay2C2P { get; set; }
        public int nAutoLogoutValue { get; set; }
        public int nCancelWrongNumberOrderAfterDays { get; set; }
        public int nCancelUnPickedUpOrderAfterDays { get; set; }
        public int callBlockTime { get; set; }
        public int cancelOrderRetries { get; set; }
        public int UCODTimeToWaitBeforeCall { get; set; }
        public int UCODTimeToWaitBeforeRecall { get; set; }
        public int payLaterWaitToRecall { get; set; }
    }
  
}