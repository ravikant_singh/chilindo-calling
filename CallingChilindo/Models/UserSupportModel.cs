﻿using CallingChilindo.Models.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallingChilindo.Models
{
    public class UserSupportModel
    {
        public UserDetailsEntity UserDetail { get; set; }
        public List<ItemEntity> ListItems { get; set; }
        public List<UserDetailsEntity> ListUserDetails { get; set; }
        //public IQueryable<UserDetailsEntity> ListUserDetails { get; set; }
        public List<OrderEntity> ListOrders { get; set; }
        public List<OrderLogEntity> ListOrderLogs { get; set; }
        public List<UserLogEntity> ListUserLogs { get; set; }
        public List<ItemRelatedEntity> ListItemRelateds { get; set; }
        public String SelectedCategoryId { get; set; }
        public ItemEntity ItemBasket{ get; set; }

    }
}