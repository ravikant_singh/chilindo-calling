﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallingChilindo.Models
{
    public class MatchPayments
    {
        public int ID { get; set; }
        public int? orderId { get; set; }
        public int? accountId { get; set; }
        public DateTime? orderCreated { get; set; }
        public DateTime? logDate { get; set; }
        public DateTime? payLaterDate { get; set; }
        public double? amountPaid { get; set; }
        public string comments { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public int? solved { get; set; }

       
    }
}