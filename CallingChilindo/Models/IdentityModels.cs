﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using CallingChilindo.Models;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace CallingChilindo.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            //userIdentity.AddClaim(new Claim("Id", this.OrganizationId));

            userIdentity.AddClaim(new Claim("Id", this.Id));
            userIdentity.AddClaim(new Claim("CallerName", this.FirstName+" "+this.LastName));
            userIdentity.AddClaim(new Claim("isActive", this.isActive.ToString()));
            //GetId

            return userIdentity;
        }
        //public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool isActive { get; set; }
        public bool isCaller { get; set; }
        public int nId { get; set; }
        public bool CallNewest { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            this.Configuration.ProxyCreationEnabled = false; 

        }
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<tblUsers> tblUsers { get; set; }
        //public DbSet<tblOrders> tblOrders { get; set; }
        
        //public DbSet<CallingLogs> CallingLogs { get; set; }
        //public DbSet<viewCallingLogs> ViewCallingLogs { get; set; }
        //public DbSet<tblSettings> Settings { get; set; }
        //public DbSet<tblUsersLogs> tblUsersLogs { get; set; }
        //public DbSet<tblUndeliveredCODs> tblUndeliveredCODs { get; set; }
        //public DbSet<CallingCODUndeliveredLogs> CallingCODUndeliveredLogs { get; set; }
        //public DbSet<LogEvents> LogEvents { get; set; }
        //public DbSet<CallingTypes> CallingTypes { get; set; }
        //public DbSet<LogEventsMappings> LogEventsMappings { get; set; }
        ////tblBasket binding done here 
        //public DbSet<tblBasket> Basket { get; set; }
        //Overrided this function for singlular table name
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //Set this convention to avoid the pluralization
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            //Mentioned the tblBasket to get it evicted from pluralization of EF
            modelBuilder.Entity<tblBasket>().ToTable("tblBasket");
            modelBuilder.Entity<LogEventsMappings>().ToTable("LogEventsMapping");
        }
        
    }
}