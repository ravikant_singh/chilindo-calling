﻿using CallingChilindo.Business.Common;
using CallingChilindo.Models;
using CallingChilindo.Models.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace CallingChilindo.Business
{
    public class UserSupportFacade
    {
        private ChilindoCallingEntities db = new ChilindoCallingEntities();
        public List<UserDetailsEntity> GetListUserOrders(string orderInfo,string typeSearch)
        {
            List<UserDetailsEntity> listUserdetail = new List<Models.Class.UserDetailsEntity>();
            string _sql = string.Empty;
            switch (typeSearch)
            {
                case "P"://phone
                    _sql = @"select distinct top 100 MEM.* from tblExecMembers MEM with(nolock)
                            left join tblOrders ORD with(nolock) on (ORD.accountid = MEM.id)
                            where MEM.deleted = 0 and ( MEM.phone like '" + orderInfo + "%' or MEM.phone like '0" + orderInfo + "%' or ORD.phone like '" + orderInfo + "%' or ORD.phone like '0" + orderInfo + "%' );";
                    break;
                case "A"://auction id
                    _sql = @"select MEM.*
                        from tblAuctionWinners WIN with(nolock)
                        inner join tblExecMembers MEM with(nolock) on(WIN.accountid = MEM.id and MEM.deleted = 0)
                        where auctionid = " + orderInfo + "; ";
                    break;
                case "N"://name
                    _sql = @"select distinct top 100 MEM.* from tblExecMembers MEM with(nolock)
                            left join tblOrders ORD with(nolock) on (ORD.accountid = MEM.id)
                            where MEM.deleted = 0 and ( MEM.firstname like N'%" + orderInfo + "%' or MEM.lastname like N'%" + orderInfo + "%' or ORD.name like N'%" + orderInfo + "%' or ORD.lastname like N'%" + orderInfo + "%' )";
                    break;
                case "O"://order id
                    _sql = @"select MEM.* from tblOrders ORD with(nolock)
                            inner join tblExecMembers MEM with(nolock) on (ORD.accountid = MEM.id and MEM.deleted = 0)
                            where ORD.id = " + orderInfo + "";
                    break;
                case "T"://trackingcode
                    string _where = " and 1 = 2 ";
                    if (!string.IsNullOrEmpty(orderInfo))
                    {
                        _where = " and trackingcode like '%" + orderInfo + "%' ";
                    }

                    _sql = @"select ORD.*, BNK.bankident, STAT.orderstatus
                            from tblOrders ORD
                            left join tblBanks BNK on (ORD.bankid = BNK.id ) 
                            left join tblOrderstatus STAT on (ORD.orderstatusid = STAT.ident)
                            where 1 = 1 " + _where + " order by id desc";
                    break;
                case "E"://email
                    _sql = @"select distinct top 100 MEM.* from tblExecMembers MEM with(nolock)
                            left join tblOrders ORD with(nolock) on (ORD.accountid = MEM.id)
                            where MEM.deleted = 0 and ( MEM.email like '" + orderInfo + "%' or ORD.email like '" + orderInfo + "%' ) ";
                    break;
                default:
                    break;
            }

            DataTable dt = GetDataTable(_sql);

            for (int i = 0; i < dt.Rows.Count; i++)
            {

                UserDetailsEntity objUserDetail = new UserDetailsEntity();
                objUserDetail.id = (int)dt.Rows[i]["id"];
                objUserDetail.userName = dt.Rows[i]["username"].ToString();
                objUserDetail.firstName = dt.Rows[i]["firstname"].ToString();
                objUserDetail.lastName = dt.Rows[i]["lastname"].ToString();
                objUserDetail.email = dt.Rows[i]["email"].ToString();
                objUserDetail.address = dt.Rows[i]["address"].ToString();
                objUserDetail.province = dt.Rows[i]["province"].ToString();
                objUserDetail.district = dt.Rows[i]["district"].ToString();
                objUserDetail.districtSub = dt.Rows[i]["districtsub"].ToString();
                //objUserDetail.building = dt.Rows[i]["Type"].ToString();
                //objUserDetail.state = dt.Rows[i]["Name"].ToString();
                //objUserDetail.postOffice = dt.Rows[i]["Image"].ToString();
                objUserDetail.zip = dt.Rows[i]["zip"].ToString();
                objUserDetail.city = dt.Rows[i]["city"].ToString();
                objUserDetail.country = dt.Rows[i]["country"].ToString();
                objUserDetail.phone = dt.Rows[i]["phone"].ToString();
                objUserDetail.lineId = dt.Rows[i]["lineid"].ToString();
                objUserDetail.wholeseller =  dt.Rows[i]["isWholeseller"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[i]["isWholeseller"]) : false;
                //objUserDetail.creditLimit = dt.Rows[i]["creditLimit"].ToString();
                objUserDetail.lastLogin = dt.Rows[i]["lastlogin"].ToString();
                objUserDetail.signUp = dt.Rows[i]["created"].ToString();
                objUserDetail.facebookProfile = dt.Rows[i]["facebookid"].ToString();
                objUserDetail.blacklist = dt.Rows[i]["blacklist"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[i]["blacklist"]) : false;
                listUserdetail.Add(objUserDetail);
            }

            return listUserdetail;
        }
        public List<UserLogEntity> GetListUserLogs(int accountid)
        {
            List<UserLogEntity> listUserLogs = new List<Models.Class.UserLogEntity>();
            string sql = @"select * from tblExecMembersLog where userid = " + accountid + " order by id desc;";
            DataTable dt = GetDataTable(sql);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                UserLogEntity userlog = new UserLogEntity();
                userlog.id = (int)dt.Rows[i]["id"];
                userlog.created = (DateTime)dt.Rows[i]["created"];
                userlog.userid = (int)dt.Rows[i]["userid"];
                userlog.comment = dt.Rows[i]["comment"].ToString();
                userlog.adminuser = dt.Rows[i]["adminuser"].ToString();

                listUserLogs.Add(userlog);
            }
            return listUserLogs;
        }
        public List<OrderEntity> GetListOrders(int accountId)
        {
            try
            {
                IUnitOfWork _uow = new UnitOfWork(new DatabaseContextFactory());
                SqlConnection _conn = _uow.DataContext.Connection;
                DataSet ds = new DataSet();
                SqlCommand cmd = new SqlCommand("GetOrdersByAccount", _conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@accountid", accountId);

                using (SqlDataAdapter oDa = new SqlDataAdapter(cmd))
                {
                    oDa.Fill(ds, "tblOrder");
                }
                DataTable dt = ds.Tables["tblOrder"];
                List<OrderEntity> listOrders = new List<OrderEntity>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OrderEntity order = new OrderEntity();
                    order.id = (int)dt.Rows[i]["id"];
                    order.orderguid = dt.Rows[i]["orderguid"].ToString();
                    //order.date = dt.Rows[i]["paidDate"]!=null?(DateTime)dt.Rows[i]["paidDate"]:DateTime.Now;
                    order.status = dt.Rows[i]["orderstatus_th"].ToString();
                    order.trackingcode = dt.Rows[i]["trackingcode"].ToString();
                    //order.bundledDate = dt.Rows[i]["sendDate"]!=null?(DateTime)dt.Rows[i]["sendDate"]:DateTime.Now;
                    //order.id = (int)dt.Rows[i]["id"];
                    //order.created = (DateTime)dt.Rows[i]["created"];
                    //order.userid = (int)dt.Rows[i]["userid"];
                    //order.comment = dt.Rows[i]["comment"].ToString();
                    //order.commentBy = (int)dt.Rows[i]["comment"];

                    listOrders.Add(order);
                }
                return listOrders;
            }
            catch
            {
                return null;
            }
        }
        public List<OrderLogEntity> GetListOrderLogs(int orderId)
        {
            try {
                string _sql = @"select SLOG.*, STAT.orderstatus from tblOrderstatusLog SLOG
                            inner join tblOrderstatus STAT on (SLOG.statusident = STAT.ident)
                            where SLOG.orderid = " + orderId + " order by SLOG.id asc;";
                DataTable dt = GetDataTable(_sql);

                List<OrderLogEntity> listOrderLogs = new List<OrderLogEntity>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OrderLogEntity orderLog = new OrderLogEntity();
                    orderLog.created = (DateTime)dt.Rows[i]["created"];
                    orderLog.supporter = dt.Rows[i]["username"].ToString();
                    orderLog.status = dt.Rows[i]["statusident"].ToString();
                    orderLog.status = dt.Rows[i]["comment"].ToString();
                    listOrderLogs.Add(orderLog);
                }
                return listOrderLogs;
            }
            catch (Exception e) {
                return null;
            }
        }
        public List<ItemEntity> GetListItemBasket(int accountId)
        {
            List<ItemEntity> listItems = new List<ItemEntity>();
            DataTable dt = GetAuctionsToBuy(accountId, "th");
            for(int i=0; i<dt.Rows.Count;i++)
            {
                ItemEntity item = new ItemEntity();
                item.itemId = (int)dt.Rows[i]["id"];
                item.itemName = dt.Rows[i]["title"].ToString();
                item.itemNo = dt.Rows[i]["itemnumber"].ToString();
                item.price = (decimal)dt.Rows[i]["currentbid"];
                item.size = dt.Rows[i]["ItemSubject"].ToString();
                item.imagePath = dt.Rows[i]["imgpath"].ToString();
                listItems.Add(item);
            }
            return listItems;
        }
        public List<ItemRelatedEntity> GetListItemRelated(string itemNo,string lang)
        {
            try
            {
                IUnitOfWork _uow = new UnitOfWork(new DatabaseContextFactory());
                SqlConnection _conn = _uow.DataContext.Connection;
                DataSet ds = new DataSet();
                SqlCommand cmd = new SqlCommand("GetItemsRelated", _conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Itemno", itemNo.ToUpper());
                cmd.Parameters.AddWithValue("@Lang", lang);
                using (SqlDataAdapter oDa = new SqlDataAdapter(cmd))
                {
                    oDa.Fill(ds, "tblItemsRelated");
                }
                DataTable dt = ds.Tables["tblItemsRelated"];
                List<ItemRelatedEntity> itemRelateds = new List<ItemRelatedEntity>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ItemRelatedEntity item = new ItemRelatedEntity();
                    item.itemType = dt.Rows[i]["ItemType"].ToString();
                    item.lagKartVarenummer = dt.Rows[i]["LagKartVarenummer"].ToString();
                    itemRelateds.Add(item);
                }
                return itemRelateds;
            }
            catch (Exception e) { return null; }
        }
        public bool UpdateUserInfo(UserDetailsEntity userDetail)
        {
            IUnitOfWork _uow = new UnitOfWork(new DatabaseContextFactory());
            SqlConnection _conn = _uow.DataContext.Connection;
            try
            {
                var isSuccess = 0;
                var strSqlInsertUser = @"Update [dbo].[tblExecMembers] set [firstname] = @firstname,[lastname] = @lastname,
                                            [address] = @address,[zip] = @zip,[city] = @city,[province] = @province,
                                            [district] = @district,[phone] = @phone,[lineid] = @lineid,[isWholeseller] = @isWholeseller,
                                            [creditLimit] = @creditLimit where[id] = @id";
                using (var cmd = _conn.CreateCommand())
                {
                    cmd.CommandText = strSqlInsertUser;
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = _uow.BeginTransaction();
                    cmd.Parameters.AddWithValue("@firstname", (object)userDetail.firstName ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@lastname", (object)userDetail.lastName ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@address", (object)userDetail.address ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@zip", (object)userDetail.zip ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@city", (object)userDetail.city ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@province", (object)userDetail.province ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@district", (object)userDetail.district ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@phone", (object)userDetail.phone ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@lineid", (object)userDetail.lineId ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@isWholeseller", (object)userDetail.wholeseller ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@creditLimit", (object)userDetail.creditLimit ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@id", (object)userDetail.id ?? DBNull.Value);
                    isSuccess = cmd.ExecuteNonQuery();
                    _uow.Commit();
                }


                if (isSuccess > 0)
                {
                    return true;
                }
                else
                    return false;
            }
            catch (Exception e)
            {
                _uow.Dispose();
                return false;
            }
        }
        public bool UpdateBlackList(int accountId, bool isActive)
        {
            IUnitOfWork _uow = new UnitOfWork(new DatabaseContextFactory());
            SqlConnection _conn = _uow.DataContext.Connection;
            try
            {
                var isSuccess = 0;

                var strSqlUpdateUser = "update [dbo].[tblExecMembers] set[blacklist] = @blacklist where[id] = @id";
                using (var cmd = _conn.CreateCommand())
                {
                    cmd.CommandText = strSqlUpdateUser;
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = _uow.BeginTransaction();
                    cmd.Parameters.AddWithValue("@blacklist", (object)isActive ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@id", (object)accountId ?? DBNull.Value);
                    isSuccess = cmd.ExecuteNonQuery();
                    _uow.Commit();
                }
                if (isSuccess > 0)
                {
                    return true;
                }
                else
                    return false;
            }
            catch (Exception e)
            {
                _uow.Dispose();
                return false;
            }
        }
        public bool UpdateLog(UserLogEntity userLog)
        {
            IUnitOfWork _uow = new UnitOfWork(new DatabaseContextFactory());
            SqlConnection _conn = _uow.DataContext.Connection;
            try
            {
                var isSuccess = 0;


                var strSql = @"insert into [dbo].[tblExecMembersLog] ([userid], [auctionid], [orderid], [comment], [adminuser]) values 
                                        (@userid, @auctionid, @orderid, @comment, @adminuser)";
                using (var cmd = _conn.CreateCommand())
                {
                    cmd.CommandText = strSql;
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = _uow.BeginTransaction();
                    cmd.Parameters.AddWithValue("@userid", (object)userLog.userid ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@auctionid", (object)userLog.auctionId ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@orderid", (object)userLog.orderId ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@comment", (object)userLog.comment ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@adminuser", (object)userLog.adminuser ?? DBNull.Value);
                    isSuccess = cmd.ExecuteNonQuery();
                    _uow.Commit();
                }
                if (isSuccess > 0)
                {
                    return true;
                }
                else
                    return false;
            }
            catch (Exception e)
            {
                _uow.Dispose();
                return false;
            }
        }
        public List<ItemRelatedEntity> MapItemRelated(SqlDataReader reader)
        {
            List<ItemRelatedEntity> itemRelateds = new List<ItemRelatedEntity>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    ItemRelatedEntity item = new ItemRelatedEntity();
                    item.itemType = reader["ItemType"].ToString();
                    item.lagKartVarenummer = reader["LagKartVarenummer"].ToString();

                    //user.UserId = 
                    //user.Lastname = reader["LNAME_TH"].ToString();
                    //user.Firstname = reader["FNAME_TH"].ToString();
                    itemRelateds.Add(item);
                }
            }
            return itemRelateds;
        }
        public bool CheckItemNo(string itemNo)
        {
            try
            {
                List<UserLogEntity> listUserLogs = new List<Models.Class.UserLogEntity>();
                string sql = @"SELECT dbo.wMacca_Items.id, dbo.wMacca_Items.ItemNumber, dbo.wMacca_ItemLangAvailability.lang
                                FROM dbo.wMacca_Items INNER JOIN dbo.wMacca_ItemLangAvailability ON dbo.wMacca_Items.id 
                                = dbo.wMacca_ItemLangAvailability.ItemNumber_DBID
                                WHERE (dbo.wMacca_ItemLangAvailability.lang = 'th') 
                                AND ItemNumber = '" + itemNo +
                                @"' AND (ItemNumber IN (SELECT ItemNumber FROM wMacca_Items INNER JOIN wMacca_ItemLanguage 
                                ON (wMacca_Items.id = wMacca_ItemLanguage.ItemNumber_DBID) WHERE ItemNumber is not null 
	                            and Lang = 'th' and (LEN(Subject) > 0 AND LEN(ProductFacts) > 0)))";
                DataTable dt = GetDataTable(sql);
                if (dt != null && dt.Rows.Count > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception e){ return false; }
        }
        public string GetNameItemBasket(string itemNo,string lang)
        {
            string sql = @"SELECT * from wMacca_Items INNER JOIN wMacca_ItemLanguage ON (wMacca_Items.id = wMacca_ItemLanguage.ItemNumber_DBID) where ItemNumber = '" + itemNo + "' and Lang = '" + lang + "' ;";
            DataTable dt = GetDataTable(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                return dt.Rows[0]["Subject"].ToString();
            }
            else
            {
                return string.Empty;
            }
        }
        public bool AddItemBasket(ItemEntity itemEntity, string orderId)
        {
            IUnitOfWork _uow = new UnitOfWork(new DatabaseContextFactory());
            SqlConnection _conn = _uow.DataContext.Connection;
            try
            {
                var isSuccess = 0;
                DataTable dtLang = dtLanguageByShortAll("th");
                if (dtLang.Rows.Count != 1)
                    return false;
                string _valuta = dtLang.Rows[0]["valuta"].ToString();
                string _currency = dtLang.Rows[0]["valuta_short"].ToString();
                if (itemEntity.itemId == 0)
                {
                    var strSqlIns = @"insert into tblAuctions (itemnumber, title, startprice, quantity, auctiontype, currency, 
                                       duration, dateofprocess, dateofend, reference,  bids, visits, hasended) 
                                       values (@itemnumber, @title, @startprice, @quantity, @auctiontype,@currency, @duration,
                                       @dateofprocess, @dateofend,@reference, @bids, @visits, @hasended )SET @id = SCOPE_IDENTITY();";
                    using (var cmd = _conn.CreateCommand())
                    {
                        int iAuctionID = 0;
                        int iBidID = 0;
                        cmd.CommandText = strSqlIns;
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = _uow.BeginTransaction();
                        cmd.Parameters.AddWithValue("@itemnumber", (object)itemEntity.itemNo ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@title", (object)itemEntity.itemName ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@startprice", (object)1 ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@quantity", (object)1 ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@auctiontype", (object)2 ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@currency", (object)_currency ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@duration", (object)0 ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@dateofprocess", (object)DateTime.Now ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@dateofend", (object)DateTime.Now ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@reference", (object)itemEntity.itemNo ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@bids", (object)1 ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@visits", (object)1 ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@hasended", (object)1 ?? DBNull.Value);
                        cmd.Parameters.Add("@id", SqlDbType.Int, 10).Direction = ParameterDirection.Output;
                        cmd.ExecuteNonQuery();
                        iAuctionID = (int)cmd.Parameters["@id"].Value;
                        if (iAuctionID > 0)
                        {
                            cmd.Parameters.Clear();
                            var _sql = @"insert into tblAuctionBids (AuctionId, Email, AccountId, CurrentBid, MaxBid, IsAutoBid, 
                                        IsOverBidByAuto, OnList, Itemno, QuantityBid ) 
                                        values (@auctionId,@email,@accountId,@currentBid,@maxBid,@isAutoBid,@isOverBidByAuto,@onList,@itemno,@quantityBid)SET @id = SCOPE_IDENTITY();";
                            cmd.CommandText = _sql;
                            cmd.CommandType = CommandType.Text;
                            cmd.Parameters.AddWithValue("@auctionId", (object)iAuctionID ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@email", (object)itemEntity.userEmail ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@accountId", (object)itemEntity.accountId ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@currentBid", (object)itemEntity.price ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@maxBid", (object)itemEntity.price ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@isAutoBid", (object)0 ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@isOverBidByAuto", (object)0 ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@onList", (object)0 ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@itemno", (object)itemEntity.itemNo ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@quantityBid", (object)1 ?? DBNull.Value);
                            cmd.Parameters.Add("@id", SqlDbType.Int, 10).Direction = ParameterDirection.Output;
                            cmd.ExecuteNonQuery();
                            iBidID = (int)cmd.Parameters["@id"].Value;
                        }
                        if (!string.IsNullOrEmpty(orderId))
                        {

                            // insert into tblBasket
                            cmd.Parameters.Clear();
                            var _sqlBasKet = @"insert into tblBasket (auctionid, itemnumber, item_type, size, antal, basValuta, 
                                                basValutaPrice, basValutaPriceTotal, paymentorderid) 
                                                values (@auctionid,@itemnumber,@item_type,@size,@antal,@basValuta,@basValutaPrice,
                                                @basValutaPriceTotal,@paymentorderid) ;";
                            cmd.CommandText = _sqlBasKet;
                            cmd.CommandType = CommandType.Text;
                            cmd.Parameters.AddWithValue("@auctionid", (object)iAuctionID ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@itemnumber", (object)itemEntity.itemNo ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@item_type", (object)itemEntity.itemName ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@size", (object)itemEntity.size ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@antal", (object)1 ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@basValuta", (object)_currency ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@basValutaPrice", (object)itemEntity.price ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@basValutaPriceTotal", (object)itemEntity.price ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@paymentorderid", (object)orderId ?? DBNull.Value);
                            isSuccess = cmd.ExecuteNonQuery();
                        }

                        cmd.Parameters.Clear();
                        var _sqlInsAuc = @"insert into tblAuctionWinners (auctionid, accountid, bidid, reserveditem, price_factor, 
                                                auctionwinnerorderid, biditemno, auchasended) 
                                                values (@auctionid,@accountid,@bidid,@reserveditem,@price_factor,@auctionwinnerorderid,
                                                @biditemno,@auchasended);";
                        cmd.CommandText = _sqlInsAuc;
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.AddWithValue("@auctionid", (object)iAuctionID ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@accountid", (object)itemEntity.accountId ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@bidid", (object)iBidID ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@reserveditem", (object)'X' ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@price_factor", (object)_valuta.Replace(",", ".") ?? DBNull.Value);
                        if (!string.IsNullOrEmpty(orderId))
                            cmd.Parameters.AddWithValue("@auctionwinnerorderid", (object)Convert.ToInt32(orderId) ?? DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@auctionwinnerorderid", DBNull.Value);
                        cmd.Parameters.AddWithValue("@biditemno", (object)itemEntity.itemNo ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@auchasended", (object)1 ?? DBNull.Value);
                        isSuccess = cmd.ExecuteNonQuery();

                        if (isSuccess > 0)
                        {
                            cmd.Parameters.Clear();
                            var _sqlBasKet = @"update tblAuctions set currentbid = @currentbid, winnerbidid = @winnerbidid  where id = @id; ";
                            cmd.CommandText = _sqlBasKet;
                            cmd.CommandType = CommandType.Text;
                            cmd.Parameters.AddWithValue("@currentbid", (object)itemEntity.price ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@winnerbidid", (object)iBidID ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@id", (object)iAuctionID ?? DBNull.Value);
                            isSuccess = cmd.ExecuteNonQuery();
                        }
                        _uow.Commit();
                    }
                }
                else
                {
                    var strSql = @"delete from tblAuctionWinners where auctionid = @auctionid and accountid = @accountid ;";
                    using (var cmd = _conn.CreateCommand())
                    {
                        cmd.CommandText = strSql;
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = _uow.BeginTransaction();
                        cmd.Parameters.AddWithValue("@auctionid", (object)itemEntity.itemId ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@accountid", (object)itemEntity.accountId ?? DBNull.Value);
                        isSuccess = cmd.ExecuteNonQuery();
                        if (isSuccess > 0)
                        {
                            cmd.Parameters.Clear();
                            var strSqlIns = @"insert into tblAuctionBids (AuctionId, Email, AccountId, CurrentBid, MaxBid, Itemno) 
                                              values (@AuctionId,@Email,@AccountId,@CurrentBid,@MaxBid,@Itemno)SET @id = SCOPE_IDENTITY();";

                            cmd.CommandText = strSqlIns;
                            cmd.CommandType = CommandType.Text;
                            cmd.Parameters.AddWithValue("@AuctionId", (object)itemEntity.itemId ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@Email", (object)itemEntity.userEmail ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@AccountId", (object)itemEntity.accountId ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@CurrentBid", (object)itemEntity.price ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@MaxBid", (object)itemEntity.price ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@Itemno", (object)itemEntity.itemNo ?? DBNull.Value);
                            cmd.Parameters.Add("@id", SqlDbType.Int, 10).Direction = ParameterDirection.Output;
                            cmd.ExecuteNonQuery();
                            var iBidID = (int)cmd.Parameters["@id"].Value;
                            // create auctionwinner
                            if (iBidID > 0)
                            {
                                cmd.Parameters.Clear();
                                var sqlInsAW = @"insert into tblAuctionWinners (auctionid, accountid, bidid, reserveditem, price_factor) values (@auctionid,@accountid,@bidid,@reserveditem,@price_factor) SET @id = SCOPE_IDENTITY();";

                                cmd.CommandText = sqlInsAW;
                                cmd.CommandType = CommandType.Text;
                                cmd.Parameters.AddWithValue("@auctionid", (object)itemEntity.itemId ?? DBNull.Value);
                                cmd.Parameters.AddWithValue("@accountid", (object)itemEntity.accountId ?? DBNull.Value);
                                cmd.Parameters.AddWithValue("@bidid", (object)iBidID ?? DBNull.Value);
                                cmd.Parameters.AddWithValue("@reserveditem", (object)'X' ?? DBNull.Value);
                                cmd.Parameters.AddWithValue("@price_factor", (object)_valuta.Replace(",", ".") ?? DBNull.Value);
                                cmd.Parameters.Add("@id", SqlDbType.Int, 10).Direction = ParameterDirection.Output;
                                //isSuccess = cmd.ExecuteNonQuery();
                                int iWinner = 0;
                                
                                cmd.ExecuteNonQuery();
                                iWinner = (int)cmd.Parameters["@id"].Value;
                                if (iWinner > 0)
                                {
                                    cmd.Parameters.Clear();
                                    var sqlUpdate = @"update tblAuctions set currentbid = @currentbid, winnerbidid = @winnerbidid where id = @id; ";
                                    cmd.CommandText = sqlUpdate;
                                    cmd.CommandType = CommandType.Text;
                                    cmd.Parameters.AddWithValue("@currentbid", (object)itemEntity.price ?? DBNull.Value);
                                    cmd.Parameters.AddWithValue("@winnerbidid", (object)iBidID ?? DBNull.Value);
                                    cmd.Parameters.AddWithValue("@id", (object)itemEntity.itemId ?? DBNull.Value);
                                    isSuccess = cmd.ExecuteNonQuery();
                                }
                            }
                        }
                        _uow.Commit();
                    }
                }
                if (isSuccess > 0)
                {
                    return true;
                }
                else
                    return false;
            }
            catch (Exception e)
            {
                _uow.Dispose();
                return false;
            }
        }
        public bool DeleteItemBasket(int itemId, string itemNo,int accountid)
        {
            IUnitOfWork _uow = new UnitOfWork(new DatabaseContextFactory());
            SqlConnection _conn = _uow.DataContext.Connection;
            try
            {
                var isSuccess = 0;


                var strSqlInsertUser = "delete [dbo].tblAuctionWinners where[auctionid] = @id and [accountid]= @accountid";
                using (var cmd = _conn.CreateCommand())
                {
                    cmd.CommandText = strSqlInsertUser;
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = _uow.BeginTransaction();
                    cmd.Parameters.AddWithValue("@id", (object)itemId ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@accountid", (object)accountid ?? DBNull.Value);
                    isSuccess = cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    if (isSuccess > 0)
                    {
                        string _sql = "SyncReservationsItemsSingle";
                        cmd.CommandText = _sql;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Itemno", itemNo.ToUpper());
                        cmd.ExecuteScalar();
                        _uow.Commit();
                    }
                }

                if (isSuccess > 0)
                {
                    return true;
                }
                else
                    return false;
            }
            catch (Exception e)
            {
                _uow.Dispose();
                return false;
            }
        }
        public ItemEntity GetItemBasket(string itemId)
        {
            List<ItemEntity> listItems = new List<ItemEntity>();
            string sql = @"select AUC.id,BID.Itemno as itemnumber, AUCLNG.[Subject] as title, AUC.currentbid,REL.ItemId, GFX.path as imgpath
                            from tblAuctionWinners WIN WITH(NOLOCK)
	                        inner join tblAuctions AUC WITH(NOLOCK) on (WIN.auctionid = AUC.id)
	                        inner join tblAuctionBids BID WITH(NOLOCK) on (WIN.bidid = BID.id)
                            inner join wMacca_Items ITM WITH(NOLOCK) ON (ITM.ItemNumber = AUC.reference)
                            left join wMacca_Graphix GFX WITH(NOLOCK) ON (ITM.id = GFX.itemId and placement = 'img1' and thumbGroup = '75' and GFX.deleted = 0) 
                            left  join wMacca_ItemLanguage AUCLNG WITH(NOLOCK) ON (ITM.id = AUCLNG.ItemNumber_DBID and AUCLNG.Lang ='th' )
                            left  join tblItemsRelated REL WITH(NOLOCK) ON (REL.LagkartVarenummer = BID.Itemno) where AUC.id = " + itemId;
            DataTable dt = GetDataTable(sql);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ItemEntity item = new ItemEntity();
                item.itemId = (int)dt.Rows[i]["id"];
                item.itemName = dt.Rows[i]["title"].ToString();
                item.itemNo = dt.Rows[i]["itemnumber"].ToString();
                item.price = (decimal)dt.Rows[i]["currentbid"];
                item.size = dt.Rows[i]["ItemId"].ToString();
                item.imagePath = dt.Rows[i]["imgpath"].ToString();
                listItems.Add(item);
            }
            return listItems.Any() ? listItems.FirstOrDefault() : null;
        }
        public bool UpdateOrderStatus(List<OrderEntity> orderIds,string _adminName,int _statusid,string _comment)
        {
            IUnitOfWork _uow = new UnitOfWork(new DatabaseContextFactory());
            SqlConnection _conn = _uow.DataContext.Connection;
            try
            {
                var dtStatus = dtOrderstatusByIdent(_statusid);
                string _reservedstatus = dtStatus.Rows[0]["reservedstatus"].ToString();
                var isSuccess = 0;
                var strSql = @"insert into tblOrderstatusLog (username, statusident, comment, orderid) values (@username,@statusident,@comment,@orderid)";

                var strSqlUpdOr = "update tblOrders set orderstatusid = @statusid where id = @orderid ";

                var strSqlUpdAuc = "update tblAuctionWinners set reserveditem = @reservedstatus where auctionwinnerorderid = @orderid ";

                using (var cmd = _conn.CreateCommand())
                {
                    cmd.Transaction = _uow.BeginTransaction();
                    foreach (OrderEntity item in orderIds)
                    {
                        cmd.CommandText = strSql;
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.AddWithValue("@username", (object)_adminName ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@statusident", (object)_statusid ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@comment", (object)_comment ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@orderid", (object)item.id ?? DBNull.Value);
                        isSuccess = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();

                        cmd.CommandText = strSqlUpdOr;
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.AddWithValue("@statusid", (object)_statusid ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@orderid", (object)item.id ?? DBNull.Value);
                        isSuccess = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();

                        cmd.CommandText = strSqlUpdAuc;
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.AddWithValue("@reservedstatus", (object)_reservedstatus ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@orderid", (object)item.id ?? DBNull.Value);
                        isSuccess = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }
                    _uow.Commit();
                }
                return true;
            }
            catch (Exception e)
            {
                _uow.Dispose();
                return false;
            }
        }
        public bool UpdateOrderInfo(UserDetailsEntity userAddress)
        {
            IUnitOfWork _uow = new UnitOfWork(new DatabaseContextFactory());
            SqlConnection _conn = _uow.DataContext.Connection;
            try {
                var isSuccess = 0;
                var strSql = @"update tblOrders set name = @firstname , lastname = @lastname , address = @address , zip = @zip, city = @city, province = @province
                                , district = @district , phone = @phone , districtsub = @districtsub , building = @building , state = @state , postoffice = @postoffice
                                where id = @orderid ";

                using (var cmd = _conn.CreateCommand())
                {
                    cmd.Transaction = _uow.BeginTransaction();

                    cmd.CommandText = strSql;
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@firstname", (object)userAddress.firstName ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@lastname", (object)userAddress.lastName ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@address", (object)userAddress.address ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@zip", (object)userAddress.zip ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@city", (object)userAddress.city ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@province", (object)userAddress.province ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@district", (object)userAddress.district ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@phone", (object)userAddress.phone ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@districtsub", (object)userAddress.districtSub ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@building", (object)userAddress.building ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@state", (object)userAddress.state ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@postoffice", (object)userAddress.postOffice ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@orderid", (object)userAddress.id ?? DBNull.Value);
                    isSuccess = cmd.ExecuteNonQuery();
                    _uow.Commit();
                }
                if (isSuccess > 0)
                    return true;
                else
                    return false;
            }
            catch { return false; }
        }
        public bool MergeOrders(List<int> _orderids, int iOrderstatusid, int _provider, out string _out)
        {
            IUnitOfWork _uow = new UnitOfWork(new DatabaseContextFactory());
            SqlConnection _conn = _uow.DataContext.Connection;
            try
            {
                bool _success = true;
                _out = string.Empty;
                string _valuta = "THB";
                _orderids.Sort();
                int iMasterOrderId = _orderids[_orderids.Count - 1];
                StringBuilder sb = new StringBuilder();
                foreach (int i in _orderids)
                {
                    sb.Append("," + i);
                }
                string _orders = sb.ToString().Substring(1);
                DataTable dtOrders = dtBasketByOrderIds(_orders);
                DataView dv = dtOrders.DefaultView;
                dv.RowFilter = "orderstatusid <> " + iOrderstatusid + "";
                if (dv.Count > 0)
                {
                    _out += "All orders must be status received. Fail at order " + dv[0]["paymentorderid"].ToString() + "";
                    return false;
                }
                DataTable dtOrdersData = dtOrderByIds(_orders, false);
                bool provideDiscountOnMerge = false;
                decimal decPrice = 0;
                decimal decCalculatedFreight = 0;
                decimal _priceSumTotal = 0;
                decimal _priceMaxTotal = 0;

                if (iOrderstatusid == 10 && _provider == 1)
                {
                    // mail 06.06.2016
                    // COD: Fragtprisen sættes til den højeste fragtpris representeret på child-ordrerne.
                    // Non-COD: Fragtprisen summeres

                    dv.RowFilter = "itemnumber = '9901'";
                    foreach (DataRowView drv in dv)
                    {
                        string _price = drv["basValutaPrice"].ToString();
                        decimal.TryParse(_price, out decPrice);
                        _priceSumTotal = decimal.Add(_priceSumTotal, decPrice);
                        if (decPrice > _priceMaxTotal)
                            _priceMaxTotal = decPrice;
                    }

                    bool isCOD = false;
                    if (dtOrdersData.Rows.Count > 0)
                        isCOD = dtOrdersData.Rows[0]["deliverytypeident"].ToString().Equals("COD");

                    if (isCOD)
                        decCalculatedFreight = _priceMaxTotal;
                    else
                    {
                        // NON-COD
                        decCalculatedFreight = _priceSumTotal;
                        provideDiscountOnMerge = true;
                    }
                }
                // find liste med auktioner fra de markerede ordrer
                string _auctionsToMove = string.Empty;
                dv.RowFilter = "auctionid > 0";
                foreach (DataRowView drv in dv)
                {
                    _auctionsToMove += "," + drv["auctionid"].ToString();
                }
                if (string.IsNullOrEmpty(_auctionsToMove))
                    _auctionsToMove = "-1";
                else
                    _auctionsToMove = _auctionsToMove.Substring(1);

                // find liste med Sales, fixed prices - to move
                string _salesToMove = string.Empty;
                dv.RowFilter = "saleid > 0";
                foreach (DataRowView drv in dv)
                {
                    _salesToMove += "," + drv["saleid"].ToString();
                }
                if (string.IsNullOrEmpty(_salesToMove))
                    _salesToMove = "-1";
                else
                    _salesToMove = _salesToMove.Substring(1);

                string _newGuid = Guid.NewGuid().ToString().ToUpper();

                int iRowsAffected = 0;
                int iNewOrderid = 0;
                using (var cmd = _conn.CreateCommand())
                {
                    cmd.Transaction = _uow.BeginTransaction();

                    string strSql = "Insert into tblOrders (created, deliverytypeident, deliveryoptionident, bankid, orderlanguage, orderguid, orderstatusid, username, lastchanged, accountid, email, name, lastname, address, zip, city, province, provinceid, district, country, countrytxt, phone, comment, accountkey, districtsub, building, state, postoffice, isPaid, paidDate, app, pickUpLocation, sendToPackingDate )";
                    strSql += " Select created, deliverytypeident, deliveryoptionident, bankid, orderlanguage, @newGuid, orderstatusid, username, lastchanged, accountid, email, name, lastname, address, zip, city, province, provinceid, district, country, countrytxt, phone, comment, accountkey, districtsub, building, state, postoffice, isPaid, paidDate, app, pickUpLocation, sendToPackingDate from tblOrders where id = @iMasterOrderId ; SET @id = SCOPE_IDENTITY();";

                    cmd.CommandText = strSql;
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@newGuid", (object)_newGuid ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@iMasterOrderId", (object)iMasterOrderId ?? DBNull.Value);
                    cmd.Parameters.Add("@id", SqlDbType.Int, 10).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    iNewOrderid = (int)cmd.Parameters["@id"].Value;
                    cmd.Parameters.Clear();
                    if (iNewOrderid > 0)
                    {
                        //** Map orders, new order to be linked to parent
                        foreach (int i in _orderids)
                        {
                            var _sql = "insert into tblOrdersMap (order_parent, order_child) values (@iNewOrderid,@order_child);";
                            cmd.CommandText = _sql;
                            cmd.CommandType = CommandType.Text;
                            cmd.Parameters.AddWithValue("@iNewOrderid", (object)iNewOrderid ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@order_child", (object)i ?? DBNull.Value);
                            iRowsAffected = cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                            if (iRowsAffected != 1)
                            {
                                _out += " ERROR: fail in OrdersMap insert";
                                _success = false;
                            }
                            _sql = "update tblBasket set paymentorderid = @iNewOrderid where paymentorderid = @paymentorderid and auctionid in (@auctionsToMove) ";
                            cmd.CommandText = _sql;
                            cmd.CommandType = CommandType.Text;
                            cmd.Parameters.AddWithValue("@iNewOrderid", (object)iNewOrderid ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@paymentorderid", (object)i ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@auctionsToMove", (object)_auctionsToMove ?? DBNull.Value);
                            iRowsAffected = cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                            if (iRowsAffected < 0)
                            {
                                _out += " ERROR: fail in basket update (auctions)";
                                _success = false;
                            }

                            // sales
                            _sql = "update tblBasket set paymentorderid = @iNewOrderid  where paymentorderid = @paymentorderid and saleid in (@salesToMove) ";
                            cmd.CommandText = _sql;
                            cmd.CommandType = CommandType.Text;
                            cmd.Parameters.AddWithValue("@iNewOrderid", (object)iNewOrderid ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@paymentorderid", (object)i ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@salesToMove", (object)_salesToMove ?? DBNull.Value);
                            iRowsAffected = cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                            if (iRowsAffected < 0)
                            {
                                _out += " ERROR: fail in basket update (sales)";
                                _success = false;
                            }

                            // 9908 Voucher
                            // 9902 Additional surcharge
                            _sql = "update tblBasket set paymentorderid = @iNewOrderid where paymentorderid = @paymentorderid and (itemnumber = '9908' or itemnumber = '9902' ); ";
                            cmd.CommandText = _sql;
                            cmd.CommandType = CommandType.Text;
                            cmd.Parameters.AddWithValue("@iNewOrderid", (object)iNewOrderid ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@paymentorderid", (object)i ?? DBNull.Value);
                            iRowsAffected = cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                            if (iRowsAffected < 0)
                            {
                                _out += " ERROR: fail in basket update (sales)";
                                _success = false;
                            }

                            if (provideDiscountOnMerge)
                            {
                                // 9909 Discount
                                _sql = "update tblBasket set paymentorderid = @iNewOrderid where paymentorderid = @paymentorderid and ( itemnumber = '9909' ); ";
                                cmd.CommandText = _sql;
                                cmd.CommandType = CommandType.Text;
                                cmd.Parameters.AddWithValue("@iNewOrderid", (object)iNewOrderid ?? DBNull.Value);
                                cmd.Parameters.AddWithValue("@paymentorderid", (object)i ?? DBNull.Value);
                                iRowsAffected = cmd.ExecuteNonQuery();
                                cmd.Parameters.Clear();
                                if (iRowsAffected < 0)
                                {
                                    _out += " ERROR: fail in basket update (ProvideDiscountOnMerge)";
                                    _success = false;
                                }
                            }

                            //** handle reservations, new order items will have reserved status, X OnHold
                            _sql = "update tblAuctionWinners set reserveditem = 'X', auctionwinnerorderid = @iNewOrderid where auctionwinnerorderid = @auctionwinnerorderid and auctionid in (@auctionsToMove) ;";
                            cmd.CommandText = _sql;
                            cmd.CommandType = CommandType.Text;
                            cmd.Parameters.AddWithValue("@iNewOrderid", (object)iNewOrderid ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@auctionwinnerorderid", (object)i ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@auctionsToMove", (object)_auctionsToMove ?? DBNull.Value);
                            iRowsAffected = cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                            if (iRowsAffected < 0)
                            {
                                _out += " ERROR: fail in AuctionWinners update";
                                _success = false;
                            }

                            // sales
                            _sql = "update tblSaleItems set reserveditem = 'X', orderid = @iNewOrderid where orderid = @orderid and id in (@salesToMove) ;";
                            cmd.CommandText = _sql;
                            cmd.CommandType = CommandType.Text;
                            cmd.Parameters.AddWithValue("@iNewOrderid", (object)iNewOrderid ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@orderid", (object)i ?? DBNull.Value);
                            cmd.Parameters.AddWithValue("@salesToMove", (object)_salesToMove ?? DBNull.Value);
                            iRowsAffected = cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                            if (iRowsAffected < 0)
                            {
                                _out += " ERROR: fail in Sale Items update";
                                _success = false;
                            }
                        }
                        // New order will have item: Servicecharge 10% (9910)
                        var _sqlNew = "insert into tblBasket (itemnumber, item_type, antal, basValuta, paymentorderid) values ('9910', N'ค่าบริการ 10%', 1, @valuta , @iNewOrderid );";
                        cmd.CommandText = _sqlNew;
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.AddWithValue("@valuta", (object)_valuta ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@iNewOrderid", (object)iNewOrderid ?? DBNull.Value);
                        iRowsAffected = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        if (iRowsAffected != 1)
                        {
                            _out += " ERROR: fail in basket insert 9910";
                            _success = false;
                        }

                        // New order will have item 0000
                        _sqlNew = "insert into tblBasket (itemnumber, item_type, antal, basValuta, paymentorderid) values ('0000', N'รวมทั้งสิ้น', 1, @valuta, @iNewOrderid);";
                        cmd.CommandText = _sqlNew;
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.AddWithValue("@valuta", (object)_valuta ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@iNewOrderid", (object)iNewOrderid ?? DBNull.Value);
                        iRowsAffected = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        if (iRowsAffected != 1)
                        {
                            _out += " ERROR: fail in basket insert 0000";
                            _success = false;
                        }

                        // New order will have delivery item 9901
                        if (decCalculatedFreight > 0)
                        {
                            string _calcFreight = decCalculatedFreight.ToString().Replace(",", ".");
                            _sqlNew = @"insert into tblBasket (itemnumber, item_type, antal, basValuta, basValutaPrice, basValutaPriceTotal, paymentorderid) 
                                        select itemnumber, item_type + ' [" + _orders + "]', antal, basValuta, '" + _calcFreight + "', '" + _calcFreight + "', " + iNewOrderid + " from tblBasket where paymentorderid = " + iMasterOrderId + " and itemnumber = 9901;";
                            cmd.CommandText = _sqlNew;
                            cmd.CommandType = CommandType.Text;
                            iRowsAffected = cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                            if (iRowsAffected != 1)
                            {
                                _out += " ERROR: fail in basket insert 9901";
                                _success = false;
                            }
                        }
                        else
                        {
                            _sqlNew = @"insert into tblBasket (itemnumber, item_type, antal, basValuta, basValutaPrice, basValutaPriceTotal, paymentorderid) 
                                    select itemnumber, item_type, antal, basValuta, basValutaPrice, basValutaPriceTotal, " + iNewOrderid + " from tblBasket where paymentorderid = " + iMasterOrderId + " and itemnumber = 9901;";
                            cmd.CommandText = _sqlNew;
                            cmd.CommandType = CommandType.Text;
                            iRowsAffected = cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                            if (iRowsAffected != 1)
                            {
                                _out += " ERROR: fail in basket insert 9901";
                                _success = false;
                            }
                        }

                        // nulstil oprindelige ordrer
                        _sqlNew = "update tblBasket set basValutaPrice = 0, basValutaPriceTotal = 0 where paymentorderid in (" + _orders + ") and itemnumber in (9909, 9901)";
                        cmd.CommandText = _sqlNew;
                        cmd.CommandType = CommandType.Text;
                        iRowsAffected = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        if (iRowsAffected < 0)
                        {
                            _out += " ERROR: clean orders";
                            _success = false;
                        }

                        // update orders with statusid 101 Merged
                        _sqlNew = "update tblOrders set orderstatusid = 101 where id in (" + _orders + ");";
                        cmd.CommandText = _sqlNew;
                        cmd.CommandType = CommandType.Text;
                        iRowsAffected = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        if (iRowsAffected < 0)
                        {
                            _out += " ERROR: set status Merged";
                            _success = false;
                        }
                    }
                    _uow.Commit();
                }
                if (string.IsNullOrEmpty(_out))
                    _out = "Mergin of orders into " + iNewOrderid + " completed";

                return _success;
            }
            catch (Exception e)
            {
                _out = ""+ e.Message;
                _uow.Dispose();
                return false;
            }
        }
        public static DataTable dtBasketByOrderIds(string _orderids)
        {
            string sql = @"select BAS.*, ORD.orderstatusid from tblBasket BAS
		                    inner join tblOrders ORD on (BAS.paymentorderid = ORD.id )
		                    where BAS.paymentorderid in (" + _orderids + ") order by BAS.paymentorderid ;";

            DataTable dt = GetDataTable(sql);
            return dt;
        }
        public static DataTable dtOrderByIds(string _orders, bool hasLeadingGuidChar2)
        {
            string _sql = "select * from tblOrders where id in (" + _orders + ")  order by id asc;";

            if (hasLeadingGuidChar2)
            {
                string _guidChar2 = string.Empty;
                string _order = string.Empty;
                string _where = string.Empty;
                foreach (string s in _orders.Split(','))
                {
                    _where += "or id = " + s.Substring(2) + " and orderguid like '" + s.Substring(0, 2) + "%' ";

                }
                _sql = "select * from tblOrders where " + _where.Substring(3) + " order by id asc;";

            }

            DataTable dt = GetDataTable(_sql);
            return dt;

        }
        //public static DataTable dtStatusOrder()
        //{
        //    string _sql = "select * from tblOrderstatus where ident in (" + _idents + "); ";

        //    DataTable dt = GetDataTable(_sql);
        //    DataRow dr = new DataRow("", "");
        //    dt.Rows.Add(dr);
        //    return dt;
        //}
        public static DataTable dtOrderstatusByIdent(int _ident)
        {
            string _sql = "select * from tblOrderstatus where ident = " + _ident + ";";
            DataTable dt = GetDataTable(_sql);
            return dt;
        }
        public static DataTable dtLanguageByShortAll(string _lang)
        {
            string _sql = "SELECT * FROM wMacca_language where lang = '" + _lang + "';";
            DataTable dt = GetDataTable(_sql);
            return dt;
        }
        public static DataTable GetAuctionsToBuy(int _accountid, string _lang)
        {
            IUnitOfWork _uow = new UnitOfWork(new DatabaseContextFactory());
            SqlConnection _conn = _uow.DataContext.Connection;
            try
            {
                //SqlConnection DB = GetConn();
                //_conn.Open();
                DataTable dt = new DataTable();

                string sql = "GetAuctionsToBuyItems";
                SqlCommand cmd = new SqlCommand(sql, _conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@AccountId", _accountid);
                cmd.Parameters.AddWithValue("@Lang", _lang);

                SqlDataAdapter oDa = new SqlDataAdapter(cmd);
                oDa.Fill(dt);
                _conn.Close();

                return dt;
            }
            catch
            {
                return null;
            }
        }
        public static DataTable GetDataTable(string sql)
        {
            IUnitOfWork _uow = new UnitOfWork(new DatabaseContextFactory());
            SqlConnection _conn = _uow.DataContext.Connection;
            try
            {
                DataTable dt = new DataTable();
                using (SqlConnection DB = _conn)
                {
                    //DB.Open();
                    SqlCommand cmd = new SqlCommand(sql, DB);

                    try
                    {
                        SqlDataAdapter oDa = new SqlDataAdapter(cmd);
                        oDa.Fill(dt);
                    }
                    catch (Exception ex)
                    { return dt; }
                }

                return dt;
            }catch (Exception e)
            {
                return null;
            }
        }
    }
}