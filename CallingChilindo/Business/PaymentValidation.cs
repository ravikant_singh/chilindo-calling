﻿using CallingChilindo.Models.Class;
using System;
using System.ComponentModel.DataAnnotations;

namespace CallingChilindo.Business
{
    public class PaymentValidation : IPaymentValidation
    {
        private const int MaximumLength = 500;
        private const string PaymentType = "BANKWIRE";

        public ValidationResult Validate(PaymentReceiptsEntity payment)
        {
            if (payment.datePaid.Value.CompareTo(payment.created) < 1)
            {
                return new ValidationResult("Payment date should not earlier than Order date!");
            }
        
            try
            {
                decimal d;
                decimal.TryParse(payment.orderAmount.ToString(), out d);
            }
            catch (Exception ex)
            {
                return new ValidationResult("Please enter valid amount!" + ' ' + ex.Message);
            }

            return ValidationResult.Success;
        }

        public ValidationResult Validate(string orderId, string amount, string datePaid, string seeReceipt, string bank)
        {
            if (datePaid == string.Empty || datePaid == null)
            {
                return new ValidationResult("Payment date should not be null!");
            }

            if (seeReceipt == string.Empty || seeReceipt == null)
            {
                return new ValidationResult("Please select Did you see receipt?");
            }

            if (bank == string.Empty || bank == null)
            {
                return new ValidationResult("Please select bank");
            }
            return ValidationResult.Success;
        }
    }
}