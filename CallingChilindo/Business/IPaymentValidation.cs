﻿using System.ComponentModel.DataAnnotations;
using CallingChilindo.Models.Class;

namespace CallingChilindo.Business
{
    public interface IPaymentValidation
    {
        ValidationResult Validate(PaymentReceiptsEntity payment);
        ValidationResult Validate(string orderId, string amount, string datePaid, string seeReceipt, string bank);
    }
}