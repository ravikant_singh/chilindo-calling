﻿using CallingChilindo.Models.Class;

namespace CallingChilindo.Business.Payment
{
    public interface IPaymentBuilder
    {
        PaymentReceiptsEntity Build(string orderId, string amount, string datePaid, string seeReceipt, string bank, string comment, string userId, int paymentId);
        string GetUserName(string userId);
    }
}
