﻿using CallingChilindo.Models;
using CallingChilindo.Models.Class;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;

namespace CallingChilindo.Business.Payment
{
    public class PaymentBuilder: IPaymentBuilder
    {
        public PaymentReceiptsEntity Build(string orderId, string amount, string datePaid, string seeReceipt, string bank, string comment, string userId, int paymentId)
        {
            var paymentReceipt = new PaymentReceiptsEntity();
            var splitter = new Splitter();
            var amountList = splitter.Split(amount);
            if (amountList != null)
            {
                BuildAmounts(amountList, paymentReceipt);
                paymentReceipt.orderidents = orderId;
                paymentReceipt.userId = GetUserName(userId);
                paymentReceipt.amountTotal = splitter.Sum(amount);
                paymentReceipt.orderAmount = splitter.Sum(amount);
                paymentReceipt.datePaid = Convert.ToDateTime(datePaid);
                paymentReceipt.showReceipt = seeReceipt;
                paymentReceipt.bankId = Convert.ToInt32(bank);
                paymentReceipt.comment = comment;
                paymentReceipt.paymentReceiptId = paymentId;
                return paymentReceipt;
            }
            return null;
        }
        public string GetUserName(string userId)
        {
            var userStore = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var manager = new UserManager<ApplicationUser>(userStore);
            var currentUser = manager.FindById(userId);
            if (currentUser != null)
            {
                return currentUser.FirstName;
            }
            return string.Empty;
        }
        private void BuildAmounts(string[] amountList, PaymentReceiptsEntity paymentReceipt)
        {
            int i = 0;
            foreach (var item in amountList)
            {
                if (i == 0) { paymentReceipt.amount1 = (item != null) ? Convert.ToDecimal(item) : 0.0m; }

                if (i == 1) { paymentReceipt.amount2 = (item != null) ? Convert.ToDecimal(item) : 0.0m; }

                if (i == 2) { paymentReceipt.amount3 = (item != null) ? Convert.ToDecimal(item) : 0.0m; }
                i++;
            }
        }
    }
}