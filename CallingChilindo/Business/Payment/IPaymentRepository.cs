﻿using CallingChilindo.Models.Class;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace CallingChilindo.Business.Payment
{
    public interface IPaymentRepository
    {
        IQueryable<BankEntity> GetBankAll();
        bool GetCheckOrderId(int orderId);
        PaymentReceiptsEntity GetPaymentReceipts(int paymentReceiptsId);
        IQueryable<PaymentReceiptsEntity> GetPatmentHistory(string userName);
        List<PaymentReceiptsEntity> GetPatmentHistorys(string username);
        DbRawSqlQuery<T> RunQuery<T>(string query, params object[] parameters);
        bool Insert(PaymentReceiptsEntity paymentEntity);
        bool Delete(int paymentReceiptId);
        bool Update(PaymentReceiptsEntity paymentEntity);
    }
}
