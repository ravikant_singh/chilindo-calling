﻿using System.Data.SqlClient;

namespace CallingChilindo.Business.Common
{
    public interface IDatabaseContext
    {
        SqlConnection Connection { get; }
        void Dispose();
    }
}
