﻿namespace CallingChilindo.Business.Common
{
    public interface IDatabaseContextFactory
    {
        IDatabaseContext Context();
    }
}
