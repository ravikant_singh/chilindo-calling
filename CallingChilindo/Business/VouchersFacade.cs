﻿using CallingChilindo.Business.Common;
using CallingChilindo.Models;
using CallingChilindo.Models.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CallingChilindo.Business
{
    public class VouchersFacade
    {
        private SqlConnection _conn;
        private IUnitOfWork _uow;
        private ChilindoCallingEntities db = new ChilindoCallingEntities();

        //public IQueryable<VouchersEntity> GetAllVouchers()
        //{
        //    try
        //    {
        //        var TransactionData = RunQuery<VouchersEntity>("Exec [dbo].[GetVouchers]");
                
        //       foreach(VouchersEntity item in TransactionData)
        //        {
        //            if (item.isActive == null)
        //                item.isActive = true;
        //        }
        //        return TransactionData.AsQueryable();
        //        //int startPageIndex = (searchFilter.PageNo - 1) * searchFilter.PageSize;
        //        //searchFilter.TotalRecords = TransactionData.Count();
        //        //if (startPageIndex >= searchFilter.TotalRecords)
        //        //{
        //        //    startPageIndex = 0;
        //        //    searchFilter.PageNo = 1;
        //        //}
        //        //return TransactionData.Skip(startPageIndex)
        //        //               .Take(searchFilter.PageSize).AsQueryable();
        //        //return TransactionData.AsQueryable();
        //        //List<VouchersEntity> vouchersAll = new List<VouchersEntity>();
        //        //VouchersEntity v1 = new VouchersEntity();
        //        //v1.id = 1;
        //        //v1.voucherCode = "TTTT";
        //        //VouchersEntity v2 = new VouchersEntity();
        //        //v2.id = 2;
        //        //v2.voucherCode = "SSSS";
        //        //vouchersAll.Add(v1);
        //        //vouchersAll.Add(v2);
        //        //return vouchersAll.AsQueryable();
        //    }
        //    catch { return null; }
        //}

        public List<VouchersEntity> GetAllVouchers()
        {
            List<VouchersEntity> listVouchers = new List<VouchersEntity>();
            string sql = @"select VOU.*, VTYP.vouchertype  
                            , (select count(*) as antal from tblVouchersBasket where UPPER(vouchercode) = UPPER(VOU.vouchercode)) as vouchers_in_basket
                            from tblVouchers VOU
                            inner join tblVoucherTypes VTYP on (VOU.vouchertypeid = VTYP.id) order by  id desc,valid desc; ";
            DataTable dt = GetDataTable(sql);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                VouchersEntity voucher = new VouchersEntity();
                voucher.id = (int)dt.Rows[i]["id"];


                try { voucher.created = (DateTime)dt.Rows[i]["created"]; }
                catch { voucher.created = null; }
                
                voucher.voucherCode = dt.Rows[i]["vouchercode"].ToString();
                voucher.maxRedemptions = (int)dt.Rows[i]["cnt2use"];
                voucher.val = (decimal)dt.Rows[i]["val"];
                voucher.useInbasket = (int)dt.Rows[i]["vouchers_in_basket"];
                voucher.additional = dt.Rows[i]["additional"].ToString();
                voucher.minPurchasePrice = (decimal)dt.Rows[i]["limit_basket_amount"];
               

                try { voucher.voucherType = (string)dt.Rows[i]["vouchertype"]; }
                catch { voucher.voucherType = string.Empty; }

                try { voucher.valid = (DateTime)dt.Rows[i]["valid"]; }
                catch { voucher.valid = null; }

                 try{ voucher.isActive = (bool)dt.Rows[i]["isActive"];}
                 catch{ voucher.isActive = false;}

                listVouchers.Add(voucher);
            }

            return listVouchers;
        }

     
        public IQueryable<VoucherTypeEntity> GetAllVoucherType()
        {
            var query = (from v in db.tblVoucherTypes
                         select new VoucherTypeEntity { id = v.id, voucherType = v.vouchertype }).AsQueryable();
            return query;
        }
        public bool AddVoucher(VouchersEntity voucher)
        {
            try
            {
                var isSuccess = 0;
                _uow = new UnitOfWork(new DatabaseContextFactory());
                _conn = _uow.DataContext.Connection;
                var strSqlInsertUser = "insert into [dbo].[tblVouchers]([created],[vouchercode],[cnt2use],[val],[additional],[vouchertypeid],[limit_basket_amount],[valid],[isActive]) values(@created, @vouchercode, @cnt2use, @val, @additional, @vouchertypeid, @limit_basket_amount, @valid, @isActive)";
                using (var cmd = _conn.CreateCommand())
                {
                    cmd.CommandText = strSqlInsertUser;
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = _uow.BeginTransaction();
                    cmd.Parameters.AddWithValue("@created", (object)DateTime.Now ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@vouchercode", (object)voucher.voucherCode ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@cnt2use", (object)voucher.maxRedemptions ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@val", (object)voucher.val ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@additional", (object)voucher.additional ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@vouchertypeid", (object)voucher.typeOfVoucher ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@limit_basket_amount", (object)voucher.minPurchasePrice ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@valid", (object)DateTime.Now.AddMonths(1) ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@isActive", (object)true ?? DBNull.Value);
                    isSuccess = cmd.ExecuteNonQuery();
                    _uow.Commit();
                }
                if (isSuccess > 0)
                {
                    return true;
                }
                else
                    return false;
            }
            catch(Exception e)
            {
                _uow.Dispose();
                return false;
            }
        }
        //public bool AddVoucher(VouchersEntity voucher)
        //{
        //    try {
        //        using (SqlConnection conn = new SqlConnection(GetConnString()))
        //        using (SqlCommand cmd = conn.CreateCommand())
        //        {
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            cmd.Parameters.AddWithValue("SeqName", "SeqNameValue");

        //            var returnParameter = cmd.Parameters.Add("@ReturnVal", SqlDbType.Int);
        //            returnParameter.Direction = ParameterDirection.ReturnValue;

        //            conn.Open();
        //          int x =   cmd.ExecuteNonQuery();
        //            //    //if (isSuccess > 0)
        //            //    //{
        //            //    //    db.SaveChanges();
        //            //    //    return true;
        //            //    //}
        //            //    //else
        //            //    //    return false;
        //        }
        //        return 1;
        //    }
        //    catch { }
        //    //try
        //    //{
        //    //    //var isSuccess = 0;
        //    //    //if (voucher.id > 0)
        //    //    //{
        //    //    //    isSuccess = 0;
        //    //    //}
        //    //    //else
        //    //    //{
        //    //    //    //isSuccess = db.sp_Ins_tblVouchers(DateTime.Now, voucher.voucherCode, voucher.maxRedemptions, voucher.val, voucher.additional, voucher.typeOfVoucher, voucher.minPurchasePrice, DateTime.Now,true);
        //    //    //    string _sql = @"insert into tblVouchers (vouchercode, cnt2use, additional, vouchertypeid, limit_basket_amount, val, valid) 
        //    //    //        values ('" + voucher.voucherCode + "', '" + voucher.maxRedemptions + "', N'" + voucher.additional + "', '" + voucher.typeOfVoucher + "', '" + voucher.minPurchasePrice + "', '" + voucher.val + "', " + ConvertDBDateTime(DateTime.MaxValue.ToString()) + ");";
        //    //    //    string xxx = ExecuteNonQueryTxt(_sql);
        //    //    //}
        //    //    //if (isSuccess > 0)
        //    //    //{
        //    //    //    db.SaveChanges();
        //    //    //    return true;
        //    //    //}
        //    //    //else
        //    //    //    return false;

        //    //}
        //    //catch(Exception e) { return false; }
        //    //try
        //    //{
        //    //    _uow = new UnitOfWork(new DatabaseContextFactory());
        //    //    _conn = _uow.DataContext.Connection;
        //    //    var strSqlInsertUser = "insert into [dbo].[tblVouchers]([created],[vouchercode],[cnt2use],[val],[additional],[vouchertypeid],[limit_basket_amount],[valid],[isActive]) values(@created, @vouchercode, @cnt2use, @val, @additional, @vouchertypeid, @limit_basket_amount, @valid, @isActive)";
        //    //    using (var cmd = _conn.CreateCommand())
        //    //    {
        //    //        cmd.CommandText = strSqlInsertUser;
        //    //        cmd.CommandType = CommandType.Text;
        //    //        cmd.Transaction = _uow.BeginTransaction();
        //    //        cmd.Parameters.AddWithValue("@created", (object)entity.UserId ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@USERNAME", (object)entity.Username ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@PASSWORD", (object)entity.Password ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@TITLE_ID", (object)entity.TitleId ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@TITLE_TH", (object)entity.TitleName ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@FNAME_TH", (object)entity.Firstname ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@LNAME_TH", (object)entity.Lastname ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@TITLE_EN", (object)entity.TitleNameEn ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@FNAME_EN", (object)entity.FirstnameEn ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@LNAME_EN", (object)entity.LastnameEn ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@EMPLOYEE_TYPE", (object)entity.EmployeeType ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@EMPLOYEE_TYPE_DESC", (object)entity.EmployeeTypeDesc ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@BRANCH_CODE", (object)entity.BranchCode ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@BRANCH_NAME", (object)entity.BranchName ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@POSITION", (object)entity.Position ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@POSITION_DESC", (object)entity.PositionDesc ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@BU1", (object)entity.Bu1 ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@BU1_DESC", (object)entity.Bu1Desc ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@BU2", (object)entity.Bu2 ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@BU2_DESC", (object)entity.Bu2Desc ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@BU3", (object)entity.Bu3 ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@BU3_DESC", (object)entity.Bu3Desc ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@JOB", (object)entity.Job ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@JOB_POSITION", (object)entity.JobPosition ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@START_DATE", (object)entity.StartDate ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@FIRST_HIRE_DATE", (object)entity.FirstHireDate ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@RESIGN_DATE", (object)entity.ResignDate ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@STATUS", (object)entity.Status ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@STATUS_DESC", (object)entity.StatusDesc ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@EMAIL", (object)entity.Email ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@NOTES_ADDRESS", (object)entity.NotesAddress ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@TEL_EXT", (object)entity.TelExt ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@BOSS", (object)entity.Boss ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@BOSS_NAME", (object)entity.BossName ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@AD_USER", (object)entity.AdUser ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@HR_JOB_CODE", (object)entity.HrJobCode ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@HR_JOB_CODE_DESC", (object)entity.HrJobCodeDesc ?? DBNull.Value);
        //    //        cmd.Parameters.AddWithValue("@COST_CENTER", (object)entity.CostCenter ?? DBNull.Value);
        //    //        i = cmd.ExecuteNonQuery();
        //    //        cmd.Parameters.Clear();
        //    //        foreach (var item in entity.Roles)
        //    //        {
        //    //            cmd.CommandText = strSqlInsertUserRole;
        //    //            cmd.Parameters.AddWithValue("@UserId", entity.UserId);
        //    //            cmd.Parameters.AddWithValue("@RoleId", item.RoleId);
        //    //            i = cmd.ExecuteNonQuery();
        //    //            cmd.Parameters.Clear();
        //    //        }
        //    //    }
        //    //}
        //    }
        public bool UpdateVoucher(int voucherId,bool isActive)
        {
            try
            {
                var isSuccess = 0;
                _uow = new UnitOfWork(new DatabaseContextFactory());
                _conn = _uow.DataContext.Connection;
                var strSqlInsertUser = "update [dbo].[tblVouchers] set[isActive] = @isActive where[id] = @id";
                using (var cmd = _conn.CreateCommand())
                {
                    cmd.CommandText = strSqlInsertUser;
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = _uow.BeginTransaction();
                    cmd.Parameters.AddWithValue("@isActive", (object)isActive ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@id", (object)voucherId ?? DBNull.Value);
                    isSuccess = cmd.ExecuteNonQuery();
                    _uow.Commit();
                }
                if (isSuccess > 0)
                {
                    return true;
                }
                else
                    return false;
            }
            catch (Exception e)
            {
                _uow.Dispose();
                return false;
            }
            //try
            //{
            //    var isSuccess = db.sp_Update_tblVouchers(voucherId,isActive);
            //    if (isSuccess > 0)
            //    {
            //        db.SaveChanges();
            //        return true;
            //    }
            //    else
            //        return false;
            //}
            //catch { return false; }
        }
        public DbRawSqlQuery<T> RunQuery<T>(string query, params object[] parameters)
        {
            return db.Database.SqlQuery<T>(query, parameters);
        }

        public static string ExecuteNonQueryTxt(string sql)
        {
            if (string.IsNullOrEmpty(sql))
                return "- missing sql";

            string _out = string.Empty;

            int iRowsAffected = 0;
            using (SqlConnection DB = GetConn())
            {
                DB.Open();
                using (SqlCommand cmd = new SqlCommand(sql, DB))
                {
                    try
                    {
                        iRowsAffected = cmd.ExecuteNonQuery();
                        _out = iRowsAffected + " row(s) affected";
                    }
                    catch (Exception ex)
                    {
                        iRowsAffected = -1;
                        _out = ex.Message;
                    }
                }
            }

            return _out;
        }
        public static DataTable GetDataTable(string sql)
        {
            IUnitOfWork _uow = new UnitOfWork(new DatabaseContextFactory());
            SqlConnection _conn = _uow.DataContext.Connection;
            try
            {
                DataTable dt = new DataTable();
                using (SqlConnection DB = _conn)
                {
                    //DB.Open();
                    SqlCommand cmd = new SqlCommand(sql, DB);

                    try
                    {
                        SqlDataAdapter oDa = new SqlDataAdapter(cmd);
                        oDa.Fill(dt);
                    }
                    catch (Exception ex)
                    { return dt; }
                }

                return dt;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public static SqlConnection GetConn()
        {
            string strConnection = GetConnString();
            SqlConnection DB = new SqlConnection(strConnection);

            return DB;
        }
        public static string GetConnString()
        {

            string conn;
            if (System.Web.HttpContext.Current == null)
            {
                return (string)System.Configuration.ConfigurationSettings.AppSettings.Get("db_macca_dk");
            }
            //string lsServername = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"];

            //if (lsServername == "localhost")
            //{
            //    conn = "db_macca_dk";
            //}
            //else if (lsServername == "mybuy.dk.virker.nu")
            //{
            //    conn = "db_macca_dk";
            //}
            //else if (lsServername == "mybuy.dk")
            //{
            //    conn = "db_macca_dk";
            //}
            //else if (lsServername == "www.mybuy.dk")
            //{
            //    conn = "db_macca_dk";
            //}
            //else if (lsServername == "beta.mybuy.dk")
            //{
            //    conn = "db_macca_dk";
            //}
            //else
            //{
            conn = "db_macca_dk";
            //}


            string strConnection = (string)System.Configuration.ConfigurationSettings.AppSettings.Get(conn);
            return strConnection;

        }
        public static string ConvertDBDateTime(string _datetime)
        {
            return "convert(datetime, '" + _datetime + "', 105)";
        }
    }
}