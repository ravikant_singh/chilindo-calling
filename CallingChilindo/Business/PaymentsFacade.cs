﻿using System;
using System.Collections.Generic;
using System.Linq;
using CallingChilindo.Models;
using System.Data.Entity.Infrastructure;
using System.Data;
using CallingChilindo.Models.Class;
using CallingChilindo.Business.Common;
using System.Data.SqlClient;

namespace CallingChilindo.Business
{
    public class PaymentsFacade
    {
        private SqlConnection _conn;
        private IUnitOfWork _uow;
        private ChilindoCallingEntities db = new ChilindoCallingEntities();
        public IQueryable<BankEntity> GetBankAll()
        {
            IQueryable<BankEntity> bankAlls = null;
            bankAlls = (from b in db.tblBanks select new BankEntity { bankId = b.id,bankName=b.bankident});
            return bankAlls;
        }
        public bool GetCheckOrderId(int orderId)
        {
            bool check = (from o in db.tblOrders.Where(o => o.id == orderId && o.orderstatusid == 1 && o.deliveryoptionident == "BANKWIRE") select o).Any();
            return check;
        }
        public PaymentReceiptsEntity GetPaymentReceipts(int paymentReceiptsId)
        {
            try
            {
                var sql = @"select REC.* from tblPaymentReceipts REC where id = " + paymentReceiptsId;
                var payment = new PaymentReceiptsEntity();
                DataTable dt = GetDataTable(sql);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    payment.id = (int)dt.Rows[i]["id"];
                    payment.created = (DateTime)dt.Rows[i]["created"];
                    payment.userId = dt.Rows[i]["userid"].ToString();
                    payment.datePaid = (DateTime)dt.Rows[i]["datepaid"];
                    payment.amountTotal = (decimal)dt.Rows[i]["amounttotal"];
                    payment.orderidents = dt.Rows[i]["orderidents"].ToString();
                    payment.showReceipt = dt.Rows[i]["shownreceipt"].ToString();
                    payment.orderAmount = (decimal)dt.Rows[i]["orderamount"];
                    payment.bankId = (int)dt.Rows[i]["bankid"];
                    payment.comment = dt.Rows[i]["comment"].ToString();
                    payment.matchType = dt.Rows[i]["matchtype"].ToString();
                    payment.amount1 = (decimal)dt.Rows[i]["amount1"];
                    payment.amount2 = (decimal)dt.Rows[i]["amount2"];
                    payment.amount3 = (decimal)dt.Rows[i]["amount3"];
                }
                return payment;
            }
            catch { return null; }
        }
        public bool AddPaymentReceipt(PaymentReceiptsEntity paymentEntity)
        {
            try
            {
                var isSuccess = 0;
                _uow = new UnitOfWork(new DatabaseContextFactory());
                _conn = _uow.DataContext.Connection;
        
                var strSqlInsertUser = @"INSERT INTO [dbo].[tblPaymentReceipts]([created],[orderidents],[amount1],[amount2],[amount3],
                                         [amounttotal],[orderamount],[datepaid],[shownreceipt],[bankid],[comment],[userid],[matchtype])
                                         VALUES(@created,@orderidents,@amount1,@amount2,@amount3,@amounttotal,@orderamount,@datepaid,
                                         @showreceipt,@bankid,@comment,@userid,@matchtype)";

                using (var cmd = _conn.CreateCommand())
                {
                        cmd.CommandText = strSqlInsertUser;
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = _uow.BeginTransaction();
                        cmd.Parameters.AddWithValue("@created", (object)DateTime.Now ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@orderidents", (object)paymentEntity.orderidents ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@amount1", (object)paymentEntity.amount1 ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@amount2", (object)paymentEntity.amount2 ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@amount3", (object)paymentEntity.amount3 ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@amounttotal", (object)paymentEntity.amountTotal ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@orderamount", (object)paymentEntity.orderAmount ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@datepaid", (object)paymentEntity.datePaid ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@showreceipt", (object)paymentEntity.showReceipt ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@bankid", (object)paymentEntity.bankId ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@comment", (object)paymentEntity.comment ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@userid", (object)paymentEntity.userId ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@matchtype", (object)paymentEntity.matchType ?? DBNull.Value);
                        isSuccess = cmd.ExecuteNonQuery();
                        _uow.Commit();
                }

                if (isSuccess > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                _uow.Dispose();
                return false;
            }
        }

        public IQueryable<PaymentReceiptsEntity> GetPatmentHistory(string userName)
        {
            try
            {
                IQueryable<PaymentReceiptsEntity> query = null;
                var TransactionData = RunQuery<GetReceiptHistoryResult>("Exec [dbo].[GetReceiptHistory] {0}", userName);
                if (TransactionData != null && TransactionData.Count() > 0)
                {
                    query = (from p in TransactionData
                             select new PaymentReceiptsEntity
                             {
                                 id = p.id,
                                 created = p.created,
                                 userId = p.userid,
                                 datePaid = p.datepaid,
                                 amountTotal = p.amounttotal,
                                 orderidents = p.orderidents,
                                 showReceipt = p.shownreceipt,
                                 orderAmount = p.orderamount,
                                 bankId = p.bankid,
                                 comment = p.comment,
                                 matchType = p.matchtype,
                                 bankName = p.bankident,
                             }).AsQueryable();
                }
                return query;
            }
            catch(Exception e){return null;}
        }
        public List<PaymentReceiptsEntity> GetPatmentHistorys(string username)
        {

            List<PaymentReceiptsEntity> listPayments = new List<PaymentReceiptsEntity>();
            string sql = @"select REC.*, BNK.bankident, ORD.id as orderid from tblPaymentReceipts REC
                inner join tblBanks BNK on (REC.bankid = BNK.id) 
                left join tblOrders ORD with(nolock) on (cast(ORD.id as char(50)) = REC.orderidents)
                where matchtype is null and (ORD.orderstatusid = 1 or ORD.orderstatusid is null) and REC.userid = '"+ username+
                "' order by REC.id desc";
            DataTable dt = GetDataTable(sql);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                PaymentReceiptsEntity payment = new PaymentReceiptsEntity();
                payment.id = (int)dt.Rows[i]["id"];
                payment.created = (DateTime)dt.Rows[i]["created"];
                payment.userId = dt.Rows[i]["userid"].ToString();
                payment.datePaid = (DateTime)dt.Rows[i]["datepaid"];
                payment.amountTotal = (decimal)dt.Rows[i]["amounttotal"];
                payment.orderidents = dt.Rows[i]["orderidents"].ToString();
                payment.showReceipt = dt.Rows[i]["shownreceipt"].ToString();
                payment.orderAmount = (decimal)dt.Rows[i]["orderamount"];
                payment.bankId = (int)dt.Rows[i]["bankid"];
                payment.comment = dt.Rows[i]["comment"].ToString();
                payment.matchType = dt.Rows[i]["matchtype"].ToString();
                payment.bankName = dt.Rows[i]["bankident"].ToString();
                listPayments.Add(payment);
            }
            return listPayments;
        }
        public DbRawSqlQuery<T> RunQuery<T>(string query, params object[] parameters)
        {
            return db.Database.SqlQuery<T>(query, parameters);
        }
        public bool DeletePaymentReceipt(int paymentReceiptId)
        {
            try
            {
                var isSuccess = 0;
                _uow = new UnitOfWork(new DatabaseContextFactory());
                _conn = _uow.DataContext.Connection;

                var strSqlInsertUser = "delete [dbo].tblPaymentReceipts where[id] = @id";
                using (var cmd = _conn.CreateCommand())
                {
                    cmd.CommandText = strSqlInsertUser;
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = _uow.BeginTransaction();
                    cmd.Parameters.AddWithValue("@id", (object)paymentReceiptId ?? DBNull.Value);
                    isSuccess = cmd.ExecuteNonQuery();
                    _uow.Commit();
                }

                if (isSuccess > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception e)
            {
                _uow.Dispose();
                return false;
            }
            //try
            //{
            //    var isSuccess = db.sp_MSdel_dbotblPaymentReceipts(paymentReceiptId);
            //    if (isSuccess > 0)
            //    {
            //        db.SaveChanges();
            //        return true;
            //    }
            //    else
            //        return false;
            //}
            //catch{ return false; }
        }
        public static SqlConnection GetConn()
        {
            string strConnection = GetConnString();
            SqlConnection DB = new SqlConnection(strConnection);

            return DB;
        }
        public static string GetConnString()
        {

            string conn;
            if (System.Web.HttpContext.Current == null)
            {
                return (string)System.Configuration.ConfigurationSettings.AppSettings.Get("db_macca_dk");
            }
            //string lsServername = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"];

            //if (lsServername == "localhost")
            //{
            //    conn = "db_macca_dk";
            //}
            //else if (lsServername == "mybuy.dk.virker.nu")
            //{
            //    conn = "db_macca_dk";
            //}
            //else if (lsServername == "mybuy.dk")
            //{
            //    conn = "db_macca_dk";
            //}
            //else if (lsServername == "www.mybuy.dk")
            //{
            //    conn = "db_macca_dk";
            //}
            //else if (lsServername == "beta.mybuy.dk")
            //{
            //    conn = "db_macca_dk";
            //}
            //else
            //{
            conn = "db_macca_dk";
            //}


            string strConnection = (string)System.Configuration.ConfigurationSettings.AppSettings.Get(conn);
            return strConnection;

        }

        public static DataTable GetDataTable(string sql)
        {
            DataTable dt = new DataTable();
            using (SqlConnection DB = GetConn())
            {
                DB.Open();
                SqlCommand cmd = new SqlCommand(sql, DB);

                try
                {
                    SqlDataAdapter oDa = new SqlDataAdapter(cmd);
                    oDa.Fill(dt);
                }
                catch (Exception ex)
                { return dt; }
            }

            return dt;
        }
    }
}